var config = {
paths: {
    'fancybox': 'js/jquery.fancybox'
},
shim: {
    'fancybox': {
        deps: ['jquery']
    },
}
};