<?php

namespace Magedelight\Authorizecim\Controller\Adminhtml\Exportdelete;

use Magedelight\Authorizecim\Model\CardsFactory;
use Magento\Customer\Model\CustomerFactory;
use Magento\Eav\Model\ResourceModel\Entity\Attribute;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\ResourceConnection;

class Index extends \Magento\Backend\App\Action
{
    
    public $cards;
    public $customers;
    public $eavAttribute;
    public $resource;
    
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        CardsFactory $cards,
        CustomerFactory $customers,
        Attribute $eavAttribute,
       ResourceConnection $resource
    ){
        parent::__construct($context);
        $this->cards = $cards;
        $this->customers = $customers;
        $this->eavAttribute = $eavAttribute;
        $this->resource = $resource;
        
    }

    public function deleteAttribute(){
        $attributeId = $this->eavAttribute->getIdByCode('customer', 'md_customer_profile_id');
        $connection = $this->resource->getConnection();
        $tableName = $this->resource->getTableName('customer_entity_varchar'); //gives table name with prefix

        //Delete Data from table
        $sql = "Delete FROM " . $tableName." Where attribute_id = $attributeId";
        $connection->query($sql);
    }
    
    public function execute()
    {
        try {
            $customers = $this->customers->create()
                    ->getCollection()
                    ->addAttributeToFilter('md_customer_profile_id',array('neq' => 'NULL'));
            if(!empty($customers->getData())){
                foreach ($customers as $singlecustomer){
                    $this->deleteAttribute();
                }
                 $cards = $this->cards->create()
                    ->getCollection();
                 if(!empty($cards->getData())){
                    foreach ($cards as $card){
                        $card->delete();
                    }
                    $this->messageManager->addSuccessMessage(__('Your Cards has been deleted.'));
                }
            }
            else{
                $this->messageManager->addErrorMessage(__('Customers Cards not found.'));
            }
            
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setUrl($this->_redirect->getRefererUrl());
            return $resultRedirect;
            
        } catch (\Exception $e){
            $this->messageManager->addErrorMessage(__("Cards not found, Try again."));
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setUrl($this->_redirect->getRefererUrl());
            return $resultRedirect;
        }
        return ;
    }
 
    protected function _isAllowed()
    {
        return true;
    }
}
