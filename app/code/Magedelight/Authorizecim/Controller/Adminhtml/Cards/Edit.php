<?php
/**
 * Magedelight
 * Copyright (C) 2017 Magedelight <info@magedelight.com>.
 *
 * NOTICE OF LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://opensource.org/licenses/gpl-3.0.html.
 *
 * @category Magedelight
 *
 * @copyright Copyright (c) 2017 Mage Delight (http://www.magedelight.com/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Magedelight <info@magedelight.com>
 */
namespace Magedelight\Authorizecim\Controller\Adminhtml\Cards;

class Edit extends \Magedelight\Authorizecim\Controller\Adminhtml\Cards\Authorizecim
{
    public function execute()
    {
        $customerId = $this->getRequest()->getParam('id', null);
        $customer = $this->customerFactory->create();
        $customer->getResource()->load($customer, $customerId);
        $cardId = $this->getRequest()->getParam('card_id');
        if (!is_null($cardId)) {
             $cardModel = $this->cardFactory->create();
             $cardModel->getResource()->load($cardModel, $cardId);
             $card = $cardModel->getData();
             $resultLayout = $this->resultLayoutFactory->create();
             $resultLayout->getLayout()->getBlock('md.authorizecim.ajax.form')
                  ->setCard($this->jsonEncoder->encode($card));
            return $resultLayout;
        }
    }
    protected function _isAllowed()
    {
        return true;
    }
}
