<?php
/**
 * Magedelight
 * Copyright (C) 2017 Magedelight <info@magedelight.com>.
 *
 * NOTICE OF LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://opensource.org/licenses/gpl-3.0.html.
 *
 * @category Magedelight
 *
 * @copyright Copyright (c) 2017 Mage Delight (http://www.magedelight.com/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Magedelight <info@magedelight.com>
 */
namespace Magedelight\Authorizecim\Controller\Adminhtml\Cards;

class Update extends \Magedelight\Authorizecim\Controller\Adminhtml\Cards\Authorizecim
{
    public function execute()
    {
        $errorMessage = '';
        $params = $this->getRequest()->getParams();
        $customerId = $params['id'];
        $customerCardData = $params['paymentParam'];
        $ccAction = 'new';
        if (isset($customerCardData['cc_action'])) {
            $ccAction = $customerCardData['cc_action'];
        }
          if ($ccAction == 'existing') {
                unset($customerCardData['cc_number']);
                unset($customerCardData['cc_type']);
                unset($customerCardData['cc_exp_month']);
                unset($customerCardData['cc_exp_year']);
                unset($customerCardData['cc_cid']);
        }
        $append = '';
        $customerModel = $this->customerFactory->create();
        $customerModel->getResource()->load($customerModel, $customerId);
        $customerProfileId = $customerModel->getMdCustomerProfileId();
        $requestObject = new \Magento\Framework\DataObject();
        try {
            if (!$customerProfileId) {
            $requestObject->addData(array(
               'customer_id' => $customerModel->getId(),
               'email' => $customerModel->getEmail(),
            ));
            $response = $this->cimXml->setInputData($requestObject)
                                ->createCustomerProfile();
            $code = $response->messages->message->code;
            $resultCode = $response->messages->resultCode;
            $customerProfileId = $response->customerProfileId;
                if ($code == 'I00001' && $resultCode == 'Ok') {
                    $paymentProfileId = (string) $response->customerPaymentProfileId;
                    $readAdapter = $this->dbResource->getConnection('core_read');
                    $writeAdapter = $this->dbResource->getConnection('core_write');
                    $query1 = "SELECT `attribute_id` FROM `{$this->dbResource->getTableName('eav_attribute')}` "
                    . "WHERE `attribute_code`='md_customer_profile_id' LIMIT 1";
                    $eavAttributeId = $readAdapter->fetchOne($query1);
                    $query2 = "SELECT `value_id` FROM `{$this->dbResource
                        ->getTableName('customer_entity_varchar')}` "
                        . "WHERE `entity_id`='{$customerId}' AND `attribute_id`='{$eavAttributeId}'";
                    $valueId = (int) $readAdapter->fetchOne($query2);
                    if ($valueId <= 0) {
                        $Query = "INSERT INTO `{$this->dbResource->getTableName('customer_entity_varchar')}` (attribute_id,entity_id,value) VALUES('{$eavAttributeId}','{$customerId}','{$customerProfileId}')";
                    } else {
                        $Query = "UPDATE `{$this->dbResource->getTableName('customer_entity_varchar')}` "
                        . "SET `value`='{$customerProfileId}' WHERE `value_id`='{$valueId}' "
                        . "AND `entity_id`='{$customerId}'";
                    }
                    $writeAdapter->query($Query);
                    } elseif ($code == 'E00039' && strpos($response->messages->message->text, 'duplicate') !== false) {
      
                   $customerProfileId = preg_replace('/[^0-9]/', '', $response->messages->message->text);
                    $readAdapter = $this->dbResource->getConnection('core_read');
                    $writeAdapter = $this->dbResource->getConnection('core_write');
                    $query1 = "SELECT `attribute_id` FROM `{$this->dbResource->getTableName('eav_attribute')}` "
                    . "WHERE `attribute_code`='md_customer_profile_id' LIMIT 1";
                    $eavAttributeId = $readAdapter->fetchOne($query1);
                    $query2 = "SELECT `value_id` FROM `{$this->dbResource->getTableName('customer_entity_varchar')}` WHERE `entity_id`='{$customerId}' AND `attribute_id`='{$eavAttributeId}'";
                    $valueId = (int) $readAdapter->fetchOne($query2);
                    if ($valueId <= 0) {
                        $Query = "INSERT INTO `{$this->dbResource->getTableName('customer_entity_varchar')}` (attribute_id,entity_id,value) VALUES('{$eavAttributeId}','{$customerId}','{$customerProfileId}')";
                    } else {
                        $Query = "UPDATE `{$this->dbResource->getTableName('customer_entity_varchar')}` SET `value`='{$customerProfileId}' WHERE `value_id`='{$valueId}' AND `entity_id`='{$customerId}'";
                    }
                    $writeAdapter->query($Query);
                } else {
                    $errorMessage = $response->messages->message->text;
                }
            }
            if (is_string($errorMessage) && strlen($errorMessage) > 0) {
                $append = '<div id="messages"><div class="messages"><div class="message message-error error">'
                    . '<div data-ui-id="messages-message-error">'.$errorMessage.'</div></div></div></div>';
                $result = ['error' => true, 'message' => $append];
            } else {
                    $requestObject->addData($customerCardData);
                    $requestObject->addData(['customer_profile_id' => $customerProfileId]);
                    $requestObject
                        ->addData(['payment_profile_id' => $customerCardData['payment_profile_id'],
                            'card_number_masked' => "XXXX" . $customerCardData['card_number_masked']]);

                    $response = $this->cimXml->setInputData($requestObject)
                            ->updateCustomerPaymentProfile();
                    $code = $response->messages->message->code;
                    $resultCode = $response->messages->resultCode;
                }
             
                if ($code == 'I00001' && $resultCode == 'Ok') {
                    $updateCardId = $customerCardData['card_id'];
                    $cardModel = $this->cardFactory->create()->load($updateCardId);
                    $oldCardData = $cardModel->getData();
                    unset($oldCardData['card_id']);
                     $paymentProfileId = (string) $customerCardData['payment_profile_id'];
                      $cardModelObject = $this->cardFactory->create();
                      $cardModelObject->setData($customerCardData)
                    ->setCustomerId($customerModel->getId())
                    ->setCustomerProfileId($customerProfileId)
                    ->setPaymentProfileId($paymentProfileId)
                    ->setCountryId($customerCardData['country_id'])
                    ->setCreatedAt($this->localeDate->date())
                    ->setUpdatedAt($this->localeDate->date());
                      if ($ccAction == 'existing') {
                            $cardModelObject->setccType($oldCardData['cc_type'])
                            ->setCc_exp_month($oldCardData['cc_exp_month'])
                            ->setCc_exp_year($oldCardData['cc_exp_year']);
                            if (isset($oldCardData['cc_last4'])):
                                $cardModelObject->setcc_last4($oldCardData['cc_last4']);
                            endif;
                        } else {
                            $cardModelObject->setccType($customerCardData['cc_type'])
                            ->setCc_exp_month($customerCardData['cc_exp_month'])
                            ->setCc_exp_year($customerCardData['cc_exp_year'])
                            ->setCc_last4(substr($customerCardData['cc_number'], -4, 4));
                        }
                    $cardModelObject->getResource()->save($cardModelObject);

                    $append = '<div id="messages"><div class="messages"><div class="message message-success success"><div data-ui-id="messages-message-success">Credit card saved successfully.</div></div></div></div>';
                    $cimBlock = $this->_view->getLayout()->createBlock(
                        'Magedelight\Authorizecim\Block\Adminhtml\CardTab'
                    );
                    $cimBlock->setChild('authorizecimAddCards', $this->_view->getLayout()->createBlock(
                        'Magedelight\Authorizecim\Block\Adminhtml\CardForm'
                    ));
                    $cimBlock->setCustomerId($customerId);
                    $carddata = $cimBlock->toHtml();

                    $result = ['error' => false, 'message' => $append, 'carddata' => $carddata];
                   

                } else {
                    $append = '<div id="messages"><div class="messages"><div class="message message-error error"><div data-ui-id="messages-message-error">'.$response->messages->message->text.'</div></div></div></div>';
                    $result = ['error' => true, 'message' => $append];
                }
            }
           catch (\Exception $e) {
            $append = '<div id="messages"><div class="messages"><div class="message message-error error"><div data-ui-id="messages-message-error">'.$e->getMessage().'</div></div></div></div>';
            $result = ['error' => true, 'message' => $append];
        }
        $resultJson = $this->resultJsonFactory->create();
        $resultJson->setData($result);

        return $resultJson;
    }

    protected function _isAllowed()
    {
        return true;
    }
}
