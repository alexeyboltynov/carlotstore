<?php
/**
 * Magedelight
 * Copyright (C) 2017 Magedelight <info@magedelight.com>.
 *
 * NOTICE OF LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://opensource.org/licenses/gpl-3.0.html.
 *
 * @category Magedelight
 *
 * @copyright Copyright (c) 2017 Mage Delight (http://www.magedelight.com/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Magedelight <info@magedelight.com>
 */

namespace Magedelight\Authorizecim\Controller\Cards;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\RequestInterface;

abstract class Authorizecim extends Action
{
    /**
     * Response.
     *
     * @var \Magento\Framework\App\ResponseInterface
     */
    public $response;

    /**
     * Magedelight\Cybersource\Api\CardRepositoryInterface
     * @var type
     */
    public $cardRepository;

    /**
     * @var PageFactory
     */
    public $resultPageFactory;

    /**
     * @var \Magento\Framework\Registry
     */
    public $registry;

    /**
     * Customer session
     *
     * @var \Magento\Customer\Model\Session
     */
    public $customerSession;

    /**
     *
     * @var \Magento\Framework\DataObjec
     */
    public $requestObject;

    /**
     *
     * @var \Magedelight\Cybersource\Model\CardsFactory
     */
    public $cardsFactory;

    /**
     * Date model
     *
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    public $localeDate;

    /**
     * @var Magedelight\Authorizecim\Model\ResourceModel\Cards\CollectionFactory
     */
    public $cardCollFactory;

    /**
     * @var Magedelight\Authorizecim\Model\CardsFactory
     */
    public $cardFactory;

    /**
     * @var Magento\Customer\Api\CustomerFactory
     */
    public $customerFactory;

    /**
     * @var Magento\Framework\DataObjectFactory
     */
    public $objectFactory;

// @codingStandardsIgnoreStart
    public function __construct(
        Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Customer\Model\Session\Proxy $customerSession,
        \Magento\Framework\DataObject $requestObject,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Directory\Helper\Data $directoryHelper,
        \Magedelight\Authorizecim\Helper\Data $cimHelper,
        \Magedelight\Authorizecim\Model\Api\Xml $cimXml,
        \Magedelight\Authorizecim\Model\ResourceModel\Cards\CollectionFactory $cardCollFactory,
        \Magedelight\Authorizecim\Model\CardsFactory $cardFactory,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Framework\DataObjectFactory $objectFactory,
        \Magento\Framework\App\ResourceConnection $resource
    ) {
      
        $this->registry         = $registry;
        $this->customerSession  = $customerSession;
        $this->requestObject    = $requestObject;
        $this->localeDate       = $localeDate;
        $this->_directoryHelper = $directoryHelper;
        $this->cimHelper        = $cimHelper;
        $this->cardCollFactory  = $cardCollFactory;
        $this->cardFactory      = $cardFactory;
        $this->customerFactory = $customerFactory;
        $this->cimXml           = $cimXml;
        $this->objectFactory    = $objectFactory;
        $this->dbResource       = $resource;
      
        return parent::__construct($context);
    }
// @codingStandardsIgnoreEnd
    public function getCustomer()
    {
        return $this->_getSession()->getCustomer();
    }

    /**
     * return customer session
     * @return type
     */
    public function _getSession()
    {
        return $this->customerSession;
    }

    /**
     * check customer is logged in
     * @param RequestInterface $request
     * @return type
     */
    public function dispatch(RequestInterface $request)
    {
        if (!$this->_getSession()->authenticate()) {
            $this->_actionFlag->set('', 'no-dispatch', true);
        }
        return parent::dispatch($request);
    }
}
