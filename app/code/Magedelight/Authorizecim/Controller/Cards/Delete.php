<?php
/**
 * Magedelight
 * Copyright (C) 2017 Magedelight <info@magedelight.com>.
 *
 * NOTICE OF LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://opensource.org/licenses/gpl-3.0.html.
 *
 * @category Magedelight
 *
 * @copyright Copyright (c) 2017 Mage Delight (http://www.magedelight.com/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Magedelight <info@magedelight.com>
 */
namespace Magedelight\Authorizecim\Controller\Cards;

class Delete extends \Magedelight\Authorizecim\Controller\Cards\Authorizecim
{
    public function execute()
    {
        $deleteCardId = $this->getRequest()->getParam('card_id');
        $cardModel = $this->cardFactory->create();
        $cardModel->getResource()->load($cardModel, $deleteCardId);
        $paymentProfileId = $cardModel->getPaymentProfileId();
        $customer = $this->customerSession->getCustomer();
        
        if ($deleteCardId) {
            $requestObject = new \Magento\Framework\DataObject();
            $requestObject->addData(array(
                'customer_profile_id' => $customer->getData('md_customer_profile_id'),
                'payment_profile_id' => $paymentProfileId,
            ));
           $response = $this->cimXml->setInputData($requestObject)
                            ->deleteCustomerPaymentProfile();
           $code = $response->messages->message->code ;
           $resultCode = $response->messages->resultCode;
           $isSuccess = false;
            if ($code == 'I00001' && $resultCode == 'Ok') {
                $cardModel->getResource()->delete($cardModel);
                $isSuccess = true;
                $this->messageManager->addSuccess(__('Card deleted successfully.'));
            } else {
                $this->messageManager->addError($response->site_display_message);
            }
        } else {
            $this->messageManager->addError(__('Card does not exists.'));
        }
        $this->_redirect('md_authorizecim/cards/lists');
    }
}
