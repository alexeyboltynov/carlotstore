<?php
/**
 * Magedelight
 * Copyright (C) 2017 Magedelight <info@magedelight.com>.
 *
 * NOTICE OF LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://opensource.org/licenses/gpl-3.0.html.
 *
 * @category Magedelight
 *
 * @copyright Copyright (c) 2017 Mage Delight (http://www.magedelight.com/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Magedelight <info@magedelight.com>
 */

namespace Magedelight\Authorizecim\Controller\Cards;

class Save extends \Magedelight\Authorizecim\Controller\Cards\Authorizecim
{
    
    public function execute()
    {
        $errorMessage = '';
        $customerId = $this->customerSession->getCustomerId();
        $customerModel = $this->customerFactory->create();
        $customerModel->getResource()->load($customerModel, $customerId);
        $customerProfileId = $customerModel->getMdCustomerProfileId();
        $params = $this->getRequest()->getParams();
        $readAdapter = $this->dbResource->getConnection('core_read');
        $writeAdapter = $this->dbResource->getConnection('core_write');
        $websiteId = $this->cimHelper->getWebsiteId();
        
        $query1 = "SELECT `attribute_id` FROM `{$this->dbResource->getTableName('eav_attribute')}` "
        . "WHERE `attribute_code`='md_customer_profile_id' LIMIT 1";

        $eavAttributeId = $readAdapter->fetchOne($query1);

        $query2 = "SELECT `value_id` FROM `{$this->dbResource->getTableName('customer_entity_varchar')}` "
        . "WHERE `entity_id`='{$customerId}' AND `attribute_id`='{$eavAttributeId}'";
         $valueId = (int) $readAdapter->fetchOne($query2);
         $requestObject = $this->objectFactory->create();
         try {
                if ($customerProfileId==null) {
                        $requestObject->addData([
                            'customer_id' => $customerId,
                            'email' => $this->customerSession->getCustomer()->getEmail(),
                        ]);
                        $response = $this->cimXml
                                        ->setInputData($requestObject)
                                        ->createCustomerProfile();
                         $code =  $response->messages->message->code;
                         $resultCode = $response->messages->resultCode;
                         $customerProfileId = $response->customerProfileId;
                          if ($code == 'I00001' && $resultCode == 'Ok') {
                        if ($valueId <= 0) {
                            $Query = "INSERT INTO `{$this->dbResource->getTableName('customer_entity_varchar')}` (attribute_id,entity_id,value) VALUES('{$eavAttributeId}','{$customerId}','{$customerProfileId}')";
                        } else {
                            $Query = "UPDATE `{$this->dbResource->getTableName('customer_entity_varchar')}` SET `value`='{$customerProfileId}' WHERE `value_id`='{$valueId}' AND `entity_id`='{$customerId}'";
                        }
                        $writeAdapter->query($Query);
                    } elseif ($code == 'E00039' && strpos($response->messages->message->text, 'duplicate') !== false) {
                        $customerProfileId = preg_replace('/[^0-9]/', '', $response->messages->message->text);
                        if ($valueId <= 0) {
                            $Query = "INSERT INTO `{$this->dbResource->getTableName('customer_entity_varchar')}` (attribute_id,entity_id,value) VALUES('{$eavAttributeId}','{$customerId}','{$customerProfileId}')";
                        } else {
                            $Query = "UPDATE `{$this->dbResource->getTableName('customer_entity_varchar')}` SET `value`='{$customerProfileId}' WHERE `value_id`='{$valueId}' AND `entity_id`='{$customerId}'";
                        }
                         $writeAdapter->query($Query);
                    } else {
                        $errorMessage .= $response->messages->message->text;
                    }
                }
                 if (is_string($errorMessage) && strlen($errorMessage) > 0) {
                     $this->messageManager->addError(__($errorMessage));
                 } else {
                    $requestObject->addData($params['md_authorizecim']['address_info']);
                    $requestObject->addData($params['md_authorizecim']['payment_info']);
                    $requestObject->addData(['country_id' => $params['country_id']]);
                    $requestObject->addData(['customer_profile_id' => $customerProfileId]);
                    $response = $this->cimXml
                                            ->setInputData($requestObject)
                                            ->createCustomerPaymentProfile();
                    $code = $response->messages->message->code;
                    $resultCode = $response->messages->resultCode;

                     if ($code == 'I00001' && $resultCode == 'Ok') {
                        $paymentProfileId = (string) $response->customerPaymentProfileId;
                    }
                    elseif ($code == 'E00039' && strpos($response->messages->message->text, 'duplicate') !== false) {
                        $paymentProfileId = (string) $response->customerPaymentProfileId;
                    }
                    else
                    {
                        $this->messageManager->addError(new \Magento\Framework\Phrase(__($response->messages->message->text)));
                        $this->_redirect('md_authorizecim/cards/lists');
                        return;
                    }
                     $requestObject = new \Magento\Framework\DataObject();
                      $requestObject->addData(array(
                            'customer_profile_id' => $customerProfileId,
                            'payment_profile_id' => $paymentProfileId,
                     ));
                      $paymentdetailResponse  = $this->cimXml->setInputData($requestObject)
                                         ->getCustomerPaymentProfile();
                      $code = (string)$paymentdetailResponse->messages->message->code;
                      $resultCode =  (string)$paymentdetailResponse->messages->resultCode;
                      if ($code == 'I00001' && $resultCode == 'Ok') {
                         $paymentdetail = $paymentdetailResponse->paymentProfile->payment;
                         $ccType = isset($this->cardArray[(string)$paymentdetail->creditCard->cardType]);
                         $ccExpMonth = '';
                         $ccExpYear = '';
                         $ccLast4 = substr((string)$paymentdetail->creditCard->cardNumber, -4, 4);
                      }
                      else
                      {
                        $this->messageManager
                            ->addError(new \Magento\Framework\Phrase(__($response->messages->message->text)));
                        $this->_redirect('md_authorizecim/cards/lists');
                        return;
                      }

                   $cardModelObject = $this->cardFactory->create();
                   $cardModelObject->setData($params['md_authorizecim']['address_info'])
                    ->setCustomerId($customerModel->getId())
                    ->setCustomerProfileId($customerProfileId)
                    ->setPaymentProfileId($paymentProfileId)
                    ->setCcType($ccType)
                    ->setCcExpMonth($ccExpMonth)
                    ->setCcExpYear($ccExpYear)
                    ->setCcLast4($ccLast4)
                    ->setCountryId($params['country_id'])
                    ->setWebsiteId($websiteId)
                    ->setCreatedAt($this->localeDate->date())
                    ->setUpdatedAt($this->localeDate->date());
                    $cardModelObject->getResource()->save($cardModelObject);
                    $this->messageManager->addSuccess(__('Credit card saved successfully.'));
                }
             } catch (\Excetion $e) {
            $this->messageManager->addError(__('We can\'t add the credit card to your account right now: %1.',
                $e->getMessage()));
        }
        $this->_redirect('md_authorizecim/cards/lists');
    }
}
