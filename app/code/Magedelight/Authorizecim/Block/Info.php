<?php
/**
 * Magedelight
 * Copyright (C) 2017 Magedelight <info@magedelight.com>.
 *
 * NOTICE OF LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://opensource.org/licenses/gpl-3.0.html.
 *
 * @category Magedelight
 *
 * @copyright Copyright (c) 2017 Mage Delight (http://www.magedelight.com/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Magedelight <info@magedelight.com>
 */
namespace Magedelight\Authorizecim\Block;

class Info extends \Magento\Payment\Block\Info\Cc
{
    /**
     * Payment config model.
     *
     * @var \Magento\Payment\Model\Config
     */
    protected $_isCheckoutProgressBlockFlag = true;
    protected $cimHelper;
    protected $_paymentConfig;
    protected $paymentModel;
    protected $_cimConfig;
    protected $_template = 'Magedelight_Authorizecim::info.phtml';
    protected $cardpayment;
    protected $storeManager;
    protected $currencyHelper;
    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Payment\Model\Config                    $paymentConfig
     * @param array                                            $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Payment\Model\Config $paymentConfig,
        \Magedelight\Authorizecim\Model\Config $cimConfig,
        \Magento\Store\Model\StoreManager $storeManager,
        \Magento\Sales\Model\Order\Payment\Transaction $payment,
        \Magedelight\Authorizecim\Helper\Data $cimHelper,
        \Magento\Framework\Pricing\Helper\Data $currencyHelper,
        \Magedelight\Authorizecim\Model\Payment\Cards $cardpayment,
        array $data = []
    ) {
      
        $this->storeManager = $storeManager;
        $this->_paymentConfig = $paymentConfig;
        $this->_cimConfig = $cimConfig;
        $this->paymentModel = $payment;
        $this->currencyHelper = $currencyHelper;
        $this->cimHelper = $cimHelper;
        $this->cardpayment = $cardpayment;
         parent::__construct($context,$paymentConfig, $data);
    }

    public function setCheckoutProgressBlock($flag)
    {
        $this->_isCheckoutProgressBlockFlag = $flag;

        return $this;
    }
 
    public function getCards()
    {
        $this->cardpayment->setPayment($this->getInfo());
        $cardsData = $this->cardpayment->getCards();
        $cards = array();
            if (is_array($cardsData)) {
                foreach ($cardsData as $cardInfo) {
                    $data = array();

                    $lastTransactionId = $this->getData('info')->getData('cc_trans_id');
                    $cardTransactionId = $cardInfo->getTransactionId();
                    if ($lastTransactionId == $cardTransactionId) {
                        if ($cardInfo->getProcessedAmount()) {
                            $amount = $this->currencyHelper->currency($cardInfo->getProcessedAmount(), true, false);
                            $data['Processed Amount'] = $amount;
                        }
                        if ($cardInfo->getBalanceOnCard() && is_numeric($cardInfo->getBalanceOnCard())) {
                            $balance = $this->currencyHelper->currency($cardInfo->getBalanceOnCard(), true, false);
                            $data['Remaining Balance'] = $balance;
                        }
                        if ($this->cimHelper->checkAdmin()) {
                            if ($cardInfo->getApprovalCode() && is_string($cardInfo->getApprovalCode())) {
                                $data['Approval Code'] = $cardInfo->getApprovalCode();
                            }
                            if ($cardInfo->getMethod() && is_numeric($cardInfo->getMethod())) {
                                $data['Method'] = ($cardInfo->getMethod() == 'CC') ? __('Credit Card') : __('eCheck');
                            }
                            if ($cardInfo->getLastTransId() && $cardInfo->getLastTransId()) {
                                $data['Transaction Id'] = str_replace(array('-capture', '-void', '-refund'), '', $cardInfo->getLastTransId());
                            }
                            if ($cardInfo->getAvsResultCode() && is_string($cardInfo->getAvsResultCode())) {
                                $data['AVS Response'] = $this->cimHelper->getAvsLabel($cardInfo->getAvsResultCode());
                            }
                            if ($cardInfo->getCVNResultCode() && is_string($cardInfo->getCVNResultCode())) {
                                $data['CVN Response'] = $this->cimHelper->getCvnLabel($cardInfo->getCVNResultCode());
                            }
                            if ($cardInfo->getCardCodeResponseCode() && is_string($cardInfo->getreconciliationID())) {
                                $data['CCV Response'] = $cardInfo->getCardCodeResponseCode();
                            }
                            if ($cardInfo->getMerchantReferenceCode() &&
                                is_string($cardInfo->getMerchantReferenceCode())) {
                                $data['Merchant Reference Code'] = $cardInfo->getMerchantReferenceCode();
                            }
                            if ($cardInfo->getCAVVResponseCode() && is_string($cardInfo->getCAVVResponseCode())) {
                                $data['CAVV Response'] = $this->cimHelper->getCavvLabel($cardInfo->getCAVVResponseCode());
                            }
                            if ($cardInfo->getFdsaction() && is_string($cardInfo->getFdsaction())) {
                                $data['FDS Action'] = $cardInfo->getFdsaction();
                            }
                            if ($cardInfo->getAfdinfo()) {
                                // $data['AFD Response'] = $cardInfo->getAfdinfo();
                                $data['FDS Response'] = base64_decode($cardInfo->getAfdinfo());
                            }
                        }
                        $this->setCardInfoObject($cardInfo);
                        $cards[] = array_merge($this->getSpecificInformation(), $data);
                        $this->unsCardInfoObject();
                        $this->_paymentSpecificInformation = null;
                    }
                }
            }
        if ($this->getInfo()->getCcType() && $this->_isCheckoutProgressBlockFlag && count($cards) == 0) {
            $cards[] = $this->getSpecificInformation();
        }
        return $cards;
    }
}
