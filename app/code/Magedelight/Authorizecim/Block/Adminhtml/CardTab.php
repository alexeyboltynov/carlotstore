<?php
/**
 * Magedelight
 * Copyright (C) 2017 Magedelight <info@magedelight.com>.
 *
 * NOTICE OF LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://opensource.org/licenses/gpl-3.0.html.
 *
 * @category Magedelight
 *
 * @copyright Copyright (c) 2017 Mage Delight (http://www.magedelight.com/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Magedelight <info@magedelight.com>
 */
namespace Magedelight\Authorizecim\Block\Adminhtml;

use Magento\Customer\Controller\RegistryConstants;
use Magento\Ui\Component\Layout\Tabs\TabInterface;

class CardTab extends \Magento\Backend\Block\Template implements TabInterface
{
    /**
     * @var Magento\Framework\Registry
     */
    protected $coreRegistry;

    /**
     * @var Magedelight\Authorizecim\Model\Config
     */
    protected $cimConfig;

    /**
     * @var Magedelight\Authorizecim\Model\Api\Xml
     */
    protected $cimXml;

    /**
     * @var Magedelight\Authorizecim\Helper\Data 
     */
    protected $cimHelper;

    /**
     * @var Magento\Customer\Model\CustomerFactory
     */
    protected $customerFactory;

    /**
     * @var Magento\Framework\Json\EncoderInterface
     */
    protected $jsonEncoder;

    /**
     * @var Magedelight\Authorizecim\Model\ResourceModel\Cards\CollectionFactory
     */
    public $cardCollFactory;

    /**
     * @var Magento\Framework\DataObject
     */
    public $dataObject;

    /**
     * @var \Magento\Customer\Block\Address\Renderer\DefaultRenderer
     */
    public $addressRender;

    /**
     * @var \Magento\Directory\Model\CountryFactory
     */
    public $countryFactory;

    protected $_template = 'tab/savedCards.phtml';

    protected $customerid;
    
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magedelight\Authorizecim\Model\Config $cimConfig,
        \Magedelight\Authorizecim\Model\Api\Xml $cimXml,
        \Magedelight\Authorizecim\Helper\Data $cimHelper,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magedelight\Authorizecim\Model\ResourceModel\Cards\CollectionFactory $cardCollFactory,
        \Magento\Framework\DataObject $dataObject,
         \Magento\Customer\Block\Address\Renderer\DefaultRenderer $addressRender,
        \Magento\Directory\Model\CountryFactory $countryFactory,
         array $data = []
    ) {
        $this->coreRegistry = $registry;
        $this->cimConfig = $cimConfig;
        $this->cimXml = $cimXml;
        $this->cimHelper = $cimHelper;
        $this->customerFactory = $customerFactory;
        $this->jsonEncoder = $jsonEncoder;
        $this->cardCollFactory  = $cardCollFactory;
        $this->dataObject            = $dataObject;
        $this->addressRender         = $addressRender;
         $this->countryFactory        = $countryFactory;
        parent::__construct($context, $data);
    }
    public function getCustomerId()
    {
        $customerId = $this->coreRegistry->registry(RegistryConstants::CURRENT_CUSTOMER_ID);
        if (empty($customerId)) {
            $customerId = $this->customerid;
        }

        return $customerId;
    }
    public function getTabLabel()
    {
        return __('Saved Cards');
    }
    public function getTabTitle()
    {
        return __('Saved Cards');
    }
    public function canShowTab()
    {
        if ($this->getCustomerId()) {
            return true;
        }

        return false;
    }
    public function isHidden()
    {
        if ($this->getCustomerId()) {
            return false;
        }

        return true;
    }
    public function getTabClass()
    {
        return '';
    }
    public function getTabUrl()
    {
        return '';
    }
    public function isAjaxLoaded()
    {
        return false;
    }
    public function setCustomerId($customerid)
    {
        $this->customerid = $customerid;
    }

    public function getCards()
    {
        $customerId = $this->getCustomerId();
        $result = [];
        if (!empty($customerId)) {
            $result =  $this->cardCollFactory->create()
            ->addFieldToFilter('customer_id', $customerId)
            ->getData();
       }
       return $result;
    }

    public function getFormatedAddress($card)
    {
        $typeObject = $this->dataObject;
        $typeObject->addData([
            'code' => 'html',
            'title' => 'HTML',
            'default_format' => $this->cimConfig->getDefaultFormat(),
        ]);
        $this->addressRender->setType($typeObject);
        $data = [];
        $countryName = '';
        if (!empty($card['country_id'])) {
            $countryModel = $this->countryFactory->create();
            $countryModel->getResource()->loadByCode($countryModel, $card['country_id']);
            $countryName = $countryModel->getName();
        }
        $data['firstname'] = $card['firstname'];
        $data['lastname'] = $card['lastname'];
        $data['street'] = $card['street'];
        $data['city'] = $card['city'];
        $data['country_id'] = $card['country_id'];
        $data['country'] = $countryName;
        $data['region_id'] = $card['region_id'];
        $data['postcode'] = $card['postcode'];
        $data['telephone'] = $card['telephone'];
        $format = $this->addressRender->getFormatArray($data);
        return $this->filterManager->template($format, ['variables' => $data]);
    }

    public function getCardsInJson()
    {
        return $this->jsonEncoder->encode($this->getCards());
    }

    public function getDeleteActionUrl()
    {
        return $this->getUrl('md_authorizecim/cards/delete', ['id' => $this->getCustomerId()]);
    }

    public function getEditCardAction()
    {
        return $this->getUrl('md_authorizecim/cards/edit', ['id' => $this->getCustomerId()]);
    }

    public function getAddAction()
    {
        return $this->getUrl('md_authorizecim/cards/add', ['id' => $this->getCustomerId()]);
    }
}
