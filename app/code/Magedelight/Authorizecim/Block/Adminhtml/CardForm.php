<?php
/**
 * Magedelight
 * Copyright (C) 2017 Magedelight <info@magedelight.com>.
 *
 * NOTICE OF LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://opensource.org/licenses/gpl-3.0.html.
 *
 * @category Magedelight
 *
 * @copyright Copyright (c) 2017 Mage Delight (http://www.magedelight.com/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Magedelight <info@magedelight.com>
 */

namespace Magedelight\Authorizecim\Block\Adminhtml;

class CardForm extends \Magento\Directory\Block\Data
{
    protected $_template = 'cards/form.phtml';

    public $address = null;

    /**
     * @var Magento\Customer\Api\AddressRepositoryInterface
     */
    public $addressRepository;

    /**
     * @var Magento\Customer\Api\Data\AddressInterfaceFactory
     */
    public $addressDataFactory;

    /**
     * @var Magento\Framework\Api\DataObjectHelper 
     */
    public $dataObjectHelper;

    /**
     * @var Magedelight\Authorizecim\Model\Config
     */
    public $cimConfig;

    /**
     * @var Magento\Payment\Model\Config
     */
    public $paymentConfig;

    /**
     * @var Magento\Customer\Model\CustomerFactory
     */
    public $customerFactory;

    /**
     * @var Magento\Framework\Json\Helper\Data
     */
    public $jsonHelper;

    public $_card = array();
   
    public function __construct(
             \Magento\Framework\View\Element\Template\Context $context,
             \Magento\Directory\Helper\Data $directoryHelper,
             \Magento\Framework\Json\EncoderInterface $jsonEncoder,
             \Magento\Framework\App\Cache\Type\Config $configCacheType,
             \Magento\Directory\Model\ResourceModel\Region\CollectionFactory $regionCollectionFactory,
             \Magento\Directory\Model\ResourceModel\Country\CollectionFactory $countryCollectionFactory,
             \Magento\Customer\Api\AddressRepositoryInterface $addressRepository,
             \Magento\Customer\Api\Data\AddressInterfaceFactory $addressDataFactory,
             \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
             \Magedelight\Authorizecim\Model\Config $cimConfig,
             \Magento\Payment\Model\Config $paymentConfig,
             \Magento\Customer\Model\CustomerFactory $customerFactory,
             \Magento\Framework\Json\Helper\Data $jsonHelper,
             array $data = []
    ) {
        $this->addressRepository = $addressRepository;
        $this->addressDataFactory = $addressDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->cimConfig = $cimConfig;
        $this->paymentConfig = $paymentConfig;
        $this->customerFactory = $customerFactory;
        $this->jsonHelper = $jsonHelper;

        parent::__construct($context, $directoryHelper, $jsonEncoder, $configCacheType, $regionCollectionFactory, $countryCollectionFactory, $data);
    }

    public function getCcAvailableTypes()
    {
        $types = $this->_getConfig()->getCcTypes();
        $availableTypes = explode(',', $this->cimConfig->getCcTypes());
        if ($availableTypes) {
            foreach ($types as $code => $name) {
                if (!in_array($code, $availableTypes)) {
                    unset($types[$code]);
                }
            }
        }

        return $types;
    }
    public function getCustomer()
    {
        $id = $this->getRequest()->getParam('id');

        return $this->customerFactory->create()->load($id);
    }
    protected function _getConfig()
    {
        return $this->paymentConfig;
    }

    public function getCcMonths()
    {
        $months = $this->getData('cc_months');
        if (is_null($months)) {
            $months[0] = __('Month');
            $months = array_merge($months, $this->paymentConfig->getMonths());
            $this->setData('cc_months', $months);
        }

        return $months;
    }
    public function getCcYears()
    {
        $years = $this->getData('cc_years');
        if (!($years)) {
            $years = $this->paymentConfig->getYears();
            $years = [0 => __('Year')] + $years;
            $this->setData('cc_years', $years);
        }

        return $years;
    }

    public function hasVerification()
    {
        return $this->cimConfig->isCardVerificationEnabled();
    }

    public function setCard($card)
    {
        $this->_card = $card;
        return $this;
    }

    public function getCard()
    {
        if (empty($this->_card)) {
            return;
        }
       return $this->jsonHelper->jsonDecode($this->_card);
    }

    public function getRegionValue($regionValue, $countryId)
    {
        $regionId = null;
        $regionCollection = $this->_regionCollectionFactory->create()
                ->addFieldToFilter('default_name', ['eq' => $regionValue])
                ->addFieldToFilter('country_id', ['eq' => $countryId]);
        if ($regionCollection->count() > 0) {
            $regionId = $regionCollection->getFirstItem()->getId();
        } else {
            $regionId = $regionValue;
        }

        return $regionId;
    }
}
