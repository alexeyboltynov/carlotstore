<?php
/**
 * Magedelight
 * Copyright (C) 2017 Magedelight <info@magedelight.com>.
 *
 * NOTICE OF LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://opensource.org/licenses/gpl-3.0.html.
 *
 * @category Magedelight
 *
 * @copyright Copyright (c) 2016 Mage Delight (http://www.magedelight.com/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Magedelight <info@magedelight.com>
 */

namespace Magedelight\Authorizecim\Block\Customer;

class Authorizecim extends \Magento\Directory\Block\Data
{
    /**
     * Magedelight\Cybersource\Api\CardRepositoryInterface
     * @var type
     */
    public $cardRepository;

    /**
     * Scope config
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    public $scopeConfig;

    /**
     * Payment config model
     *
     * @var \Magento\Payment\Model\Config
     */
    public $paymentConfig;

    /**
     * @var \Magento\Customer\Model\Session
     */
    public $customerSession;

    /**
     * @var \Magedelight\Firstdata\Model\Config
     */
    public $getconfig;

    /**
     * @var \Magedelight\Firstdata\Model\ResourceModel\Cards\CollectionFactory
     */
    public $cardCollectionFactory;

    /**
     * @var \Magento\Customer\Block\Address\Renderer\DefaultRenderer
     */
    public $addressRender;

    /**
     * @var \Magento\Directory\Model\CountryFactory
     */
    public $countryFactory;

    /**
     * @var \Magedelight\Firstdata\Model\CardsFactory
     */
    public $cardFactory;

    /**
     * @var Magento\Framework\DataObject
     */
    public $dataObject;

    /**
     * @var Magedelight\Authorizecim\Model\Config
     */
    public $cimConfig;

    /**
     * @var Magedelight\Authorizecim\Model\CardsFactory
     */
    public $cardsFactory;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Directory\Helper\Data $directoryHelper
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
     * @param \Magento\Framework\App\Cache\Type\Config $configCacheType
     * @param \Magento\Directory\Model\ResourceModel\Region\CollectionFactory $regionCollectionFactory
     * @param \Magento\Directory\Model\ResourceModel\Country\CollectionFactory $countryCollectionFactory
     * @param \Magento\Payment\Model\Config $paymentConfig
     * @param \Magedelight\Firstdata\Model\Config $getconfig
     * @param \Magento\Customer\Model\Session $customerSession
     * @param array $data
     */
    // @codingStandardsIgnoreStart
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Directory\Helper\Data $directoryHelper,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Framework\App\Cache\Type\Config $configCacheType,
        \Magento\Directory\Model\ResourceModel\Region\CollectionFactory $regionCollectionFactory,
        \Magento\Directory\Model\ResourceModel\Country\CollectionFactory $countryCollectionFactory,
        \Magento\Payment\Model\Config $paymentConfig,
        \Magento\Customer\Model\Session\Proxy $customerSession,
        \Magento\Customer\Block\Address\Renderer\DefaultRenderer $addressRender,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        \Magedelight\Authorizecim\Helper\Data $cimHelper,
        \Magento\Framework\DataObject $dataObject,
        \Magedelight\Authorizecim\Model\Config $cimConfig,
        \Magento\Directory\Model\ResourceModel\Country $countryResource,
        \Magedelight\Authorizecim\Model\ResourceModel\Cards\CollectionFactory $cardCollFactory,
        \Magedelight\Authorizecim\Model\CardsFactory $cardsFactory,
        array $data = []
    ) {
        $this->scopeConfig           = $context->getScopeConfig();
        $this->paymentConfig         = $paymentConfig;
        $this->customerSession       = $customerSession;
        $this->addressRender         = $addressRender;
        $this->countryFactory        = $countryFactory;
        $this->cimHelper             = $cimHelper;
        $this->dataObject            = $dataObject;
        $this->countryResource       = $countryResource;
        $this->cimConfig             = $cimConfig;
        $this->cardCollFactory       = $cardCollFactory;
        $this->cardsFactory          = $cardsFactory;
        parent::__construct(
            $context,
            $directoryHelper,
            $jsonEncoder,
            $configCacheType,
            $regionCollectionFactory,
            $countryCollectionFactory,
            $data
        );
    }
 // @codingStandardsIgnoreEnd
    public function getCustomer()
    {
        return $this->_customer;
    }

    /**
     * get cc types
     * @return type
     */
    public function getCcAvailableTypes()
    {
        $types          = $this->paymentConfig->getCcTypes();
        $availableTypes = explode(',', $this->cimConfig->getCcTypes());
        $typesCode      = array_keys($types);
        if ($availableTypes) {
            foreach ($typesCode as $code) {
                if (!in_array($code, $availableTypes)) {
                    unset($types[$code]);
                }
            }
        }

        return $types;
    }

    /**
     * get cc months
     * @return type
     */
    public function getCcMonths()
    {
        $months = $this->getData('cc_months');
        if ($months===null) {
            $months[0] = __('Month');
            $months    = array_merge($months, $this->paymentConfig->getMonths());
            $this->setData('cc_months', $months);
        }

        return $months;
    }

    /**
     * get cc years
     * @return type
     */
    public function getCcYears()
    {
        $years = $this->getData('cc_years');
        if (!($years)) {
            $yearsConfig = $this->paymentConfig->getYears();
            $years       = [0 => __('Year')] + $yearsConfig;
            $this->setData('cc_years', $years);
        }

        return $years;
    }

    /**
     * get config value
     * @param type $path
     * @return type
     */
    public function getConfig($path)
    {
        return $this->scopeConfig->getValue(
            $path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * check varification is enable
     * @return type
     */
    public function hasVerification()
    {
        return $this->cimConfig->isCardVerificationEnabled();
    }

    /**
     * get back url
     * @return string
     */
    public function getBackUrl()
    {
        return $this->_urlBuilder->getUrl('md_authorizecim/cards/lists');
    }
}
