<?php
/**
 * Magedelight
 * Copyright (C) 2017 Magedelight <info@magedelight.com>.
 *
 * NOTICE OF LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://opensource.org/licenses/gpl-3.0.html.
 *
 * @category Magedelight
 *
 * @copyright Copyright (c) 2017 Mage Delight (http://www.magedelight.com/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Magedelight <info@magedelight.com>
 */
namespace Magedelight\Authorizecim\Block\Customer\Cards;

class Lists extends \Magedelight\Authorizecim\Block\Customer\Authorizecim
{
   
    public function getCards()
    {
       $customerId = $this->customerSession->getId();
       $websiteId = $this->cimHelper->getWebsiteId();
       $customer_account_scope =  $this->cimHelper->getCustomerAccountScope();
       $result = [];
       if (!empty($customerId)) {
            $cardData =  $this->cardCollFactory->create()
            ->addFieldToFilter('customer_id', $customerId);
             if ($customer_account_scope) {
               $cardData->addFieldToFilter('website_id', $websiteId);
            }
            $result = $cardData->getData();
       }
       return $result;
    }


    protected function _construct()
    {
        parent::_construct();
        $this->pageConfig->getTitle()->set(__('My Saved Cards'));
    }

    public function getAddUrl()
    {
        return $this->getUrl('md_authorizecim/cards/add');
    }

    public function getFormatedAddress($card)
    {
      $typeObject = $this->dataObject;
        $typeObject->addData([
            'code' => 'html',
            'title' => 'HTML',
            'default_format' => $this->cimConfig->getDefaultFormat(),
        ]);
        $this->addressRender->setType($typeObject);
        $data = [];
        $countryName = '';
        if (!empty($card['country_id'])) {
            $countryModel = $this->countryFactory->create();
            $this->countryResource->loadByCode($countryModel, $card['country_id']);
            $countryName = $countryModel->getName();
        }
        $data['firstname'] = $card['firstname'];
        $data['lastname'] = $card['lastname'];
        $data['street'] = $card['street'];
        $data['city'] = $card['city'];
        $data['country_id'] = $card['country_id'];
        $data['country'] = $countryName;
        $data['region_id'] = $card['region_id'];
        $data['postcode'] = $card['postcode'];
        $data['telephone'] = $card['telephone'];
        $format = $this->addressRender->getFormatArray($data);
        return $this->filterManager->template($format, ['variables' => $data]);
    }

    public function getPostUrl()
    {
        return $this->getUrl('*/*/edit');
    }

    public function getDeleteAction()
    {
        return $this->getUrl('*/*/delete');
    }
}
