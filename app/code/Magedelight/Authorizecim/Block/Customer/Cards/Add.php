<?php
/**
 * Magedelight
 * Copyright (C) 2017 Magedelight <info@magedelight.com>.
 *
 * NOTICE OF LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://opensource.org/licenses/gpl-3.0.html.
 *
 * @category Magedelight
 *
 * @copyright Copyright (c) 2017 Mage Delight (http://www.magedelight.com/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Magedelight <info@magedelight.com>
 */

namespace Magedelight\Authorizecim\Block\Customer\Cards;

class Add extends \Magedelight\Authorizecim\Block\Customer\Authorizecim
{
    protected $_template = 'md/authorizecim/cards/add.phtml';

    protected function _construct() {
        if($this->cimConfig->getIsTestMode())
        {
            $this->pageConfig->addRemotePageAsset(
                'https://jstest.authorize.net/v1/Accept.js',
                'js',
                ['attributes' => ['charset' => 'utf-8']]
            );
        }
        else {
                $this->pageConfig->addRemotePageAsset(
                'https://js.authorize.net/v1/Accept.js',
                 'js',
                 ['attributes' => ['charset' => 'utf-8']]
            );
        }

    }
    public function getFormAction()
    {
        return $this->getUrl('md_authorizecim/cards/save');
    }
    public function getClientKey()
    {
        return $this->cimConfig->getClientKey();
    }
    public function getApiLoginId()
    {
        return $this->cimConfig->getApiLoginId();
    }
    public function canacceptjs()
    {
        return $this->cimConfig->getAcceptJsEnable();
    }
}