define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'md_authorizecim',
                component: 'Magedelight_Authorizecim/js/view/payment/method-renderer/authorizecim-method'
            }
        );
        /** Add view logic here if needed */
        return Component.extend({});
    }
);