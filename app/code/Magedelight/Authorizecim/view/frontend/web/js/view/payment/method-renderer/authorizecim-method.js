var ref = null;
define(
    [
        'underscore',
        'jquery',
        'Magento_Payment/js/view/payment/cc-form',
        'Magento_Checkout/js/model/quote',
        'mage/translate',
        'Magento_Checkout/js/model/full-screen-loader',
         'ko',
         'Magento_Payment/js/model/credit-card-validation/credit-card-data',
        'Magento_Payment/js/model/credit-card-validation/credit-card-number-validator',
        'Magento_Checkout/js/model/resource-url-manager',
    ],
    function (
        _,
        $,
        Component,
        quote,
       $t,
       fullScreenLoader,
        ko,
        creditCardData, 
        cardNumberValidator,
        resourceUrlManager
    )  {
        'use strict';
        var configCim = window.checkoutConfig.payment.md_authorizecim;
        return Component.extend({
            defaults: {
                template: 'Magedelight_Authorizecim/payment/authorizecim',
                creditCardType: '',
                creditCardExpYear: '',
                creditCardExpMonth: '',
                creditCardNumber: '',
                creditCardSsStartMonth: '',
                creditCardSsStartYear: '',
                creditCardSsIssue: '',
                creditCardVerificationNumber: '',
                selectedCardType: null
            },
            newCardVisible: ko.observable(false),
            newCardVisibleSave: ko.observable(false),
            savechecked: ko.observable(true),
            saveunchecked: ko.observable(false),
            noSavedCardAvail: ko.observable(false),
             /**
             * Set list of observable attributes
             *
             * @returns {exports.initObservable}
             */
            initObservable: function () {
                
                 this._super()
                    .observe([
                        'creditCardType',
                        'creditCardExpYear',
                        'creditCardExpMonth',
                        'creditCardNumber',
                        'creditCardVerificationNumber',
                        'creditCardSsStartMonth',
                        'creditCardSsStartYear',
                        'creditCardSsIssue',
                        'selectedCardType'
                    ]);
                    return this;
            },
           
           initialize: function () {
                var self = this;

                this._super();

                //Set credit card number to credit card data object
                this.creditCardNumber.subscribe(function (value) {
                    var result;

                    self.selectedCardType(null);

                    if (value === '' || value === null) {
                        return false;
                    }
                    result = cardNumberValidator(value);
                   
                    if (!result.isPotentiallyValid && !result.isValid) {
                        return false;
                    }

                    if (result.card !== null) {
                        self.selectedCardType(result.card.type);
                        creditCardData.creditCard = result.card;
                    }

                    if (result.isValid) {
                        creditCardData.creditCardNumber = value;
                        var availableTypes = configCim.availableCardTypes;
                        var typearray = $.map(availableTypes, function(value, index) {
                           return [index];
                       });
                       if($.inArray( result.card.type, typearray)!=-1)
                       {
                           self.creditCardType(result.card.type);
                       }
                       else
                       {
                           self.creditCardType("");
                       }
                       
                        
                       // self.creditCardType("");
                    }
                });

                //Set expiration year to credit card data object
                this.creditCardExpYear.subscribe(function (value) {
                    creditCardData.expirationYear = value;
                });

                //Set expiration month to credit card data object
                this.creditCardExpMonth.subscribe(function (value) {
                    creditCardData.expirationMonth = value;
                });

                //Set cvv code to credit card data object
                this.creditCardVerificationNumber.subscribe(function (value) {
                    creditCardData.cvvCode = value;
                });
           },
            getCode: function () {
                return 'md_authorizecim';
            },
            getCanAcceptJs:function()
            {
                var canacceptjs = (configCim.canacceptjs == 1) ? true: false;
                 return canacceptjs;
               
            },
            getData: function() {
                if(configCim.canacceptjs==1)
                {
                     return {
                        'method': this.item.method,
                        'additional_data': {
                            'payment_profile_id': $('#'+this.getCode()+'_payment_profile_id').val(),
                            'data_value': $('#'+this.getCode()+'_data_value').val(),
                            'data_descriptor': $('#'+this.getCode()+'_data_descriptor').val(),
                            'cc_cid': $('#'+this.getCode()+'_cc_cid').val(),
                            'save_card': $('#'+this.getCode()+'_save_card').val(),
                        }
                    };
                }
                else
                {
                    return {
                        'method': this.item.method,
                        'additional_data': {
                            'payment_profile_id': $('#'+this.getCode()+'_payment_profile_id').val(),
                            'cc_type': $('#'+this.getCode()+'_cc_type').val(),
                            'cc_number': $('#'+this.getCode()+'_cc_number').val(),
                            'expiration': $('#'+this.getCode()+'_expiration').val(),
                            'expiration_yr': $('#'+this.getCode()+'_expiration_yr').val(),
                            'cc_cid': $('#'+this.getCode()+'_cc_cid').val(),
                            'save_card': $('#'+this.getCode()+'_save_card').val(),
                        }
                    };
                }
            },
            getCcAvailableTypes: function() {
                var availableTypes = configCim.availableCardTypes;
                var typearray = $.map(availableTypes, function(value, index) {
                    return [index];
                });
                return typearray;
            },
            getIcons: function (type) {
                return window.checkoutConfig.payment.ccform.icons.hasOwnProperty(type)
                    ? window.checkoutConfig.payment.ccform.icons[type]
                    : false
            },
            getCcMonths: function() {
                return window.checkoutConfig.payment.ccform.months[this.getCode()];
            },
            getCcYears: function() {
                return window.checkoutConfig.payment.ccform.years[this.getCode()];
            },
            hasVerification: function() {
               // return window.checkoutConfig.payment.ccform.hasVerification[this.getCode()];
                return configCim.hasVerification;
            },
            hasSsCardType: function() {
                return window.checkoutConfig.payment.ccform.hasSsCardType[this.getCode()];
            },
            getCvvImageUrl: function() {
                return window.checkoutConfig.payment.ccform.cvvImageUrl[this.getCode()][0];
            },
            getCvvImageHtml: function() {
                return '<img src="' + this.getCvvImageUrl()
                    + '" alt="' + $t('Card Verification Number Visual Reference')
                    + '" title="' + $t('Card Verification Number Visual Reference')
                    + '" />';
            },
            getSsStartYears: function() {
                return window.checkoutConfig.payment.ccform.ssStartYears[this.getCode()];
            },
            getCcAvailableTypesValues: function() {
                return _.map(this.getCcAvailableTypes(), function(value, key) {
                    return {
                        'value': key,
                        'type': value
                    }
                });
            },
            getCcMonthsValues: function() {
                return _.map(configCim.ccMonths, function(value, key) {
                    return {
                        'value': key,
                        'month': value
                    }
                });
            },
            getCcYearsValues: function() {
                return _.map(configCim.ccYears, function(value, key) {
                    return {
                        'value': key,
                        'year': value
                    }
                });
            },
            getSsStartYearsValues: function() {
                return _.map(this.getSsStartYears(), function(value, key) {
                    return {
                        'value': key,
                        'year': value
                    }
                });
            },
            isShowLegend: function() {
                return false;
            },
            getCcTypeTitleByCode: function(code) {
                var title = '';
                _.each(this.getCcAvailableTypesValues(), function (value) {
                    if (value['value'] == code) {
                        title = value['type'];
                    }
                });
                return title;
            },
            formatDisplayCcNumber: function(number) {
                return 'xxxx-' + number.substr(-4);
            },
            getInfo: function() {
                return [
                    {'name': 'Credit Card Type', value: this.getCcTypeTitleByCode(this.creditCardType())},
                    {'name': 'Credit Card Number', value: this.formatDisplayCcNumber(this.creditCardNumber())}
                ];
            },
            getStoredCard: function(){
                return _.map(configCim.storedCards, function(value, key) {
                    return {
                        'value': key,
                        'optText': value
                    }
                });
            },
            isNewEnabled: function(){
                var result = true;
                if(_.size(configCim.storedCards) > 1){
                    result =  false;
                }
                return result;
            },
            isStoreCardDropdownEnabled: function()
            {
                var result = false;
                if(_.size(configCim.storedCards) > 1){
                    result = true;
                }
                else if(_.size(configCim.storedCards) == 1)
                {
                    this.newCardVisible(true);
                    this.noSavedCardAvail(true);
                }
                return result;
            },
            displayNewCard: function(){
                var elementValue = $('#'+this.getCode()+'_payment_profile_id').val();
                if(elementValue == 'new'){
                    this.newCardVisible(true);
                    this.newCardVisibleSave(true);
                }else{
                    this.newCardVisible(false);
                    this.newCardVisibleSave(false);
                }
            },
            isSaveCardOptional: function(){
                return (configCim.canSaveCard == 0) ? true: false;
            },
            isGuestCustomer: function(){
                return (resourceUrlManager.getCheckoutMethod()=='guest')? true: false;
            },
            prepareCimPayment: function(){
                if ($('#cim-transparent-form').valid()) {
                     if(configCim.canacceptjs==1 && $('#'+this.getCode()+'_payment_profile_id').val()=='new')
                     {
                        this.sendPaymentDataToAnet();
                     }
                     else
                     {
                        this.placeOrder();
                     }
                    
                  }
            },
            sendPaymentDataToAnet: function()
            {
                var secureData = {}, authData = {}, cardData = {};
                cardData.cardNumber = $('#'+this.getCode()+'_cc_number').val();
                cardData.month = $('#'+this.getCode()+'_expiration').val();
                cardData.year = $('#'+this.getCode()+'_expiration_yr').val();
                if(this.hasVerification())
                {
                    cardData.cardCode = $('#'+this.getCode()+'_cc_cid').val();
                }
                secureData.cardData = cardData;
                authData.clientKey = configCim.clientkey;
                authData.apiLoginID = configCim.apiloginid;
                secureData.authData = authData;
                Accept.dispatchData(secureData, 'responseHandler');
                ref = this;
            },
           
            
        });
        
        
    }
);
function responseHandler(response)
{
     if (response.messages.resultCode === 'Error') {
            for (var i = 0; i < response.messages.message.length; i++) {
                console.log(response.messages.message[i].code + ':' + response.messages.message[i].text);
            }
            } else {
                useOpaqueData(response.opaqueData)
            } 
}
function useOpaqueData(responseData) {
    jQuery('#md_authorizecim_data_value').val(responseData.dataValue);
    jQuery('#md_authorizecim_data_descriptor').val(responseData.dataDescriptor);
    ref.placeOrder();
}