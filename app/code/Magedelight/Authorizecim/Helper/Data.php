<?php
/**
 * Magedelight
 * Copyright (C) 2017 Magedelight <info@magedelight.com>.
 *
 * NOTICE OF LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://opensource.org/licenses/gpl-3.0.html.
 *
 * @category Magedelight
 *
 * @copyright Copyright (c) 2017 Mage Delight (http://www.magedelight.com/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Magedelight <info@magedelight.com>
 */
namespace Magedelight\Authorizecim\Helper;

use Magento\Framework\App\Helper\Context;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const REQUEST_TYPE_AUTH_ONLY = 'AUTH_ONLY';
    const REQUEST_TYPE_AUTH_CAPTURE = 'AUTH_CAPTURE';
    const REQUEST_TYPE_PRIOR_AUTH_CAPTURE = 'PRIOR_AUTH_CAPTURE';
    const REQUEST_TYPE_CREDIT = 'CREDIT';
    const REQUEST_TYPE_VOID = 'VOID';
     /**
     * @var int|null
     */
     protected $today = null;

     /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
     protected $dateTime;

     /**
     * @var \Magento\Braintree\Model\Config\Cc
     */
    protected $cimCcConfig;

    /**
     * @var PaymentConfig
     */
    protected $paymentConfig;
    
    protected $_storeManager;
    
    protected $errorMessage = [
        'I00001' => ['short_msg' => 'Successful', 'description' => 'The request was processed successfully.'],
        'I00003' => ['short_msg' => 'The record has already been deleted.',
            'description' => 'The record has already been deleted.'],
        'E00001' => ['short_msg' => 'An error occurred during processing. Please try again.',
            'description' => 'An unexpected system error occurred while processing this request.'],
        'E00002' => ['short_msg' => 'The content-type specified is not supported.',
            'description' => 'The only supported content-types are text/xml and application/xml.'],
        'E00003' => ['short_msg' => 'An error occurred while parsing the XML request.',
            'description' => 'This is the result of an XML parser error.'],
        'E00004' => ['short_msg' => 'The name of the requested API method is invalid.',
            'description' => 'The name of the root node of the XML request is the API method being called. It is not valid.'],
        'E00005' => ['short_msg' => 'The merchantAuthentication.transactionKey is invalid or not present.',
            'description' => 'Merchant authentication requires a valid value for transaction key'],
        'E00006' => ['short_msg' => ' The merchantAuthentication.name is invalid or not present.',
            'description' => 'Merchant authentication requires a valid value for name.'],
        'E00007' => ['short_msg' => ' User authentication failed due to invalid authentication values.',
            'description' => 'The name/and or transaction key is invalid.'],
        'E00008' => ['short_msg' => 'User authentication failed. The payment gateway account or user is inactive.',
            'description' => 'The payment gateway or user account is not currently active.'],
        'E00009' => ['short_msg' => ' The payment gateway account is in Test Mode.The request cannot be processed.',
            'description' => 'The requested API method cannot be executed while the payment gateway account is in Test'],
        'E00010' => ['short_msg' => 'User authentication failed. You do not have the appropriate permissions.',
            'description' => 'The user does not have permission to call the API.'],
        'E00011' => ['short_msg' => 'Access denied. You do not have the appropriate permissions.',
            'description' => 'The user does not have permission to call the API method.'],
        'E00013' => ['short_msg' => 'The field is invalid.', 'description' => 'One of the field values is not valid.'],
        'E00014' => ['short_msg' => 'A required field is not present.',
            'description' => 'One of the required fields was not present.'],
        'E00015' => ['short_msg' => ' The field length is invalid.',
            'description' => 'One of the fields has an invalid length.'],
        'E00016' => ['short_msg' => 'The field type is invalid.', 'description' => 'The field type is not valid.'],
        'E00019' => ['short_msg' => 'The customer taxId or driversLicense information is required.',
            'description' => 'The customer tax ID or driver’s license information (driver’s license number'
            .', driver’s license state, driver’s license DOB) is required for the subscription.'],
        'E00027' => ['short_msg' => 'The transaction was unsuccessful.',
            'description' => 'An approval was not returned for the transaction.'],
        'E00029' => ['short_msg' => 'Payment information is required.',
            'description' => 'Payment information is required when creating a subscription or payment profile.'],
        'E00039' => ['short_msg' => 'A duplicate record already exists.',
            'description' => ' A duplicate of the customer profile, customer payment profile,'
            . ' or customer address was already submitted.'],
        'E00040' => ['short_msg' => 'The record cannot be found.',
            'description' => 'The profileID, paymentProfileId, or shippingAddressId for '
            . 'this request is not valid for this merchant. '],
        'E00041' => ['short_msg' => 'One or more fields must contain a value.',
            'description' => 'All of the fields were empty or missing.'],
        'E00042' => ['short_msg' => ' The maximum number of payment profiles allowed for the customer profile is {0}.',
            'description' => 'The maximum number of payment profiles for the customer profile has been reached.'],
        'E00043' => ['short_msg' => 'The maximum number of shipping addresses allowed for the customer profile is {0}',
            'description' => 'The maximum number of shipping addresses for the customer profile has been reached.'],
        'E00044' => ['short_msg' => 'Customer Information Manager is not enabled.',
            'description' => 'The payment gateway account is not enabled for Customer Information Manager (CIM).'],
        'E00045' => ['short_msg' => ' The root node does not reference a valid XML namespace.',
            'description' => 'An error exists in the XML namespace. This error is similar to E00003'],
        'E00051' => ['short_msg' => 'The original transaction was not issued for this payment profile.',
            'description' => 'If the customer profile ID, payment profile ID, and shipping address ID are included, '
            . 'they must match the original transaction.'],
        'E00098' => ['short_msg' => 'Customer Profile ID or Shipping Profile ID not found.',
            'description' => 'Search for shipping profile using customer profile id and shipping profile id'
            . ' did not find any records.'],
        'E00099' => ['short_msg' => 'Customer profile creation failed. This transaction ID is invalid.',
            'description' => 'Customer profile creation failed. This transaction ID is invalid.'],
        'E00100' => ['short_msg' => 'Customer profile creation failed. This transaction type does not support '
            . 'profile creation.',
            'description' => 'Customer profile creation failed. This transaction type does not support profile creation.'],
        'E00101' => ['short_msg' => 'Customer profile creation failed.',
            'description' => 'Error creating a customer payment profile from transaction.'],
        'E00102' => ['short_msg' => 'Customer profile creation failed.',
            'description' => 'Error creating a customer profile from transaction.'],
        'E00088' => ['short_msg' => 'ProfileIds cannot be sent when requesting CreateProfile.',
            'description' => 'CreateProfile and profileIds are mutually exclusive, only one of them can be provided at a time.'],
        'E00089' => ['short_msg' => 'Payment data is required when requesting CreateProfile.',
            'description' => 'When requesting CreateProfile payment data cannot be null.'],
        'E00090' => ['short_msg' => 'PaymentProfile cannot be sent with payment data.',
            'description' => 'PaymentProfile and PaymentData are mutually exclusive, only one of them can be provided at a time.'],
        'E00091' => ['short_msg' => 'PaymentProfileId cannot be sent with payment data.',
            'description' => 'PaymentProfileId and payment data are mutually exclusive, only one of them can be provided at a time.'],
        'E00092' => ['short_msg' => 'ShippingProfileId cannot be sent with ShipTo data.',
            'description' => 'ShippingProfileId and ShipToAddress are mutually exclusive, only one of them can be provided at a time.'],
        'E00093' => ['short_msg' => 'PaymentProfile cannot be sent with billing data.',
            'description' => 'PaymentProfile and Billing information are mutually exclusive, only one of them can be provided at a time.'],
        'E00094' => ['short_msg' => 'Paging Offset exceeds the maximum allowed value.',
            'description' => 'Paging Offset exceeds allowed value. Check and lower the value.'],
        'E00095' => ['short_msg' => 'ShippingProfileId is not provided within Customer Profile.',
            'description' => 'When using Customer Profile with Credit Card Info to specify Shipping Profile, Shipping Profile Id must be included.'],
    ];

     protected $_avsResponses = array(
        'B' => 'No address submitted; could not perform AVS check.',
        'E' => 'AVS data invalid',
        'R' => 'AVS unavailable',
        'G' => 'AVS not supported',
        'U' => 'AVS unavailable',
        'S' => 'AVS not supported',
        'N' => 'Street and zipcode do not match.',
        'A' => 'Street matches; zipcode does not.',
        'Z' => '5-digit zip matches; street does not.',
        'W' => '9-digit zip matches; street does not.',
        'Y' => 'Perfect match',
        'X' => 'Perfect match',
        'P' => 'N/A',
    );

    protected $_ccvResponses = array(
        'M' => 'Passed',
        'N' => 'Failed',
        'P' => 'Not processed',
        'S' => 'Not received',
        'U' => 'N/A',
    );

    protected $_cavvResponses = array(
        '0' => 'Not validated; bad data',
        '1' => 'Failed',
        '2' => 'Passed',
        '3' => 'CAVV unavailable',
        '4' => 'CAVV unavailable',
        '7' => 'Failed',
        '8' => 'Passed',
        '9' => 'Failed (issuer unavailable)',
        'A' => 'Passed (issuer unavailable)',
        'B' => 'Passed (info only)',
    );
    
      public function __construct(
            Context $context,
            \Magento\Framework\App\ResourceConnection $resource,
            \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
            \Magedelight\Authorizecim\Model\Config $cimCcConfig,
            \Magento\Payment\Model\Config $paymentConfig,
            \Magento\Store\Model\StoreManagerInterface $storeManager,
            \Magento\Backend\Model\Session\Quote $sessionquote
      ) {
        $this->dbResource = $resource;
        $this->dateTime = $dateTime;
        $this->cimCcConfig = $cimCcConfig;
        $this->paymentConfig = $paymentConfig;
        $this->_storeManager = $storeManager;
        $this->sessionquote = $sessionquote;
        parent::__construct($context);
    }
    
    public function isEnabled()
    {
        $currentUrl = $this->_storeManager->getStore()->getBaseUrl();
        $domain = $this->getDomainName($currentUrl);
        $selectedWebsites = $this->getConfig('md_authorizecim/general/select_website');
        $websites = explode(',',$selectedWebsites);
        if(in_array($domain, $websites) && $this->getConfig('payment/md_authorizecim/active') && $this->getConfig('md_authorizecim/license/data'))
        {
          return true;
        }else{
          return false;
        }
    }

    public function getDomainName($domain){
        $string = '';
        
        $withTrim = str_replace(array("www.","http://","https://"),'',$domain);
        
        /* finding the first position of the slash  */
        $string = $withTrim;
        
        $slashPos = strpos($withTrim,"/",0);
        
        if($slashPos != false){
            $parts = explode("/",$withTrim);
            $string = $parts[0];
        }
        return $string;
    }

    public function getWebsites()
    {
        $websites = $this->_storeManager->getWebsites();
        $websiteUrls = array();
        foreach($websites as $website)
        {
            foreach($website->getStores() as $store){
                $wedsiteId = $website->getId();
                $storeObj = $this->_storeManager->getStore($store);
                $storeId = $storeObj->getId();
                $url = $storeObj->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
                $parsedUrl = parse_url($url);
                $websiteUrls[] = str_replace(array('www.', 'http://', 'https://'), '', $parsedUrl['host']);
            }
        }

        return $websiteUrls;
    }

    public function getConfig($config_path)
    {
        return $this->scopeConfig->getValue(
            $config_path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
    
     /**
     * @param type $code
     * @return string
     */
    public function getErrorDescription($code)
    {
        $code = (string)$code;
        if (!empty($code)) {
            if (isset($this->errorMessage[$code])) {
                return $this->errorMessage[$code]['description'];
            } else {
                return __("Please Try Again");
            }
        }
    }
    public function getShortErrorDescription($code)
    {
        $code = (string)$code;
        if (!empty($code)) {
            if (isset($this->errorMessage[$code])) {
                return $this->errorMessage[$code]['short_msg'];
            } else {
                return __("Please Try Again");
            }
        }
    }
     protected function _formatPrice($payment, $amount)
    {
        return $payment->getOrder()->getBaseCurrency()->formatTxt($amount);
    }
    public function getTodayYear()
    {
        if (!$this->today) {
            $this->today = $this->dateTime->gmtTimestamp();
        }

        return date('Y', $this->today);
    }

    public function getTodayMonth()
    {
        if (!$this->today) {
            $this->today = $this->dateTime->gmtTimestamp();
        }

        return date('m', $this->today);
    }

    public function getCcTypeNameByCode($code)
    {
        $ccTypes = $this->paymentConfig->getCcTypes();
        if (isset($ccTypes[$code])) {
            return $ccTypes[$code];
        } else {
            return false;
        }
    }

     public function getCcTypes()
    {
        $ccTypes = $this->paymentConfig->getCcTypes();
        if (is_array($ccTypes)) {
            return $ccTypes;
        } else {
            return false;
        }
    }
    
    public function getCcAvailableCardTypes($country = null)
    {
        $types = array_flip(explode(',', $this->cimCcConfig->getCcTypes()));
        $mergedArray = [];

        if (is_array($types)) {
            foreach (array_keys($types) as $type) {
                $types[$type] = $this->getCcTypeNameByCode($type);
            }
        }
        //preserve the same credit card order
        $allTypes = $this->getCcTypes();
        if (is_array($allTypes)) {
            foreach ($allTypes as $ccTypeCode => $ccTypeName) {
                if (array_key_exists($ccTypeCode, $types)) {
                    $mergedArray[$ccTypeCode] = $ccTypeName;
                }
            }
        }
        return $mergedArray;
    }
    
    public function getTransactionMessage($payment, $requestType, $lastTransactionId, $card, $amount = false,
        $exception = false
    ) {
        return $this->getExtendedTransactionMessage(
            $payment, $requestType, $lastTransactionId, $card, $amount, $exception
        );
    }
    protected function _getOperation($requestType)
    {
        switch ($requestType) {
                case self::REQUEST_TYPE_AUTH_ONLY:
                    return __('authorize');
                case self::REQUEST_TYPE_AUTH_CAPTURE:
                    return __('authorize and capture');
                case self::REQUEST_TYPE_PRIOR_AUTH_CAPTURE:
                    return __('capture');
                case self::REQUEST_TYPE_CREDIT:
                    return __('refund');
                case self::REQUEST_TYPE_VOID:
                    return __('void');
                default:
                    return false;
            }
    }
     /**
      *
      * @param type $payment
      * @param type $requestType
      * @param type $lastTransactionId
      * @param type $card
      * @param type $amount
      * @param type $exception
      * @param type $additionalMessage
      * @return boolean
      */
     public function getExtendedTransactionMessage($payment, $requestType, $lastTransactionId, $card, $amount = false,
            $exception = false, $additionalMessage = false
        ) {
         $operation = $this->_getOperation($requestType);

         if (!$operation) {
             return false;
         }
         if ($amount) {
             $amount = sprintf('amount %s', $this->_formatPrice($payment, $amount));
         }

         if ($exception) {
             $result = sprintf('failed');
         } else {
             $result = sprintf('successful');
         }
         $card = sprintf('Credit Card: xxxx-%s', $card->getCcLast4());
         $pattern = '%s %s %s - %s.';
         $texts = array($card, $amount, $operation, $result);

         if (!is_null($lastTransactionId)) {
             $pattern .= ' %s.';
             $texts[] = sprintf('Authorizecim Transaction ID %s', $lastTransactionId);
         }

         if ($additionalMessage) {
             $pattern .= ' %s.';
             $texts[] = $additionalMessage;
         }
         $pattern .= ' %s';
         $texts[] = $exception;
         return call_user_func_array('sprintf', array_merge(array($pattern), $texts));
     }
     public function getAvsLabel($avs)
     {
        if (isset($this->_avsResponses[ $avs ])) {
            return __(sprintf('%s (%s)', $avs, $this->_avsResponses[ $avs ]));
        }

        return $avs;
    }

    public function getCvnLabel($cvn)
    {
        if (isset($this->_cvnResponses[ $cvn ])) {
            return __(sprintf('%s (%s)', $cvn, $this->_cvnResponses[ $cvn ]));
        }

        return $cvn;
    }
    public function getCavvLabel($cavv)
    {
        if (isset($this->_cavvResponses[ $cavv ])) {
            return __(sprintf('%s (%s)', $cavv, $this->_cavvResponses[ $cavv ]));
        }

        return $code;
    }
    public function checkAdmin()
    {
        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $app_state = $om->get('Magento\Framework\App\State');
        if ($app_state->getAreaCode() == \Magento\Backend\App\Area\FrontNameResolver::AREA_CODE) {
            return true;
        } else {
            return false;
        }
    }
    
    public function getWebsiteId(){
        if($this->checkAdmin()){
            $storeId =  $this->sessionquote->getQuote()->getStoreId();
            $store = $this->_storeManager->getStore($storeId);
            $website_id = $store->getWebsiteId();
        } else {
            $website_id =  $this->_storeManager->getStore()->getWebsiteId();
        }
        return $website_id;
    }
    
    public function getCustomerAccountScope()
    {
        return $this->cimCcConfig->getCustomerAccountShare();
    }
}
