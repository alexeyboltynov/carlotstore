<?php
/**
 * Magedelight
 * Copyright (C) 2017 Magedelight <info@magedelight.com>.
 *
 * NOTICE OF LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://opensource.org/licenses/gpl-3.0.html.
 *
 * @category Magedelight
 *
 * @copyright Copyright (c) 2017 Mage Delight (http://www.magedelight.com/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Magedelight <info@magedelight.com>
 */

namespace Magedelight\Authorizecim\Model;

class Config
{
    const AUTHORIZECIM_ACTIVE = 'payment/md_authorizecim/active';
    const AUTHORIZECIM_TITLE = 'payment/md_authorizecim/title';
    const AUTHORIZECIM_API_LOGIN = 'payment/md_authorizecim/login';
    const AUTHORIZECIM_TRANS_KEY = 'payment/md_authorizecim/trans_key';
    const AUTHORZECIM_TEST = 'payment/md_authorizecim/test';
    const AUTHORIZECIM_PAYMENT_ACTION = 'payment/md_authorizecim/payment_action';
    const AUTHORIZECIM_GETEWAY_WSDL = 'payment/md_authorizecim/gateway_wsdl';
    const AUTHORIZECIM_GATEWAY_URL = 'payment/md_authorizecim/gateway_url';
    const AUTHORIZECIM_TEST_GATEWAY_URL = 'payment/md_authorizecim/test_gateway_url';
    const AUTHORIZECIM_ACCEPTED_CURRENCY = 'payment/md_authorizecim/currency';
    const AUTHORIZECIM_DEBUG = 'payment/md_authorizecim/debug';
    const AUTHORIZECIM_CCTYPES = 'payment/md_authorizecim/cctypes';
    const AUTHORIZECIM_CCV = 'payment/md_authorizecim/useccv';
    const AUTHORIZECIM_VALIDATION_TYPE = 'payment/md_authorizecim/validation_mode';
    const AUTHORIZECIM_CARD_SAVE_OPTIONAL = 'payment/md_authorizecim/save_optional';
    const AUTHORIZECIM_NEW_ORDER_STATUS = 'payment/md_authorizecim/order_status';
    const AUTHORIZECIM_TRANSACTION_TIMEOUT = 'payment/md_authorizecim/transaction_timeout';
    const AUTHORIZECIM_CART_LINE_ITEMS = 'payment/md_authorizecim/line_items';
    const AUTHORIZECIM_ENABLE_ACCEPTJS = 'payment/md_authorizecim/accept_js';
    const AUTHORIZECIM_CLIENT_KEY = 'payment/md_authorizecim/client_key';
    const SOLUTION_ID = 'AAA100302';
    const PRODUCTION_SOLUTION_ID = 'AAA171474';
    /**
     * @var int
     */
    public $storeId = null;

    /**
     * @var Magento\Store\Model\StoreManagerInterface
     */
    public $storeConfig;

     /**
     * @var Magento\Framework\App\Config\ScopeConfigInterface
     */
    public $scopeConfig;

    /**
     * @var Magento\Framework\Encryption\Encryptor
     */
    public $encryptor;

    /**
     *  get system config values
     * @param \Magento\Store\Model\StoreManagerInterface $storeConfig
     * @param \Magento\Framework\Encryption\Encryptor $encryptor
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeConfig,
        \Magento\Framework\Encryption\Encryptor $encryptor,
        \Magento\Customer\Model\Config\Share $customerScope,
        \Magento\Backend\Model\Session\Quote $quoteSession,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Sales\Api\Data\OrderInterface $order,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    )
    {
        $this->storeConfig   = $storeConfig;
        $this->scopeConfig   = $scopeConfig;
        $this->encryptor     = $encryptor;
        $this->customerScope = $customerScope;
        $this->_session      = $quoteSession;
        $this->request       = $request;
        $this->order         = $order;
        $this->storeId       = $this->storeConfig->getStore()->getId();
        $this->_backend      = $this->checkAdmin() ? true : false;
    }

    /**
     * @param type $storeId
     */
    public function setStoreId($storeId = 0)
    {
        $this->storeId = $this->getStoreId();
        return $this;
    }

    /**
     * This method will return whether Authorize.net CIM Payment is enabled or not in configuration. 
     *
     * @return bool
     */
    public function getIsActive()
    {
        return (boolean) $this->scopeConfig->getValue(self::AUTHORIZECIM_ACTIVE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,$this->getStoreId());
    }

    /**
     * This methos will return Authorize.net CIM payment method title
     * set by admin to display on onepage checkout payment step.
     *
     * @return string
     */
    public function getMethodTitle()
    {
        return (string) $this->scopeConfig->getValue(self::AUTHORIZECIM_TITLE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,$this->getStoreId());
    }

    /**
     * This method will return merchant api login id set by admin in configuration. 
     *
     * @return string
     */
    public function getApiLoginId()
    {
        return $this->encryptor->decrypt($this->scopeConfig->getValue(self::AUTHORIZECIM_API_LOGIN,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,$this->getStoreId()));
    }

    /**
     * This method will return merchant api transaction key set by admin in configuration.
     *
     * @return string
     */
    public function getApiTransKey()
    {
        return $this->encryptor->decrypt($this->scopeConfig->getValue(self::AUTHORIZECIM_TRANS_KEY,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,$this->getStoreId()));
    }

    /**
     * This method will return whether test mode is enabled or not.
     *
     * @return bool
     */
    public function getIsTestMode()
    {
        return (boolean) $this->scopeConfig->getValue(self::AUTHORZECIM_TEST,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,$this->getStoreId());
    }

    public function getSolutionId()
    {
       $isTestMode = $this->getIsTestMode();
       return ($isTestMode) ? self::SOLUTION_ID :
            self::PRODUCTION_SOLUTION_ID;
    }
    /**
     * This will returne payment action whether it is authorized or authorize and capture.
     *
     * @return string
     */
    public function getPaymentAction()
    {
        return (boolean) $this->scopeConfig->getValue(self::AUTHORIZECIM_PAYMENT_ACTION,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,$this->getStoreId());
    }

     /**
     * This method will return Authorize.net CIM Gateway WSDL.
     *
     * @return string
     */
    public function getGatewayWsdl()
    {
       return (string) $this->scopeConfig->getValue(self::AUTHORIZECIM_GETEWAY_WSDL,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,$this->getStoreId());
    }
    /**
     * This metod will return Authorize.net CIM Gateway url depending on test mode enabled or not.
     *
     * @return string
     */
    public function getGatewayUrl()
    {
        
        $isTestMode = $this->getIsTestMode();
        $gatewayUrl = ($isTestMode) ? (string) $this->scopeConfig->getValue(self::AUTHORIZECIM_TEST_GATEWAY_URL,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,$this->getStoreId()) :
            (string) $this->scopeConfig->getValue(self::AUTHORIZECIM_GATEWAY_URL,
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,$this->getStoreId());
        return $gatewayUrl;
    }

    /**
     * This method will return accepted currency set by admin.
     *
     * @return string
     */
    public function getAcceptedCurrency()
    {
        return (string) $this->scopeConfig->getValue(self::AUTHORIZECIM_ACCEPTED_CURRENCY,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,$this->getStoreId());
    }

    /**
     * This method will return whether debug is enabled from config.
     *
     * @return bool
     */
    public function getIsDebugEnabled()
    {
        return (boolean) $this->scopeConfig->getValue(self::AUTHORIZECIM_DEBUG,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,$this->getStoreId());
    }

    /**
     * This method return whether card verification is enabled or not.
     *
     * @return bool
     */
    public function isCardVerificationEnabled()
    {
        return (boolean) $this->scopeConfig->getValue(self::AUTHORIZECIM_CCV,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,$this->getStoreId());
    }

    /**
     * Authorize.net CIM validation mode.
     *
     * @return string
     */
    public function getValidationMode()
    {
        return (string) $this->scopeConfig->getValue(self::AUTHORIZECIM_VALIDATION_TYPE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,$this->getStoreId());
    }

    /**
     * Method which will return whether customer must save credit card as profile of not.
     *
     * @return bool
     */
    public function getSaveCardOptional()
    {
        return (boolean) $this->scopeConfig->getValue(self::AUTHORIZECIM_CARD_SAVE_OPTIONAL,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,$this->getStoreId());
    }


    public function getCcTypes()
    {
        return (string) $this->scopeConfig->getValue(self::AUTHORIZECIM_CCTYPES,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,$this->getStoreId());
    }

    public function sendLineItems()
    {
        return (boolean) $this->scopeConfig->getValue(self::AUTHORIZECIM_CART_LINE_ITEMS,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,$this->getStoreId());
    }
    public function getDefaultFormat()
    {
        return $this->scopeConfig->getValue(
            'customer/address_templates/html',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,$this->getStoreId()
        );
    }
    public function getAcceptJsEnable()
    {
        return (string) $this->scopeConfig->getValue(self::AUTHORIZECIM_ENABLE_ACCEPTJS,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,$this->getStoreId());
    }
    public function getClientKey()
    {
        return (string) $this->scopeConfig->getValue(self::AUTHORIZECIM_CLIENT_KEY,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,$this->getStoreId());
    }
    
    public function getCustomerAccountShare()
    {
       return $this->customerScope->isWebsiteScope();
    }
    
    public function checkAdmin()
    {
        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $app_state = $om->get('Magento\Framework\App\State');
        $area_code = $app_state->getAreaCode();
        if ($app_state->getAreaCode() == \Magento\Backend\App\Area\FrontNameResolver::AREA_CODE) {
            return true;
        } else {
            return false;
        }
    }
    
    public function getStoreId() 
    {
       if($this->checkAdmin()){
           $controller =  $this->request->getControllerName(); 
           $action =  $this->request->getActionName(); 
           if($controller == 'order_invoice' || $controller == 'order_creditmemo' 
                   || $action == 'reviewPayment' || $action == 'voidPayment')
           {
               $order_id = $this->request->getParam('order_id');
               $order = $this->order->load($order_id);
               $storeId = $order->getStoreId();
           } else {
               $storeId =  $this->_session->getQuote()->getStoreId();
           }
        } else {
            $storeId =  $this->storeConfig->getStore()->getId();
        }
        return  $storeId;  
    }
}
