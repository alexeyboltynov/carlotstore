<?php
/**
 * Magedelight
 * Copyright (C) 2017 Magedelight <info@magedelight.com>.
 *
 * NOTICE OF LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://opensource.org/licenses/gpl-3.0.html.
 *
 * @category Magedelight
 *
 * @copyright Copyright (c) 2016 Mage Delight (http://www.magedelight.com/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Magedelight <info@magedelight.com>
 */

namespace Magedelight\Authorizecim\Model;

use Magento\Sales\Model\ResourceModel\Order\Payment\Transaction\CollectionFactory
as TransactionCollectionFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Phrase;
use Magento\Quote\Api\Data\CartInterface;
use Magento\Payment\Model\InfoInterface;
use Magento\Payment\Model\MethodInterface;

class Payment implements MethodInterface
{
    const METHOD_CODE = 'md_authorizecim';

    const SUCCESS = 1;
    const REVIEW = 4;

    const STATUS_UNKNOWN = 'UNKNOWN';

    const STATUS_APPROVED = 'APPROVED';

    const STATUS_ERROR = 'ERROR';

    const STATUS_DECLINED = 'DECLINED';

    const STATUS_VOID = 'VOID';

    const STATUS_SUCCESS = 'SUCCESS';

    /**
     * Different payment method checks.
     */
   
    const RESPONSE_CODE_APPROVED = 1;
    const RESPONSE_CODE_DECLINED = 2;
    const RESPONSE_CODE_ERROR = 3;
    const RESPONSE_CODE_HELD = 4;
    const REQUEST_TYPE_AUTH_CAPTURE = 'AUTH_CAPTURE';
    const REQUEST_TYPE_AUTH_ONLY = 'AUTH_ONLY';
    const REQUEST_TYPE_CAPTURE_ONLY = 'CAPTURE_ONLY';
    const REQUEST_TYPE_CREDIT = 'CREDIT';
    const REQUEST_TYPE_VOID = 'VOID';
    const REQUEST_TYPE_PRIOR_AUTH_CAPTURE = 'PRIOR_AUTH_CAPTURE';
    protected $_realTransactionIdKey = 'real_transaction_id';
    public $isTransactionFraud = 'is_transaction_fraud';
    public $realTransactionIdKey = 'real_transaction_id';
    public $splitTenderIdKey = 'split_tender_id';
    public $_isGatewayActionsLockedKey = 'is_gateway_actions_locked';
    public $formBlockType = 'Magedelight\Authorizecim\Block\Form';
    public $infoBlockType = 'Magedelight\Authorizecim\Block\Info';
    public $infoInstance;

    public $storeId;
    public $cardsStorage               = null;
    public $store                      = 0;
    public $customer                   = null;
    public $backend                    = false;
    public $configModel                = null;
    public $invoice                    = null;
    public $creditmemo                 = null;
    public $postData                   = [];
    public $canReviewPayment           = true;
    protected $cardArray = ['American Express'=>'AE',
                            'Visa'=>'VI',
                            'MasterCard'=>'MC',
                            'Discover' => 'DI',
                            'JCB'=>'JCB',
                            'Diners'=>'DN'];
    /**
     *
     * @var Magento\Framework\Event\ManagerInterface
     */
    public $eventManager;

    /**
     *
     * @var Magento\Framework\Registry
     */
    public $registry;

    /**
     *
     * @var Magento\Store\Model\StoreManagerInterface
     */
    public $storeManager;

    /**
     *
     * @var Magento\Framework\App\Config\ScopeConfigInterface
     */
    public $scopeConfig;

    /**
     * @var Magento\Sales\Model\ResourceModel\Order\Payment\Transaction\CollectionFactory
     */
    public $salesTransactionCollectionFactory;

    /**
     *
     * @var Magento\Directory\Model\RegionFactory
     */
    public $regionFactory;

    /**
     *
     * @var Magento\Sales\Api\OrderRepositoryInterface
     */
    public $orderRepository;

    /**
     *
     * @var Magento\Framework\DataObjectFactory
     */
    public $objectFactory;

    /**
     *
     * @var Magento\Checkout\Model\Session\Proxy
     */
    public $checkoutsession;

    /**
     *
     * @var Magento\Sales\Model\OrderFactory
     */
    public $ordermodelFactory;

    /**
     *
     * @var Magento\Payment\Model\Config
     */
    public $paymentconfig;

    /**
     *
     * @var Magedelight\Authorizecim\Model\CardsFactory
     */
    public $cardFactory;

    /**
     *
     * @var Magento\Framework\Stdlib\DateTime\DateTime
     */
    public $date;

    /**
     *
     * @var Magento\Sales\Model\Order\Payment\Transaction
     */
    public $paymentTrans;

    /**
     *
     * @var \Magento\Customer\Model\Session\Proxy
     */
    public $customerSession;

    /**
     *
     * @var Magento\Framework\App\Request\Http
     */
    public $requestHttp;

    /**
     *
     * @var Magento\Framework\Encryption\EncryptorInterface
     */
    public $encryptor;

    /**
     *
     * @var Magento\Backend\Model\Session\Quote
     */
    public $sessionQuote;

    /**
     *
     * @var Magento\Customer\Api\CustomerRepositoryInterface
     */
    public $customerRepository;

     /**
     * @var Magedelight\Authorizecim\Model\Config
     */
    public $cimConfig;

    /**
     *
     * @var Magedelight\Authorizecim\Model\Api\Xml
     */
    public $cimXml;

    /**
     *
     * @var Magedelight\Authorizecim\Model\Payment\Cards
     */
    public $cardpayment;

    /**
     *
     * @var Magedelight\Authorizecim\Helper\Data
     */
    public $cimHelper;

    /**
     *
     * @var Magedelight\Authorizecim\Model\ResourceModel\Cards\CollectionFactory
     */
    public $cardCollectionFactory;

    /**
     * Date model
     *
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    public $localeDate;

    /**
     * \Magento\Framework\App\ResourceConnection
     */
    public $dbResource;

    /**
     * Magento\Framework\ObjectManagerInterface
     */
    public $objectManager;

    // @codingStandardsIgnoreStart
    public function __construct(
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Framework\Registry $registry,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        TransactionCollectionFactory $salesTransactionCollectionFactory,
        \Magento\Directory\Model\RegionFactory $regionFactory,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Framework\DataObjectFactory $objectFactory,
        \Magento\Checkout\Model\Session\Proxy $checkoutsession,
        \Magento\Sales\Model\OrderFactory $ordermodelFactory,
        \Magento\Payment\Model\Config $paymentconfig,
        \Magedelight\Authorizecim\Model\CardsFactory $cardFactory,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Sales\Model\Order\Payment\Transaction $paymentTrans,
        \Magento\Customer\Model\Session\Proxy $customerSession,
        \Magento\Framework\App\Request\Http $requestHttp,
        \Magento\Framework\Encryption\EncryptorInterface $encryptor,
        \Magento\Backend\Model\Session\Quote $sessionQuote,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magedelight\Authorizecim\Model\Config $cimConfig,
        \Magedelight\Authorizecim\Model\Api\Xml $cimXml,
        \Magedelight\Authorizecim\Model\Payment\Cards $cardpayment,
        \Magedelight\Authorizecim\Helper\Data $cimHelper,
        \Magedelight\Authorizecim\Model\ResourceModel\Cards\CollectionFactory $cardCollectionFactory,
        \Magento\Sales\Model\ResourceModel\Order\Payment\Transaction\CollectionFactory $transactionFactory,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Framework\ObjectManagerInterface $objectManager
    ) {
        $this->eventManager                      = $eventManager;
        $this->registry                          = $registry;
        $this->storeManager                      = $storeManager;
        $this->scopeConfig                       = $scopeConfig;
        $this->salesTransactionCollectionFactory = $salesTransactionCollectionFactory;
        $this->regionFactory                     = $regionFactory;
        $this->orderRepository                   = $orderRepository;
        $this->objectFactory                     = $objectFactory;
        $this->checkoutsession                   = $checkoutsession;
        $this->ordermodelFactory                 = $ordermodelFactory;
        $this->paymentconfig                     = $paymentconfig;
        $this->cardFactory                       = $cardFactory;
        $this->date                              = $date;
        $this->paymentTrans                      = $paymentTrans;
        $this->customerSession                   = $customerSession;
        $this->requestHttp                       = $requestHttp;
        $this->encryptor                         = $encryptor;
        $this->sessionQuote                      = $sessionQuote;
        $this->customerRepository                = $customerRepository;
        $this->cimConfig                         = $cimConfig;
        $this->cimXml                            = $cimXml;
        $this->cardpayment                       = $cardpayment;
        $this->cimHelper                         = $cimHelper;
        $this->cardCollectionFactory             = $cardCollectionFactory;
        $this->transactionFactory                = $transactionFactory;
        $this->customerFactory                   = $customerFactory;
        $this->localeDate                        = $localeDate;
        $this->dbResource                        = $resource;
        $this->objectManager                     = $objectManager;
//        $this->backend = ($this->storeManager->getStore()->getId() == 0) ? true
//                : false;
        $this->cimConfig->checkAdmin() ? true : false;
        if ($this->backend && $this->registry->registry('current_order')) {
            $this->setStore($this->registry->registry('current_order')->getStoreId());
        } elseif ($this->backend && $this->registry->registry('current_invoice')) {
            $this->setStore($this->registry->registry('current_invoice')->getStoreId());
        } elseif ($this->backend && $this->registry->registry('current_creditmemo')) {
            $this->setStore($this->registry->registry('current_creditmemo')->getStoreId());
        } elseif ($this->backend && $this->registry->registry('current_customer')
            != false) {
            $this->setStore($this->registry->registry('current_customer')->getStoreId());
        } elseif ($this->backend && $this->sessionQuote->getStoreId() > 0) {
            $this->setStore($this->sessionQuote->getStoreId());
        } else {
            $this->setStore($this->storeManager->getStore()->getId());
        }
    }

    // @codingStandardsIgnoreEnd
     /**
      * Retrieve payment method code
      *
      * @return string
      *
      */
    public function getCode()
    {
        return self::METHOD_CODE;
    }

    /**
     * Retrieve block type for method form generation
     *
     * @return string
     *
     * @deprecated
     */
    public function getFormBlockType()
    {
        return $this->formBlockType;
    }

    /**
     * Retrieve payment method title
     *
     * @return string
     *
     */
    public function getTitle()
    {
        return  $this->cimConfig->getMethodTitle();
    }

    /**
     * Store id getter
     * @return int
     */
    public function getStore()
    {
        return $this->storeId;
    }

    /**
     * Check order availability
     *
     * @return bool
     *
     */
    public function canOrder()
    {
        return true;
    }

    /**
     * Check authorize availability
     *
     * @return bool
     *
     */
    public function canAuthorize()
    {
        return true;
    }

    /**
     * Check capture availability
     *
     * @return bool
     *
     */
    public function canCapture()
    {
        return true;
    }

    /**
     * Check partial capture availability
     *
     * @return bool
     *
     */
    public function canCapturePartial()
    {
        return true;
    }

    /**
     * Check whether capture can be performed once and no further capture possible
     *
     * @return bool
     *
     */
    public function canCaptureOnce()
    {
        return false;
    }

    /**
     * Check refund availability
     *
     * @return bool
     *
     */
    public function canRefund()
    {
        return true;
    }

    /**
     * Check partial refund availability for invoice
     *
     * @return bool
     *
     */
    public function canRefundPartialPerInvoice()
    {
        return true;
    }

    /**
     * Check void availability
     * @return bool
     *
     */
    public function canVoid()
    {
        return true;
    }

    /**
     * Using internal pages for input payment data
     * Can be used in admin
     *
     * @return bool
     */
    public function canUseInternal()
    {
        return true;
    }

    /**
     * Can be used in regular checkout
     *
     * @return bool
     */
    public function canUseCheckout()
    {
        return true;
    }

    /**
     * Can be edit order (renew order)
     *
     * @return bool
     *
     */
    public function canEdit()
    {
        return true;
    }

    /**
     * Check fetch transaction info availability
     *
     * @return bool
     *
     */
    public function canFetchTransactionInfo()
    {
        return true;
    }

    /**
     * Fetch transaction info
     *
     * @param InfoInterface $payment
     * @param string $transactionId
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     *
     */
    public function fetchTransactionInfo(InfoInterface $payment, $transactionId)
    {
        $response = $this->cimXml
                    ->fetchTransInfo($transactionId);
        $code = $response->messages->message->code;
        $resultCode = $response->messages->resultCode;
        if ($code == 'I00001' && $resultCode == 'Ok') {
            $transResponse = $response->transaction;
            if((int)$transResponse->responseCode == self::SUCCESS &&
                (string)$transResponse->transactionStatus != 'voided')
            {
               $payment->setData('parent_transaction_id', $transactionId);
               $transaction = $payment->getAuthorizationTransaction();
               $transaction->setAdditionalInformation('is_transaction_fraud', false);
               $payment->setIsTransactionApproved(true);
            }
            elseif ((int)$transResponse->responseReasonCode == 254
            || (string)$transResponse->transactionStatus == 'voided'
            ) {
                $payment->setIsTransactionDenied(true);
            }
        }

    }

    /**
     * Retrieve payment system relation flag
     *
     * @return bool
     *
     */
    public function isGateway()
    {
        return true;
    }

    /**
     * Retrieve payment method online/offline flag
     *
     * @return bool
     *
     */
    public function isOffline()
    {
        return false;
    }

    /**
     * Flag if we need to run payment initialize while order place
     *
     * @return bool
     *
     */
    public function isInitializeNeeded()
    {
        return false;
    }

    /**
     * To check billing country is allowed for the payment method
     *
     * @param string $country
     * @return bool
     */
    public function canUseForCountry($country)
    {
         /*
        for specific country, the flag will set up as 1
        */
        if ($this->getConfigData('allowspecific') == 1) {
            $availableCountries = explode(',', $this->getConfigData('specificcountry'));
            if (!in_array($country, $availableCountries)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Retrieve block type for display method information
     *
     * @return string
     *
     * @deprecated
     */
    public function getInfoBlockType()
    {
        return $this->infoBlockType;
    }

   /**
    * @inheritdoc
    */
    public function getInfoInstance()
    {
        return $this->infoInstance;
    }

    /**
     * @inheritdoc
     */
    public function setInfoInstance(InfoInterface $info)
    {
        $this->infoInstance = $info;
    }

    /**
     * Order payment abstract method
     *
     * @param InfoInterface $payment
     * @param float $amount
     * @return $this
     *
     */
    // @codingStandardsIgnoreStart
    public function order(\Magento\Payment\Model\InfoInterface $payment, $amount)
    {
        if (!$this->canOrder()) {
            throw new \Magento\Framework\Exception\LocalizedException(__('The order action is not available.'));
        }
        return $this;
    }
   // @codingStandardsIgnoreEnd
    /**
     * Whether this method can accept or deny payment
     * @return bool
     *
     */
    public function canReviewPayment()
    {
        return $this->canReviewPayment;
    }

    /**
     * Attempt to accept a payment that us under review
     *
     * @param InfoInterface $payment
     * @return false
     * @throws \Magento\Framework\Exception\LocalizedException
     *
     */
    // @codingStandardsIgnoreStart
    public function acceptPayment(InfoInterface $payment)
    {
        if (!$this->canReviewPayment()) {
            throw new \Magento\Framework\Exception\LocalizedException(__('The payment '
                . 'review action is unavailable.'));
        }
        $transactionId = $payment->getLastTransId();
        $response = $this->cimXml
                    ->acceptPaymentTrans($transactionId);
        $code = $response->messages->message->code;
        $resultCode = $response->messages->resultCode;
        if ($code == 'I00001' && $resultCode == 'Ok') {
            $transResponse = $response->transactionResponse;
            if((int)$transResponse->responseCode == self::SUCCESS)
            {
               $payment->setData('parent_transaction_id', $transactionId);
               $transaction = $payment->getAuthorizationTransaction();
               $transaction->setAdditionalInformation('is_transaction_fraud', false);
               $payment->setIsTransactionApproved(true);
               return true;
            }
            else
            {
                  throw new \Magento\Framework\Exception\LocalizedException(__('The payment '
                . 'review action is unavailable.'));

            }
        }
        else
        {
             throw new \Magento\Framework\Exception\LocalizedException(__('The payment '
                . 'review action is unavailable.'));
        }
        return false;
    }
    // @codingStandardsIgnoreEnd
    /**
     * Attempt to deny a payment that us under review
     *
     * @param InfoInterface $payment
     * @return false
     * @throws \Magento\Framework\Exception\LocalizedException
     *
     */
      // @codingStandardsIgnoreStart
    public function denyPayment(InfoInterface $payment)
    {
        if (!$this->canReviewPayment()) {
            throw new \Magento\Framework\Exception\LocalizedException(__('The payment '
                . 'review action is unavailable.'));
        }
        $transactionId = $payment->getLastTransId();
        $response = $this->cimXml
                    ->denyPaymentTrans($transactionId);
        $code = $response->messages->message->code;
        $resultCode = $response->messages->resultCode;
        if ($code == 'I00001' && $resultCode == 'Ok') {
            $transResponse = $response->transactionResponse;
            if((int)$transResponse->responseCode == self::SUCCESS)
            {
               $payment->setIsTransactionDenied(true);
               return true;
            }
            else
            {
                  throw new \Magento\Framework\Exception\LocalizedException(__('The payment '
                . 'review action is unavailable.'));
            }
        }
        else
        {
             throw new \Magento\Framework\Exception\LocalizedException(__('The payment '
                . 'review action is unavailable.'));
        }
        return false;
    }
  // @codingStandardsIgnoreEnd
    /**
     * Retrieve information from payment configuration
     *
     * @param string $field
     * @param int|string|null|\Magento\Store\Model\Store $storeId
     *
     * @return mixed
     */
    public function getConfigData($field, $storeId = null)
    {
        if (null === $storeId) {
            $storeId = $this->getStore();
        }
        $path = 'payment/' . $this->getCode() . '/' . $field;
        return $this->scopeConfig->getValue($path, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
    }

    /**
     * Is active
     *
     * @param int|null $storeId
     * @return bool
     *
     */
    public function isActive($storeId = null)
    {
        return (bool)(int)$this->getConfigData('active', $storeId);
    }

    /**
     * Method that will be executed instead of authorize or capture
     * if flag isInitializeNeeded set to true
     *
     * @param string $paymentAction
     * @param object $stateObject
     *
     * @return $this
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     *
     */
    public function initialize($paymentAction, $stateObject)
    {
        return $this;
    }

    /**
     * Get config payment action url
     * Used to universalize payment actions when processing payment place
     *
     * @return string
     *
     */
    public function getConfigPaymentAction()
    {
        return $this->getConfigData('payment_action');
    }

    public function setStore($id)
    {
        $this->storeId = $id;

        return $this;
    }

    public function setCustomer($customer)
    {
        $this->customer = $customer;
        if ($customer->getStoreId() > 0) {
            $this->setStore($customer->getStoreId());
        }

        return $this;
    }

    public function getCustomer()
    {
        if (isset($this->customer)) {
            $customer = $this->customer;
        } elseif ($this->backend) {
            $customer = $this->customerRepository->getById($this->sessionQuote->getCustomerId());
        } else {
            $customer = $this->customerSession->getCustomer();
        }
        $this->setCustomer($customer);
        return $customer;
    }

    public function getPaymentCardInfo($paymentProfileId = null)
    {
        $card = null;
        if (!($paymentProfileId === null)) {
            $cardModel = $this->cardFactory->create();
            $card      = $cardModel->getCollection()
                ->addFieldToFilter('payment_profile_id', $paymentProfileId)
                ->getData();
        }

        return $card;
    }
   public function assignData(\Magento\Framework\DataObject $data)
   {
         $post = $data->getData()['additional_data'];
         if(!isset($post["save_card"]))
         {
             $post["save_card"] = false;
         }
         if (empty($this->postData)) {
            $this->postData = $post;
         }
        $this->registry->register('postdata', $this->postData);
        $paymentProfileId = (isset($post['payment_profile_id'])) ? $post['payment_profile_id'] : 'new';
        if ($paymentProfileId != 'new') {
            $paymentProfileId = $this->encryptor->decrypt($post['payment_profile_id']);
            $creditCard = $this->getPaymentCardInfo($paymentProfileId);
            if ($creditCard != '' && !empty($creditCard)) {
                $this->getInfoInstance()->setCcLast4($creditCard[0]['cc_last_4'])
                ->setCcType($creditCard[0]['cc_type'])
                ->setAdditionalInformation('md_payment_profile_id', $post['payment_profile_id']);
                /* code for clear previous data */
                $this->getInfoInstance()
                    ->setAdditionalInformation('data_value',"");
                $this->getInfoInstance()
                    ->setAdditionalInformation('data_descriptor',"");
                $this->getInfoInstance()
                    ->setAdditionalInformation('md_save_card',"");
                /* end code for clear previous data */

            }
             if (isset($post['cc_cid'])) {
                    $this->getInfoInstance()->setCcCid($post['cc_cid']);
            }
            unset($this->postData['cc_type']);
            unset($this->postData['cc_number']);
            unset($this->postData['expiration']);
            unset($this->postData['expiration_yr']);
            $this->registry->unregister('postdata');
            $this->registry->register('postdata', $this->postData);
        } else {
            if(isset($post['data_value']) && $post['data_value']!='')
            {
                $this->getInfoInstance()
                    ->setAdditionalInformation('data_value',$post["data_value"]);
                $this->getInfoInstance()
                    ->setAdditionalInformation('data_descriptor',$post["data_descriptor"]);
                $this->getInfoInstance()
                    ->setAdditionalInformation('md_save_card',$post["save_card"]);
                $this->getInfoInstance()
                    ->setAdditionalInformation('md_payment_profile_id',"");
                $this->checkoutsession->setSaveCardFlag($post['save_card']);
                $this->checkoutsession->setDataValue($post['data_value']);
            }
            else
            {
                $this->getInfoInstance()->setCcType($post['cc_type'])
                    ->setCcLast4(substr($post['cc_number'], -4))
                    ->setCcNumber($post['cc_number'])
                    ->setCcCid(isset($post['cc_cid']) ? $post['cc_cid'] : null)
                    ->setCcExpMonth($post['expiration'])
                    ->setCcExpYear($post['expiration_yr'])
                    ->setAdditionalInformation('md_save_card', $post['save_card']);
                 $this->getInfoInstance()
                    ->setAdditionalInformation('md_payment_profile_id',"");
                $this->checkoutsession->setSaveCardFlag($post['save_card']);
                $this->checkoutsession->setCardccnumber($post['cc_number']);
                $cc_cid = isset($post['cc_cid']) ? $post['cc_cid'] : '';
                $this->checkoutsession->setCcCid($cc_cid);
            }
        }
        return $this;
    }

    public function validate()
    {
        if (empty($this->postData)) {
            $this->postData = $this->registry->registry('postdata');
        }
        $post = $this->postData;

        if ($post['payment_profile_id'] == 'new' && !empty($post['cc_number']) && !isset($post['data_value'])) {
            try {
                $this->parentValidate();
                $info           = $this->getInfoInstance();
                $initerrorMsg   = false;
                $availableTypes = explode(',', $this->getConfigData('cctypes'));
                $ccNumberInfo   = $info->getCcNumber();
                $ccNumber       = preg_replace('/[\-\s]+/', '', $ccNumberInfo);
                $info->setCcNumber($ccNumber);
                $ccTypeinit     = '';
                $result         = $this->checkValidate(
                    $initerrorMsg,
                    $ccTypeinit,
                    $info,
                    $availableTypes,
                    $ccNumber
                );
                $errorMsgg      = $result["error_msg"];
                $errorMsg       = $this->prepareValidate(
                    $errorMsgg,
                    $info
                );
                if ($errorMsg) {
                    // @codingStandardsIgnoreStart
                    throw new LocalizedException(new Phrase($errorMsg));
                    // @codingStandardsIgnoreEnd
                }
                return $this;
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        } else {
            return true;
        }
    }

    public function checkValidate(
        $errorMsg,
        $ccType,
        $info,
        $availableTypes,
        $ccNumber
    ) {

        if (in_array($info->getCcType(), $availableTypes)) {
            if ($this->validateCcNum($ccNumber)) {
                $ccTypeRegExpList    = [
                    'SO' => '/(^(6334)[5-9](\d{11}$|\d{13,14}$))|(^(6767)(\d{12}$|\d{14,15}$))/',
                    'SM' => '/(^(5[0678])\d{11,18}$)|(^(6[^05])\d{11,18}$)|(^(601)[^1]\d{9,16}$)|(^(6011)\d{9,11}$)'.
                    '|(^(6011)\d{13,16}$)|(^(65)\d{11,13}$)|(^(65)\d{15,18}$)'.
                    '|(^(49030)[2-9](\d{10}$|\d{12,13}$))|(^(49033)[5-9](\d{10}$|\d{12,13}$))'.
                    '|(^(49110)[1-2](\d{10}$|\d{12,13}$))|(^(49117)[4-9](\d{10}$|\d{12,13}$))'.
                    '|(^(49118)[0-2](\d{10}$|\d{12,13}$))|(^(4936)(\d{12}$|\d{14,15}$))/',
                    'VI' => '/^4[0-9]{12}([0-9]{3})?$/',
                    'MC' => '/^(5[1-5][0-9]{2}|222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}$/',
                    'AE' => '/^3[47][0-9]{13}$/',
                    'DI' => '/^6(?:011|5[0-9]{2})[0-9]{12}$/',
                    'JCB' => '/^(30[0-5][0-9]{13}|3095[0-9]{12}|35(2[8-9][0-9]{12}|[3-8][0-9]{13})|36[0-9]{12}'.
                    '|3[8-9][0-9]{14}|6011(0[0-9]{11}|[2-4][0-9]{11}|74[0-9]{10}|7[7-9][0-9]{10}'.
                    '|8[6-9][0-9]{10}|9[0-9]{11})|62(2(12[6-9][0-9]{10}|1[3-9][0-9]{11}|[2-8][0-9]{12}'.
                    '|9[0-1][0-9]{11}|92[0-5][0-9]{10})|[4-6][0-9]{13}|8[2-8][0-9]{12})|6(4[4-9][0-9]{13}'.
                    '|5[0-9]{14}))$/',
                    'DC' => '/^3(?:0[0-5]|[68][0-9])[0-9]{11}$/',
                    'MAESTRO'=>
                    '/(^(5[0678])\d{11,18}$)|(^(6[^05])\d{11,18}$)|(^(601)[^1]\d{9,16}$)|(^(6011)\d{9,11}$)'.
                    '|(^(6011)\d{13,16}$)|(^(65)\d{11,13}$)|(^(65)\d{15,18}$)'.
                    '|(^(49030)[2-9](\d{10}$|\d{12,13}$))|(^(49033)[5-9](\d{10}$|\d{12,13}$))'.
                    '|(^(49110)[1-2](\d{10}$|\d{12,13}$))|(^(49117)[4-9](\d{10}$|\d{12,13}$))'.
                    '|(^(49118)[0-2](\d{10}$|\d{12,13}$))|(^(4936)(\d{12}$|\d{14,15}$))/',
                    'SWITCH' =>
                    '/(^(5[0678])\d{11,18}$)|(^(6[^05])\d{11,18}$)|(^(601)[^1]\d{9,16}$)|(^(6011)\d{9,11}$)'.
                    '|(^(6011)\d{13,16}$)|(^(65)\d{11,13}$)|(^(65)\d{15,18}$)'.
                    '|(^(49030)[2-9](\d{10}$|\d{12,13}$))|(^(49033)[5-9](\d{10}$|\d{12,13}$))'.
                    '|(^(49110)[1-2](\d{10}$|\d{12,13}$))|(^(49117)[4-9](\d{10}$|\d{12,13}$))'.
                    '|(^(49118)[0-2](\d{10}$|\d{12,13}$))|(^(4936)(\d{12}$|\d{14,15}$))/',
                ];
                $ccNumAndTypeMatches = isset(
                    $ccTypeRegExpList[$info->getCcType()]
                ) && preg_match(
                    $ccTypeRegExpList[$info->getCcType()],
                    $ccNumber
                );
                $ccType              = $ccNumAndTypeMatches ? $info->getCcType()
                        : 'OT';

                if (!$ccNumAndTypeMatches && !$this->otherCcType($info->getCcType())) {
                    $errorMsg = __('The credit card number doesn\'t match the credit card type.');
                }
            } else {
                $errorMsg = __('Invalid Credit Card Number');
            }
        } else {
            $errorMsg = __('This credit card type is not allowed for this payment method.');
        }
        $result["error_msg"] = $errorMsg;
        $result["cc_type"]   = $ccType;
        return $result;
    }

     /**
     * @param string $type
     * @return bool
     * @api
     */
    public function otherCcType($type)
    {
        return $type == 'OT';
    }
    /**
     * Validate credit card number
     *
     * @param   string $ccNumber
     * @return  bool
     * @api
     */
    public function validateCcNum($ccNumber)
    {
        $cardNumber = strrev($ccNumber);
        $numSum = 0;
        $line_length = strlen($cardNumber);
        for ($i = 0; $i < $line_length; $i++) {
            $currentNum = substr($cardNumber, $i, 1);

            /**
             * Double every second digit
             */
            if ($i % 2 == 1) {
                $currentNum *= 2;
            }

            /**
             * Add digits of 2-digit numbers together
             */
            if ($currentNum > 9) {
                $firstNum = $currentNum % 10;
                $secondNum = ($currentNum - $firstNum) / 10;
                $currentNum = $firstNum + $secondNum;
            }

            $numSum += $currentNum;
        }

        /**
         * If the total has no remainder it's OK
         */
        return $numSum % 10 == 0;
    }
     /**
      * @return bool
      * @api
      */
    public function hasVerification()
    {
        $configData = $this->getConfigData('useccv');
        if ($configData === null) {
            return true;
        }
        return (bool)$configData;
    }
    public function prepareValidate($errorMsg, $info)
    {
        if ($errorMsg === false && $this->hasVerification()) {
            $verifcationRegEx = $this->getVerificationRegEx();
            $regExp           = isset($verifcationRegEx[$info->getCcType()]) ? $verifcationRegEx[$info->getCcType()]
                    : '';
            if (!$info->getCcCid() || !$regExp || !preg_match(
                $regExp,
                $info->getCcCid()
            )) {
                $errorMsg = __('Please enter a valid credit card verification number.');
            }
        }

        return $errorMsg;
    }
    public function getVerificationRegEx()
    {
        $verificationExpList = [
            'VI' => '/^[0-9]{3}$/',
            'MC' => '/^[0-9]{3}$/',
            'AE' => '/^[0-9]{4}$/',
            'DI' => '/^[0-9]{3}$/',
            'SS' => '/^[0-9]{3,4}$/',
            'SM' => '/^[0-9]{3,4}$/',
            'SO' => '/^[0-9]{3,4}$/',
            'OT' => '/^[0-9]{3,4}$/',
            'JCB' => '/^[0-9]{3,4}$/',
            'MAESTRO' => '/^[0-9]{3}$/',
            'SWITCH' => '/^[0-9]{3}$/',
            'DC' => '/^[0-9]{3}$/',
        ];

        return $verificationExpList;
    }

    public function parentValidate()
    {
        $paymentInfo = $this->getInfoInstance();
        if ($paymentInfo instanceof \Magento\Sales\Model\Order\Payment) {
            $billingCountry = $paymentInfo->getOrder()->getBillingAddress()->getCountryId();
        } else {
            $billingCountry = $paymentInfo->getQuote()->getBillingAddress()->getCountryId();
        }
        if (!$this->canUseForCountry($billingCountry)) {
            // @codingStandardsIgnoreStart
            throw new LocalizedException(
                new Phrase(__('You can\'t use the payment type '
                    . 'you selected to make payments to the billing country.'))
            );
             // @codingStandardsIgnoreEnd
        }

        return $this;
    }
    public function getPaymentProfileCardInfo($paymentProfileCheck = null)
    {
        $card = null;
        if (!($paymentProfileCheck === null)) {
            $cardModel = $this->cardFactory->create();
            $card      = $cardModel->getCollection()
                        ->addFieldToFilter('payment_profile_id', $paymentProfileCheck)
                        ->getData();
        }
        return $card;
    }
    public function saveCustomerProfileData($profileResponse, $payment, $customerid = null)
    {
        $websiteId = $this->cimHelper->getWebsiteId();
        
        $customerPaymenprofileId = $profileResponse->customerPaymentProfileIdList->numericString;
        $customerProfileId       = $profileResponse->customerProfileId;
        if (empty($customerid)) {
            $customerid = $payment->getOrder()->getCustomerId();
        }
        $requestObject = new \Magento\Framework\DataObject();
        $requestObject->addData(array(
               'customer_profile_id' => $customerProfileId,
               'payment_profile_id' => $customerPaymenprofileId,
        ));
       $paymentdetailResponse  = $this->cimXml->setInputData($requestObject)
                                 ->getCustomerPaymentProfile();
        $code = (string)$paymentdetailResponse->messages->message->code;
        $resultCode =  (string)$paymentdetailResponse->messages->resultCode;
        if ($code == 'I00001' && $resultCode == 'Ok') {
            $paymentdetail = $paymentdetailResponse->paymentProfile->payment;
            $ccType = (isset($this->cardArray[(string)$paymentdetail->creditCard->cardType]))? $this->cardArray[(string)$paymentdetail->creditCard->cardType] : (string)$paymentdetail->creditCard->cardType;
            $ccExpMonth = '';
            $ccExpYear = '';
            $ccLast4 = substr((string)$paymentdetail->creditCard->cardNumber, -4, 4);
        }
        else
        {
            throw new LocalizedException(new Phrase(__('Unable to save customer profile')));
        }
        if (!empty($customerPaymenprofileId) && $customerid) {
            $billing = $payment->getOrder()->getBillingAddress();
            $streetAddr = (is_array($billing->getStreet()))
                    ?implode(",", $billing->getStreet()):$billing->getStreet();
            try {
                 /* code for save customer profile id */
                 $readAdapter = $this->dbResource->getConnection('core_read');
                 $writeAdapter = $this->dbResource->getConnection('core_write');

                 $query1 = "SELECT `attribute_id` FROM `{$this->dbResource->getTableName('eav_attribute')}` "
                . "WHERE `attribute_code`='md_customer_profile_id' LIMIT 1";

                 $eavAttributeId = $readAdapter->fetchOne($query1);

                 $query2 = "SELECT `value_id` FROM `{$this->dbResource->getTableName('customer_entity_varchar')}` "
                . "WHERE `entity_id`='{$customerid}' AND `attribute_id`='{$eavAttributeId}'";
                 $valueId = (int) $readAdapter->fetchOne($query2);

                 if ($valueId <= 0) {
                            $Query = "INSERT INTO `{$this->dbResource
                                ->getTableName('customer_entity_varchar')}` "
                                . "(attribute_id,entity_id,value) VALUES('{$eavAttributeId}','{$customerid}',"
                                . "'{$customerProfileId}')";
                    } else {
                        $Query = "UPDATE `{$this->dbResource
                            ->getTableName('customer_entity_varchar')}` SET `value`='{$customerProfileId}' "
                            . "WHERE `value_id`='{$valueId}' AND `entity_id`='{$customerid}'";
                 }
                 $writeAdapter->query($Query);
                /* end code for save customer profile id */

             $model = $this->cardFactory->create();
             $cardColl = $model->getCollection()
                      ->addFieldToFilter('customer_profile_id',$customerProfileId)
                      ->addFieldToFilter('payment_profile_id',$customerPaymenprofileId);
              if($cardColl->getSize()==0)
              {
                   $model->setFirstname($billing->getFirstname())
                        ->setLastname($billing->getLastname())
                        ->setPostcode($billing->getPostcode())
                        ->setCountryId($billing->getCountryId())
                        ->setRegionId($billing->getRegionId())
                        ->setState($billing->getRegion())
                        ->setCity($billing->getCity())
                        ->setCompany($billing->getCompany())
                        ->setStreet($streetAddr)
                        ->setTelephone($billing->getTelephone())
                        ->setCustomerId($customerid)
                        ->setCustomerProfileId($customerProfileId)
                        ->setPaymentProfileId($customerPaymenprofileId)
                        ->setccType($ccType)
                        ->setcc_exp_month($ccExpMonth)
                        ->setcc_exp_year($ccExpYear)
                        ->setcc_last4($ccLast4)
                        ->setWebsiteId($websiteId)
                        ->setCreatedAt($this->date->gmtDate())
                        ->setUpdatedAt($this->date->gmtDate());
                 $model->getResource()->save($model);
              }
               
                return;
            } catch (\Exception $e) {
                throw new \Magento\Framework\Exception\LocalizedException(new \Magento\Framework\Phrase(__('Unable to save customer profile due to: %1', $e->getMessage())));
            }
        }
    }
    public function prepareProfileResponse($payment,$customerProfileId,$transId)
    {
        $websiteId = $this->cimHelper->getWebsiteId();
        /* code for create payment profile from transaction */
        $response      = $this->cimXml
            ->createPaymentProfileFromTransaction($customerProfileId,$transId);
       $code = $response->messages->message->code;
       $resultCode = $response->messages->resultCode;
       $successflag = true;
       if ($code == 'I00001' && $resultCode == 'Ok') {
               $paymentProfileId = (string) $response->customerPaymentProfileIdList->numericString;
       }
       elseif ($code == 'E00039' && strpos($response->messages->message->text, 'duplicate') !== false) {
            $paymentProfileId = (string) $response->customerPaymentProfileId;
        }
       else
       {
          /* code for error log */
           $successflag = false;
       }
          if($successflag)
          {
              $requestObject = new \Magento\Framework\DataObject();
              $requestObject->addData(array(
                    'customer_profile_id' => $customerProfileId,
                    'payment_profile_id' => $paymentProfileId,
             ));
              $paymentdetailResponse  = $this->cimXml->setInputData($requestObject)
                                 ->getCustomerPaymentProfile();
              $code = (string)$paymentdetailResponse->messages->message->code;
              $resultCode =  (string)$paymentdetailResponse->messages->resultCode;
              if ($code == 'I00001' && $resultCode == 'Ok') {
                 $paymentdetail = $paymentdetailResponse->paymentProfile->payment;
                 $ccType = (isset($this->cardArray[(string)$paymentdetail->creditCard->cardType]))? $this->cardArray[(string)$paymentdetail->creditCard->cardType] : (string)$paymentdetail->creditCard->cardType;
                 $ccExpMonth = '';
                 $ccExpYear = '';
                 $ccLast4 = substr((string)$paymentdetail->creditCard->cardNumber, -4, 4);
              }
              else
              {
                throw new LocalizedException(new Phrase(__('Unable to save customer profile')));
              }

              $billing = $payment->getOrder()->getBillingAddress();
              $streetAddr = (is_array($billing->getStreet()))
                        ?implode(",", $billing->getStreet()):$billing->getStreet();

              $cardModelObject = $this->cardFactory->create();
              $cardColl = $cardModelObject->getCollection()
                          ->addFieldToFilter('customer_profile_id',$customerProfileId)
                          ->addFieldToFilter('payment_profile_id',$paymentProfileId);
              if($cardColl->getSize()==0)
              {
                  $cardModelObject->setFirstname($billing->getFirstname())
                            ->setLastname($billing->getLastname())
                            ->setPostcode($billing->getPostcode())
                            ->setCountryId($billing->getCountryId())
                            ->setRegionId($billing->getRegionId())
                            ->setState($billing->getRegion())
                            ->setCity($billing->getCity())
                            ->setCompany($billing->getCompany())
                            ->setStreet($streetAddr)
                            ->setTelephone($billing->getTelephone())
                            ->setCustomerId($payment->getOrder()->getCustomerId())
                            ->setCustomerProfileId($customerProfileId)
                            ->setPaymentProfileId($paymentProfileId)
                            ->setccType($ccType)
                            ->setcc_exp_month($ccExpMonth)
                            ->setcc_exp_year($ccExpYear)
                            ->setcc_last4($ccLast4)
                            ->setWebsiteId($websiteId)
                            ->setCreatedAt($this->date->gmtDate())
                            ->setUpdatedAt($this->date->gmtDate());
                $cardModelObject->getResource()->save($cardModelObject);
              }
            return $requestObject;
          }
    }
    public function authorize(
        \Magento\Payment\Model\InfoInterface $payment,
        $amount
    ) {
       $exceptionMessage = false;
        if ($amount <= 0) {
            $payment->unsAdditionalInformation();
            throw new LocalizedException(new Phrase(__('Invalid amount for authorization.')));
        }
        $this->_initCardsStorage($payment);
        if (empty($this->postData)) {
            $this->postData = $this->registry->registry('postdata');
        }
        $post = $this->postData;
        $saveCard = $post['save_card'];
        try {
                $isMultiShipping = $this->checkoutsession->getQuote()->getData('is_multi_shipping');
                $paymentProfileCheck = $payment->getData('additional_information', 'md_payment_profile_id');
                $saveFlag = 'false';
                if ((!empty($paymentProfileCheck) && empty($post['cc_number'])) || ($isMultiShipping == '1'
                    && !empty($paymentProfileCheck))) {
                $paymentProfileCheck = $this->encryptor->decrypt($paymentProfileCheck);
                $payment->setMdPaymentProfileId($paymentProfileCheck);
                $cardcollection = $this->cardCollectionFactory->create()
                    ->addFieldToFilter('payment_profile_id',$paymentProfileCheck);
                if($cardcollection->getSize()>0)
                {
                     $customerProfile = $cardcollection->getFirstItem()->getCustomerProfileId();
                     $payment->setAdditionalInformation('md_payment_profile_id', $paymentProfileCheck);
                     $payment->setAdditionalInformation('md_customer_profile_id', $customerProfile);
                }
                else
                {
                    $payment->unsAdditionalInformation();
                     throw new LocalizedException(new Phrase('Card does not exists'));
                }

                $response = $this->cimXml
                    ->prepareAuthorizeResponse($payment, $amount,$saveFlag, true);
            } else {
                 if ((($saveCard == 'true' && isset($post['cc_number'])) && $post['cc_number'] != '' ||
                     ($saveCard == 'true' && isset($post['data_value'])) && $post['data_value'] != '')
                      && ($this->customerSession->getCustomerId() || ($this->cimHelper->checkAdmin()
                            && $this->objectManager->get('Magento\Backend\Model\Session\Quote')
                          ->getQuote()->getCustomerId()))) {
                     /* check customer profile id is exist or not */
                       $customerid = $payment->getOrder()->getCustomerId();
                       $customerModel = $this->customerFactory->create();
                       $customerModel->getResource()->load($customerModel, $customerid);
                       $customerProfileId = $customerModel->getMdCustomerProfileId();
                       if($customerProfileId=='')
                       {
                           /* save card will true only for first request by customer */
                            $saveFlag = 'true';
                       }

                }
                $response = $this->cimXml
                    ->prepareAuthorizeResponse($payment, $amount,$saveFlag, false);
            }

            $code = (string)$response->messages->message->code;
            $resultCode =  (string)$response->messages->resultCode;
            if ($code == 'I00001' && $resultCode == 'Ok') {
               $transResponse = $response->transactionResponse;
               if(!in_array((string)$transResponse->responseCode, [2, 3]))
               {
                if (!empty($paymentProfileCheck) && empty($post['cc_number'])) {
                    $card = $this->getPaymentProfileCardInfo($paymentProfileCheck);
                    $payment->setCcLast4($card[0]['cc_last_4']);
                    $payment->setCcType($card[0]['cc_type']);
                    $payment->setAdditionalInformation('md_payment_profile_id', $paymentProfileCheck);
                    $payment->setMdPaymentProfileId($paymentProfileCheck);
                } else {
                    $payment->setCcLast4(substr((string)$transResponse->accountNumber, -4, 4));
                    if(isset($this->cardArray[(string)$transResponse->accountType])):
                        $cardType = $this->cardArray[(string)$transResponse->accountType];
                    elseif(isset($post['cc_type'])):
                        $cardType = $post['cc_type'];
                    else:
                        $cardType = '';
                    endif;
                    $payment->setCcType($cardType);
                }
                $saveCard = $payment->getData('additional_information', 'md_save_card');
                if ((($saveCard == 'true' && isset($post['cc_number'])) && $post['cc_number'] != '' ||
                     ($saveCard == 'true' && isset($post['data_value'])) && $post['data_value'] != '')
                    && ($this->customerSession->getCustomerId()
                        || ($this->cimHelper->checkAdmin()
                        && $this->objectManager->get('Magento\Backend\Model\Session\Quote')
                        ->getQuote()->getCustomerId()))) {

                    if($saveFlag=='true')
                    {
                        /* save card from profile response */
                        $profileResponse = $response->profileResponse;
                        $resultCode =  (string)$profileResponse->messages->resultCode;
                        $code = (string)$profileResponse->messages->message->code;
                         if ($code == 'I00001' && $resultCode == 'Ok') {
                            $customerid = $this->cimHelper->checkAdmin() ?
                                $this->objectManager->get('Magento\Backend\Model\Session\Quote')
                                ->getQuote()->getCustomerId() : $this->customerSession->getCustomer()->getId();
                            if($customerid=='')
                            {
                                $customerid = $payment->getOrder()->getCustomerId();
                            }
                            $this->saveCustomerProfileData($profileResponse, $payment, $customerid);
                        }  elseif ($code == 'E00039' &&
                            strpos($profileResponse->messages->message->text, 'duplicate') !== false) {
                            $customerProfileId = preg_replace('/[^0-9]/', '',
                                $profileResponse->messages->message->text);
                            $readAdapter = $this->dbResource->getConnection('core_read');
                            $writeAdapter = $this->dbResource->getConnection('core_write');
                            $query1 = "SELECT `attribute_id` FROM `{$this->dbResource
                                ->getTableName('eav_attribute')}` "
                            . "WHERE `attribute_code`='md_customer_profile_id' LIMIT 1";

                            $eavAttributeId = $readAdapter->fetchOne($query1);

                            $query2 = "SELECT `value_id` FROM `{$this->dbResource
                                ->getTableName('customer_entity_varchar')}` "
                            . "WHERE `entity_id`='{$customerid}' AND `attribute_id`='{$eavAttributeId}'";
                             $valueId = (int) $readAdapter->fetchOne($query2);
                        if ($valueId <= 0) {
                            $Query = "INSERT INTO `{$this->dbResource
                                ->getTableName('customer_entity_varchar')}` "
                                . "(attribute_id,entity_id,value) VALUES('{$eavAttributeId}','{$customerid}',"
                                . "'{$customerProfileId}')";
                        } else {
                            $Query = "UPDATE `{$this->dbResource->getTableName('customer_entity_varchar')}` "
                            . "SET `value`='{$customerProfileId}' WHERE `value_id`='{$valueId}' "
                            . "AND `entity_id`='{$customerid}'";
                        }
                        $writeAdapter->query($Query);
                        $this->prepareProfileResponse($payment,$customerProfileId,(string)$transResponse->transId);

                    }  elseif ($code == 'E00103') {
                          $customerProfileRes = $this->cimXml->createCustPaymentProfileFromTransaction((string)
                              $transResponse->transId);
                           $code = $customerProfileRes->messages->message->code;
                           $resultCode = $response->messages->resultCode;
                           if ($code == 'I00001' && $resultCode == 'Ok') {
                               $customerProfileId = (string)$customerProfileRes->customerProfileId;
                               $readAdapter = $this->dbResource->getConnection('core_read');
                               $writeAdapter = $this->dbResource->getConnection('core_write');
                               $query1 = "SELECT `attribute_id` FROM `{$this->dbResource
                                ->getTableName('eav_attribute')}` "
                            . "WHERE `attribute_code`='md_customer_profile_id' LIMIT 1";
                               $eavAttributeId = $readAdapter->fetchOne($query1);
                               $query2 = "SELECT `value_id` FROM `{$this->dbResource
                                ->getTableName('customer_entity_varchar')}` "
                            . "WHERE `entity_id`='{$customerid}' AND `attribute_id`='{$eavAttributeId}'";
                             $valueId = (int) $readAdapter->fetchOne($query2);
                            if ($valueId <= 0) {
                                $Query = "INSERT INTO `{$this->dbResource
                                ->getTableName('customer_entity_varchar')}` "
                                . "(attribute_id,entity_id,value) VALUES('{$eavAttributeId}','{$customerid}',"
                                . "'{$customerProfileId}')";
                            } else {
                            $Query = "UPDATE `{$this->dbResource->getTableName('customer_entity_varchar')}` "
                            . "SET `value`='{$customerProfileId}' WHERE `value_id`='{$valueId}' "
                            . "AND `entity_id`='{$customerid}'";
                            }
                            $writeAdapter->query($Query);
                            $this->prepareProfileResponse($payment,$customerProfileId,
                              (string)$transResponse->transId);
                           }
                    }
                    else {
                            /* print log message */

                         }
                    }
                    else
                    {
                         /* save card from new request */
                          $this->prepareProfileResponse($payment,$customerProfileId,
                              (string)$transResponse->transId);
                    }
                }
                $cimToRequestMap = self::REQUEST_TYPE_AUTH_ONLY;
                $payment->setAnetTransType($cimToRequestMap);
                $payment->setAmount($amount);
                $newTransactionType = \Magento\Sales\Model\Order\Payment\Transaction::TYPE_AUTH;
                $transactionResponse = $response->transactionResponse;
                $transactionResponse->amount = $amount;
                $card = $this->_registerCard($transactionResponse, $payment);
                $card->setLastTransId((string)$transactionResponse->transId);
                $this->_addTransaction($payment,
                                        $card->getLastTransId(),
                                        $newTransactionType,
                                        array('is_transaction_closed' => 0),
                                        array($this->realTransactionIdKey => $card->getLastTransId()),
                                        $this->cimHelper->getTransactionMessage(
                                            $payment, $cimToRequestMap, (string)$transactionResponse->getTransId,
                                            $card, $amount
                                        )
                                    );

                $payment->setLastTransId((string)$transactionResponse->transId)
                    ->setCcTransId((string)$transactionResponse->transId)
                    ->setTransactionId((string)$transactionResponse->transId)
                    ->setIsTransactionClosed(0)
                    ->setCcAvsStatus((string)$transactionResponse->avsResultCode);
                    if((int)$transResponse->responseCode==self::REVIEW)
                    {
                          $payment->setIsTransactionPending(true)
                            ->setIsFraudDetected(true)
                            ->setTransactionAdditionalInfo('is_transaction_fraud', true);
                    }
                    else
                    {
                          $payment->setStatus(self::STATUS_APPROVED);
                    }

                    /*
                    * checking if we have cvCode in response bc
                    * if we don't send cvn we don't get cvCode in response
                    */
                    if (isset($transactionResponse->cvvResultCode)) {
                        $payment->setCcCidStatus($transactionResponse->cvvResultCode);
                    }
                }
                else
                {
                    $exceptionMessage = (string)$transResponse->errors->error->errorText;
                    $payment->unsAdditionalInformation();
                    throw new LocalizedException(new Phrase($exceptionMessage));
                }
          } else {
                   $exceptionMessage = $this->cimHelper->getErrorDescription($response->messages->message->code);
                   $payment->unsAdditionalInformation();
                   throw new LocalizedException(new Phrase($exceptionMessage));
            }
        } catch (\Exception $e) {
            throw new LocalizedException(new Phrase('Authorize.net Cim Gateway request error: '. $e->getMessage()));
        }
        $payment->setSkipTransactionCreation(true);
        return $this;
    }
     public function capture(
        \Magento\Payment\Model\InfoInterface $payment,
        $amount
    ) {
        if ($amount <= 0) {
            throw new \Magento\Framework\Exception\LocalizedException(new \Magento\Framework\Phrase(__('Invalid amount for capture.')));
        }
        $this->_initCardsStorage($payment);

        if (empty($this->_postData)) {
            $this->_postData = $this->registry->registry('postdata');
        }
        $post = $this->_postData;
        $saveCard = $post['save_card'];
        try {
            if ($this->_isPreauthorizeCapture($payment)) {
                $this->_preauthorizeCapture($payment, $amount);
            } else {

               $isMultiShipping = $this->checkoutsession->getQuote()->getData('is_multi_shipping');
               $paymentProfileCheck = $payment->getData('additional_information', 'md_payment_profile_id');
                $saveFlag = 'false';
                if ((!empty($paymentProfileCheck) && empty($post['cc_number'])) || ($isMultiShipping == '1'
                    && !empty($paymentProfileCheck))) {
                $paymentProfileCheck = $this->encryptor->decrypt($paymentProfileCheck);
                $payment->setMdPaymentProfileId($paymentProfileCheck);
                $cardcollection = $this->cardCollectionFactory->create()->addFieldToFilter('payment_profile_id',$paymentProfileCheck);
                if($cardcollection->getSize()>0)
                {
                     $customerProfileId = $cardcollection->getFirstItem()->getCustomerProfileId();
                     $payment->setAdditionalInformation('md_payment_profile_id', $paymentProfileCheck);
                     $payment->setAdditionalInformation('md_customer_profile_id', $customerProfileId);
                }
                else
                {
                    $payment->unsAdditionalInformation();
                     throw new LocalizedException(new Phrase('Card does not exists'));
                }
                $response = $this->cimXml
                    ->prepareCaptureResponse($payment, $amount,$saveFlag, true);
            } else {
                  if ((($saveCard == 'true' && isset($post['cc_number'])) && $post['cc_number'] != '' ||
                     ($saveCard == 'true' && isset($post['data_value'])) && $post['data_value'] != '')
                      && ($this->customerSession->getCustomerId() || ($this->cimHelper->checkAdmin()
                            && $this->objectManager->get('Magento\Backend\Model\Session\Quote')
                          ->getQuote()->getCustomerId()))) {
                     /* check customer profile id is exist or not */
                       $customerid = $payment->getOrder()->getCustomerId();
                       $customerModel = $this->customerFactory->create();
                       $customerModel->getResource()->load($customerModel, $customerid);
                       $customerProfileId = $customerModel->getMdCustomerProfileId();
                       if($customerProfileId=='')
                       {
                            $saveFlag = 'true';
                       }
                  }
                 $response = $this->cimXml
                    ->prepareCaptureResponse($payment, $amount,$saveFlag, false);
            }
            

            $code = $response->messages->message->code;
            $resultCode =  $response->messages->resultCode;
            if ($code == 'I00001' && $resultCode == 'Ok') {
                $transResponse = $response->transactionResponse;
                if(!in_array((string)$transResponse->responseCode, [2, 3]))
                {
                    if (!empty($paymentProfileCheck) && empty($post['cc_number'])) {
                        $card = $this->getPaymentProfileCardInfo($paymentProfileCheck);
                        $payment->setCcLast4($card[0]['cc_last_4']);
                        $payment->setCcType($card[0]['cc_type']);
                        $payment->setAdditionalInformation('md_payment_profile_id', $paymentProfileCheck);
                        $payment->setMdPaymentProfileId($paymentProfileCheck);
                    } else {
                        $payment->setCcLast4(substr((string)$transResponse->accountNumber, -4, 4));
                        if(isset($this->cardArray[(string)$transResponse->accountType])):
                            $cardType = $this->cardArray[(string)$transResponse->accountType];
                        elseif(isset($post['cc_type'])):
                            $cardType = $post['cc_type'];
                        else:
                            $cardType = '';
                        endif;
                        $payment->setCcType($cardType);
                    }
                        $saveCard = $payment->getData('additional_information', 'md_save_card');
                        if ((($saveCard == 'true' && isset($post['cc_number'])) && $post['cc_number'] != '' ||
                     ($saveCard == 'true' && isset($post['data_value'])) && $post['data_value'] != '')
                    && ($this->customerSession->getCustomerId()
                        || ($this->cimHelper->checkAdmin()
                        && $this->objectManager->get('Magento\Backend\Model\Session\Quote')
                        ->getQuote()->getCustomerId()))) {
                        if($saveFlag=='true')
                        {
                            /* save card from profile response */
                            $profileResponse = $response->profileResponse;
                            $resultCode =  (string)$profileResponse->messages->resultCode;
                            $code = (string)$profileResponse->messages->message->code;
                             if ($code == 'I00001' && $resultCode == 'Ok') {
                                $customerid = $this->cimHelper->checkAdmin() ?
                                    $this->objectManager->get('Magento\Backend\Model\Session\Quote')
                                    ->getQuote()->getCustomerId() : $this->customerSession->getCustomer()->getId();
                                if($customerid=='')
                                {
                                    $customerid = $payment->getOrder()->getCustomerId();
                                }
                                $this->saveCustomerProfileData($profileResponse, $payment, $customerid);
                            }  elseif ($code == 'E00039' &&
                                strpos($profileResponse->messages->message->text, 'duplicate') !== false) {
                                $customerProfileId = preg_replace('/[^0-9]/', '',
                                    $profileResponse->messages->message->text);
                                $readAdapter = $this->dbResource->getConnection('core_read');
                                $writeAdapter = $this->dbResource->getConnection('core_write');
                                $query1 = "SELECT `attribute_id` FROM `{$this->dbResource
                                    ->getTableName('eav_attribute')}` "
                                . "WHERE `attribute_code`='md_customer_profile_id' LIMIT 1";

                                $eavAttributeId = $readAdapter->fetchOne($query1);

                                $query2 = "SELECT `value_id` FROM `{$this->dbResource
                                    ->getTableName('customer_entity_varchar')}` "
                                . "WHERE `entity_id`='{$customerid}' AND `attribute_id`='{$eavAttributeId}'";
                                 $valueId = (int) $readAdapter->fetchOne($query2);
                            if ($valueId <= 0) {
                                $Query = "INSERT INTO `{$this->dbResource
                                    ->getTableName('customer_entity_varchar')}` "
                                    . "(attribute_id,entity_id,value) VALUES('{$eavAttributeId}','{$customerid}',"
                                    . "'{$customerProfileId}')";
                            } else {
                                $Query = "UPDATE `{$this->dbResource->getTableName('customer_entity_varchar')}` "
                                . "SET `value`='{$customerProfileId}' WHERE `value_id`='{$valueId}' "
                                . "AND `entity_id`='{$customerid}'";
                            }
                            $writeAdapter->query($Query);
                            $this->prepareProfileResponse($payment,$customerProfileId,(string)$transResponse->transId);

                            }elseif ($code == 'E00103') {
                              $customerProfileRes = $this->cimXml->createCustPaymentProfileFromTransaction((string)
                                  $transResponse->transId);
                               $code = $customerProfileRes->messages->message->code;
                               $resultCode = $response->messages->resultCode;
                               if ($code == 'I00001' && $resultCode == 'Ok') {
                                   $customerProfileId = (string)$customerProfileRes->customerProfileId;
                                   $readAdapter = $this->dbResource->getConnection('core_read');
                                   $writeAdapter = $this->dbResource->getConnection('core_write');
                                   $query1 = "SELECT `attribute_id` FROM `{$this->dbResource
                                    ->getTableName('eav_attribute')}` "
                                . "WHERE `attribute_code`='md_customer_profile_id' LIMIT 1";
                                   $eavAttributeId = $readAdapter->fetchOne($query1);
                                   $query2 = "SELECT `value_id` FROM `{$this->dbResource
                                    ->getTableName('customer_entity_varchar')}` "
                                . "WHERE `entity_id`='{$customerid}' AND `attribute_id`='{$eavAttributeId}'";
                                 $valueId = (int) $readAdapter->fetchOne($query2);
                                if ($valueId <= 0) {
                                    $Query = "INSERT INTO `{$this->dbResource
                                    ->getTableName('customer_entity_varchar')}` "
                                    . "(attribute_id,entity_id,value) VALUES('{$eavAttributeId}','{$customerid}',"
                                    . "'{$customerProfileId}')";
                                } else {
                                $Query = "UPDATE `{$this->dbResource->getTableName('customer_entity_varchar')}` "
                                . "SET `value`='{$customerProfileId}' WHERE `value_id`='{$valueId}' "
                                . "AND `entity_id`='{$customerid}'";
                                }
                                $writeAdapter->query($Query);
                                $this->prepareProfileResponse($payment,$customerProfileId,
                                  (string)$transResponse->transId);
                               }
                        }
                            else {
                                    /* print log message */
                                 }
                            }
                            else
                            {
                                 /* save card from new request */
                                  $this->prepareProfileResponse($payment,$customerProfileId,
                                    (string)$transResponse->transId);
                            }

                        }

                        $transactionResponse = $response->transactionResponse;
                        $transactionResponse->amount = $amount;
                        $card = $this->_registerCard($transactionResponse, $payment);
                        $cimToRequestMap = self::REQUEST_TYPE_AUTH_CAPTURE;
                        $payment->setAnetTransType($cimToRequestMap);
                        $newTransactionType = \Magento\Sales\Model\Order\Payment\Transaction::TYPE_CAPTURE;
                        $card->setLastTransId((string)$transactionResponse->transId);
                        $this->_addTransaction(
                                    $payment,
                                    $card->getLastTransId(),
                                    $newTransactionType,
                                    array('is_transaction_closed' => 0),
                                    array($this->_realTransactionIdKey => $card->getLastTransId()),
                                    $this->cimHelper->getTransactionMessage(
                                        $payment, $cimToRequestMap, (string)$transactionResponse->transId, $card, $amount
                                    )
                                );


                        $card->setCapturedAmount($card->getProcessedAmount());
                        $captureTransactionId = (string)$transactionResponse->transId;
                        $card->setLastCapturedTransactionId($captureTransactionId);
                        $this->getCardsStorage()->updateCard($card);

                        $payment->setLastTransId((string)$transactionResponse->transId)
                            ->setCcTransId((string)$transactionResponse->transId)
                            ->setTransactionId((string)$transactionResponse->transId)
                            ->setIsTransactionClosed(0)
                            ->setCcAvsStatus((string)$transactionResponse->avsResultCode);
                        if((int)$transResponse->responseCode==self::REVIEW)
                        {
                              $payment->setIsTransactionPending(true)
                                ->setIsFraudDetected(true)
                                ->setTransactionAdditionalInfo('is_transaction_fraud', true);
                        }
                        else
                        {
                              $payment->setStatus(self::STATUS_APPROVED);
                        }
                        if (isset($transactionResponse->cvvResultCode)) {
                            $payment->setCcCidStatus($transactionResponse->cvvResultCode);
                        }
                    }
                    else
                    {
                        $exceptionMessage = (string)$transResponse->errors->error->errorText;
                        $payment->unsAdditionalInformation();
                        throw new LocalizedException(new Phrase($exceptionMessage));
                    }
                } else {
                    $transResponse = $response->transactionResponse;
                    $payment->unsAdditionalInformation();
                    $exceptionMessage = $this->cimHelper->getErrorDescription($response->messages->message->code);
                    throw new LocalizedException(new Phrase($exceptionMessage));
                }
          }
        } catch (\Exception $e) {
            $payment->unsAdditionalInformation();
            throw new LocalizedException(new Phrase(__('Gateway request error:'. $e->getMessage())));
        }
        $payment->setSkipTransactionCreation(true);
        return $this;
    }
     public function refund(
        \Magento\Payment\Model\InfoInterface $payment,
        $amount
    ) {
       $cardsStorage = $this->getCardsStorage($payment);
        if ($this->_formatAmount(
                $cardsStorage->getCapturedAmount() - $cardsStorage->getRefundedAmount()
                ) < $amount
            ) {
            throw new \Magento\Framework\Exception\LocalizedException(new Phrase(__('Invalid amount for refund.')));
        }
        $messages = array();
        $isSuccessful = false;
        $isFiled = false;
            // Grab the invoice in case partial invoicing
            $creditmemo = $this->registry->registry('current_creditmemo');
        if (!is_null($creditmemo)) {
            $this->_invoice = $creditmemo->getInvoice();
        }
        foreach ($cardsStorage->getCards() as $card) {
            $lastTransactionId = $payment->getData('cc_trans_id');
            $cardTransactionId = $card->getTransactionId();

            if ($lastTransactionId == $cardTransactionId) {
                if ($amount > 0) {
                    $cardAmountForRefund = $this->_formatAmount($card->getCapturedAmount() - $card->getRefundedAmount());
                    if ($cardAmountForRefund <= 0) {
                        continue;
                    }
                    if ($cardAmountForRefund > $amount) {
                        $cardAmountForRefund = $amount;
                    }
                    try {
                        $newTransaction = $this->_refundCardTransaction($payment, $cardAmountForRefund, $card);
                        if ($newTransaction != null) {
                            $messages[] = $newTransaction->getMessage();
                            $isSuccessful = true;
                        }
                    } catch (\Exception $e) {
                        $messages[] = $e->getMessage();
                        $isFiled = true;
                        continue;
                    }
                    $card->setRefundedAmount($this->_formatAmount($card->getRefundedAmount() + $cardAmountForRefund));
                    $cardsStorage->updateCard($card);
                    $amount = $this->_formatAmount($amount - $cardAmountForRefund);
                } else {
                    $payment->setSkipTransactionCreation(true);

                    return $this;
                }
            }
        }

        if ($isFiled) {
            $this->_processFailureMultitransactionAction($payment, $messages, $isSuccessful);
        }

        $payment->setSkipTransactionCreation(true);

        return $this;
    }

    public function void(\Magento\Payment\Model\InfoInterface $payment)
    {
        $cardsStorage = $this->getCardsStorage($payment);
        $messages = array();
        $isSuccessful = false;
        $isFiled = false;
        foreach ($cardsStorage->getCards() as $card) {
            $lastTransactionId = $payment->getData('cc_trans_id');
            $cardTransactionId = $card->getTransactionId();
            if ($lastTransactionId == $cardTransactionId) {
                try {
                    $newTransaction = $this->_voidCardTransaction($payment, $card);
                    if ($newTransaction != null) {
                        $messages[] = $newTransaction->getMessage();
                        $isSuccessful = true;
                    }
                } catch (\Exception $e) {
                    $messages[] = $e->getMessage();
                    $isFiled = true;
                    continue;
                }
                $cardsStorage->updateCard($card);
            }
        }
        if ($isFiled) {
            $this->_processFailureMultitransactionAction($payment, $messages, $isSuccessful);
        }

        $payment->setSkipTransactionCreation(true);

        return $this;
    }
    protected function _voidCardTransaction($payment, $card)
    {
        $authTransactionId = $card->getLastTransId();
        if ($payment->getCcTransId()) {
            $realAuthTransactionId = $payment->getTransactionId();
            $payment->setAnetTransType(self::REQUEST_TYPE_VOID);
            $payment->setTransId($realAuthTransactionId);
            $transactionId = str_replace(['-capture','-void','-refund'], '', $realAuthTransactionId);
            $response = $this->cimXml
                ->prepareVoidResponse($payment,$transactionId);
            $code = $response->messages->message->code;
            $resultCode =  $response->messages->resultCode;
            if ($code == 'I00001' && $resultCode == 'Ok') {
                $transactionResponse = $response->transactionResponse;
                $voidTransactionId = $realAuthTransactionId.'-void';
                $card->setLastTransId($voidTransactionId);
                $payment->setTransactionId((string)$transactionResponse->transId)
                    ->setIsTransactionClosed(1);

                $this->_addTransaction(
                                $payment,
                                $voidTransactionId,
                                \Magento\Sales\Model\Order\Payment\Transaction::TYPE_VOID,
                                array(
                                    'is_transaction_closed' => 1,
                                    'should_close_parent_transaction' => 1,
                                    'parent_transaction_id' => $authTransactionId,
                                ),
                                array($this->_realTransactionIdKey => (string)$transactionResponse->transId),

                            $this->cimHelper->getTransactionMessage(
                                    $payment, self::REQUEST_TYPE_VOID, (string)$transactionResponse->transId, $card
                                )
                            );
            } else {
                 $exceptionMessage = $this->cimHelper
                                    ->getErrorDescription($response->messages->message->code);
                // @codingStandardsIgnoreStart
                throw new LocalizedException(new Phrase($exceptionMessage));
                // @codingStandardsIgnoreEnd
            }
        } else {
            return;
        }
    }
    public function cancel(\Magento\Payment\Model\InfoInterface $payment)
    {
        return $this->void($payment);
    }

    /**
     * Payment method available? Yes.
     */
    public function getConfigModel()
    {
        return $this->cimConfig;
    }

    public function isAvailable(CartInterface $quote = null)
    {
        $checkResult                   = $this->objectFactory->create();
        $isActive                      = $this->getConfigModel()->getIsActive();
        $checkResult->isAvailable      = $isActive;
        $checkResult->isDeniedInConfig = !$isActive;
        $checkResult->setData('is_available', $isActive);
        $this->eventManager->dispatch(
            'payment_method_is_active',
            [
                'result' => $checkResult,
                'method_instance' => $this,
                'quote' => $quote
            ]
        );

        return $checkResult->getData('is_available');
    }

    protected function _isPreauthorizeCapture($payment)
    {
        if ($this->getCardsStorage()->getCardsCount() <= 0) {
            return false;
        }
        foreach ($this->getCardsStorage()->getCards() as $card) {
            $lastTransactionId = $payment->getData('cc_trans_id');
            $cardTransactionId = $card->getTransactionId();
            if ($lastTransactionId == $cardTransactionId) {
                if ($payment->getCcTransId()) {
                    return true;
                }

                return false;
            }
        }
    }

    public function getCardsStorage($payment = null)
    {
        if ($payment === null) {
            $payment = $this->getInfoInstance();
        }
        if ($this->cardsStorage === null) {
            $this->_initCardsStorage($payment);
        }

        return $this->cardsStorage;
    }

    public function _initCardsStorage($payment)
    {
        $this->cardsStorage = $this->cardpayment->setPayment($payment);
    }

    protected function _registerCard($response, \Magento\Sales\Model\Order\Payment $payment)
    {
        $cardsStorage = $this->getCardsStorage($payment);
        $card = $cardsStorage->registerCard();
        $card
            ->setRequestedAmount((string)$response->requestedAmount)
            ->setBalanceOnCard((string)$response->balanceOnCard)
            ->setLastTransId((string)$response->transId)
            ->setProcessedAmount((string)$response->amount)
            ->setCcType((string)$response->cardType)
            ->setCcOwner((string)$response->ccOwner)
            ->setCcLast4((string)$response->ccLast4)
            ->setCcExpMonth((string)$response->ccExpMonth)
            ->setCcExpYear((string)$response->ccExpYear)
            ->setCcSsIssue((string)$response->ccSsIssue)
            ->setCcSsStartMonth((string)$response->ccSsStartMonth)
            ->setCcSsStartYear((string)$response->ccSsStartYear)
            ->setApprovalCode((string)$response->approvalCode)
            ->setAvsResultCode((string)$response->avsResultCode)
            ->setTransactionId((string)$response->transId)
            ->setDescription((string)$response->description)
            ->setMethod((string)$response->method)
            ->setTransactionType((string)$response->transactionType)
            ->setCardCodeResponseCode((string)$response->cardCodeResponseCode)
            ->setCAVVResponseCode((string)$response->cAVVResponseCode);
        
        if((int)$response->responseCode==self::REVIEW)
        {
            $transinforesponse = $this->cimXml
                    ->fetchTransInfo((string)$response->transId);
            $code = $transinforesponse->messages->message->code;
            $resultCode = $transinforesponse->messages->resultCode;
            if ($code == 'I00001' && $resultCode == 'Ok') {
               $card->setFdsaction((string)$transinforesponse->transaction->FDSFilterAction); 
               $filterData =  $transinforesponse->transaction->FDSFilters;
               $filterDataArray = json_decode(json_encode((array)$filterData), TRUE);
               $fdshtml = '<table class="data-table admin__table-secondary info-table"><tbody>';
               $fdsFilterArray = (isset($filterDataArray['FDSFilter']['name']))? $filterDataArray: $filterDataArray['FDSFilter'];
               foreach ($fdsFilterArray as $key=>$filter) {
                   $fdshtml .= '<tr>';
                   $fdshtml .= '<td>';
                   $fdshtml .= __('Filter Name:');
                   $fdshtml .= '</td>';
                   $fdshtml .= '<td>';
                   $fdshtml .= $filter['name'];
                   $fdshtml .= '</td>';
                   $fdshtml .= '</tr>';
                   $fdshtml .= '<tr>';
                   $fdshtml .= '<td>';
                   $fdshtml .= __('Action:');
                   $fdshtml .= '</td>';
                   $fdshtml .= '<td>';
                   $fdshtml .= $filter['action'];
                   $fdshtml .= '</td>';
                   $fdshtml .= '</tr>';
                   $fdshtml .= '<tr><td colspan="2" style="border-bottom:1px solid #ccc"></td></tr>';
                }
                $fdshtml .= '</tbody></table>';
                $fdsencode = base64_encode($fdshtml);
                $card->setAfdinfo($fdsencode);
            }   
        }
        $cardsStorage->updateCard($card);
        return $card;
    }

    public function _preauthorizeCapture($payment, $requestedAmount)
    {
        $cardsStorage = $this->getCardsStorage($payment);
        if ($this->_formatAmount(
            $cardsStorage->getProcessedAmount() - $cardsStorage->getCapturedAmount()
        ) < $requestedAmount
        ) {
            // @codingStandardsIgnoreStart
            throw new LocalizedException(new Phrase(__('Invalid amount for capture.')));
            // @codingStandardsIgnoreEnd
        }
        $result       = $this->getPreauthorizeCapture(
            $cardsStorage,
            $payment,
            $requestedAmount
        );
        
        $payment      = $result["payment"];
        $messages     = $result["message"];
        $isFiled      = $result["is_field"];
        $isSuccessful = $result["isSuccessful"];
        if ($isFiled) {
            $this->_processFailureMultitransactionAction(
                $payment,
                $messages,
                $isSuccessful
            );
        }
    }

    public function getPreauthorizeCapture(
        $cardsStorage,
        $payment,
        $requestedAmount
    ) {

        $messages     = [];
        $isSuccessful = false;
        $isFiled      = true;
        foreach ($cardsStorage->getCards() as $card) {
            $lastTransactionId = $payment->getData('cc_trans_id');
            $cardTransactionId = $card->getTransactionId();
            if ($lastTransactionId == $cardTransactionId) {
                if ($requestedAmount > 0) {
                    $prevCaptureAmount    = $card->getCapturedAmount();
                    $cardAmountForCapture = $card->getProcessedAmount();
                    if ($cardAmountForCapture > $requestedAmount) {
                        $cardAmountForCapture = $requestedAmount;
                    }
                    try {
                        $newTransaction = $this->_preauthorizeCaptureCardTransaction(
                            $payment,
                            $cardAmountForCapture,
                            $card
                        );
                        if ($newTransaction != null) {
                            $messages[]   = $newTransaction->getMessage();
                            $isSuccessful = true;
                        }
                        $isFiled = false;
                    } catch (\Exception $e) {
                        $messages[] = $e->getMessage();
                        $isFiled    = true;
                        continue;
                    }
                    $newCapturedAmount = $prevCaptureAmount + $cardAmountForCapture;
                    $card->setCapturedAmount($newCapturedAmount);
                    $cardsStorage->updateCard($card);
                    $requestedAmount   = $this->_formatAmount($requestedAmount - $cardAmountForCapture);
                    if ($isSuccessful) {
                        $balance = $card->getProcessedAmount() - $card->getCapturedAmount();
                        if ($balance > 0) {
                            $payment->setAnetTransType(self::REQUEST_TYPE_AUTH_ONLY);
                            $payment->setAmount($balance);
                        }
                    }
                }
            }
        }
        $result["payment"]      = $payment;
        $result["message"]      = $messages;
        $result["is_field"]     = $isFiled;
        $result["isSuccessful"] = $isSuccessful;
        return $result;
    }

    public function _preauthorizeCaptureCardTransaction(
        $payment,
        $amount,
        $card
    ) {
        $authTransactionId = $card->getLastTransId();
        if ($payment->getCcTransId()) {
            $newTransactionType = \Magento\Sales\Model\Order\Payment\Transaction::TYPE_CAPTURE;
            $payment->setAnetTransType(self::REQUEST_TYPE_PRIOR_AUTH_CAPTURE);

            $payment->setAmount($amount);
            $response = $this->cimXml
                ->prepareAuthorizeCaptureResponse($payment, $amount);

            $code = $response->messages->message->code;
            $resultCode =  $response->messages->resultCode;
            if ($code == 'I00001' && $resultCode == 'Ok') {
                $transactionResponse = $response->transactionResponse;
                $transactionResponse->amount = $amount;
                $captureTransactionId = (string)$transactionResponse->transId.'-capture';
                $card->setLastCapturedTransactionId($captureTransactionId);
                $this->_addTransaction(
                    $payment,
                    $captureTransactionId,
                    $newTransactionType,
                    [
                    'is_transaction_closed' => 0,
                    'parent_transaction_id' => $authTransactionId,
                    ],
                    [$this->realTransactionIdKey => (string)$transactionResponse->transId],
                    $this->cimHelper->getTransactionMessage(
                        $payment,
                        self::REQUEST_TYPE_PRIOR_AUTH_CAPTURE,
                        (string)$transactionResponse->transId,
                        $card,
                        $amount
                    )
                );
            } else {
                $exceptionMessage = $this->cimHelper
                                    ->getErrorDescription($response->messages->message->code);
                // @codingStandardsIgnoreStart
                throw new LocalizedException(new Phrase($exceptionMessage));
                // @codingStandardsIgnoreEnd
            }
        } else {
            return;
        }
    }

    public function _formatAmount($amount, $asFloat = false)
    {
        $amount = sprintf('%.2F', $amount);
        return $asFloat ? (float) $amount : $amount;
    }

    public function _isGatewayActionsLocked($payment)
    {
        return $payment->getAdditionalInformation($this->isGatewayActionsLockedKey);
    }

    public function _generateChecksum(
        \Magento\Framework\DataObject $object,
        $checkSumDataKeys = []
    ) {
        $data = [];
        foreach ($checkSumDataKeys as $dataKey) {
            $data[] = $dataKey;
            $data[] = $object->getData($dataKey);
        }

        return hash('sha256', implode($data, '_'));
    }

    public function _processFailureMultitransactionAction(
        $payment,
        $messages,
        $isSuccessfulTransactions
    ) {
        if ($isSuccessfulTransactions) {
            $msg1 = 'Gateway actions are locked because the gateway cannot complete one or more of the ';
            $msg2 = 'transactions. Please log in to your Cybersource account to manually resolve the issue(s).';
            $messages[]     = $msg1 .$msg2 ;
            $currentOrderId = $payment->getOrder()->getId();
            $orderModel     = $this->ordermodelFactory->create();
            $orderModel->getResource()->load($orderModel, $currentOrderId);
            $copyOrder      = $orderModel;
            $copyOrder->getPayment()->setAdditionalInformation(
                $this->isGatewayActionsLockedKey,
                1
            );
            foreach ($messages as $message) {
                $copyOrder->addStatusHistoryComment($message);
            }
            $copyOrder->getResource()->save($copyOrder);
        } else {
            // @codingStandardsIgnoreStart
            throw new LocalizedException(new Phrase(implode(
                ' | ',
                $messages
            )));
            // @codingStandardsIgnoreEnd
        }
    }

    public function _refundCardTransaction($payment, $amount, $card)
    {
        $credit_memo          = $this->registry->registry('current_creditmemo');
        $captureTransactionId = $credit_memo->getInvoice()->getTransactionId();
        if ($payment->getCcTransId()) {
            $payment->setAnetTransType(self::REQUEST_TYPE_CREDIT);
            $payment->setXTransId($payment->getTransactionId());
            $payment->setAmount($amount);
            $realTransactionid = $payment->getTransactionId();
            $transactionId = str_replace(['-capture','-void','-refund'], '', $realTransactionid);
            $response = $this->cimXml
                ->prepareRefundResponse(
                    $payment,
                    $amount,
                    $transactionId
                );
            $code = $response->messages->message->code;
            $resultCode =  $response->messages->resultCode;
            if ($code == 'I00001' && $resultCode == 'Ok') {
                $transactionResponse = $response->transactionResponse;
                $transactionResponse->amount = $amount;
                $refundTransactionId           = $realTransactionid.'-refund';
                $shouldCloseCaptureTransaction = 0;

                if ($this->_formatAmount($card->getCapturedAmount() - $card->getRefundedAmount())
                    == $amount) {
                    $card->setLastTransId($refundTransactionId);
                    $shouldCloseCaptureTransaction = 1;
                }
                $this->_addTransaction(
                    $payment,
                    $refundTransactionId,
                    \Magento\Sales\Model\Order\Payment\Transaction::TYPE_REFUND,
                    [
                    'is_transaction_closed' => 1,
                    'should_close_parent_transaction' => $shouldCloseCaptureTransaction,
                    'parent_transaction_id' => $captureTransactionId,
                    ],
                    [$this->realTransactionIdKey => (string)$transactionResponse->transId],
                    $this->cimHelper->getTransactionMessage(
                        $payment,
                        self::REQUEST_TYPE_CREDIT,
                        (string)$transactionResponse->transId,
                        $card,
                        $amount
                    )
                );
            } else {
               $exceptionMessage = $this->cimHelper
                                    ->getErrorDescription($response->messages->message->code);
                // @codingStandardsIgnoreStart
                throw new LocalizedException(new Phrase($exceptionMessage));
                // @codingStandardsIgnoreEnd
            }
        } else {
            return;
        }
    }

    public function _wrapGatewayError($text)
    {
        return __('Gateway error:'.$text);
    }

    private function _clearAssignedData($payment)
    {
        $payment->setCcType(null)
            ->setCcOwner(null)
            ->setCcLast4(null)
            ->setCcNumber(null)
            ->setCcCid(null)
            ->setCcExpMonth(null)
            ->setCcExpYear(null)
            ->setCcSsIssue(null)
            ->setCcSsStartMonth(null)
            ->setCcSsStartYear(null);

        return $this;
    }

    public function _addTransaction(
        \Magento\Sales\Model\Order\Payment $payment,
        $transactionId,
        $transactionType,
        array $transactionDetails = [],
        array $transactionAdditionalInfo = [],
        $message = false
    ) {
        $payment->resetTransactionAdditionalInfo();
        $payment->setTransactionId($transactionId);
        $payment->setLastTransId($transactionId);

        foreach ($transactionDetails as $key => $value) {
            $payment->setData($key, $value);
        }
        foreach ($transactionAdditionalInfo as $key => $value) {
            $payment->setTransactionAdditionalInfo($key, $value);
        }
        $transaction = $payment->addTransaction($transactionType, null, false, $message);
        $transaction->setMessage($message);

        return $transaction;
    }

    public function processInvoice($invoice, $payment)
    {
        $lastCaptureTransId = '';
        $cardsStorage       = $this->getCardsStorage($payment);
        foreach ($cardsStorage->getCards() as $card) {
            $lastTransactionId = $payment->getData('cc_trans_id');
            $cardTransactionId = $card->getTransactionId();
            if ($lastTransactionId == $cardTransactionId) {
                $lastCapId = $card->getData('last_captured_transaction_id');
                if ($lastCapId && !empty($lastCapId) && !($lastCapId === null)) {
                    $lastCaptureTransId = $lastCapId;
                    break;
                }
            }
        }

        $invoice->setTransactionId($lastCaptureTransId);

        return $this;
    }

    public function processCreditmemo($creditmemo, $payment)
    {
        $lastRefundedTransId = '';
        $cardsStorage        = $this->getCardsStorage($payment);
        foreach ($cardsStorage->getCards() as $card) {
            $lastTransactionId = $payment->getData('cc_trans_id');
            $cardTransactionId = $card->getTransactionId();
            if ($lastTransactionId == $cardTransactionId) {
                $lastCardTransId = $card->getData('last_refunded_transaction_id');
                if ($lastCardTransId && !empty($lastCardTransId) && !($lastCardTransId
                    === null)) {
                    $lastRefundedTransId = $lastCardTransId;
                    break;
                }
            }
        }
        $creditmemo->setTransactionId($lastRefundedTransId);

        return $this;
    }

    public function canUseForCurrency($currencyCode)
    {
        return true;
    }

    public function isApplicableToQuote($quote, $checksBitMask)
    {
        if ($checksBitMask & MethodInterface::CHECK_USE_FOR_COUNTRY) {
            if (!$this->canUseForCountry($quote->getBillingAddress()->getCountry())) {
                return false;
            }
        }
        if ($checksBitMask & MethodInterface::CHECK_USE_FOR_CURRENCY) {
            if (!$this->canUseForCurrency($quote->getStore()->getBaseCurrencyCode())) {
                return false;
            }
        }
        return $this->checkApplicableToQuote($quote, $checksBitMask);
    }

    public function checkApplicableToQuote($quote, $checksBitMask)
    {
        if ($checksBitMask & MethodInterface::CHECK_USE_CHECKOUT) {
            if (!$this->canUseCheckout()) {
                return false;
            }
        }
        if ($checksBitMask & MethodInterface::CHECK_USE_INTERNAL) {
            if (!$this->canUseInternal()) {
                return false;
            }
        }
        if ($checksBitMask & MethodInterface::CHECK_ORDER_TOTAL_MIN_MAX) {
            $total    = $quote->getBaseGrandTotal();
            $minTotal = $this->getConfigData('min_order_total');
            $maxTotal = $this->getConfigData('max_order_total');
            if (!empty($minTotal) && $total < $minTotal || !empty($maxTotal) && $total
                > $maxTotal) {
                return false;
            }
        }
        return true;
    }

    public function _formatCcType($ccType)
    {
        $allTypes = $this->paymentconfig->getCcTypes();
        $allTypes = array_flip($allTypes);

        if (isset($allTypes[$ccType]) && !empty($allTypes[$ccType])) {
            return $allTypes[$ccType];
        }

        return $ccType;
    }
}
