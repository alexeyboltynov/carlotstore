<?php
/**
 * Magedelight
 * Copyright (C) 2017 Magedelight <info@magedelight.com>.
 *
 * NOTICE OF LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://opensource.org/licenses/gpl-3.0.html.
 *
 * @category Magedelight
 *
 * @copyright Copyright (c) 2017 Mage Delight (http://www.magedelight.com/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Magedelight <info@magedelight.com>
 */

namespace Magedelight\Authorizecim\Model\Api;

class AbstractInterface extends \Magento\Framework\DataObject
{
   
    /**
     * @var Magedelight\Authorizecim\Model\Config
     */
    protected $configModel;

    /**
     * @var Magento\Store\Model\StoreManager
     */
    protected $storeManager;

    /**
     * @var Magento\Directory\Model\RegionFactory
     */
    protected $regionFactory;

    /**
     * @var Magento\Directory\Model\CountryFactory
     */
    protected $countryFactory;

    /**
     * @var array
     */
    protected $inputData = [];

    /**
     * @var array
     */
    protected $responseData = [];
    
    /**
     * @var string 
     */
    protected $apiLogin;

    /**
     * @var string
     */
    protected $apiTransKey;

    /**
     * @var string
     */
    protected $apiGatewayUrl;
    
    /**
     * @var string 
     */
    protected $apiMode;
    
    /**
     * @var string 
     */
    protected $cvvEnabled;

    /**
     * @var string
     */
    protected $timeout;

    /**
     * @var Magedelight\Authorizecim\Helper\Data
     */
    public $cimHelper;

    /**
     * @var Magento\Framework\App\RequestInterface
    */
    public $requestHttp;

    public $stream;
    public $zendlogger;
    
    /**
     *
     * @var Magento\Framework\HTTP\PhpEnvironment\RemoteAddress 
     */
    protected $_remoteAddress;

    public function __construct(\Magedelight\Authorizecim\Model\Config $configModel,
            \Magento\Store\Model\StoreManager $storeManager,
            \Magento\Directory\Model\RegionFactory $regionFactory,
            \Magento\Directory\Model\CountryFactory $countryFactory,
            \Magedelight\Authorizecim\Helper\Data $cimHelper,
            \Magento\Framework\App\RequestInterface $requestHttp,
            \Magento\Framework\HTTP\PhpEnvironment\RemoteAddress $remoteAddress,
             array $data = [])
    {
        $this->configModel = $configModel;
        $this->storeManager = $storeManager;
        $this->apiLogin = $configModel->getApiLoginId();
        $this->apiTransKey = $configModel->getApiTransKey();
        $this->apiGatewayUrl = $configModel->getGatewayUrl();
        $this->apiGatewayWsdl = $configModel->getGatewayWsdl();
        $this->apiMode = $configModel->getValidationMode();
        $this->cvvEnabled = $configModel->isCardVerificationEnabled();
        $this->solutionId = $configModel->getSolutionId();
        $this->regionFactory = $regionFactory;
        $this->countryFactory = $countryFactory;
        $this->cimHelper  = $cimHelper;
        $this->requestHttp    = $requestHttp;
        $this->_remoteAddress = $remoteAddress;
        parent::__construct($data);
    }

    /**
     * @param type $input
     * @return \Magedelight\Authorizecim\Model\Api\AbstractInterface
     */
    public function setInputData($input = null)
    {
        $this->inputData = $input;
        return $this;
    }

    /**
     * get input data
     * @return type
     */
    public function getInputData()
    {
        return $this->inputData;
    }

    /**
     * set response data
     * @param type $response
     * @return \Magedelight\Authorizecim\Model\Api\AbstractInterface
     */
    public function setResponseData($response = [])
    {
        $this->responseData = $response;

        return $this;
    }

    /**
     * get response data
     * @return array
     */
    public function getResponseData()
    {
        return $this->responseData;
    }

    /**
     * get config model
     * @return string
     */
    public function getConfigModel()
    {
        return $this->configModel;
    }
}
