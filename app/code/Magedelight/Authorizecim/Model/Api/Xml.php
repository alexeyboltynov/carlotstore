<?php
/**
 * Magedelight
 * Copyright (C) 2017 Magedelight <info@magedelight.com>.
 *
 * NOTICE OF LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://opensource.org/licenses/gpl-3.0.html.
 *
 * @category Magedelight
 *
 * @copyright Copyright (c) 2017 Mage Delight (http://www.magedelight.com/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Magedelight <info@magedelight.com>
 */

namespace Magedelight\Authorizecim\Model\Api;

use Magento\Framework\Exception\LocalizedException;
use Magento\Payment\Model\InfoInterface;

class Xml extends \Magedelight\Authorizecim\Model\Api\AbstractInterface
{
    const RESPONSE_DELIM_CHAR = '(~)';

    /* customer profile request */
    public function createCustomerPaymentProfileRequest()
    {
        $inputData = $this->getInputData();
        $regionId = $inputData->getRegionId();
        $regionName = ($regionId) ? $this->regionFactory->create()->load($regionId)->getName() : $inputData->getState();
        $string = '<?xml version="1.0" encoding="utf-8"?>';
        $string .= '<createCustomerPaymentProfileRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">';
        $string .= '<merchantAuthentication>';
        $string .= '<name>'.$this->apiLogin.'</name>';
        $string .= '<transactionKey>'.$this->apiTransKey.'</transactionKey>';
        $string .= '</merchantAuthentication>';
        $string .= '<customerProfileId>'.$inputData->getCustomerProfileId().'</customerProfileId>';
        $string .= '<paymentProfile>';
        $string .= '<billTo>';
        $string .= '<firstName>'.$inputData->getFirstname().'</firstName>';
        $string .= '<lastName>'.$inputData->getLastname().'</lastName>';
        $string .= '<company>'.$inputData->getCompany().'</company>';
        $string .= '<address>'.$this->removeSpecials($inputData->getStreet()[0]).'</address>';
        $string .= '<city>'.$inputData->getCity().'</city>';
        $string .= '<state>'.$regionName.'</state>';
        $string .= '<zip>'.$inputData->getPostcode().'</zip>';
        $string .= '<country>'.$inputData->getCountryId().'</country>';
        $string .= '<phoneNumber>'.$inputData->getPhone().'</phoneNumber>';
        $string .= '<faxNumber>'.$inputData->getFax().'</faxNumber>';
        $string .= '</billTo>';
        $string .= '<payment>';
        if($inputData->getDataValue()!=''){
             $string .= '<opaqueData>';
             $string .= '<dataDescriptor>'.$inputData->getDataDescriptor().'</dataDescriptor>';
             $string .= '<dataValue>'.$inputData->getDataValue().'</dataValue>';
             $string .= '</opaqueData>';
        }
        else {
            $string .= '<creditCard>';
            $string .= '<cardNumber>'.$inputData->getCcNumber().'</cardNumber><expirationDate>'
                .sprintf('%04d-%02d', $inputData->getCcExpYear(), $inputData->getCcExpMonth()).'</expirationDate>';
            if ($this->cvvEnabled) {
                $string .= '<cardCode>'.$inputData->getCcCid().'</cardCode>';
            }
            $string .= '</creditCard>';
        }
        $string .= '</payment>';
        $string .= '</paymentProfile>';
        $string .= '<validationMode>'.$this->apiMode.'</validationMode>';
        $string .= '</createCustomerPaymentProfileRequest>';
        return $string;
    }

    public function createCustomerPaymentProfile()
    {
        $request = $this->createCustomerPaymentProfileRequest();
        $response = $this->_postRequest($request, 'createCustomerPaymentProfile');
         return $response;
    }

    public function deleteCustomerProfileRequest()
    {
        $inputData = $this->getInputData();
        $string = '<?xml version="1.0" encoding="utf-8"?>';
        $string .= '<deleteCustomerProfileRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">';
        $string .= '<merchantAuthentication>';
        $string .= '<name>'.$this->apiLogin.'</name>';
        $string .= '<transactionKey>'.$this->apiTransKey.'</transactionKey>';
        $string .= '</merchantAuthentication>';
        $string .= '<customerProfileId>'.$inputData->getCustomerProfileId().'</customerProfileId>';
        $string .= '</deleteCustomerProfileRequest>';

        return $string;
    }

    public function deleteCustomerProfile()
    {
        $request = $this->deleteCustomerProfileRequest();
        $response = $this->_postRequest($request, 'deleteCustomerProfile');

        return $response;
    }

    public function getCustomerProfileRequest()
    {
        $inputData = $this->getInputData();
        $string = '<?xml version="1.0" encoding="utf-8"?>';
        $string .= '<getCustomerProfileRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">';
        $string .= '<merchantAuthentication>';
        $string .= '<name>'.$this->apiLogin.'</name>';
        $string .= '<transactionKey>'.$this->apiTransKey.'</transactionKey>';
        $string .= '</merchantAuthentication>';
        $string .= '<customerProfileId>'.$inputData->getCustomerProfileId().'</customerProfileId>';
        $string .= '</getCustomerProfileRequest>';

        return $string;
    }

    public function getCustomerProfile()
    {
        $request = $this->getCustomerProfileRequest();
        $response = $this->_postRequest($request, 'getCustomerProfile');

        return $response;
    }
    /**
     * create customer profile request
     * @return string
     */
    public function createCustomerProfileRequest()
    {
        $inputData = $this->getInputData();
        $string = '<?xml version="1.0" encoding="utf-8"?>';
        $string .= '<createCustomerProfileRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">';
        $string .= '<merchantAuthentication>';
        $string .= '<name>'.$this->apiLogin.'</name>';
        $string .= '<transactionKey>'.$this->apiTransKey.'</transactionKey>';
        $string .= '</merchantAuthentication>';
        $string .= '<profile>';
        $string .= '<merchantCustomerId>'.$inputData->getCustomerId().'</merchantCustomerId>';
        $string .= '<description>'.$this->storeManager->getStore()->getBaseUrl().'</description>';
        $string .= '<email>'.$inputData->getEmail().'</email>';
        $string .= '</profile>';
        $string .= '<validationMode>'.$this->apiMode.'</validationMode>';
        $string .= '</createCustomerProfileRequest>';
        return $string;
    }

    public function createCustomerProfile()
    {
        $request = $this->createCustomerProfileRequest();
        $response = $this->_postRequest($request, 'createCustomerProfile');
        return $response;
    }

    public function deleteCustomerPaymentProfileRequest()
    {
        $inputData = $this->getInputData();
        $string = '<?xml version="1.0" encoding="utf-8"?>';
        $string .= '<deleteCustomerPaymentProfileRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">';
        $string .= '<merchantAuthentication>';
        $string .= '<name>'.$this->apiLogin.'</name>';
        $string .= '<transactionKey>'.$this->apiTransKey.'</transactionKey>';
        $string .= '</merchantAuthentication>';
        $string .= '<customerProfileId>'.$inputData->getCustomerProfileId().'</customerProfileId>';
        $string .= '<customerPaymentProfileId>'.$inputData->getPaymentProfileId().'</customerPaymentProfileId>';
        $string .= '</deleteCustomerPaymentProfileRequest>';

        return $string;
    }

    public function deleteCustomerPaymentProfile()
    {
        $request = $this->deleteCustomerPaymentProfileRequest();
        $response = $this->_postRequest($request, 'deleteCustomerPaymentProfile');

        return $response;
    }

    public function getCustomerPaymentProfile()
    {
        $request = $this->getCustomerPaymentProfileRequest();
        $response = $this->_postRequest($request, 'getCustomerPaymentProfile');

        return $response;
    }

    public function getCustomerPaymentProfileRequest()
    {
        $inputData = $this->getInputData();
        $string = '<?xml version="1.0" encoding="utf-8"?>';
        $string .= '<getCustomerPaymentProfileRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">';
        $string .= '<merchantAuthentication>';
        $string .= '<name>'.$this->apiLogin.'</name>';
        $string .= '<transactionKey>'.$this->apiTransKey.'</transactionKey>';
        $string .= '</merchantAuthentication>';
        $string .= '<customerProfileId>'.$inputData->getCustomerProfileId().'</customerProfileId>';
        $string .= '<customerPaymentProfileId>'.$inputData->getPaymentProfileId().'</customerPaymentProfileId>';
        $string .= '</getCustomerPaymentProfileRequest>';

        return $string;
    }

    public function updateCustomerPaymentProfile()
    {
        $request = $this->updateCustomerPaymentProfileRequest();
        $response = $this->_postRequest($request, 'updateCustomerPaymentProfile');

        return $response;
    }

    public function updateCustomerPaymentProfileRequest()
    {
        $inputData = $this->getInputData();
        $isNewCard = false;
        if ($inputData->getDataValue()) {
             $paymentString = '<opaqueData>';
             $paymentString .= '<dataDescriptor>'.$inputData->getDataDescriptor().'</dataDescriptor>';
             $paymentString .= '<dataValue>'.$inputData->getDataValue().'</dataValue>';
             $paymentString .= '</opaqueData>';
        }
        else
        {
            $paymentString = '<creditCard>';
             if ($inputData->getCcNumber()) {
             $isNewCard = true;
             $paymentString .= '<cardNumber>'.str_replace(array(' ', '-'), '', $inputData->getCcNumber()).'</cardNumber>';
            } else {
                $paymentString .= '<cardNumber>'.$inputData->getCardNumberMasked().'</cardNumber>';
            }
            if ($inputData->getCcExpMonth() && $inputData->getCcExpYear()) {
                $isNewCard = true;
                $paymentString .= '<expirationDate>'.sprintf('%04d-%02d', $inputData->getCcExpYear(), $inputData->getCcExpMonth()).'</expirationDate>';
            } else {
                $paymentString .= '<expirationDate>XXXX</expirationDate>';
            }
            if ($this->cvvEnabled && $isNewCard) {
                if ($inputData->getCcCid()) {
                    $paymentString .= '<cardCode>'.$inputData->getCcCid().'</cardCode>';
                } else {
                    throw new LocalizedException(__('Credit card Varification number required.'));
                }
            }
            $paymentString .= '</creditCard>';
        }
        $regionId = $inputData->getRegionId();
        $street = (is_array($inputData->getStreet()))? implode(" ", $inputData->getStreet()) : $inputData->getStreet();
        $regionName = ($regionId) ? $this->regionFactory->create()->load($regionId)->getName() : $inputData->getState();
        $string = '<?xml version="1.0" encoding="utf-8"?>';
        $string .= '<updateCustomerPaymentProfileRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">';
        $string .= '<merchantAuthentication>';
        $string .= '<name>'.$this->apiLogin.'</name>';
        $string .= '<transactionKey>'.$this->apiTransKey.'</transactionKey>';
        $string .= '</merchantAuthentication>';
        $string .= '<customerProfileId>'.$inputData->getCustomerProfileId().'</customerProfileId>';
        $string .= '<paymentProfile>';
        $string .= '<billTo>';
        $string .= '<firstName>'.$inputData->getFirstname().'</firstName>';
        $string .= '<lastName>'.$inputData->getLastname().'</lastName>';
        $string .= '<company>'.$inputData->getCompany().'</company>';
        $string .= '<address>'.$this->removeSpecials($street).'</address>';
        $string .= '<city>'.$inputData->getCity().'</city>';
        $string .= '<state>'.$regionName.'</state>';
        $string .= '<zip>'.$inputData->getPostcode().'</zip>';
        $string .= '<country>'.$inputData->getCountryId().'</country>';
        $string .= '<phoneNumber>'.$inputData->getPhone().'</phoneNumber>';
        $string .= '<faxNumber>'.$inputData->getFax().'</faxNumber>';
        $string .= '</billTo>';
        if (strlen($paymentString) > 0) {
            $string .= '<payment>';
            $string .= $paymentString;
            $string .= '</payment>';
        }
        $string .= '<customerPaymentProfileId>'.$inputData->getPaymentProfileId().'</customerPaymentProfileId>';
        $string .= '</paymentProfile>';
        $string .= '<validationMode>'.$this->apiMode.'</validationMode>';
        $string .= '</updateCustomerPaymentProfileRequest>';
        return $string;
    }
    /* end of customer profile request */

     /* transaction request */
    /* start authorize transaction */
     public function prepareAuthorizeResponse(
        InfoInterface $payment,
        $amount,
        $saveFlag,
        $paymentprofile = false
    ) {
       $this->request = $this->prepareAuthorizeRequest(
            $payment,
            $amount,
            $saveFlag,
            $paymentprofile
        );

        $response      = $this->_postRequest($this->request);
        return $response;
    }

    public function prepareAuthorizeRequest(
        InfoInterface $payment,
        $amount,
        $saveFlag,
        $paymentprofile
    ) {
        $billingAddress = $payment->getOrder()->getBillingAddress();
        $shippingAddress = $payment->getOrder()->getShippingAddress();
        if($shippingAddress==null)
        {
            $shippingAddress = $billingAddress;
        }
        $billregionId  = $billingAddress->getRegionId();
        $regionModel = $this->regionFactory->create();
        if ($billregionId) :
             $regionModel->getResource()->load($regionModel, $billregionId);
             $billregionCode = $regionModel->getCode();
        else :
             $billregionCode = $billingAddress->getRegion();
        endif;
        $dataValue = $payment->getAdditionalInformation('data_value');
        if ($paymentprofile == false) {
              $post     = $this->requestHttp->getParam('payment');
              $string = '<?xml version="1.0" encoding="utf-8"?>';
              $string .= '<createTransactionRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">';
              $string .= '<merchantAuthentication>';
              $string .= '<name>'.$this->apiLogin.'</name>';
              $string .= '<transactionKey>'.$this->apiTransKey.'</transactionKey>';
              $string .= '</merchantAuthentication>';
              $string .= '<refId>'.$payment->getOrder()->getIncrementId().'</refId>';
              $string .= '<transactionRequest>';
              $string .= '<transactionType>authOnlyTransaction</transactionType>';
              $string .= '<amount>'.$amount.'</amount>';
              $string .= '<payment>';
              if(!empty($payment->getCcNumber()) || isset($post['cc_number']))
              {
                $ccNumber = empty($payment->getCcNumber()) ? $post['cc_number'] : $payment->getCcNumber();
                $expMonth = empty($payment->getCcExpMonth()) ? $post['cc_exp_month'] : $payment->getCcExpMonth();
                $expYear  = empty($payment->getCcExpYear()) ? $post['cc_exp_year'] : $payment->getCcExpYear();
                $expDate  = sprintf('%04d-%02d', $expYear,$expMonth);
                $string .= '<creditCard>';
                $string .= '<cardNumber>'.$ccNumber.'</cardNumber>';
                $string .= '<expirationDate>'.$expDate .'</expirationDate>';
                if ($this->cvvEnabled) {
                      $ccId = empty($payment->getCcCid()) ? $post['cc_cid'] : $payment->getCcCid();
                      if(isset($ccId) && !empty($ccId)){
                        $string .= '<cardCode>'.$ccId.'</cardCode>';
                      }
                }
                $string .= '</creditCard>';
              }
              elseif($dataValue!='')
              {
                  $dataDescriptor = $payment->getAdditionalInformation('data_descriptor');;
                  $string .= '<opaqueData>';
                  $string .= '<dataDescriptor>'.$dataDescriptor.'</dataDescriptor>';
                  $string .= '<dataValue>'.$dataValue.'</dataValue>';
                  $string .= '</opaqueData>';
              }

              $string .= '</payment>';
              $string .= '<profile>';
              $string .= '<createProfile>'.$saveFlag.'</createProfile>';
              $string .= '</profile>';
              $string .= '<solution>';
              $string .= '<id>'.$this->solutionId.'</id>';
              $string .= '</solution>';

              $string .= '<order>';
              $string .= '<invoiceNumber>'.$payment->getOrder()->getIncrementId().'</invoiceNumber>';
              $string .= '<description>Product Description</description>';
              $string .= '</order>';
              if ($this->configModel->sendLineItems()) {
                $string .= $this->_prepareLineItems($payment);
              }
              $string .= '<tax>';
              $string .= '<amount>'.round($payment->getOrder()->getBaseTaxAmount(), 4).'</amount>';
              $string .= '</tax>';
              $string .= '<shipping>';
              $string .= '<amount>'.round($payment->getBaseShippingAmount(), 4).'</amount>';
              $string .= '</shipping>';
              $string .= '<customer>';
              $string .= '<id>'.$payment->getOrder()->getCustomerId().'</id>';
              $string .= '</customer>';
              $string .= '<billTo>';
              $string .= '<firstName>'.$billingAddress->getFirstname().'</firstName>';
              $string .= '<lastName>'.$billingAddress->getLastname().'</lastName>';
              $string .= '<company>'.$billingAddress->getCompany().'</company>';
              $billaddr = (is_array($billingAddress->getStreet()))
                    ?implode(",", $billingAddress->getStreet()):$billingAddress->getStreet();
              $string .= '<address>'.$this->removeSpecials($billaddr).'</address>';
              $string .= '<city>'.$billingAddress->getCity().'</city>';
              $string .= '<state>' . $billregionCode .'</state>';
              $string .= '<zip>'.$billingAddress->getPostcode().'</zip>';
              $string .= '<country>'.$billingAddress->getCountryId().'</country>';
              $string .= '</billTo>';
                $regionId  = $shippingAddress->getRegionId();
                if ($regionId) :
                     $regionModel->getResource()->load($regionModel, $regionId);
                     $regionCode = $regionModel->getCode();
                else :
                     $regionCode = $billingAddress->getRegion();
                endif;
              $string .= '<shipTo>';
              $string .= '<firstName>'.$shippingAddress->getFirstname().'</firstName>';
              $string .= '<lastName>'.$shippingAddress->getLastname().'</lastName>';
              $string .= '<company>'.$shippingAddress->getCompany().'</company>';
              $shipaddr  = (is_array($shippingAddress->getStreet()))
                    ?implode(",", $shippingAddress->getStreet()):$shippingAddress->getStreet();
              $string .= '<address>'.$this->removeSpecials($shipaddr).'</address>';
              $string .= '<city>'.$shippingAddress->getCity().'</city>';
              $string .= '<state>'.$regionCode.'</state>';
              $string .= '<zip>'.$shippingAddress->getPostcode().'</zip>';
              $string .= '<country>'.$shippingAddress->getCountryId().'</country>';
              $string .= '</shipTo>';
              $string .= '<customerIP>'.$this->_remoteAddress->getRemoteAddress().'</customerIP>';
              $string .= '<transactionSettings>';
              $string .= '<setting>';
              $string .= '<settingName>duplicateWindow</settingName>';
              $string .= '<settingValue>0</settingValue>';
              $string .= '</setting>';
              $string .= '</transactionSettings>';
              
              $string .= '</transactionRequest>';
              $string .= '</createTransactionRequest>';
        }
        else
        {
              $post     = $this->requestHttp->getParam('payment');
              $customerProfileid = $payment->getAdditionalInformation('md_customer_profile_id');
              $paymentProfileid = $payment->getAdditionalInformation('md_payment_profile_id');
              $string = '<?xml version="1.0" encoding="utf-8"?>';
              $string .= '<createTransactionRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">';
              $string .= '<merchantAuthentication>';
              $string .= '<name>'.$this->apiLogin.'</name>';
              $string .= '<transactionKey>'.$this->apiTransKey.'</transactionKey>';
              $string .= '</merchantAuthentication>';
              $string .= '<refId>'.$payment->getOrder()->getIncrementId().'</refId>';
              $string .= '<transactionRequest>';
              $string .= '<transactionType>authOnlyTransaction</transactionType>';
              $string .= '<amount>'.$amount.'</amount>';
              $string .= '<profile>';
              $string .= '<customerProfileId>'.$customerProfileid.'</customerProfileId>';
              $string .= '<paymentProfile>';
              $string .= '<paymentProfileId>'.$paymentProfileid.'</paymentProfileId>';
              if ($this->cvvEnabled) {
                    $ccId = empty($payment->getCcCid()) ? $post['cc_cid'] : $payment->getCcCid();
                    if(isset($ccId) && !empty($ccId)){
                        $string .= '<cardCode>'.$ccId.'</cardCode>';
                    }
              }
              $string .= '</paymentProfile>';
              $string .= '</profile>';
              $string .= '<solution>';
              $string .= '<id>'.$this->solutionId.'</id>';
              $string .= '</solution>';
              $string .= '<order>';
              $string .= '<invoiceNumber>'.$payment->getOrder()->getIncrementId().'</invoiceNumber>';
              $string .= '<description>Product Description</description>';
              $string .= '</order>';
              if ($this->configModel->sendLineItems()) {
                $string .= $this->_prepareLineItems($payment);
              }
              $string .= '<tax>';
              $string .= '<amount>'.round($payment->getOrder()->getBaseTaxAmount(), 4).'</amount>';
              $string .= '</tax>';
              $string .= '<shipping>';
              $string .= '<amount>'.round($payment->getBaseShippingAmount(), 4).'</amount>';
              $string .= '</shipping>';
              $string .= '<customer>';
              $string .= '<id>'.$payment->getOrder()->getCustomerId().'</id>';
              $string .= '</customer>';
              $regionId  = $shippingAddress->getRegionId();
                if ($regionId) :
                     $regionModel->getResource()->load($regionModel, $regionId);
                     $regionCode = $regionModel->getCode();
                else :
                     $regionCode = $billingAddress->getRegion();
                endif;
              $string .= '<shipTo>';
              $string .= '<firstName>'.$shippingAddress->getFirstname().'</firstName>';
              $string .= '<lastName>'.$shippingAddress->getLastname().'</lastName>';
              $string .= '<company>'.$shippingAddress->getCompany().'</company>';
              $shipaddr  = (is_array($shippingAddress->getStreet()))
                    ?implode(",", $shippingAddress->getStreet()):$shippingAddress->getStreet();
              $string .= '<address>'.$this->removeSpecials($shipaddr).'</address>';
              $string .= '<city>'.$shippingAddress->getCity().'</city>';
              $string .= '<state>'.$regionCode.'</state>';
              $string .= '<zip>'.$shippingAddress->getPostcode().'</zip>';
              $string .= '<country>'.$shippingAddress->getCountryId().'</country>';
              $string .= '</shipTo>';
              $string .= '<customerIP>'.$this->_remoteAddress->getRemoteAddress().'</customerIP>';
              $string .= '<transactionSettings>';
              $string .= '<setting>';
              $string .= '<settingName>duplicateWindow</settingName>';
              $string .= '<settingValue>0</settingValue>';
              $string .= '</setting>';
              $string .= '</transactionSettings>';
              $string .= '</transactionRequest>';
              $string .= '</createTransactionRequest>';
        }
        return $string;
    }
    protected function _prepareLineItems(\Magento\Payment\Model\InfoInterface $payment)
    {
        $string = '<lineItems>';
        if (is_object($payment)) {
            $order = $payment->getOrder();
            if ($order instanceof \Magento\Sales\Model\Order) {
              foreach ($order->getAllVisibleItems() as $_item) {
                    array('sku' => substr($_item->getSku(), 0, 30), 'name' => substr($_item->getName(), 0, 30), 'description' => substr($_item->getName(), 0, 250), 'qty' => $_item->getQtyOrdered(), 'price' => round($_item->getBasePrice(), 2), 'taxable' => true);
                    $string .= '<lineItem>';
                    $string .= '<itemId>'.$_item->getProductId().'</itemId>';
                    $string .= '<name>'.$this->removeSpecials(substr($_item->getName(), 0, 30)).'</name>';
                    $string .= '<description>'.$this->removeSpecials(substr($_item->getName(), 0, 250)).'</description>';
                    $string .= '<quantity>'.$_item->getQtyOrdered().'</quantity>';
                    $string .= '<unitPrice>'.round($_item->getBasePrice(), 2).'</unitPrice>';
                    $string .= '</lineItem>';
                }

            }
        }
        $string .= '</lineItems>';
        return $string;
    }
    public function removeSpecials($str)
    {
         return preg_replace("/[^ \w]+/", "", $str);
    }
    /* capture preauthorized amount */
    public function prepareAuthorizeCaptureResponse($payment, $amount)
    {
              $string = '<?xml version="1.0" encoding="utf-8"?>';
              $string .= '<createTransactionRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">';
              $string .= '<merchantAuthentication>';
              $string .= '<name>'.$this->apiLogin.'</name>';
              $string .= '<transactionKey>'.$this->apiTransKey.'</transactionKey>';
              $string .= '</merchantAuthentication>';
              $string .= '<refId>'.$payment->getOrder()->getIncrementId().'</refId>';
              $string .= '<transactionRequest>';
              $string .= '<transactionType>priorAuthCaptureTransaction</transactionType>';
              $string .= '<amount>'.$amount.'</amount>';
              $string .= '<refTransId>'.$payment->getCcTransId().'</refTransId>';
              $string .= '<order>';
              $string .= '<invoiceNumber>'.$payment->getOrder()->getIncrementId().'</invoiceNumber>';
              $string .= '<description>Product Description</description>';
              $string .= '</order>';
              $string .= '</transactionRequest>';
              $string .= '</createTransactionRequest>';
         $response      = $this->_postRequest($string);
         return $response;
    }
    /**
     * request for refund
     * @param type $payment
     * @param type $amount
     * @param type $realCaptureTransactionId
     * @return type
     */
    public function prepareRefundResponse( $payment,
                    $amount,
                    $realCaptureTransactionId)
    {
          $billingAddress = $payment->getOrder()->getBillingAddress();
          $shippingAddress = $payment->getOrder()->getShippingAddress();
          if($shippingAddress==null)
          {
            $shippingAddress = $billingAddress;
          }
          $billregionId  = $billingAddress->getRegionId();
          $regionModel = $this->regionFactory->create();
          if ($billregionId) :
             $regionModel->getResource()->load($regionModel, $billregionId);
             $billregionCode = $regionModel->getCode();
          else :
             $billregionCode = $billingAddress->getRegion();
          endif;
          $string = '<?xml version="1.0" encoding="utf-8"?>';
          $string .= '<createTransactionRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">';
          $string .= '<merchantAuthentication>';
          $string .= '<name>'.$this->apiLogin.'</name>';
          $string .= '<transactionKey>'.$this->apiTransKey.'</transactionKey>';
          $string .= '</merchantAuthentication>';
          $string .= '<refId>'.$payment->getOrder()->getIncrementId().'</refId>';
          $string .= '<transactionRequest>';
          $string .= '<transactionType>refundTransaction</transactionType>';
          $string .= '<amount>'.$amount.'</amount>';
          $string .= '<payment>';
          $string .= '<creditCard>';
          $string .= '<cardNumber>'.$payment->getCcLast4().'</cardNumber>';
          $string .= '<expirationDate>XXXX</expirationDate>';
          $string .= '</creditCard>';
          $string .= '</payment>';
          $string .= '<refTransId>'.$realCaptureTransactionId.'</refTransId>';
          $string .= '<billTo>';
          $string .= '<firstName>'.$billingAddress->getFirstname().'</firstName>';
          $string .= '<lastName>'.$billingAddress->getLastname().'</lastName>';
          $string .= '<company>'.$billingAddress->getCompany().'</company>';
          $billaddr = (is_array($billingAddress->getStreet()))
                ?implode(",", $billingAddress->getStreet()):$billingAddress->getStreet();
          $string .= '<address>'.$this->removeSpecials($billaddr).'</address>';
          $string .= '<city>'.$billingAddress->getCity().'</city>';
          $string .= '<state>' . $billregionCode .'</state>';
          $string .= '<zip>'.$billingAddress->getPostcode().'</zip>';
          $string .= '<country>'.$billingAddress->getCountryId().'</country>';
          $string .= '</billTo>';
          $regionId  = $shippingAddress->getRegionId();
          if ($regionId) :
             $regionModel->getResource()->load($regionModel, $regionId);
             $regionCode = $regionModel->getCode();
          else :
             $regionCode = $billingAddress->getRegion();
          endif;
          $string .= '<shipTo>';
          $string .= '<firstName>'.$shippingAddress->getFirstname().'</firstName>';
          $string .= '<lastName>'.$shippingAddress->getLastname().'</lastName>';
          $string .= '<company>'.$shippingAddress->getCompany().'</company>';
          $shipaddr  = (is_array($shippingAddress->getStreet()))
                ?implode(",", $shippingAddress->getStreet()):$shippingAddress->getStreet();
          $string .= '<address>'.$this->removeSpecials($shipaddr).'</address>';
          $string .= '<city>'.$shippingAddress->getCity().'</city>';
          $string .= '<state>'.$regionCode.'</state>';
          $string .= '<zip>'.$shippingAddress->getPostcode().'</zip>';
          $string .= '<country>'.$shippingAddress->getCountryId().'</country>';
          $string .= '</shipTo>';
          $string .= '</transactionRequest>';
          $string .= '</createTransactionRequest>';
          $response = $this->_postRequest($string);
          return $response;
    }

    public function prepareVoidResponse($payment,$transactionId)
    {
         $string = '<?xml version="1.0" encoding="utf-8"?>';
         $string .= '<createTransactionRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">';
         $string .= '<merchantAuthentication>';
         $string .= '<name>'.$this->apiLogin.'</name>';
         $string .= '<transactionKey>'.$this->apiTransKey.'</transactionKey>';
         $string .= '</merchantAuthentication>';
         $string .= '<refId>'.$payment->getOrder()->getIncrementId().'</refId>';
         $string .= '<transactionRequest>';
         $string .= '<transactionType>voidTransaction</transactionType>';
         $string .= '<refTransId>'.$transactionId.'</refTransId>';
         $string .= '</transactionRequest>';
         $string .= '</createTransactionRequest>';
         $response = $this->_postRequest($string);
         return $response;
  }


   public function prepareCaptureResponse(
        InfoInterface $payment,
        $amount,
        $saveFlag,
        $paymentprofile = false
    ) {
       $this->request = $this->prepareCaptureRequest(
            $payment,
            $amount,
            $saveFlag,
            $paymentprofile
        );
        $response      = $this->_postRequest($this->request);
        return $response;
    }

    public function prepareCaptureRequest(
        InfoInterface $payment,
        $amount,
        $saveFlag,
        $paymentprofile
    ) {
        $billingAddress = $payment->getOrder()->getBillingAddress();
        $shippingAddress = $payment->getOrder()->getShippingAddress();
        if($shippingAddress==null)
        {
           $shippingAddress =  $billingAddress;
        }
        $billregionId  = $billingAddress->getRegionId();
        $regionModel = $this->regionFactory->create();
        if ($billregionId) :
             $regionModel->getResource()->load($regionModel, $billregionId);
             $billregionCode = $regionModel->getCode();
        else :
             $billregionCode = $billingAddress->getRegion();
        endif;
        $dataValue = $payment->getAdditionalInformation('data_value');
         $post     = $this->requestHttp->getParam('payment');
        if ($paymentprofile == false) {
             
              $string = '<?xml version="1.0" encoding="utf-8"?>';
              $string .= '<createTransactionRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">';
              $string .= '<merchantAuthentication>';
              $string .= '<name>'.$this->apiLogin.'</name>';
              $string .= '<transactionKey>'.$this->apiTransKey.'</transactionKey>';
              $string .= '</merchantAuthentication>';
              $string .= '<refId>'.$payment->getOrder()->getIncrementId().'</refId>';
              $string .= '<transactionRequest>';
              $string .= '<transactionType>authCaptureTransaction</transactionType>';
              $string .= '<amount>'.$amount.'</amount>';
              $string .= '<payment>';
              if(!empty($payment->getCcNumber()) || isset($post['cc_number']))
              {
                $ccNumber = empty($payment->getCcNumber()) ? $post['cc_number'] : $payment->getCcNumber();
                $expMonth = empty($payment->getCcExpMonth()) ? $post['cc_exp_month'] : $payment->getCcExpMonth();
                $expYear  = empty($payment->getCcExpYear()) ? $post['cc_exp_year'] : $payment->getCcExpYear();
                $expDate  = sprintf('%04d-%02d', $expYear,$expMonth);
                $string .= '<creditCard>';
                $string .= '<cardNumber>'.$ccNumber.'</cardNumber>';
                $string .= '<expirationDate>'.$expDate .'</expirationDate>';
                if ($this->cvvEnabled) {
                    $ccId = empty($payment->getCcCid()) ? $post['cc_cid'] : $payment->getCcCid();
                    if(isset($ccId) && !empty($ccId)){
                        $string .= '<cardCode>'.$ccId.'</cardCode>';
                    }
                }
                $string .= '</creditCard>';
              }
              elseif($dataValue!='')
              {
                  $dataDescriptor = $payment->getAdditionalInformation('data_descriptor');;
                  $string .= '<opaqueData>';
                  $string .= '<dataDescriptor>'.$dataDescriptor.'</dataDescriptor>';
                  $string .= '<dataValue>'.$dataValue.'</dataValue>';
                  $string .= '</opaqueData>';
              }
              $string .= '</payment>';
              $string .= '<profile>';
              $string .= '<createProfile>'.$saveFlag.'</createProfile>';
              $string .= '</profile>';
              $string .= '<solution>';
              $string .= '<id>'.$this->solutionId.'</id>';
              $string .= '</solution>';
              $string .= '<order>';
              $string .= '<invoiceNumber>'.$payment->getOrder()->getIncrementId().'</invoiceNumber>';
              $string .= '<description>Product Description</description>';
              $string .= '</order>';
              if ($this->configModel->sendLineItems()) {
                $string .= $this->_prepareLineItems($payment);
              }
              $string .= '<tax>';
              $string .= '<amount>'.round($payment->getOrder()->getBaseTaxAmount(), 4).'</amount>';
              $string .= '</tax>';
              $string .= '<shipping>';
              $string .= '<amount>'.round($payment->getBaseShippingAmount(), 4).'</amount>';
              $string .= '</shipping>';
              $string .= '<customer>';
              $string .= '<id>'.$payment->getOrder()->getCustomerId().'</id>';
              $string .= '</customer>';
              $string .= '<billTo>';
              $string .= '<firstName>'.$billingAddress->getFirstname().'</firstName>';
              $string .= '<lastName>'.$billingAddress->getLastname().'</lastName>';
              $string .= '<company>'.$billingAddress->getCompany().'</company>';
              $billaddr = (is_array($billingAddress->getStreet()))
                    ?implode(",", $billingAddress->getStreet()):$billingAddress->getStreet();
              $string .= '<address>'.$this->removeSpecials($billaddr).'</address>';
              $string .= '<city>'.$billingAddress->getCity().'</city>';
              $string .= '<state>' . $billregionCode .'</state>';
              $string .= '<zip>'.$billingAddress->getPostcode().'</zip>';
              $string .= '<country>'.$billingAddress->getCountryId().'</country>';
              $string .= '</billTo>';
              $regionId  = $shippingAddress->getRegionId();
                if ($regionId) :
                     $regionModel->getResource()->load($regionModel, $regionId);
                     $regionCode = $regionModel->getCode();
                else :
                     $regionCode = $billingAddress->getRegion();
                endif;
              $string .= '<shipTo>';
              $string .= '<firstName>'.$shippingAddress->getFirstname().'</firstName>';
              $string .= '<lastName>'.$shippingAddress->getLastname().'</lastName>';
              $string .= '<company>'.$shippingAddress->getCompany().'</company>';
              $shipaddr  = (is_array($shippingAddress->getStreet()))
                    ?implode(",", $shippingAddress->getStreet()):$shippingAddress->getStreet();
              $string .= '<address>'.$this->removeSpecials($shipaddr).'</address>';
              $string .= '<city>'.$shippingAddress->getCity().'</city>';
              $string .= '<state>'.$regionCode.'</state>';
              $string .= '<zip>'.$shippingAddress->getPostcode().'</zip>';
              $string .= '<country>'.$shippingAddress->getCountryId().'</country>';
              $string .= '</shipTo>';
              $string .= '<customerIP>'.$this->_remoteAddress->getRemoteAddress().'</customerIP>';
              $string .= '<transactionSettings>';
              $string .= '<setting>';
              $string .= '<settingName>duplicateWindow</settingName>';
              $string .= '<settingValue>0</settingValue>';
              $string .= '</setting>';
              $string .= '</transactionSettings>';
              $string .= '</transactionRequest>';
              $string .= '</createTransactionRequest>';
        }
        else
        {
              $customerProfileid = $payment->getAdditionalInformation('md_customer_profile_id');
              $paymentProfileid = $payment->getAdditionalInformation('md_payment_profile_id');
              $string = '<?xml version="1.0" encoding="utf-8"?>';
              $string .= '<createTransactionRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">';
              $string .= '<merchantAuthentication>';
              $string .= '<name>'.$this->apiLogin.'</name>';
              $string .= '<transactionKey>'.$this->apiTransKey.'</transactionKey>';
              $string .= '</merchantAuthentication>';
              $string .= '<refId>'.$payment->getOrder()->getIncrementId().'</refId>';
              $string .= '<transactionRequest>';
              $string .= '<transactionType>authCaptureTransaction</transactionType>';
              $string .= '<amount>'.$amount.'</amount>';
              $string .= '<profile>';
              $string .= '<customerProfileId>'.$customerProfileid.'</customerProfileId>';
              $string .= '<paymentProfile>';
              $string .= '<paymentProfileId>'.$paymentProfileid.'</paymentProfileId>';
               if ($this->cvvEnabled) {
                    $ccId = empty($payment->getCcCid()) ? $post['cc_cid'] : $payment->getCcCid();
                    if(isset($ccId) && !empty($ccId)){
                        $string .= '<cardCode>'.$ccId.'</cardCode>';
                    }
              }
              $string .= '</paymentProfile>';
              $string .= '</profile>';
              $string .= '<solution>';
              $string .= '<id>'.$this->solutionId.'</id>';
              $string .= '</solution>';
              $string .= '<order>';
              $string .= '<invoiceNumber>'.$payment->getOrder()->getIncrementId().'</invoiceNumber>';
              $string .= '<description>Product Description</description>';
              $string .= '</order>';
              if ($this->configModel->sendLineItems()) {
                $string .= $this->_prepareLineItems($payment);
              }
              $string .= '<tax>';
              $string .= '<amount>'.round($payment->getOrder()->getBaseTaxAmount(), 4).'</amount>';
              $string .= '</tax>';
              $string .= '<shipping>';
              $string .= '<amount>'.round($payment->getBaseShippingAmount(), 4).'</amount>';
              $string .= '</shipping>';
              $string .= '<customer>';
              $string .= '<id>'.$payment->getOrder()->getCustomerId().'</id>';
              $string .= '</customer>';
              $regionId  = $shippingAddress->getRegionId();
                if ($regionId) :
                     $regionModel->getResource()->load($regionModel, $regionId);
                     $regionCode = $regionModel->getCode();
                else :
                     $regionCode = $billingAddress->getRegion();
                endif;
              $string .= '<shipTo>';
              $string .= '<firstName>'.$shippingAddress->getFirstname().'</firstName>';
              $string .= '<lastName>'.$shippingAddress->getLastname().'</lastName>';
              $string .= '<company>'.$shippingAddress->getCompany().'</company>';
              $shipaddr  = (is_array($shippingAddress->getStreet()))
                    ?implode(",", $shippingAddress->getStreet()):$shippingAddress->getStreet();
              $string .= '<address>'.$this->removeSpecials($shipaddr).'</address>';
              $string .= '<city>'.$shippingAddress->getCity().'</city>';
              $string .= '<state>'.$regionCode.'</state>';
              $string .= '<zip>'.$shippingAddress->getPostcode().'</zip>';
              $string .= '<country>'.$shippingAddress->getCountryId().'</country>';
              $string .= '</shipTo>';
              $string .= '<customerIP>'.$this->_remoteAddress->getRemoteAddress().'</customerIP>';
              $string .= '<transactionSettings>';
              $string .= '<setting>';
              $string .= '<settingName>duplicateWindow</settingName>';
              $string .= '<settingValue>0</settingValue>';
              $string .= '</setting>';
              $string .= '</transactionSettings>';
              $string .= '</transactionRequest>';
              $string .= '</createTransactionRequest>';
        }
        return $string;
    }


    /* end transaction request */

    /* create profile from transaction */
    public function createPaymentProfileFromTransactionRequest($customerProfileId,$transId)
    {

        $string = '<?xml version="1.0" encoding="utf-8"?>';
        $string .= '<createCustomerProfileFromTransactionRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">';
        $string .= '<merchantAuthentication>';
        $string .= '<name>'.$this->apiLogin.'</name>';
        $string .= '<transactionKey>'.$this->apiTransKey.'</transactionKey>';
        $string .= '</merchantAuthentication>';
        $string .= '<transId>'.$transId.'</transId>';
        $string .= '<customerProfileId>'.$customerProfileId.'</customerProfileId>';
        $string .= '</createCustomerProfileFromTransactionRequest>';
        return $string;
    }
    public function createPaymentProfileFromTransaction($customerProfileId,$transId)
    {
        $request = $this->createPaymentProfileFromTransactionRequest($customerProfileId,$transId);
        $response = $this->_postRequest($request, 'createCustomerPaymentProfile');
        return $response;
    }
    /* create profile from transaction */
    public function createCustPaymentProfileFromTransactionRequest($transId)
    {

        $string = '<?xml version="1.0" encoding="utf-8"?>';
        $string .= '<createCustomerProfileFromTransactionRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">';
        $string .= '<merchantAuthentication>';
        $string .= '<name>'.$this->apiLogin.'</name>';
        $string .= '<transactionKey>'.$this->apiTransKey.'</transactionKey>';
        $string .= '</merchantAuthentication>';
        $string .= '<transId>'.$transId.'</transId>';
        $string .= '</createCustomerProfileFromTransactionRequest>';
        return $string;
    }
    public function createCustPaymentProfileFromTransaction($transId)
    {
        $request = $this->createCustPaymentProfileFromTransactionRequest($transId);
        $response = $this->_postRequest($request, 'createCustomerPaymentProfile');
        return $response;
    }
    /* end create profile from transaction */
    /* transaction update */
    public function fetchTransInfo($transactionId)
    {
        $string = '<?xml version="1.0" encoding="utf-8"?>';
        $string .= '<getTransactionDetailsRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">';
        $string .= '<merchantAuthentication>';
        $string .= '<name>'.$this->apiLogin.'</name>';
        $string .= '<transactionKey>'.$this->apiTransKey.'</transactionKey>';
        $string .= '</merchantAuthentication>';
        $string .= '<transId>'.$transactionId.'</transId>';
        $string .= '</getTransactionDetailsRequest>';
        $response = $this->_postRequest($string);
        return $response;
    }
    public function acceptPaymentTrans($transactionId)
    {
        $string = '<?xml version="1.0" encoding="utf-8"?>';
        $string .= '<updateHeldTransactionRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">';
        $string .= '<merchantAuthentication>';
        $string .= '<name>'.$this->apiLogin.'</name>';
        $string .= '<transactionKey>'.$this->apiTransKey.'</transactionKey>';
        $string .= '</merchantAuthentication>';
        $string .= '<refId>'.$transactionId.'</refId>';
        $string .= '<heldTransactionRequest>';
        $string .= '<action>approve</action>';
        $string .= '<refTransId>'.$transactionId.'</refTransId>';
        $string .= '</heldTransactionRequest>';
        $string .= '</updateHeldTransactionRequest>';
        $response = $this->_postRequest($string);
        return $response;
    }
    public function denyPaymentTrans($transactionId)
    {
        $string = '<?xml version="1.0" encoding="utf-8"?>';
        $string .= '<updateHeldTransactionRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">';
        $string .= '<merchantAuthentication>';
        $string .= '<name>'.$this->apiLogin.'</name>';
        $string .= '<transactionKey>'.$this->apiTransKey.'</transactionKey>';
        $string .= '</merchantAuthentication>';
        $string .= '<refId>'.$transactionId.'</refId>';
        $string .= '<heldTransactionRequest>';
        $string .= '<action>decline</action>';
        $string .= '<refTransId>'.$transactionId.'</refTransId>';
        $string .= '</heldTransactionRequest>';
        $string .= '</updateHeldTransactionRequest>';
        $response = $this->_postRequest($string);
        return $response;
    }

    /* end transaction update */
    protected function _postRequest($request, $type = null)
    {
        $client = new \Magento\Framework\HTTP\ZendClient();
        $client->setUri($this->apiGatewayUrl);
        $client->setConfig(array('timeout' => 60));
        $client->setHeaders(array('Content-Type: text/xml'));
        $client->setMethod(\Zend_Http_Client::POST);
        $client->setRawData($request);
        $debug = array();
        try {
            $responseBody = $client->request()->getBody();
            libxml_use_internal_errors(true);
            $responseXmlDocument = new \Magento\Framework\Simplexml\Element($responseBody);
            libxml_use_internal_errors(false);
        } catch (\Exception $e) {
            throw new LocalizedException(__($e->getMessage()));
        }
        if ($this->getConfigModel()->getIsDebugEnabled()) {
            $replaceArray = array('transactionKey', 'customerProfileId', 'customerPaymentProfileId', 'cardNumber', 'expirationDate', 'cardCode','paymentProfileId','dataDescriptor','dataValue');
            foreach ($replaceArray as $replaceString) {
                $startStrlen = strlen('<'.$replaceString.'>');
                $startTagPos = strrpos($request, '<'.$replaceString.'>') + $startStrlen;
                $endTagPos = strrpos($request, '</'.$replaceString.'>');
                $tagLength = $endTagPos - $startTagPos;
                if ($tagLength > 0) {
                    $request = substr_replace($request, '****', $startTagPos, $tagLength);
                }
            }

            $debug['request'][$type] = $request;
            $debug['response'][$type] = $responseBody;
            $requestlog = str_replace('xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd"', '', $request);
            $requestlogobj = new \Magento\Framework\Simplexml\Element($requestlog);
            $dom            = new \DOMDocument();
            $requestXMLFile = $requestlogobj->asXML();
            $dom->loadXML($requestXMLFile);
            $dom->formatOutput          = true;
            $RequestXML = $dom->saveXML();
            $responseXMLFile = $responseXmlDocument->asXML();
            $responseXMLFileObj = str_replace('xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd"', '', $responseXMLFile);
            $dom->loadXML($responseXMLFileObj);
            $dom->formatOutput          = true;
            $ResponseXML = $dom->saveXML();
            $logger = new \Zend\Log\Logger();
            $this->cimlog = new \Zend\Log\Writer\Stream(BP.'/var/log/md_authorizecim.log');
            $logger->addWriter($this->cimlog);
            $logger->info("=========================Request===============");
            $logger->info("$RequestXML");
            $logger->info("=========================Response===============");
            $logger->info("$ResponseXML");
        }
        return $responseXmlDocument;
    }
}
