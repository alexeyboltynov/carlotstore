<?php
/**
 * Magedelight
 * Copyright (C) 2017 Magedelight <info@magedelight.com>.
 *
 * NOTICE OF LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://opensource.org/licenses/gpl-3.0.html.
 *
 * @category Magedelight
 *
 * @copyright Copyright (c) 2017 Mage Delight (http://www.magedelight.com/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Magedelight <info@magedelight.com>
 */

namespace Magedelight\Authorizecim\Model;

use Magento\Payment\Model\CcGenericConfigProvider;
use Magento\Payment\Helper\Data as PaymentHelper;
use Magento\Payment\Model\CcConfig;

class ConfigProvider extends CcGenericConfigProvider
{
    protected $methodCodes = [
        Payment::METHOD_CODE,
    ];

    protected $cards;

    /**
     *
     * @var Magedelight\Authorizecim\Model\Config
     */
    protected $config;

    /**
     *
     * @var Magento\Checkout\Model\Session\Proxy
     */
    protected $checkoutSession;

    /**
     *
     * @var Magento\Customer\Model\Session\Proxy
     */
    protected $customerSession;

    /**
     *
     * @var Magento\Customer\Model\CustomerFactory
     */
    protected $customerFactory;

    /**
     *
     * @var Magento\Framework\UrlInterface 
     */
    protected $urlBuilder;

    /**
     *
     * @var Magedelight\Authorizecim\Helper\Data
     */
    protected $dataHelper;

    /**
     *
     * @var Magento\Backend\Model\Session\Quote
     */
    protected $sessionquote;

    /**
     *
     * @var Magedelight\Authorizecim\Model\Api\Xml
     */
    protected $xmlApi;

    /**
     *
     * @var Magento\Payment\Model\Config
     */
    protected $paymentConfig;

     /**
     * @var Magedelight\Authorizecim\Model\CardsFactory
     */
    public $cardFactory;

    /**
     *
     * @var Magento\Framework\Encryption\EncryptorInterface
     */
    protected $encryptor;

    public function __construct(
            CcConfig $ccConfig,
            PaymentHelper $paymentHelper,
            \Magedelight\Authorizecim\Model\Config $config,
            \Magento\Checkout\Model\Session\Proxy $checkoutSession,
            \Magento\Customer\Model\Session\Proxy $customerSession,
            \Magento\Customer\Model\CustomerFactory $customerFactory,
            \Magento\Framework\UrlInterface $urlBuilder,
            \Magedelight\Authorizecim\Helper\Data $dataHelper,
            \Magento\Backend\Model\Session\Quote $sessionquote,
            \Magedelight\Authorizecim\Model\Api\Xml $cimXml,
            \Magento\Payment\Model\Config $paymentConfig,
            \Magedelight\Authorizecim\Model\CardsFactory $cardFactory,
            \Magento\Framework\Encryption\EncryptorInterface $encryptor,
            array $methodCodes = []
    ) {
        parent::__construct($ccConfig, $paymentHelper, $methodCodes);
        $this->config = $config;
        $this->checkoutSession = $checkoutSession;
        $this->customerSession = $customerSession;
        $this->customerFactory = $customerFactory;
        $this->urlBuilder = $urlBuilder;
        $this->dataHelper = $dataHelper;
        $this->sessionquote = $sessionquote;
        $this->cimXml = $cimXml;
        $this->paymentConfig = $paymentConfig;
        $this->cardFactory      = $cardFactory;
        $this->encryptor   = $encryptor;
    }

    /**
     * Returns applicable stored cards.
     *
     * @return array
     */
    public function getStoredCards()
    {
        $result = array();
        $cardData = [];
        $websiteId = $this->dataHelper->getWebsiteId();
        $customer_account_scope =  $this->dataHelper->getCustomerAccountScope();
        
        if ($this->dataHelper->checkAdmin()) {
            $customerId = $this->sessionquote->getQuote()->getCustomerId();
            $customerModel = $this->customerFactory->create();
            $customerModel->getResource()->load($customerModel,$customerId);
        } else {
            $customer = $this->customerSession->getCustomer();
            $customerId = $customer->getId();
        }

         if (!empty($customerId)) {
            $cardModel = $this->cardFactory->create();
            $cardData = $cardModel->getCollection()
                ->addFieldToFilter('customer_id', $customerId);
            if ($customer_account_scope) {
               $cardData->addFieldToFilter('website_id', $websiteId);
            }
            
            $cardData->getData();

            foreach ($cardData as $key => $_card) {
            $cardReplaced = 'XXXX-'.$_card['cc_last_4'];
              $result[$this->encryptor->encrypt($_card['payment_profile_id'])] = sprintf('%s, %s %s', $cardReplaced,
                      $_card['firstname'], $_card['lastname']);
            }
        }
        $result['new'] = 'Use other card';
        return $result;
    }

    protected function getCcAvailableCcTypes()
    {
        return $this->dataHelper->getCcAvailableCardTypes();
    }

    public function canSaveCard()
    {
        if (!$this->config->getSaveCardOptional() && $this->customerSession->isLoggedIn()) {
            return true;
        }

        return false;
    }

    /**
     * Retrieve credit card expire months.
     *
     * @return array
     */
    public function getCcMonths()
    {
        return $this->paymentConfig->getMonths();
    }

    /**
     * Retrieve credit card expire years.
     *
     * @return array
     */
    public function getCcYears()
    {
        return $this->paymentConfig->getYears();
    }

    public function show3dSecure()
    {
        return false;
    }

    public function getConfig()
    {
        if (!$this->config->getIsActive()) {
            return [];
        }
        $config = parent::getConfig();
        $config = array_merge_recursive($config, [
            'payment' => [
                \Magedelight\Authorizecim\Model\Payment::METHOD_CODE => [
                    'canSaveCard' => $this->canSaveCard(),
                    'storedCards' => $this->getStoredCards(),
                    'ccMonths' => $this->getCcMonths(),
                    'ccYears' => $this->getCcYears(),
                    'creditCardExpMonth' => (int) $this->dataHelper->getTodayMonth(),
                    'creditCardExpYear' => (int) $this->dataHelper->getTodayYear(),
                    'availableCardTypes' => $this->getCcAvailableCcTypes(),
                    'hasVerification' => $this->config->isCardVerificationEnabled(),
                    'canacceptjs' => $this->config->getAcceptJsEnable(),
                    'apiloginid' => $this->config->getApiLoginId(),
                    'clientkey' => $this->config->getClientKey()
                ],
            ],
        ]);

        return $config;
    }
}
