<?php
/**
 * Magedelight
 * Copyright (C) 2017 Magedelight <info@magedelight.com>.
 *
 * NOTICE OF LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://opensource.org/licenses/gpl-3.0.html.
 *
 * @category Magedelight
 *
 * @copyright Copyright (c) 2017 Mage Delight (http://www.magedelight.com/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Magedelight <info@magedelight.com>
 */

namespace Magedelight\Authorizecim\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        $table = $installer->getConnection()->newTable(
                $installer->getTable('md_authorizecim')
        )->addColumn(
            'card_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
             null,
             ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
             'Authorize CIM Card Primary Key'
        )->addColumn(
                'website_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                '10',
                ['unsigned' => true, 'nullable' => true],
                'Customer Website Id'
        )->addColumn(
             'customer_id',
             \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
             10,
             ['unsigned' => true, 'nullable' => true],
             'Authorize CIM Card Store Customer Id'
        )->addColumn(
             'customer_profile_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
             50,
             ['identity' => false, 'unsigned' => false, 'nullable' => false, 'primary' => false],
             'Authorize CIM Customer Profile Id'
        )->addColumn(
             'payment_profile_id',
             \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
             50,
             ['identity' => false, 'unsigned' => false, 'nullable' => false, 'primary' => false],
             'Authorize CIM Payment Profile Id'
        )->addColumn(
            'firstname',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            150,
            [],
            'Card Customer First Name'
        )->addColumn(
            'lastname',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            150,
            [],
            'Card Customer Last Name'
        )
        ->addColumn(
            'postcode',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            50,
            [],
            'PostCode'
        )
        ->addColumn(
            'country_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            10,
            [],
            'Country ID'
        )
        ->addColumn(
            'region_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '150',
            [],
            'Region ID'
        )
        ->addColumn(
            'state',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '150',
            [],
            'State'
        )
        ->addColumn(
            'city',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '150',
            [],
            'CustomerCity'
        )
        ->addColumn(
            'company',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '150',
            [],
            'Customer Company'
        )
        ->addColumn(
            'street',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '64k',
            [],
            'Customer Street'
        )
        ->addColumn(
            'telephone',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '50',
            [],
            'Customer Telephone'
        )
        ->addColumn(
            'cc_exp_month',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '12',
            [],
            'Card Exp Month'
        )
        ->addColumn(
            'cc_last_4',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '100',
            [],
            'Card Last Four Digit'
        )
        ->addColumn(
            'cc_type',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '32',
            [],
            'Card Type'
        )
        ->addColumn(
            'cc_exp_year',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '4',
            [],
            'Card Exp Year'
        )
        ->addColumn(
            'created_at',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
            'Card Creation Time'
        )->addColumn(
            'updated_at',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
            'Card Modification Time'
        )
        ->addForeignKey(// pri col name         //ref tabname     //ref cou name
            'AUTHORIZECIM_CUSTOMER_ID',
            'customer_id', // table column name
            $installer->getTable('customer_entity'), // ref table name
            'entity_id', // ref column name
            \Magento\Framework\DB\Ddl\Table::ACTION_SET_NULL  // on delete
        )->addIndex(
            $setup->getIdxName(
                $installer->getTable('md_authorizecim'),
                ['customer_profile_id', 'firstname'],
                AdapterInterface::INDEX_TYPE_FULLTEXT
            ),
            ['customer_profile_id', 'firstname'],
            ['type' => AdapterInterface::INDEX_TYPE_FULLTEXT]
        );
        $installer->getConnection()->createTable($table);
        $installer->endSetup();
    }
}
