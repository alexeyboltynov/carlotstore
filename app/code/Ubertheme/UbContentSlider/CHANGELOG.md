1.1.2
===== 
* Fix bugs and improvements:
    * Fixed issues: 
        - Issue #19 (https://www.ubertheme.com/question/how-to-disable-slider/)
        - https://www.ubertheme.com/question/slider-does-not-get-loaded-in-mobile-view/
    * Tested and fixed compatible with Magento CE 2.2.5
    
1.1.1
===== 
* Fix bugs and improvements:
    * Fixed issues: #18
    * Tested and fixed compatible with Magento CE 2.2.2
    
1.1.0
========= 
* Fix bugs and improvements:
    * Fixed issues: #13, #15, #16
    * Tested and fixed compatible with Magento CE 2.2.0
    
1.0.9
===== 
* Fixed bugs:
    * Fixed issue #11
    * Tested and fixed compatible with Magento CE 2.1.0, CE 2.1.1

1.0.8
===== 
* Tuning to improve performance. (For resubmit to Magento.com only)
    
1.0.7
=====
* Added Features:
    * Allow setting sort field, sort order in case slider with content type is products
    * Allow setting publish time, end publish time when add/edit a slide item
    * Fixed some CSS issues
    
1.0.6
=============
* Added Features:
    * Supported widgets configuration: Allow settings and showing a slider any where in theme via widget module manager.
    * Allow enable/disable block/widget title
    
1.0.5
=============
* Fixed:
    * Added CHANGELOG.md
    * Tested compatible with Magento 2 CE 2.0.1
    * Tested compatible with Magento 2 CE 2.0.2
    
1.0.4
=============
* Fixed:
    * Improvement the default style CSS
    * Supported addition_class param. Included two addition class CSS: ub-style-1, ub-style-2
    * Allow to on/off slide title/desc in case slider is slider images uploaded
    
1.0.3
=============
* Fixed:
    * Tuning to get config value to support multiple websites, stores config
    * Tuning to show product rating in list
    * Tuning html markup and style
    
1.0.2
=============
* Fixed:
    * Fixed compatible with Magento2 CE 2.0.0

