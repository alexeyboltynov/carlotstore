<?php
/**
 * Copyright © 2016 Ubertheme.com All rights reserved.
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Ubertheme_UbContentSlider',
    __DIR__
);
