<?php

namespace Mangoit\CustomReviewSection\Block;

class CustomReview extends \Magento\Framework\View\Element\Template
{
    protected $_reviewsColFactory;
    protected $_objectReview;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Review\Model\ReviewFactory $_reviewFactory,
        \Magento\Framework\Registry $registry,
        \Magento\Review\Model\Review $reviewFactory,
        array $data = []
    )
    {
        $this->_objectReview =  $reviewFactory;
        $this->_registry = $registry;
        $this->_reviewFactory = $_reviewFactory;
        parent::__construct($context, $data);
    }
    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }
    
    public function getCurrentProduct()
    {        
        return $this->_registry->registry('current_product');
    }    
    
    public function getRatingSummary($product)
    {
       $this->_reviewFactory->create()->getEntitySummary($product, $this->_storeManager->getStore()->getId());
       $ratingSummary = $product->getRatingSummary()->getRatingSummary();
       return $ratingSummary;
    }
    public function getAllStart($pid) {
    $review = $this->_objectReview->getCollection()     //\Magento\Review\Model\Review $reviewFactory (_objectReview)
            ->addFieldToFilter('main_table.status_id', 1)
            ->addEntityFilter('product', $pid)          //$pid = > your current product ID
            ->addStoreFilter($this->_storeManager->getStore()->getId())
            ->addFieldToSelect('review_id')
    ;
    $review->getSelect()->columns('detail.detail_id')->joinInner(
            ['vote' => $review->getTable('rating_option_vote')], 'main_table.review_id = vote.review_id', array('review_value' => 'vote.value')
    );
    $review->getSelect()->order('review_value DESC');
    $review->getSelect()->columns('count(vote.vote_id) as total_vote')->group('review_value');
    for ($i = 5; $i >= 1; $i--) {
        $arrRatings[$i]['value'] = 0;
    }
    foreach ($review as $_result) {
        $arrRatings[$_result['review_value']]['value'] = $_result['total_vote'];
    }
    return $arrRatings;
    }
}