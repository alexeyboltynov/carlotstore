<?php
namespace Mangoit\ReplacementAccessoryPage\Block;
 
class Upsell extends \Magento\Framework\View\Element\Template
{
    protected $_registry;
    protected $_storeManager;
    protected $request;
        
    public function __construct(
        \Magento\Backend\Block\Template\Context $context, 
        \Magento\Framework\App\Request\Http $request,       
        \Magento\Framework\Registry $registry,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        array $data = []
    )
    {        
        $this->_registry = $registry;
        $this->_storeManager = $storeManager;
        $this->request = $request;
        parent::__construct($context, $data);
    }

    public function getCurrentProductDetails()
    {        
        $file_name = '';
        $productData = $this->_registry->registry('current_product');
        $id = $productData->getEntityId();
        return $id;
    } 

    public function getStoreManagerData()
    {    
        $baseUrl = $this->_storeManager->getStore()->getBaseUrl();
        return $baseUrl;
    } 
    public function getParamData() {
        $param = $this->request->getParams();
        return $param['param'];
    }
    public function getProductCollection($id)
    {
        $id_array=array();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('catalog_product_link');
        $sql = "Select linked_product_id FROM " . $tableName." "."Where product_id = ".$id." And  link_type_id=6 ";
        $result = $connection->fetchAll($sql); 
        foreach ($result as $key => $value) {
            $id_array[] = $value["linked_product_id"];
        }
        return $id_array;
    }
}

