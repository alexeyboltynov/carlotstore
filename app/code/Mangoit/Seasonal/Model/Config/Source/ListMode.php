<?php 
namespace Mangoit\Seasonal\Model\Config\Source;
class ListMode implements \Magento\Framework\Option\ArrayInterface
{
    protected $sliderFactory;
    public function __construct(
		\Ecomteck\ProductSlider\Model\ResourceModel\ProductSlider\Collection $sliderFactory
	)
	{
		$this->sliderFactory = $sliderFactory;
	}
	public function toOptionArray()
	{
	    $slider_array = $result =  array();
		$slider_data = $this->sliderFactory;
		foreach ($slider_data->getData() as $key => $value) {
			$slider_array['value'] = $value['slider_id'];
			$slider_array['label'] = $value['title'];
			$result[] = $slider_array;
		}
	    return $result;
	}
}