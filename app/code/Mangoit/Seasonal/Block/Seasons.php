<?php

namespace Mangoit\Seasonal\Block;

class Seasons extends \Magento\Catalog\Block\Product\AbstractProduct {

    protected $_catalogProductVisibility;

    protected $_productCollectionFactory;
    protected $scopeConfig;
    protected $sliderFactory;

    protected $_categoryFactory;

    protected $urlHelper;

    const XML_PATH_SEASON = 'seasonal/general/enable';

    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Ecomteck\ProductSlider\Model\ResourceModel\ProductSlider\Collection $sliderFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Framework\Url\Helper\Data $urlHelper,
        array $data = []
    ) {
        $this->_categoryFactory = $categoryFactory;
        $this->scopeConfig = $scopeConfig;
        $this->sliderFactory = $sliderFactory;
        $this->_checkoutSession = $checkoutSession;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_catalogProductVisibility = $catalogProductVisibility;
        $this->urlHelper = $urlHelper;
        parent::__construct($context, $data);
    }
    public function getSeasonalProducts($id_array) {
        $count = $this->getProductCount();
        $collection = $this->_productCollectionFactory->create();
        $collection = $this->_addProductAttributesAndPrices($collection)->addStoreFilter()->addIdFilter($id_array);
        $collection->addMinimalPrice()
                ->addFinalPrice()
                ->addTaxPercents()
                ->addAttributeToSelect('name')
                ->addAttributeToSelect('image')
                ->addAttributeToSelect('small_image')
                ->addAttributeToSelect('thumbnail')
                ->addAttributeToSelect($this->_catalogConfig->getProductAttributes())
                ->addUrlRewrite()
                ->addAttributeToFilter('is_saleable', 1, 'left');
        $collection->getSelect()
            ->order('rand()')
            ->limit($count);

        return $collection;
    }
    
    public function getProductCount() {
        $limit = $this->getData("product_count");
        if(!$limit)
            $limit = 10;
        return $limit;
    }
    public function getSeasons()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('ecomteck_productslider_products'); 
        $product_array = array();
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $slider_id = $this->scopeConfig->getValue(self::XML_PATH_SEASON, $storeScope);
        $sql = "Select * FROM " . $tableName ." Where `slider_id` =".$slider_id;
        $result = $connection->fetchAll($sql); 
        $table = $resource->getTableName('ecomteck_productslider'); 
        $query = "Select * FROM " . $table ." Where `slider_id` =".$slider_id;
        $img_result = $connection->fetchAll($query);
        $image = $img_result[0]['slider_image'];
        $product_array['image'] = $image;
        foreach ($result as $key => $value) {
            $product_array['products'][] = $value['product_id'];
        }
        return $product_array;
    }
}
