<?php
namespace Mangoit\CategoryPage\Block;
class Category extends \Magento\Framework\View\Element\Template
{
	protected $_registry;
	public function __construct(\Magento\Framework\View\Element\Template\Context $context,\Magento\Framework\Registry $registry)
	{
		$this->_registry = $registry;
		parent::__construct($context);
	}

	public function getcurrentCategory()
	{
		$category = $this->_registry->registry('current_category');
	    return $category;
	}
	public function getCategoryCollection($entity_id,$limit)
	{
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$category = $this->_registry->registry('current_category');
		$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$baseUrl = $storeManager->getStore()->getBaseUrl('media');   		
        $categoryFactory = $objectManager->create('Magento\Catalog\Model\ResourceModel\Category\CollectionFactory');
        $categories = $categoryFactory->create()
        ->addAttributeToSelect('*')
        ->addAttributeToFilter('parent_id',$entity_id)
        ->setStore($storeManager->getStore())->setPageSize($limit)->setOrder('position','ASC');

		$categoryInfo = [];
        foreach ($categories as $category){
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $catObj = $objectManager->create('Magento\Catalog\Model\Category');
            $categorycs = $catObj->load($category->getId());
            $childCategory=$categorycs->getChildrenCategories();

            $product_count = $categorycs->getProductCollection()->addAttributeToSelect('*')->count();
            $name = $categorycs->getName();
            $url = $categorycs->getUrl();
            $imageUrl = $categorycs->getImageUrl();
            $image = $imageUrl;
            $desription = $categorycs->getDescription();
            $Template = $categorycs->getTemplateType();
            $postion = $categorycs->getCategoryPosition();
            $short_description = $categorycs->getShortDescription();
            $categoryInfo[] = array(
                'name' => $name,
                'url' => $url,
                'image' => $image,
                'description' => $desription,
                'children_count' => $childCategory->count(),
                'entity_id' => $categorycs->getEntityId(),
                'template' => $Template,
                'product_count' => $product_count,
                'cat_position' => $postion,
                'short_description' => $short_description
            );     
        }	
        return $categoryInfo;
	}

    public function getOptionLabelText($optionId) { 
        $optionArray = [];
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $attribute = $objectManager->create('Magento\Eav\Model\Config')->getAttribute('catalog_category', 'category_position');
            if ($attribute->usesSource()) {
                $options = $attribute->getSource()->getAllOptions(false);
                foreach ($options as $option) {
                    $vars = get_object_vars ($option['label']);
                    $optionArray[$option['value']] = $option['label'];
                }
            }
        $categoryLabel = (array)$optionArray[$optionId]->getText();
        $option_text = $categoryLabel[0];
        return $option_text;
    }
}