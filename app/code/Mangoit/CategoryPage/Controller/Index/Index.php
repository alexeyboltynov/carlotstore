<?php
namespace Mangoit\CategoryPage\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action
{
	protected $_pageFactory;
	private $productRepository; 
	public $_storeManager;

	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		\Magento\Framework\View\Result\PageFactory $pageFactory)
	{
		$this->_pageFactory = $pageFactory;
		$this->productRepository = $productRepository;
		$this->_storeManager = $storeManager;
		return parent::__construct($context);
	}

	public function execute()
	{
		
		$post = $this->getRequest()->getPostValue();
		$sku = $post['sku'];
		$final_html = $label = '';
		$product_data = $this->productRepository->get($sku);
		$good = $product_data->getGood();
		$better = $product_data->getBetter();
		$best = $product_data->getBest();
		$label ='good';
		//$additionalClass = 'new-good';
		$final_html .= $this->getProductDetails($good,$label);
		$label ='better';
		//$additionalClass = 'new-better';
		$final_html .= $this->getProductDetails($better,$label);
		$label ='best';
		$additionalClass = 'new-best';
		
		$final_html .= $this->getProductDetails($best,$label);
		echo $final_html;
	}

	public function getProductDetails($sku,$label) {
		if($label == "best")
		{
			$new = "new-best";
		}
		if($label == "good")
		{
			$new = "new-good";
		}
		if($label == "better")
		{
			$new = "new-better";
		}
		$html = '';
		$product_data = $this->productRepository->get($sku);
		if($product_data->getShortDescription()) {
			$description = $product_data->getShortDescription();
		} else {
			$description = '';
		}
		$price = $product_data->getPrice();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $objectManager->get('Magento\Store\Model\StoreManagerInterface');
        $currencyFactory = $objectManager->create('Magento\Directory\Model\CurrencyFactory');
        $currencyCode = $storeManager->getStore()->getCurrentCurrencyCode();
        $currency = $currencyFactory->create()->load($currencyCode);
        $currencySymbol = $currency->getCurrencySymbol();
        $price = number_format($price, 2, '.', '');
        //$new='new';
		$name = $product_data->getName();
		$mediaUrl = $this ->_storeManager-> getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA );
		if($product_data->getImage() && file_exists($mediaUrl.'catalog/product'.$product_data->getImage())){
			$image = $mediaUrl.'catalog/product'.$product_data->getImage();
			$pro_img = $objectManager->create('Mangoit\ResizedImage\Helper\Image')->popupImageResize($image,70,70);
		} else {
			$pro_img = $mediaUrl."catalog/category/default2.jpg";
		}
		//$label ='testttt';
		
		$url = $product_data->getProductUrl();
		$html = '<div class="popup_item"><div class="product_image"><a href="'.$url.'"><img src="'.$pro_img.'"></img></a></div><div class="details"><div class="label_text '.$new.'"><span class="quality_text">'.$label.'</span></div><a href="'.$url.'">'.$name.'</a><div class="pricing"><p class="price">'.$currencySymbol.$price.'</p>
      	</div></div><div class="text">'.$description.'</div></div>';
      	return $html;
	}
}
