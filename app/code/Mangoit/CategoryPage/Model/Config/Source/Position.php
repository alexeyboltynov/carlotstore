<?php


namespace Mangoit\CategoryPage\Model\Config\Source;

class Position extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    /**
     * getAllOptions
     *
     * @return array
     */
    public function getAllOptions()
    {
        if ($this->_options === null) {
            $this->_options = [
                ['value' => '0', 'label' => __('Select an option')],
                ['value' => '1', 'label' => __('Full Vertical with 8 categories')],
                ['value' => '2', 'label' => __('Full Vertical with 6 categories')],
                ['value' => '3', 'label' => __('Full Vertical with 5 categories')],
                ['value' => '4', 'label' => __('Half Vertical with 2 categories')],
                ['value' => '5', 'label' => __('Half Vertical with 3 categories')],
                ['value' => '6', 'label' => __('Half Vertical with 4 categories')],
                ['value' => '7', 'label' => __('Half Vertical with 5 categories')],
                ['value' => '8', 'label' => __('Half Vertical with 6 categories')],
                ['value' => '9', 'label' => __('Full Horizontal with 6 categories')],
                ['value' => '10', 'label' => __('Full Horizontal with 3 categories')]
            ];
        }
        return $this->_options;
    }
}