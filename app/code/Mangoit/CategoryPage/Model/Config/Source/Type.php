<?php


namespace Mangoit\CategoryPage\Model\Config\Source;

class Type extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    /**
     * getAllOptions
     *
     * @return array
     */
    public function getAllOptions()
    {
        if ($this->_options === null) {
            $this->_options = [
                ['value' => '0', 'label' => __('Select an option')],
                ['value' => '1', 'label' => __('No Subcategories')],
                ['value' => '2', 'label' => __('Two Subcategories')],
                ['value' => '3', 'label' => __('Three Subcategories')],
                ['value' => '4', 'label' => __('Four Subcategories')],
                ['value' => '5', 'label' => __('Five Subcategories')] ,
                ['value' => '6', 'label' => __('Six Subcategories')]                
            ];
        }
        return $this->_options;
    }
}