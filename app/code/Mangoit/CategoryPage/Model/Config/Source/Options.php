<?php


namespace Mangoit\CategoryPage\Model\Config\Source;

class Options extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    /**
     * getAllOptions
     *
     * @return array
     */
    public function getAllOptions()
    {
        if ($this->_options === null) {
            $this->_options = [
                ['value' => '0', 'label' => __('Select an option')],
                ['value' => '2', 'label' => __('Enable')],
                ['value' => '1', 'label' => __('Disable')]
            ];
        }
        return $this->_options;
    }
}