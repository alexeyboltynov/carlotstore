<?php
 
namespace Mangoit\CategoryPage\Data;
 
class Collection extends \Magento\Framework\Data\Collection
{
    public function addItem(\Magento\Framework\DataObject $item)
    {
        $itemId = $this->_getItemId($item);
        if ($itemId !== null) {
            if (isset($this->_items[$itemId])) {
            return $this;
            }
            $this->_items[$itemId] = $item;
        } else {
            $this->_addItem($item);
        }
        return $this;
    }
}