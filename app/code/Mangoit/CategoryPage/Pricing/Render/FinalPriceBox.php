<?php
    /**
     * Hello Rewrite Product ListProduct Block
     *
     * @category    Webkul
     * @package     Webkul_Hello
     * @author      Webkul Software Private Limited
     *
     */
namespace Mangoit\CategoryPage\Pricing\Render;

use Magento\Catalog\Pricing\Price;
use Magento\Framework\Pricing\Render\PriceBox as BasePriceBox;
use Magento\Msrp\Pricing\Price\MsrpPrice;
use Magento\Catalog\Model\Product\Pricing\Renderer\SalableResolverInterface;
use Magento\Framework\Pricing\SaleableInterface;
use Magento\Framework\Pricing\Price\PriceInterface;
use Magento\Framework\Pricing\Render\RendererPool;
use Magento\Framework\App\ObjectManager;
use Magento\Catalog\Pricing\Price\MinimalPriceCalculatorInterface;

 
class FinalPriceBox extends \Magento\Catalog\Pricing\Render\FinalPriceBox {
    protected function wrapResult($content_html)
        {   
            $tier_array = $this->getSaleableItem()->getTierPrice();
            $html = '';
            if(!empty($tier_array)) {
                $html.= '<div class="price-box ' . $this->getData('css_classes') . '" ' .
                'data-role="priceBox" ' .
                'data-product-id="' . $this->getSaleableItem()->getId() . '" ' .
                'data-price-box="product-id-' . $this->getSaleableItem()->getId() . '"' .
                '>' . $content_html . '</div>';
                $html .= '<table class="table table-bordered table-condensed" border="1"><thead><tr>';
                $i = $j = 0;
                foreach ($tier_array as $key => $value) {
                    if($i < 3) {
                        $html .= '<th>'.(int)$value['price_qty'].'</th>';
                        $i++;
                    }
                }
                $html .= '</tr><tr>';
                foreach ($tier_array as $key => $value) {
                    if($j < 3) {
                        $html .= '<td>$'.(int)$value['website_price'].'</td>';
                        $j++;
                    }
                }
                $html .= '</tr></thead></table>';
            } else {
                $html .= '<div class="price-box ' . $this->getData('css_classes') . '" ' .
                'data-role="priceBox" ' .
                'data-product-id="' . $this->getSaleableItem()->getId() . '" ' .
                'data-price-box="product-id-' . $this->getSaleableItem()->getId() . '"' .
                '>' . $content_html . '</div>';
            }
            return $html;
        }


}