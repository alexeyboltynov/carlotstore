<?php
    /**
     * @package     Mangoit
     * @module      Category
     */
namespace Mangoit\Category\Setup;
class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{
    public function install(\Magento\Framework\Setup\SchemaSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
	{
	    $installer = $setup;
		$installer->startSetup();
		if (!$installer->tableExists('category_order')) {
			$table = $installer->getConnection()->newTable(
				$installer->getTable('category_order')
			)
			    ->addColumn(
					'id',
					\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
					null,
					[
						'identity' => true,
						'nullable' => false,
						'primary'  => true,
						'unsigned' => true,
					],
					'ID'
				)
			    ->addColumn(
					'category_id',
					\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
					null,
					[
						'nullable' => false,
						'unsigned' => true,
					],
					'Category ID'
				)
				->addColumn(
					'category_name',
					\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					255,
					['nullable => false'],
					'Category Name'
				)
				->addColumn(
					'show_on_home',
					\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
					null,
					['nullable => false'],
					'Show ON Home Page'
				)
				->addColumn(
					'cat_order',
					\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
					null,
					['nullable => false',
					  'default' => '0',
					],
					'Order'
				)
				->setComment('Category Order');
			$installer->getConnection()->createTable($table);
		}
		
		$installer->endSetup();
	}
}