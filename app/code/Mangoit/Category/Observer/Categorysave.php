<?php
namespace Mangoit\Category\Observer;

class Categorysave implements \Magento\Framework\Event\ObserverInterface
{
    private $category = null;
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
    	$category = $observer->getEvent()->getCategory();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $model = $objectManager->get('Mangoit\Category\Model\ResourceModel\CategoryOrder\Collection');
        $category_name = $category->getName();
        $cat_id = $category->getId();
        $show_on_home = $category->getShowCatOnHome();
        $collection = $model->addFieldToFilter('category_id',$cat_id);
        $catModel = $objectManager->get('Mangoit\Category\Model\CategoryOrder');        
        $ar = $collection->getData();
        if($show_on_home == 1) {
            if(empty($ar))
            {
               //save
                $catModel->setCategoryId($cat_id);
                $catModel->setCategoryName($category_name);
                $catModel->setShowOnHome($show_on_home);
                $catModel->save();
            }
            else
            {
                //$logger->info(print_r($ar, true));
                $new_order = $ar[0]['cat_order'];
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $update = $objectManager->get('Mangoit\Category\Model\ResourceModel\CategoryOrder\Collection');
                $col = $update->addFieldToFilter('category_id',$cat_id);
                $data = $col->getData();

                $id = '';
                foreach ($data as $ar) {
                    $id = $ar['id'];
                    break;
                }
                $saveModel = $objectManager->get('Mangoit\Category\Model\CategoryOrderFactory')->create()->load($id);
                //$saveModel->delete();
                 
                $saveModel->setCategoryId($cat_id);
                $saveModel->setCategoryName($category_name);
                $saveModel->setShowOnHome($show_on_home);
                $saveModel->setCatOrder($new_order);
                $saveModel->save();
            }
        }
        else
        {
            //delet existing category when already saved
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $update = $objectManager->get('Mangoit\Category\Model\ResourceModel\CategoryOrder\Collection');
            $col = $update->addFieldToFilter('category_id',$cat_id);
            $data = $col->getData();

            $id = '';
            foreach ($data as $ar) {
                $id = $ar['id'];
                break;
            }
            $saveModel = $objectManager->get('Mangoit\Category\Model\CategoryOrderFactory')->create()->load($id);
            $saveModel->delete();
        }
    }
}