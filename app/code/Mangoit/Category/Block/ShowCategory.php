<?php
namespace Mangoit\Category\Block;
use Magento\Framework\Data\Tree\Node;
use Magento\Framework\Data\Tree\NodeFactory;
use Magento\Framework\Data\TreeFactory;
use Magento\Framework\DataObject\IdentityInterface;

class ShowCategory extends \Magento\Framework\View\Element\Template
{
	public $_scopeConfig;
	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
		)
	{	
		$this->_scopeConfig = $scopeConfig;
		parent::__construct($context);
	}

	
	public function getCategoryOrder()
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$category = $objectManager ->create('Magento\Catalog\Model\ResourceModel\Category\Collection');
		$category->addAttributeToSelect('*')->addAttributeToFilter('show_cat_on_home',['eq'=>1])->setPageSize(12);
		$data = $category->getData();
		return $data;
	}

	public function getCategoryCollection($id){
		
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$category = $objectManager->create('Magento\Catalog\Model\Category')->load($id);
        $data = $category->getData();
        return $data;
   	}
   	public function getCategoryExtension()
   	{
   		$default_url = $this->_scopeConfig->getValue('catalog/seo/category_url_suffix', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		return $default_url;
   	}	
}