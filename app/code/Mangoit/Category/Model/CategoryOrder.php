<?php
namespace Mangoit\Category\Model;
class CategoryOrder extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
	const CACHE_TAG = 'category_order';

	protected $_cacheTag = 'category_order';

	protected $_eventPrefix = 'category_order';

	protected function _construct()
	{
		$this->_init('Mangoit\Category\Model\ResourceModel\CategoryOrder');
	}

	public function getIdentities()
	{
		return [self::CACHE_TAG . '_' . $this->getId()];
	}

	public function getDefaultValues()
	{
		$values = [];

		return $values;
	}
}