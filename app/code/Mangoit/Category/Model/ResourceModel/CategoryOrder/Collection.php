<?php

namespace Mangoit\Category\Model\ResourceModel\CategoryOrder;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Mangoit\Category\Model\CategoryOrder', 'Mangoit\Category\Model\ResourceModel\CategoryOrder');
    }
}
