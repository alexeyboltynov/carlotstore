<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Mangoit\Category\Controller\Adminhtml\Category;

class Delete extends \Magento\Catalog\Controller\Adminhtml\Category
{
    /**
     * @var \Magento\Catalog\Api\CategoryRepositoryInterface
     */
    protected $categoryRepository;
    protected $_categoryOrder;
    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository,
        \Mangoit\Category\Model\CategoryOrder $categoryOrder
    ) {
        $this->_categoryOrder = $categoryOrder;
        parent::__construct($context);
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Delete category action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        $categoryId = (int)$this->getRequest()->getParam('id');
        $parentId = null;
        if ($categoryId) {
            try {
                $category = $this->categoryRepository->get($categoryId);
                $parentId = $category->getParentId();
                $this->_eventManager->dispatch('catalog_controller_category_delete', ['category' => $category]);
                $this->_auth->getAuthStorage()->setDeletedPath($category->getPath());
                $this->categoryRepository->delete($category);
                $model = $this->_categoryOrder->getCollection()->addFieldToFilter('category_id',$categoryId);
                if(!empty($model->getData()) ){
                    $data = $model->getData();
                    $idCategory = $data[0]['id'];
                    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                    $deleteModel = $objectManager->get('Mangoit\Category\Model\CategoryOrderFactory')->create()->load($idCategory);
                    $deleteModel->delete();
                }
                $this->messageManager->addSuccessMessage(__('You deleted the category.'));
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('catalog/*/edit', ['_current' => true]);
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(__('Something went wrong while trying to delete the category.'));
                return $resultRedirect->setPath('catalog/*/edit', ['_current' => true]);
            }
        }
        return $resultRedirect->setPath('catalog/*/', ['_current' => true, 'id' => $parentId]);
    }
}
