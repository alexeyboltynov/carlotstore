<?php
namespace Mangoit\CustomerRegister\Observer;

class Afterregister implements \Magento\Framework\Event\ObserverInterface
{

	public function execute(\Magento\Framework\Event\Observer $observer)
	{
		$customer = $observer->getEvent()->getCustomer();
        $billing_id = $customer->getDefaultBilling();
		$customerId = $customer->getId();
	  	$customerData = $_POST;   
	  	$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); /*To set Customer default shipping address*/
	  	$address = $objectManager->create('\Magento\Customer\Model\Address');

	  	if($customerData['default_shipping'] === 0) {
	  		$address->setFirstname($customerData['firstname']);
            $address->setLastname($customerData['lastname']);
            if(isset($customerData['shipping_country_id']) && !empty($customerData['shipping_country_id']))
            $address->setCountryId($customerData['shipping_country_id']);
            if(isset($customerData['shipping_city']))
            $address->setCity($customerData['shipping_city']);  
            if(isset($customerData['shipping_postcode']))
            $address->setPostcode($customerData['shipping_postcode']);
            if(isset($customerData['shipping_telephone']))
            $address->setTelephone($customerData['shipping_telephone']);
            if(isset($customerData['shipping_street']))
            $address->setStreet($customerData['shipping_street']);
            if(isset($customerData['shipping_region_id']))
            $address->setRegionId($customerData['shipping_region_id']);
            if(isset($customerData['shipping_region']) && !empty($customerData['shipping_region'])) {
                $address->setRegion($customerData['shipping_region']);
            }
            if(isset($customerData['shipping_company']) && !empty($customerData['shipping_company'])) {
                $address->setCompany($customerData['shipping_company']);
            }
            $address->setCustomerId($customerId); 
            $address->save();
            $addressId = $address->getId();
            $this->setCustomerDefaultAddress($customerId,$addressId,$objectManager); 
	  	}
        else {
            $this->setCustomerDefaultAddress($customerId,$billing_id,$objectManager); 
        }
	}

	private function setCustomerDefaultAddress($customerId, $addressId,$objectManager) {
        $customerFactory = $objectManager->create('Magento\Customer\Model\Customer');
        $customerFactory->load($customerId);
        $customerFactory->setData('default_shipping', $addressId);
        $customerFactory->save();
    }
}