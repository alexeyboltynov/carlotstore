<?php

namespace Mangoit\CustomerRegister\Block;

use Magento\Framework\View\Element\Template;

class RegisterLink extends \Magento\Customer\Block\Account\RegisterLink

{
    protected function _toHtml()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->get('Magento\Customer\Model\Session');
        if($customerSession->isLoggedIn()) {
            return null;
        }
        else
        {
            return '<li class="register-link"><a ' . $this->getLinkAttributes() . ' ><span class="register_icon"></span>Register</a></li>';
        }
    }
}