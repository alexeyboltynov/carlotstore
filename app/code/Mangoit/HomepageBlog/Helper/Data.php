<?php

namespace Mangoit\HomepageBlog\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $_request;

    public function __construct
    (
        \Magento\Framework\App\Request\Http $request
    ) {
        $this->_request = $request;
    }

    public function getLogo()
    {
        if ($this->_request->getFullActionName() == 'cms_index_index') {
            $logo =  'pdf.png';
        } else {
            $logo =  'pdf.png';
        }

        return $logo;
    }
} 