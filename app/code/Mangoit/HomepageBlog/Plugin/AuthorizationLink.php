<?php
 
namespace Mangoit\HomepageBlog\Plugin;
 
class AuthorizationLink
{                
    public function afterGetLabel(\Magento\Customer\Block\Account\AuthorizationLink $subject, $result)
    {                    
        return $subject->isLoggedIn() ? __('Logout') : __('Login');
    }    
}
?>