<?php
namespace Mangoit\ResizedImage\Helper;
use Magento\Framework\Filesystem;
use Magento\Framework\Url;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Filesystem\DirectoryList;
class Image extends \Magento\Framework\App\Helper\AbstractHelper {
    protected $scopeConfig;
    protected $storeManager;
    protected $messageManager;
    protected $_response;
    protected $_resourceConfig;
    protected $_responseFactory;
    protected $_url;
    protected $_filesystem;
    protected $_directory;
    protected $_imageFactory;
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\App\ResponseInterface $response,
        \Magento\Framework\App\Config\Storage\WriterInterface $resourceConfig,
        \Magento\Framework\App\ResponseFactory $responseFactory,
        \Magento\Framework\UrlInterface $url,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\Image\AdapterFactory $imageFactory
    )
    {
        $this->scopeConfig = $scopeConfig;
        $this->_storeManager=$storeManager;
        $this->messageManager=$messageManager;
        $this->_response=$response;
        $this->_resourceConfig=$resourceConfig;
         $this->_responseFactory = $responseFactory;
        $this->_url = $url;
        $this->_filesystem = $filesystem;
        $this->_directory = $filesystem->getDirectoryWrite(DirectoryList::MEDIA);
        $this->_imageFactory = $imageFactory;
    }

    public function startsWith($string, $startString) 
    { 
        $len = strlen($startString); 
        return (substr($string, 0, $len) === $startString); 
    } 

    public function imageResize(
    $src,
    $width=100,
    $height=100,
    $dir='resized/'
    ){
        $baseurl = $this->_storeManager->getStore()->getBaseUrl();
        if($this->startsWith($src,"/pub")) {
            $src = $baseurl.$src;
        }  
        $baseurl = $this->_storeManager->getStore()->getBaseUrl();
        $src = str_replace("https://smhttp-ssl-88237-mangoitsol.nexcesscdn.net",$baseurl,$src);
        $dir_path = $dir.$height.'*'.$width.'/';
        $absPath = $src;
        $imageResized = $this->_filesystem
        ->getDirectoryRead(DirectoryList::MEDIA)
        ->getAbsolutePath($dir_path).
        $this->getNewDirectoryImage($src);
        $imageResize = $this->_imageFactory->create(); 
        $imageResize->open($absPath);
        $imageResize->backgroundColor([255, 255, 255]);
        $imageResize->constrainOnly(TRUE);
        $imageResize->keepTransparency(TRUE);
        $imageResize->keepFrame(true);
        $imageResize->keepAspectRatio(true);
        $imageResize->resize($width,$height);
        $dest = $imageResized ;
        $imageResize->save($dest);
        $resizedURL= $this->_storeManager->getStore()
        ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).
        $dir_path.$this->getNewDirectoryImage($src);
        return $resizedURL;
    }
    public function getNewDirectoryImage($src){
        $segments = array_reverse(explode('/',$src));
        $first_dir = substr($segments[0],0,1);
        $second_dir = substr($segments[0],1,1);
        return 'cache/'.$first_dir.'/'.$second_dir.'/'.$segments[0];
    }

    public function homeimageResize(
    $src,
    $width=100,
    $height=100,
    $dir='resized/'
    ){
        $dir_path = $dir.$height.'*'.$width.'/';
        $absPath = $src;
        $imageResized = $this->_filesystem
        ->getDirectoryRead(DirectoryList::MEDIA)
        ->getAbsolutePath($dir_path).
        $this->getNewDirectoryImage($src);
        $imageResize = $this->_imageFactory->create(); 
        $imageResize->open($absPath);
        $imageResize->backgroundColor([255, 255, 255]);
        $imageResize->constrainOnly(TRUE);
        $imageResize->keepTransparency(TRUE);
        $imageResize->keepFrame(true);
        $imageResize->keepAspectRatio(true);
        $imageResize->resize($width,$height);
        $dest = $imageResized ;
        $imageResize->save($dest);
        $resizedURL= $this->_storeManager->getStore()
        ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).
        $dir_path.$this->getNewDirectoryImage($src);
        return $resizedURL;
    }
    public function popupImageResize(
    $src,
    $width=100,
    $height=100,
    $dir='resized/popup'
    ){
        $dir_path = $dir.$height.'*'.$width.'/';
        $absPath = $src;
        $imageResized = $this->_filesystem
        ->getDirectoryRead(DirectoryList::MEDIA)
        ->getAbsolutePath($dir_path).
        $this->getNewDirectoryImage($src);
        $imageResize = $this->_imageFactory->create(); 
        $imageResize->open($absPath);
        $imageResize->backgroundColor([255, 255, 255]);
        $imageResize->constrainOnly(TRUE);
        $imageResize->keepTransparency(TRUE);
        $imageResize->keepFrame(true);
        $imageResize->keepAspectRatio(true);
        $imageResize->resize($width,$height);
        $dest = $imageResized ;
        $imageResize->save($dest);
        $resizedURL= $this->_storeManager->getStore()
        ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).
        $dir_path.$this->getNewDirectoryImage($src);
        return $resizedURL;
    }
}