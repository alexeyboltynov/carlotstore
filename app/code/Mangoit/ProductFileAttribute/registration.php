<?php
/**
 * Mangoit Software
 *
 * @category Magento
 * @package  Mangoit_ProductFileAttribute
 * @author   Mangoit
 * @license  https://store.mangoit.com/license.html
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Mangoit_ProductFileAttribute',
    __DIR__
);