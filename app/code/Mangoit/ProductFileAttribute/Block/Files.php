<?php
namespace Mangoit\ProductFileAttribute\Block;
class Files extends \Magento\Framework\View\Element\Template
{
	protected $_registry;
	protected $_storeManager;
        
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,        
        \Magento\Framework\Registry $registry,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        array $data = []
    )
    {        
        $this->_registry = $registry;
        $this->_storeManager = $storeManager;
        parent::__construct($context, $data);
    }

	public function getCurrentProductDetails()
    {        
    	$file_name = '';
        $productData = $this->_registry->registry('current_product');
        if($productData->getAgreementFile() !== null && !empty($productData->getAgreementFile())) {
        	$file_name= $productData->getAgreementFile();
        }
        return $file_name;
    }  

    public function getStoreManagerData()
    {    
        $media_url = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        return $media_url;
    } 
}
