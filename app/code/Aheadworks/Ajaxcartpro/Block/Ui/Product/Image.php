<?php
/**
 * Copyright 2018 aheadWorks. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Aheadworks\Ajaxcartpro\Block\Ui\Product;

use Magento\Framework\View\Element\Template;
use Magento\Catalog\Block\Product\ImageBuilder;
use Magento\Framework\View\Element\Template\Context;

/**
 * Class Image
 * @package Aheadworks\Ajaxcartpro\Block\Ui\Product
 */
class Image extends Template
{
    /**
     * @var ImageBuilder
     */
    private $productImageBuilder;

    /**
     * @var string
     */
    protected $_template = 'ui/product/image.phtml';

    /**
     * @param Context $context
     * @param ImageBuilder $productImageBuilder
     * @param array $data
     */
    public function __construct(
        Context $context,
        ImageBuilder $productImageBuilder,
        array $data
    ) {
        parent::__construct($context, $data);
        $this->productImageBuilder = $productImageBuilder;
    }

    /**
     * Get product image
     *
     * @return string
     */
    public function getProductImage()
    {
        if ($product = $this->getProduct()) {
            return $this->productImageBuilder->setProduct($product)
                ->setImageId('category_page_grid')
                ->create()
                ->getImageUrl();
                /*->toHtml();*/
        }
        return '';
    }
}
