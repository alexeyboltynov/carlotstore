<?php
/**
 * Copyright 2018 aheadWorks. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Aheadworks\Ajaxcartpro\Block\Ui;

use Magento\Framework\Message\MessageInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Message\Collection;

/**
 * Class Messages
 * @package Aheadworks\Ajaxcartpro\Block\Ui
 */
class Messages extends Template
{
    /**
     * @var ManagerInterface
     */
    private $messageManager;

    /**
     * @var Collection|null
     */
    private $messages = null;

    /**
     * @var string
     */
    protected $_template = 'ui/messages.phtml';

    /**
     * @param Context $context
     * @param ManagerInterface $messageManager
     * @param array $data
     */
    public function __construct(
        Context $context,
        ManagerInterface $messageManager,
        array $data
    ) {
        parent::__construct($context, $data);
        $this->messageManager = $messageManager;
    }

    /**
     * Get messages
     *
     * @return Collection|null
     */
    private function getMessages()
    {
        if ($this->messages === null) {
            $this->messages = $this->messageManager->getMessages(true);
        }
        return $this->messages;
    }

    /**
     * Get error messages
     *
     * @return MessageInterface[]
     */
    public function getErrorMessages()
    {
        return $this->getMessages()->getItemsByType(MessageInterface::TYPE_ERROR);
    }

    /**
     * Get notice messages
     *
     * @return MessageInterface[]
     */
    public function getNoticeMessages()
    {
        return $this->getMessages()->getItemsByType(MessageInterface::TYPE_NOTICE);
    }

    /**
     * Get success messages
     *
     * @return MessageInterface[]
     */
    public function getSuccessMessages()
    {
        return $this->getMessages()->getItemsByType(MessageInterface::TYPE_SUCCESS);
    }
}
