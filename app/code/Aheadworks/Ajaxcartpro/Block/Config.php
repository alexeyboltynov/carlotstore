<?php
/**
 * Copyright 2018 aheadWorks. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Aheadworks\Ajaxcartpro\Block;

use Magento\Framework\View\Element\Template;

/**
 * Class Config
 * @package Aheadworks\Ajaxcartpro\Block
 */
class Config extends Template
{
    /**
     * Get JSON-formatted ACP options
     *
     * @return string
     */
    public function getOptions()
    {
        return \Zend_Json::encode([
            'acpAddToCartUrl' => $this->getUrl('aw_ajaxcartpro/cart/add'),
            'acpGetBlockContentUrl' => $this->getUrl('aw_ajaxcartpro/block/content'),
            'checkoutUrl' => $this->getUrl('checkout', ['_secure' => true])
        ]);
    }
}
