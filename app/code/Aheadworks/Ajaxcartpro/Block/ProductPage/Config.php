<?php
/**
 * Copyright 2018 aheadWorks. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Aheadworks\Ajaxcartpro\Block\ProductPage;

use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use Aheadworks\Ajaxcartpro\Model\Config as ConfigModel;

/**
 * Class Config
 * @package Aheadworks\Ajaxcartpro\Block\ProductPage
 */
class Config extends Template
{
    /**
     * @var Registry
     */
    private $coreRegistry;

    /**
     * @var ConfigModel
     */
    private $config;

    /**
     * @param Registry $coreRegistry
     * @param Context $context
     * @param ConfigModel $config
     * @param array $data
     */
    public function __construct(
        Registry $coreRegistry,
        Context $context,
        ConfigModel $config,
        array $data = []
    ) {
        $this->coreRegistry = $coreRegistry;
        $this->config = $config;
        parent::__construct($context, $data);
    }

    /**
     * Get additional JSON-formatted ACP options for product page
     *
     * @return string
     */
    public function getOptions()
    {
        return \Zend_Json::encode([
            'productCategoryUrl' => $this->getCategoryUrl(),
            'displayConfirmation' => $this->config->getDisplayConfigrmatiom()
        ]);
    }

    /**
     * Get first of the product categories
     *
     * @return string|null
     */
    private function getCategoryUrl()
    {
        if (!$product = $this->coreRegistry->registry('current_product')) {
            return null;
        }
        $firstCategory = $product->getCategoryCollection()->getFirstItem();
        return  $firstCategory->getUrl();
    }
}
