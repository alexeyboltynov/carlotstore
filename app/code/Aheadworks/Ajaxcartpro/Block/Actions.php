<?php
/**
 * Copyright 2018 aheadWorks. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Aheadworks\Ajaxcartpro\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\Data\Form\FormKey;
use Magento\Framework\View\Element\Template\Context;
use Aheadworks\Ajaxcartpro\Model\Config as ajaxCartProConfig;

/**
 * Class Actions
 * @package Aheadworks\Ajaxcartpro\Block
 */
class Actions extends Template
{
    /**
     * @var FormKey
     */
    private $formKey;

    /**
     * @var ajaxCartProConfig
     */
    private $config;

    /**
     * @param Context $context
     * @param FormKey $formKey
     * @param ajaxCartProConfig $config
     * @param array $data
     */
    public function __construct(
        Context $context,
        FormKey $formKey,
        ajaxCartProConfig $config,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->formKey = $formKey;
        $this->config = $config;
    }

    /**
     * Check if can redirect to cart
     *
     * @return bool
     */
    public function canRedirectToCart()
    {
        return $this->config->getCheckoutRedirectToCart();
    }

    /**
     * Get form key
     *
     * @return string
     */
    public function getFormKey()
    {
        return \Zend_Json::encode($this->formKey->getFormKey());
    }
}
