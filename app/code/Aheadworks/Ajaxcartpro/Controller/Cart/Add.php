<?php
/**
 * Copyright 2018 aheadWorks. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Aheadworks\Ajaxcartpro\Controller\Cart;

use Aheadworks\Ajaxcartpro\Model\Source\DisplayFor;
use Aheadworks\Ajaxcartpro\Model\Config;
use Magento\UrlRewrite\Service\V1\Data\UrlRewrite;
use Zend\Uri\UriFactory;
use Magento\Catalog\Model\Product;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\App\Action\Action;
use Magento\Checkout\Helper\Cart;
use Magento\Framework\App\Router\PathConfigInterface;
use Magento\UrlRewrite\Model\UrlFinderInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\DataObject;
use Aheadworks\Ajaxcartpro\Model\AddToCartChecker;

/**
 * Class Add
 *
 * @package Aheadworks\Ajaxcartpro\Controller
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 */
class Add extends Action
{
    /**
     * @var JsonFactory
     */
    private $resultJsonFactory;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var Cart
     */
    private $cartHelper;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var PathConfigInterface
     */
    private $pathConfig;

    /**
     * @var UrlFinderInterface
     */
    private $urlFinder;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var AddToCartChecker
     */
    private $addToCartChecker;

    /**
     * @param Context $context
     * @param JsonFactory $resultJsonFactory
     * @param ProductRepositoryInterface $productRepository
     * @param Cart $cartHelper
     * @param Config $config
     * @param PathConfigInterface $pathConfig
     * @param UrlFinderInterface $urlFinder
     * @param StoreManagerInterface $storeManager
     * @param AddToCartChecker $addToCartChecker
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        ProductRepositoryInterface $productRepository,
        Cart $cartHelper,
        Config $config,
        PathConfigInterface $pathConfig,
        UrlFinderInterface $urlFinder,
        StoreManagerInterface $storeManager,
        AddToCartChecker $addToCartChecker
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->productRepository = $productRepository;
        $this->cartHelper = $cartHelper;
        $this->config = $config;
        $this->pathConfig = $pathConfig;
        $this->urlFinder = $urlFinder;
        $this->storeManager = $storeManager;
        $this->addToCartChecker = $addToCartChecker;
    }

    /**
     * Dispatch request
     *
     * @return Json
     */
    public function execute()
    {
        $resultData = [];
        try {
            /** @var Product $product */
            $product = $this->productRepository->getById($this->getProductId());
            $canBeBought = $this->canBeBought($product);
            if ($this->isProductAddedFromItsPage()) {
                if ($this->isNeedToPreventPopupBlockDisplaying($product, $canBeBought)) {
                    $resultData['reloadUrl'] = $this->getProductViewUrl($product);
                } else {
                    $resultData['backUrl'] = $this->cartHelper->getAddUrl($product);
                }
            } else {
                if ($this->isNeedToRedirectToProductPage($product, $canBeBought)) {
                    $resultData['reloadUrl'] = $this->getProductViewUrl($product);
                } elseif ($this->isNeedToDisplayPopupBlock($product, $canBeBought)) {
                    $resultData['backUrl'] = $this->getProductViewUrl($product);
                } else {
                    $resultData['backUrl'] = $this->cartHelper->getAddUrl($product);
                }
            }
        } catch (NoSuchEntityException $e) {
            $resultData['error'] = $e->getMessage();
        }

        return $this->resultJsonFactory->create()
            ->setData($resultData);
    }

    /**
     * Get product ID
     *
     * @return int|null
     */
    private function getProductId()
    {
        $request = $this->getRequest();
        $productId = $request->getParam('product');
        if (!$productId && $actionUrl = $request->getParam('action_url')) {
            $path = trim(UriFactory::factory($actionUrl)->getPath(), '/');
            $params = explode('/', $path ? $path : $this->pathConfig->getDefaultPath());
            for ($i = 0, $l = sizeof($params); $i < $l; $i++) {
                if ($params[$i] != $this->getBaseFileName()) {
                    if ($params[$i] == 'product' && isset($params[$i + 1])) {
                        $productId = urldecode($params[$i + 1]);
                        break;
                    } else {
                        /** @var UrlRewrite $urlRewrite */
                        $urlRewrite = $this->urlFinder->findOneByData([
                            UrlRewrite::REQUEST_PATH => $params[$i],
                            UrlRewrite::STORE_ID => $this->storeManager->getStore()->getId(),
                        ]);
                        if ($urlRewrite && $urlRewrite->getEntityType() == 'product') {
                            $productId = $urlRewrite->getEntityId();
                            break;
                        }
                    }
                }
            }
        }

        return $productId;
    }

    /**
     * Get base file name
     *
     * @return string
     */
    private function getBaseFileName()
    {
        $scriptName = $_SERVER['SCRIPT_FILENAME'] ? : $_SERVER['SCRIPT_NAME'];
        return basename($scriptName);
    }

    /**
     * Check if product is added from its page
     *
     * @return bool
     */
    private function isProductAddedFromItsPage()
    {
        return (bool)$this->getRequest()->getParam('product_page', false);
    }

    /**
     * Retrieves product view url
     *
     * @param Product $product
     * @return string
     */
    private function getProductViewUrl(Product $product)
    {
        $params = ['_escape' => true];
        if ($product->getTypeInstance()->hasOptions($product)) {
            $params['_query'] = ['options' => 'cart'];
        }
        return $product->getUrlModel()->getUrl($product, $params);
    }

    /**
     * Check if request contains all required options to buy the product
     *
     * @param Product $product
     * @return bool
     */
    private function canBeBought(Product $product)
    {
        $request = new DataObject($this->getRequest()->getParams());
        $cartCandidates = $product->getTypeInstance()->prepareForCartAdvanced($request, $product);
        if (is_string($cartCandidates) || $cartCandidates instanceof \Magento\Framework\Phrase) {
            return false;
        }
        return true;
    }

    /**
     * Check if need to prevent displaying of the options popup for specified product
     *
     * @param Product $product
     * @param bool $canBeBought
     * @return bool
     */
    private function isNeedToPreventPopupBlockDisplaying($product, $canBeBought)
    {
        return (
            !$canBeBought
            && $this->addToCartChecker->isOptionsPopupNotSupportedForProduct($product)
        );
    }

    /**
     * Check if need to redirect to product page instead of adding to cart
     *
     * @param Product $product
     * @param bool $canBeBought
     * @return bool
     */
    private function isNeedToRedirectToProductPage($product, $canBeBought)
    {
        return (
            !($this->addToCartChecker->isProductSupported($product))
            || $this->isNeedToPreventPopupBlockDisplaying($product, $canBeBought)
        );
    }

    /**
     * Check is need to display popup block
     *
     * @param Product $product
     * @param bool $canBeBought
     * @return bool
     */
    private function isNeedToDisplayPopupBlock($product, $canBeBought)
    {
        $displayOptionsFor = $this->config->getDisplayBlockFor();
        return (
            !($this->addToCartChecker->isOptionsPopupNotSupportedForProduct($product))
            && (
                $displayOptionsFor == DisplayFor::PRODUCTS_ALL
                || $this->isNeedDisplayPopupWithRequiredOptions($product, $displayOptionsFor, $canBeBought)
                || $this->isNeedDisplayPopupWithAnyOptions($product, $displayOptionsFor)
            )
        );
    }

    /**
     * Check is need to display popup with required options
     *
     * @param Product $product
     * @param bool $displayOptionsFor
     * @param bool $canBeBought
     * @return bool
     */
    private function isNeedDisplayPopupWithRequiredOptions($product, $displayOptionsFor, $canBeBought)
    {
        return $displayOptionsFor == DisplayFor::PRODUCTS_WITH_REQUIRED_OPTIONS
            && $product->getTypeInstance()->hasRequiredOptions($product)
            && !$canBeBought;
    }

    /**
     * Check is need to display popup with any options
     *
     * @param Product $product
     * @param bool $displayOptionsFor
     * @return bool
     */
    private function isNeedDisplayPopupWithAnyOptions($product, $displayOptionsFor)
    {
        return $displayOptionsFor == DisplayFor::PRODUCTS_WITH_ANY_OPTIONS
            && $product->getTypeInstance()->hasOptions($product);
    }
}
