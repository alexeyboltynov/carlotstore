<?php
/**
 * Copyright 2018 aheadWorks. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Aheadworks\Ajaxcartpro\Controller\Block;

use Aheadworks\Ajaxcartpro\Model\Renderer;
use Magento\Framework\App\Action\Action;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;

/**
 * Class Content
 * @package Aheadworks\Ajaxcartpro\Controller\Block
 */
class Content extends Action
{
    /**
     * @var JsonFactory
     */
    private $resultJsonFactory;

    /**
     * @var PageFactory
     */
    private $resultPageFactory;

    /**
     * @var Renderer
     */
    private $renderer;

    /**
     * @var array
     */
    private $layoutUpdates = [
        Renderer::PART_CHECKOUT_CART => 'checkout_cart_index'
    ];

    /**
     * @param Context $context
     * @param JsonFactory $resultJsonFactory
     * @param PageFactory $resultPageFactory
     * @param Renderer $renderer
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        PageFactory $resultPageFactory,
        Renderer $renderer
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->resultPageFactory = $resultPageFactory;
        $this->renderer = $renderer;
    }

    /**
     * Dispatch request
     *
     * @return Json
     */
    public function execute()
    {
        $resultData = [];
        $partName = $this->getRequest()->getParam('part');
        if ($partName) {
            $resultPage = $this->resultPageFactory->create();
            $resultPage->addHandle($this->layoutUpdates[$partName]);

            $resultData['content'] = $this->renderer->render(
                $resultPage->getLayout(),
                Renderer::PART_CHECKOUT_CART
            );
        }

        return $this->resultJsonFactory->create()
            ->setData($resultData);
    }
}
