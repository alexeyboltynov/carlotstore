<?php
/**
 * Copyright 2018 aheadWorks. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Aheadworks\Ajaxcartpro\Controller\Plugin;

use Aheadworks\Ajaxcartpro\Model\Processor;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Action\Action as AppAction;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\View\Result\Page;

/**
 * Class Action
 * @package Aheadworks\Ajaxcartpro\Controller\Plugin
 */
class Action
{
    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var Processor
     */
    private $processor;

    /**
     * @param RequestInterface $request
     * @param Processor $processor
     */
    public function __construct(
        RequestInterface $request,
        Processor $processor
    ) {
        $this->request = $request;
        $this->processor = $processor;
    }

    /**
     * After dispatch plugin
     *
     * @param AppAction $action
     * @param ResponseInterface|Page $response
     * @return mixed
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterDispatch($action, $response)
    {
        $route = implode(
            '/',
            [
                $this->request->getModuleName(),
                $this->request->getControllerName(),
                $this->request->getActionName()
            ]
        );
        $processRoutes = [
            Processor::ROUTE_ADD_TO_CART,
            Processor::ROUTE_PRODUCT_VIEW
        ];
        if (in_array($route, $processRoutes) && $this->request->getParam('aw_acp', false)) {
            return $this->processor->process($this->request, $response, $route);
        }
        return $response;
    }
}
