<?php
/**
 * Copyright 2018 aheadWorks. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Aheadworks\Ajaxcartpro\Test\Unit\Model\Plugin;

use Aheadworks\Ajaxcartpro\Model\Cart\AddResult;
use Aheadworks\Ajaxcartpro\Model\Plugin\Quote;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Magento\Quote\Model\Quote as QuoteModel;
use Magento\Quote\Model\Quote\Item;
use Magento\Catalog\Model\Product;
use Magento\Framework\DataObject;
use PHPUnit\Framework\TestCase;

/**
 * Test for \Aheadworks\Ajaxcartpro\Model\Plugin\Quote
 */
class QuoteTest extends TestCase
{
    /**
     * @var Quote
     */
    private $quotePlugin;

    /**
     * @var AddResult|\PHPUnit_Framework_MockObject_MockObject
     */
    private $cartAddResultMock;

    /**
     * Init mocks for tests
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);

        $this->cartAddResultMock = $this->createPartialMock(AddResult::class, ['setAddSuccess', 'setSaveSuccess']);
        $this->quotePlugin = $objectManager->getObject(
            Quote::class,
            ['cartAddResult' => $this->cartAddResultMock]
        );
    }

    /**
     * Testing of afterAddProduct method with standard request
     */
    public function testBeforeAddProductStandardRequest()
    {
        $requestData = [
            'action_url' => 'http://example.com/index.php/',
            'aw_acp' => 1
        ];
        $expectedData = [];

        $quoteMock = $this->getMockBuilder(QuoteModel::class)
            ->setMethods([])
            ->disableOriginalConstructor()
            ->getMock();
        $productMock = $this->getMockBuilder(Product::class)
            ->setMethods([])
            ->disableOriginalConstructor()
            ->getMock();
        $requestMock = $this->getMockBuilder(DataObject::class)
            ->setMethods(['getData', 'setData'])
            ->disableOriginalConstructor()
            ->getMock();

        $requestMock->expects($this->exactly(2))
            ->method('getData')
            ->willReturn($requestData);
        $requestMock->expects($this->once())
            ->method('setData')
            ->with($expectedData)
            ->willReturnSelf();

        $this->assertTrue(is_array($this->quotePlugin->beforeAddProduct($quoteMock, $productMock, $requestMock)));
    }

    /**
     * Testing of afterAddProduct method with empty request
     */
    public function testBeforeAddProductEmptyRequest()
    {
        $requestData = [];

        $quoteMock = $this->getMockBuilder(QuoteModel::class)
            ->setMethods([])
            ->disableOriginalConstructor()
            ->getMock();
        $productMock = $this->getMockBuilder(Product::class)
            ->setMethods([])
            ->disableOriginalConstructor()
            ->getMock();
        $requestMock = $this->getMockBuilder(DataObject::class)
            ->setMethods(['getData', 'setData'])
            ->disableOriginalConstructor()
            ->getMock();

        $requestMock->expects($this->once())
            ->method('getData')
            ->willReturn($requestData);
        $requestMock->expects($this->never())
            ->method('setData')
            ->willReturnSelf();

        $this->assertTrue(is_array($this->quotePlugin->beforeAddProduct($quoteMock, $productMock, $requestMock)));
    }

    /**
     * Testing of afterAddProduct method
     *
     * @param \Magento\Quote\Model\Quote\Item|string $result
     * @param bool $success
     * @dataProvider afterAddProductResults
     */
    public function testAfterAddProduct($result, $success)
    {
        $quoteModel = $this->createMock(QuoteModel::class);
        $this->cartAddResultMock->expects($this->once())
            ->method('setAddSuccess')
            ->with($success);
        $this->quotePlugin->afterAddProduct($quoteModel, $result);
    }

    /**
     * Data provider for testAfterAddProduct()
     */
    public function afterAddProductResults()
    {
        $result = $this->createMock(Item::class);
        return [[$result, true], ['Error message', false]];
    }

    /**
     * Testing of afterSave method
     */
    public function testAfterSave()
    {
        $quoteModel = $this->createMock(QuoteModel::class);
        $this->cartAddResultMock->expects($this->once())
            ->method('setSaveSuccess')
            ->with(true);
        $this->quotePlugin->afterSave($quoteModel, $quoteModel);
    }
}
