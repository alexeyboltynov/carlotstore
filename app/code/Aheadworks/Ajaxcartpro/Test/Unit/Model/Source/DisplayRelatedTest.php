<?php
/**
 * Copyright 2018 aheadWorks. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Aheadworks\Ajaxcartpro\Test\Unit\Model\Source;

use Aheadworks\Ajaxcartpro\Model\Source\DisplayRelated;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;

/**
 * Test for \Aheadworks\Ajaxcartpro\Model\Source\DisplayRelated
 */
class DisplayRelatedTest extends TestCase
{
    /**
     * @var DisplayRelated
     */
    private $displayRelated;

    /**
     * Init mocks for tests
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);

        $this->displayRelated = $objectManager->getObject(
            DisplayRelated::class,
            []
        );
    }

    /**
     * Testing of toOptionArray method
     */
    public function testToOptionArray()
    {
        $this->assertTrue(is_array($this->displayRelated->toOptionArray()));
    }
}
