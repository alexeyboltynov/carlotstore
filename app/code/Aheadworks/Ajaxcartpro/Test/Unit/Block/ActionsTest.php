<?php
/**
 * Copyright 2018 aheadWorks. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Aheadworks\Ajaxcartpro\Test\Unit\Block;

use Aheadworks\Ajaxcartpro\Block\Actions;
use Aheadworks\Ajaxcartpro\Model\Config;
use Magento\Framework\Data\Form\FormKey;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;

/**
 * Test for Aheadworks\Ajaxcartpro\Block\Actions
 */
class ActionsTest extends TestCase
{
    /**
     * @var Actions
     */
    private $actions;

    /**
     * @var Config|\PHPUnit_Framework_MockObject_MockObject
     */
    private $configMock;

    /**
     * @var FormKey|\PHPUnit_Framework_MockObject_MockObject
     */
    private $formKeyMock;

    /**
     * Init mocks for tests
     *
     * @return void
     */
    protected function setUp()
    {
        $objectManager = new ObjectManager($this);
        $this->configMock = $this->createPartialMock(Config::class, ['getCheckoutRedirectToCart']);
        $this->formKeyMock = $this->createPartialMock(FormKey::class, ['getFormKey']);

        $this->actions = $objectManager->getObject(
            Actions::class,
            [
                'config' => $this->configMock,
                'formKey' => $this->formKeyMock,
            ]
        );
    }

    /**
     * Testing of canRedirectToCart method
     */
    public function testCanRedirectToCart()
    {
        $this->configMock
            ->expects($this->once())
            ->method('getCheckoutRedirectToCart')
            ->willReturn(true);
        $this->assertEquals(true, $this->actions->canRedirectToCart());
    }

    /**
     * Testing of getFormKey method
     */
    public function testGetFormKey()
    {
        $this->formKeyMock
            ->expects($this->once())
            ->method('getFormKey')
            ->willReturn('12345678');
        $this->assertJson($this->actions->getFormKey());
    }
}
