<?php
/**
 * Copyright 2018 aheadWorks. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Aheadworks\Ajaxcartpro\Test\Unit\Block\ProductPage;

use Aheadworks\Ajaxcartpro\Block\ProductPage\Config;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;

/**
 * Test for Aheadworks\Ajaxcartpro\Block\Config
 */
class ConfigTest extends TestCase
{
    /**
     * @var Config
     */
    private $config;

    /**
     * Setting up mocks
     */
    protected function setUp()
    {
        $objectManager = new ObjectManager($this);
        $this->config = $objectManager->getObject(Config::class);
    }

    /**
     * Testing of getOptions method
     */
    public function testGetOptions()
    {
        $this->assertJson($this->config->getOptions());
    }
}
