<?php
/**
 * Copyright 2018 aheadWorks. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Aheadworks\Ajaxcartpro\Test\Unit\Block\Ui\Product;

use Aheadworks\Ajaxcartpro\Block\Ui\Product\Image;
use Magento\Catalog\Model\Product;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Magento\Catalog\Block\Product\ImageBuilder;
use PHPUnit\Framework\TestCase;

/**
 * Test for Aheadworks\Ajaxcartpro\Test\Unit\Block\Ui\Product\Image
 */
class ImageTest extends TestCase
{
    /**
     * @var Image
     */
    private $image;

    /**
     * @var ImageBuilder|\PHPUnit_Framework_MockObject_MockObject
     */
    private $productImageBuilderMock;

    /**
     * Setting up mocks
     */
    protected function setUp()
    {
        $this->productImageBuilderMock = $this->createPartialMock(
            ImageBuilder::class,
            ['setProduct', 'setImageId', 'create', 'toHtml']
        );
        $productMock = $this->createMock(Product::class);
        $objectManager = new ObjectManager($this);
        $this->image = $objectManager->getObject(
            Image::class,
            [
                'productImageBuilder' => $this->productImageBuilderMock,
                'data' => ['product' => $productMock]
            ]
        );
    }

    /**
     * Testing of getProductImage method
     */
    public function testGetProductImage()
    {
        $html = 'image_html';
        $this->productImageBuilderMock->expects($this->once())->method('setProduct')->willReturnSelf();
        $this->productImageBuilderMock->expects($this->once())->method('setImageId')
            ->with('category_page_grid')
            ->willReturnSelf();
        $this->productImageBuilderMock->expects($this->once())->method('create')->willReturnSelf();
        $this->productImageBuilderMock->expects($this->once())->method('toHtml')
            ->willReturn($html);
        $this->assertEquals($html, $this->image->getProductImage());
    }
}
