<?php
/**
 * Copyright 2018 aheadWorks. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Aheadworks\Ajaxcartpro\Test\Unit\Controller\Plugin;

use PHPUnit\Framework\TestCase;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Magento\Framework\App\RequestInterface;
use Aheadworks\Ajaxcartpro\Model\Processor;
use Aheadworks\Ajaxcartpro\Controller\Plugin\Action;
use Magento\Framework\App\Action\Action as AppAction;
use Magento\Framework\App\ResponseInterface;

/**
 * Class ActionTest
 * @package Aheadworks\Ajaxcartpro\Test\Unit\Controller\Plugin
 */
class ActionTest extends TestCase
{
    /**
     * @var RequestInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $requestMock;

    /**
     * @var Processor|\PHPUnit_Framework_MockObject_MockObject
     */
    private $processorMock;

    /**
     * @var Action
     */
    private $action;

    protected function setUp()
    {
        $objectManager = new ObjectManager($this);
        $this->requestMock = $this->getMockBuilder(RequestInterface::class)
            ->setMethods(['getControllerName'])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $this->processorMock = $this->createPartialMock(Processor::class, ['process']);
        $this->action = $objectManager->getObject(
            Action::class,
            [
                'request' => $this->requestMock,
                'processor' => $this->processorMock
            ]
        );
    }

    /**
     * @param bool $awacpParamValue
     * @dataProvider testAfterDispatchProvider
     */
    public function testAfterDispatch($awacpParamValue)
    {
        $responseMock = $this->getMockBuilder(ResponseInterface::class)
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $appActionMock = $this->createMock(AppAction::class);
        $route = 'checkout/cart/add';
        $moduleName = 'checkout';
        $controllerName = 'cart';
        $actionName = 'add';
        $executionCount = $awacpParamValue ? 1 : 0;

        $this->requestMock->expects($this->once())
            ->method('getModuleName')
            ->willReturn($moduleName);
        $this->requestMock->expects($this->once())
            ->method('getControllerName')
            ->willReturn($controllerName);
        $this->requestMock->expects($this->once())
            ->method('getActionName')
            ->willReturn($actionName);
        $this->requestMock->expects($this->once())
            ->method('getParam')
            ->with('aw_acp')
            ->willReturn($awacpParamValue);
        $this->processorMock->expects($this->exactly($executionCount))
            ->method('process')
            ->with($this->requestMock, $responseMock, $route)
            ->willReturn($responseMock);

        $this->assertEquals($this->action->afterDispatch($appActionMock, $responseMock), $responseMock);
    }

    /**
     * Data provider
     *
     * @return array
     */
    public function testAfterDispatchProvider()
    {
        return [
            [true],
            [false]
        ];
    }
}
