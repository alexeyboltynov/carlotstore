<?php
/**
 * Copyright 2018 aheadWorks. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Aheadworks\Ajaxcartpro\Test\Unit\Controller\Cart;

use Aheadworks\Ajaxcartpro\Controller\Cart\Add;
use Aheadworks\Ajaxcartpro\Model\Config;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Type\AbstractType;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Request\Http;
use PHPUnit\Framework\TestCase;
use Magento\Checkout\Helper\Cart;
use Magento\Framework\App\Router\PathConfigInterface;
use Magento\UrlRewrite\Model\UrlFinderInterface;
use Magento\Store\Model\StoreManagerInterface;
use Aheadworks\Ajaxcartpro\Model\AddToCartChecker;

/**
 * Test for Aheadworks\Ajaxcartpro\Controller\Cart\Add
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class AddTest extends TestCase
{
    /**
     * @var Add
     */
    private $controller;

    /**
     * @var JsonFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    private $resultJsonFactoryMock;

    /**
     * @var ProductRepositoryInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $productRepositoryMock;

    /**
     * @var Cart|\PHPUnit_Framework_MockObject_MockObject
     */
    private $cartHelperMock;

    /**
     * @var Config|\PHPUnit_Framework_MockObject_MockObject
     */
    private $configMock;

    /**
     * @var PathConfigInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $pathConfigMock;

    /**
     * @var UrlFinderInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $urlFinderMock;

    /**
     * @var StoreManagerInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $storeManagerMock;

    /**
     * @var AddToCartChecker|\PHPUnit_Framework_MockObject_MockObject
     */
    private $addToCartCheckerMock;

    /**
     * @var Http|\PHPUnit_Framework_MockObject_MockObject
     */
    private $requestMock;

    /**
     * Setting up mocks
     */
    protected function setUp()
    {
        $objectManager = new ObjectManager($this);
        $this->resultJsonFactoryMock = $this->createPartialMock(JsonFactory::class, ['create']);
        $this->productRepositoryMock = $this->getMockForAbstractClass(ProductRepositoryInterface::class);
        $this->cartHelperMock = $this->createPartialMock(Cart::class, ['getAddUrl']);
        $this->configMock = $this->createPartialMock(Config::class, ['getDisplayBlockFor']);
        $this->pathConfigMock = $this->getMockForAbstractClass(PathConfigInterface::class);
        $this->urlFinderMock = $this->getMockForAbstractClass(UrlFinderInterface::class);
        $this->storeManagerMock = $this->getMockForAbstractClass(StoreManagerInterface::class);
        $this->addToCartCheckerMock = $this->createPartialMock(
            AddToCartChecker::class,
            [
                'isProductSupported',
                'isOptionsPopupNotSupportedForProduct',
            ]
        );

        $this->requestMock = $this->createPartialMock(
            Http::class,
            [
                'getParam',
                'getParams'
            ]
        );

        $contextMock = $objectManager->getObject(
            Context::class,
            [
                'request' => $this->requestMock,
            ]
        );

        $this->controller = $objectManager->getObject(
            Add::class,
            [
                'context' => $contextMock,
                'resultJsonFactory' => $this->resultJsonFactoryMock,
                'productRepository' => $this->productRepositoryMock,
                'cartHelper' => $this->cartHelperMock,
                'config' => $this->configMock,
                'pathConfig' => $this->pathConfigMock,
                'urlFinder' => $this->urlFinderMock,
                'storeManager' => $this->storeManagerMock,
                'addToCartChecker' => $this->addToCartCheckerMock
            ]
        );
    }

    /**
     * Testing of execute method
     */
    public function testExecute()
    {
        $productId = 1;
        $requestParams = [];
        $productPageFlag = true;
        $isOptionsPopupNotSupportedForProduct = false;
        $addUrl = 'https://example.com';
        $resultData = [
            'backUrl' => $addUrl
        ];

        $this->requestMock->expects($this->exactly(2))
            ->method('getParam')
            ->withConsecutive(
                ['product'],
                ['product_page']
            )->willReturnOnConsecutiveCalls(
                $productId,
                $productPageFlag
            );

        $productMock = $this->createPartialMock(
            Product::class,
            [
                'getTypeInstance'
            ]
        );
        $typeInstance = $this->createMock(AbstractType::class);
        $typeInstance->expects($this->once())
            ->method('prepareForCartAdvanced')
            ->willReturn('error_string');

        $productMock->expects($this->any())
            ->method('getTypeInstance')
            ->willReturn($typeInstance);

        $this->productRepositoryMock->expects($this->once())
            ->method('getById')
            ->with($productId)
            ->willReturn($productMock);

        $this->requestMock->expects($this->once())
            ->method('getParams')
            ->willReturn($requestParams);

        $this->addToCartCheckerMock->expects($this->once())
            ->method('isOptionsPopupNotSupportedForProduct')
            ->with($productMock)
            ->willReturn($isOptionsPopupNotSupportedForProduct);

        $this->cartHelperMock->expects($this->once())
            ->method('getAddUrl')
            ->with($productMock)
            ->willReturn($addUrl);

        $jsonMock = $this->createPartialMock(Json::class, ['setData']);
        $jsonMock->expects($this->once())
            ->method('setData')
            ->with($resultData)
            ->willReturnSelf();
        $this->resultJsonFactoryMock->expects($this->once())
            ->method('create')
            ->willReturn($jsonMock);

        $this->assertInstanceOf(Json::class, $this->controller->execute());
    }
}
