<?php
/**
 * Copyright 2018 aheadWorks. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Aheadworks\Ajaxcartpro\Test\Unit\Controller\Block;

use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\View\Result\PageFactory;
use Aheadworks\Ajaxcartpro\Model\Renderer;
use Magento\Framework\App\RequestInterface;
use Aheadworks\Ajaxcartpro\Controller\Block\Content;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\LayoutInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;

/**
 * Class ContentTest
 * @package Aheadworks\Ajaxcartpro\Test\Unit\Controller\Block
 */
class ContentTest extends TestCase
{
    /**
     * @var JsonFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    private $resultJsonFactoryMock;

    /**
     * @var PageFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    private $resultPageFactoryMock;

    /**
     * @var Renderer|\PHPUnit_Framework_MockObject_MockObject
     */
    private $rendererMock;

    /**
     * @var Content|\PHPUnit_Framework_MockObject_MockObject
     */
    private $content;

    /**
     * @var RequestInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $requestMock;

    protected function setUp()
    {
        $objectManager = new ObjectManager($this);
        $this->rendererMock = $this->createPartialMock(Renderer::class, ['render']);
        $this->resultPageFactoryMock = $this->createPartialMock(PageFactory::class, ['create']);
        $this->resultJsonFactoryMock = $this->createPartialMock(JsonFactory::class, ['create']);
        $this->requestMock = $this->getMockBuilder(RequestInterface::class)
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $context = $objectManager->getObject(
            Context::class,
            [
                'request' => $this->requestMock
            ]
        );
        $this->content = $objectManager->getObject(
            Content::class,
            [
                'context' => $context,
                'resultJsonFactory' => $this->resultJsonFactoryMock,
                'resultPageFactory' => $this->resultPageFactoryMock,
                'renderer' => $this->rendererMock
            ]
        );
    }

    /**
     * @param bool $partsPresent
     * @dataProvider testExecuteProvider
     */
    public function testExecute($partsPresent)
    {
        $jsonMock = $this->createPartialMock(Json::class, ['setData']);
        $pageMock = $this->createPartialMock(Page::class, ['getLayout', 'addHandle']);
        $layoutMock = $this->getMockBuilder(LayoutInterface::class)
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $html = '<p>test content</p>';
        $renderedData = $partsPresent ? ['content' => $html] : [];
        $executionCount = $partsPresent ? 1 : 0;
        $parts = $partsPresent ? Renderer::PART_CHECKOUT_CART : null;

        $this->requestMock->expects($this->once())
            ->method('getParam')
            ->with('part')
            ->willReturn($parts);
        $this->resultPageFactoryMock->expects($this->exactly($executionCount))
            ->method('create')
            ->willReturn($pageMock);
        $this->resultJsonFactoryMock->expects($this->once())
            ->method('create')
            ->willReturn($jsonMock);
        $this->rendererMock->expects($this->exactly($executionCount))
            ->method('render')
            ->with($layoutMock, Renderer::PART_CHECKOUT_CART)
            ->willReturn($html);
        $pageMock->expects($this->exactly($executionCount))
            ->method('addHandle')
            ->with('checkout_cart_index')
            ->willReturnSelf();
        $pageMock->expects($this->exactly($executionCount))
            ->method('getLayout')
            ->willReturn($layoutMock);
        $jsonMock->expects($this->once())
            ->method('setData')
            ->with($renderedData)
            ->willReturnSelf();

        $this->assertEquals($this->content->execute(), $jsonMock);
    }

    /**
     * Data provider
     *
     * @return array
     */
    public function testExecuteProvider()
    {
        return [
            [true],
            [false]
        ];
    }
}
