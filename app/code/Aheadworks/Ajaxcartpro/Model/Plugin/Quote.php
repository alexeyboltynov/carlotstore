<?php
/**
 * Copyright 2018 aheadWorks. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Aheadworks\Ajaxcartpro\Model\Plugin;

use Magento\Quote\Model\Quote as QuoteModel;
use Aheadworks\Ajaxcartpro\Model\Cart\AddResult;
use Magento\Catalog\Model\Product;
use Magento\Framework\DataObject;
use Magento\Quote\Model\Quote\Item;

/**
 * Class Quote
 * @package Aheadworks\Ajaxcartpro\Model\Plugin
 */
class Quote
{
    /**
     * @var AddResult
     */
    private $cartAddResult;

    /**
     * @param AddResult $cartAddResult
     */
    public function __construct(
        AddResult $cartAddResult
    ) {
        $this->cartAddResult = $cartAddResult;
    }

    /**
     * Before addProduct() method plugin
     *
     * @param QuoteModel $quote
     * @param Product $product
     * @param DataObject $request
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function beforeAddProduct($quote, Product $product, $request)
    {
        if (is_object($request) && !empty($request->getData())) {
            $data = $request->getData();
            if (isset($data['action_url'])) {
                unset($data['action_url']);
            }
            if (isset($data['aw_acp'])) {
                unset($data['aw_acp']);
            }
            $request->setData($data);
        }
        return [$product, $request];
    }

    /**
     * After addProduct() method plugin
     *
     * @param QuoteModel $quote
     * @param Item|string $result
     * @return Item|string
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterAddProduct($quote, $result)
    {
        $this->cartAddResult->setAddSuccess(!is_string($result));
        return $result;
    }

    /**
     * After save() method plugin
     *
     * @param QuoteModel $quote
     * @param QuoteModel $result
     * @return QuoteModel
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterSave($quote, $result)
    {
        $this->cartAddResult->setSaveSuccess(true);
        return $result;
    }

    /**
     * Object saved in the resource model.
     * Not is calling method $quote->save()
     *
     * @param QuoteModel $quote
     * @param QuoteModel $result
     * @return QuoteModel
     */
    public function afterAfterSave($quote, $result)
    {
        return $this->afterSave($quote, $result);
    }
}
