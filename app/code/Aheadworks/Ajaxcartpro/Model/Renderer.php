<?php
/**
 * Copyright 2018 aheadWorks. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Aheadworks\Ajaxcartpro\Model;

use Aheadworks\Ajaxcartpro\Model\Renderer\Cart;
use Aheadworks\Ajaxcartpro\Model\Renderer\Confirmation;
use Aheadworks\Ajaxcartpro\Model\Renderer\Related;
use Aheadworks\Ajaxcartpro\Model\Renderer\Options;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\View\Layout;

/**
 * Block Renderer
 * @package Aheadworks\Ajaxcartpro\Model
 */
class Renderer
{
    /**#@+
     * Parts to render
     */
    const PART_OPTIONS = 'options';

    const PART_CONFIRMATION = 'confirmation';

    const PART_RELATED = 'related';

    const PART_CHECKOUT_CART = 'cart';
    /**#@-*/

    /**
     * @var array
     */
    private $partRenderers = [
        self::PART_OPTIONS => Options::class,
        self::PART_CONFIRMATION => Confirmation::class,
        self::PART_RELATED => Related::class,
        self::PART_CHECKOUT_CART => Cart::class
    ];

    /**
     * @var ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @param ObjectManagerInterface $objectManager
     */
    public function __construct(ObjectManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * Render layout
     *
     * @param Layout $layout
     * @param string $part
     * @return string
     */
    public function render($layout, $part)
    {
        if (isset($this->partRenderers[$part])
            && $renderer = $this->objectManager->get($this->partRenderers[$part])
        ) {
            return $renderer->render($layout);
        }
        return '';
    }
}
