<?php
/**
 * Copyright 2018 aheadWorks. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Aheadworks\Ajaxcartpro\Model;

use Aheadworks\Ajaxcartpro\Model\AddToCartChecker\SupportedProductsMetadata;
use Aheadworks\Ajaxcartpro\Model\AddToCartChecker\SubscribeAbilityChecker;
use Magento\Catalog\Api\Data\ProductInterface;

/**
 * Class SupportedProductTypesResolver
 *
 * @package Aheadworks\Ajaxcartpro\Model
 */
class AddToCartChecker
{
    /**
     * @var SupportedProductsMetadata
     */
    private $supportedProductsMetadata;

    /**
     * @var SubscribeAbilityChecker
     */
    private $subscribeAbilityChecker;

    /**
     * @param SupportedProductsMetadata $supportedProductsMetadata
     * @param SubscribeAbilityChecker $subscribeAbilityChecker
     */
    public function __construct(
        SupportedProductsMetadata $supportedProductsMetadata,
        SubscribeAbilityChecker $subscribeAbilityChecker
    ) {
        $this->supportedProductsMetadata = $supportedProductsMetadata;
        $this->subscribeAbilityChecker = $subscribeAbilityChecker;
    }

    /**
     * Check if it is possible to add specified product to cart using Ajaxcartpro module
     *
     * @param ProductInterface $product
     * @return bool
     */
    public function isProductSupported($product)
    {
        return (
            in_array($product->getTypeId(), $this->supportedProductsMetadata->getSupportedProductTypes())
            && $this->isAllProductOptionsSupported($product)
            && !($this->subscribeAbilityChecker->isSubscribeAvailableForProduct($product))
        );
    }

    /**
     * Check if all product options are supported by Ajaxcartpro module
     *
     * @param ProductInterface $product
     * @return bool
     */
    private function isAllProductOptionsSupported($product)
    {
        $isAllProductOptionsSupported = true;
        $productOptions = $product->getOptions();
        if (!empty($productOptions)) {
            foreach ($productOptions as $option) {
                if (!in_array($option->getType(), $this->supportedProductsMetadata->getSupportedProductOptionTypes())) {
                    $isAllProductOptionsSupported = false;
                    break;
                }
            }
        }
        return $isAllProductOptionsSupported;
    }

    /**
     * Check if product options popup block isn't supported for specified product
     *
     * @param ProductInterface $product
     * @return bool
     */
    public function isOptionsPopupNotSupportedForProduct($product)
    {
        return in_array(
            $product->getTypeId(),
            $this->supportedProductsMetadata->getSupportedWithoutOptionsPopupProductTypes()
        );
    }
}
