<?php
/**
 * Copyright 2018 aheadWorks. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Aheadworks\Ajaxcartpro\Model\AddToCartChecker;

use Magento\Framework\Module\Manager as ModuleManager;
use Magento\Framework\ObjectManagerInterface;

/**
 * Class SubscribeAbilityChecker
 * @package Aheadworks\Ajaxcartpro\Model
 */
class SubscribeAbilityChecker
{
    /**#@+
     * Constants defined for compatibility with AW SARP module
     */
    const SUBSCRIPTION_MODULE_NAME = 'Aheadworks_Sarp';
    const SUBSCRIBE_ABILITY_CHECKER_CLASS_NAME = 'Aheadworks\Sarp\Model\Product\SubscribeAbilityChecker';
    /**#@-*/

    /**
     * @var ModuleManager
     */
    protected $moduleManager;

    /**
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @param ModuleManager $moduleManager
     * @param ObjectManagerInterface $objectManager
     */
    public function __construct(
        ModuleManager $moduleManager,
        ObjectManagerInterface $objectManager
    ) {
        $this->moduleManager = $moduleManager;
        $this->objectManager = $objectManager;
    }

    /**
     * Check if subscribe action available for product
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return bool
     */
    public function isSubscribeAvailableForProduct($product)
    {
        $flag = false;
        if ($this->isSubscriptionModuleEnabled()) {
            $flag = $this->isProductAllowedForSubscriptionWithModule($product);
        }
        return $flag;
    }

    /**
     * Check if subscription module enabled
     *
     * @return bool
     */
    private function isSubscriptionModuleEnabled()
    {
        return $this->moduleManager->isOutputEnabled(self::SUBSCRIPTION_MODULE_NAME);
    }

    /**
     * Check if subscribe action within definite module available for product
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return bool
     */
    private function isProductAllowedForSubscriptionWithModule($product)
    {
        $flag = false;
        if (class_exists(self::SUBSCRIBE_ABILITY_CHECKER_CLASS_NAME)) {
            $subscribeAbilityChecker = $this->objectManager->create(self::SUBSCRIBE_ABILITY_CHECKER_CLASS_NAME);
            if (is_object($subscribeAbilityChecker)) {
                $flag = $subscribeAbilityChecker->isSubscribeAvailable($product);
            }
        }
        return $flag;
    }
}
