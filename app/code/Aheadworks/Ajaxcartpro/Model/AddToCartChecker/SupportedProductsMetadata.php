<?php
/**
 * Copyright 2018 aheadWorks. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Aheadworks\Ajaxcartpro\Model\AddToCartChecker;

/**
 * Class SupportedProductsMetadata
 *
 * @package Aheadworks\Ajaxcartpro\Model\AddToCartChecker
 */
class SupportedProductsMetadata
{
    /**
     * Supported product types
     *
     * @var array
     */
    private $supportedProductTypes = [];

    /**
     * Product types supported without options popup block
     *
     * @var array
     */
    private $supportedWithoutOptionsPopupProductTypes = [];

    /**
     * Supported product option types
     *
     * @var array
     */
    private $supportedProductOptionTypes = [];

    /**
     * @param array $supportedProductTypes
     * @param array $supportedWithoutOptionsPopupProductTypes
     * @param array $supportedProductOptionTypes
     */
    public function __construct(
        $supportedProductTypes = [],
        $supportedWithoutOptionsPopupProductTypes = [],
        $supportedProductOptionTypes = []
    ) {
        $this->supportedProductTypes = $supportedProductTypes;
        $this->supportedWithoutOptionsPopupProductTypes = $supportedWithoutOptionsPopupProductTypes;
        $this->supportedProductOptionTypes = $supportedProductOptionTypes;
    }

    /**
     * Retrieve supported product types
     *
     * @return array
     */
    public function getSupportedProductTypes()
    {
        return $this->supportedProductTypes;
    }

    /**
     * Retrieve product types supported without options popup block
     *
     * @return array
     */
    public function getSupportedWithoutOptionsPopupProductTypes()
    {
        return $this->supportedWithoutOptionsPopupProductTypes;
    }

    /**
     * Retrieve supported product option types
     *
     * @return array
     */
    public function getSupportedProductOptionTypes()
    {
        return $this->supportedProductOptionTypes;
    }
}
