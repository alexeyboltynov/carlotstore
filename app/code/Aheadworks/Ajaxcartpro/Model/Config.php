<?php
/**
 * Copyright 2018 aheadWorks. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Aheadworks\Ajaxcartpro\Model;

use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Class Config
 * @package Aheadworks\Ajaxcartpro\Model
 */
class Config
{
    /**#@+
     * Constants for config path
     */
    const XML_PATH_ADDTOCART_BLOCK_DISPLAY_REVIEWS = 'aw_ajaxcartpro/add_to_cart_block/display_product_reviews';
    const XML_PATH_ADDTOCART_BLOCK_DISPLAY_SHORT_DESCRIPTION =
        'aw_ajaxcartpro/add_to_cart_block/display_product_short_description';
    const XML_PATH_ADDTOCART_BLOCK_DISPLAY_FOR = 'aw_ajaxcartpro/add_to_cart_block/display_for';
    const XML_PATH_ADDITIONAL_DISPLAY_CONFIRMATION_PRODUCT_PAGE = 'aw_ajaxcartpro/additional/enable_on_product_page';
    const XML_PATH_ADDITIONAL_RELATED_PRODUCTS_TYPE = 'aw_ajaxcartpro/additional/related_products_type';

    const XML_PATH_CHECKOUT_CART_LINK_USE_QTY = 'checkout/cart_link/use_qty';
    const XML_PATH_CHECKOUT_CART_REDIRECT_TO_CART = 'checkout/cart/redirect_to_cart';
    /**#@-*/

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Get "Display Confirmation Message on Product Page" option value
     *
     * @param int|null $storeId
     * @return bool
     */
    public function getDisplayConfigrmatiom($storeId = null)
    {
        return (bool)$this->scopeConfig->getValue(
            self::XML_PATH_ADDITIONAL_DISPLAY_CONFIRMATION_PRODUCT_PAGE,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Get "Display Related Products" option value
     *
     * @param int|null $storeId
     * @return string
     */
    public function getDisplayRelatedProductsType($storeId = null)
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_ADDITIONAL_RELATED_PRODUCTS_TYPE,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Get "On Categories, Display Block For" option value
     *
     * @param int|null $storeId
     * @return int
     */
    public function getDisplayBlockFor($storeId = null)
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_ADDTOCART_BLOCK_DISPLAY_FOR,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Get "Display Product Reviews Summary" option value
     *
     * @param int|null $storeId
     * @return bool
     */
    public function getDisplayReviewsSummary($storeId = null)
    {
        return (bool)$this->scopeConfig->getValue(
            self::XML_PATH_ADDTOCART_BLOCK_DISPLAY_REVIEWS,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Get "Display Product Short Description" option value
     *
     * @param int|null $storeId
     * @return bool
     */
    public function getDisplayShortDescription($storeId = null)
    {
        return (bool)$this->scopeConfig->getValue(
            self::XML_PATH_ADDTOCART_BLOCK_DISPLAY_SHORT_DESCRIPTION,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Get option value from checkout config
     *
     * @param int|null $storeId
     * @return bool
     */
    public function getCheckotCartLinkUseQty($storeId = null)
    {
        return (bool)$this->scopeConfig->getValue(
            self::XML_PATH_CHECKOUT_CART_LINK_USE_QTY,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Get option value from checkout config
     *
     * @param int|null $storeId
     * @return bool
     */
    public function getCheckoutRedirectToCart($storeId = null)
    {
        return (bool)$this->scopeConfig->getValue(
            self::XML_PATH_CHECKOUT_CART_REDIRECT_TO_CART,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }
}
