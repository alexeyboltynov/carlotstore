<?php
/**
 * Copyright 2018 aheadWorks. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Aheadworks\Ajaxcartpro\Model\Renderer;

use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\View\Element\Template;
use Aheadworks\Ajaxcartpro\Model\Config;
use Magento\Framework\App\RequestInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Checkout\Model\Session;
use Magento\Catalog\Model\Product;
use Magento\Quote\Model\Quote;

/**
 * Class Confirmation
 * @package Aheadworks\Ajaxcartpro\Model\Renderer
 */
class Confirmation extends AbstractRenderer
{
    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var Session
     */
    private $checkoutSession;

    /**
     * @var PriceCurrencyInterface
     */
    private $priceCurrency;

    /**
     * Confirmation constructor.
     * @param RequestInterface $request
     * @param ProductRepositoryInterface $productRepository
     * @param Session $checkoutSession
     * @param PriceCurrencyInterface $priceCurrency
     * @param Config $config
     */
    public function __construct(
        RequestInterface $request,
        ProductRepositoryInterface $productRepository,
        Session $checkoutSession,
        PriceCurrencyInterface $priceCurrency,
        Config $config
    ) {
        parent::__construct($config);
        $this->request = $request;
        $this->productRepository = $productRepository;
        $this->checkoutSession = $checkoutSession;
        $this->priceCurrency = $priceCurrency;
    }

    /**
     * @inheritdoc
     */
    public function render($layout)
    {
        /** @var Template $block */
        $block = $layout->createBlock(
            Template::class,
            'aw_ajaxcartpro.ui.confirmation',
            [
                'data' => [
                    'cart_subtotal' => $this->getCartSubtotal(),
                    'cart_summary' => $this->getCartSummary()
                ]
            ]
        );
        $this
            ->appendProductImage($block, $layout, $this->getProduct())
            ->appendReviewSummary($block, $layout, $this->getProduct())
            ->appendMessages($block, $layout);
        return $block
            ->setTemplate('Aheadworks_Ajaxcartpro::ui/confirmation.phtml')
            ->toHtml();
    }

    /**
     * Get product
     *
     * @return Product |bool
     */
    private function getProduct()
    {
        $productId = (int)$this->request->getParam('product');
        if ($productId) {
            return $this->productRepository->getById($productId);
        }
        return false;
    }

    /**
     * Get quote
     *
     * @return Quote
     */
    private function getQuote()
    {
        return $this->checkoutSession->getQuote();
    }

    /**
     * Get cart subtotal
     *
     * @return string
     */
    private function getCartSubtotal()
    {
        $totals = $this->getQuote()->getTotals();
        return $this->priceCurrency->format(
            isset($totals['subtotal']) ? $totals['subtotal']->getValue() : 0,
            true,
            PriceCurrencyInterface::DEFAULT_PRECISION,
            $this->getQuote()->getStore()
        );
    }

    /**
     * Get cart summary
     *
     * @return float|int|null
     */
    private function getCartSummary()
    {
        $useQty = $this->config->getCheckotCartLinkUseQty();
        return $useQty
            ? $this->getQuote()->getItemsQty()
            : $this->getQuote()->getItemsCount();
    }
}
