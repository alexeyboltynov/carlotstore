<?php
/**
 * Copyright 2018 aheadWorks. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Aheadworks\Ajaxcartpro\Model\Renderer;

use Aheadworks\Ajaxcartpro\Block\Ui\Messages;
use Aheadworks\Ajaxcartpro\Block\Ui\Product\Image;
use Aheadworks\Ajaxcartpro\Block\Ui\Product\Reviews;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Layout;
use Aheadworks\Ajaxcartpro\Model\RendererInterface;
use Magento\Catalog\Model\Product;
use Aheadworks\Ajaxcartpro\Model\Config;

/**
 * Class AbstractRenderer
 * @package Aheadworks\Ajaxcartpro\Model\Renderer
 */
abstract class AbstractRenderer implements RendererInterface
{
    /**
     * @var Config
     */
    protected $config;

    /**
     * @param Config $config
     */
    public function __construct(
        Config $config
    ) {
        $this->config = $config;
    }

    /**
     * Append product image to block
     *
     * @param Template $block
     * @param Layout $layout
     * @param Product $product
     * @return $this
     */
    protected function appendProductImage($block, $layout, $product)
    {
        $imageBlock = $layout->createBlock(
            Image::class,
            'aw_ajaxcartpro.ui.product.image',
            ['data' => ['product' => $product]]
        );
        $block->append($imageBlock, 'product_image');
        return $this;
    }

    /**
     * Append reviews info to block
     *
     * @param Template $block
     * @param Layout $layout
     * @param Product $product
     * @return $this
     */
    protected function appendReviewSummary($block, $layout, $product)
    {
        if ($this->config->getDisplayReviewsSummary()) {
            $reviewsBlock = $layout->createBlock(
                Reviews::class,
                'aw_ajaxcartpro.ui.product.reviews',
                ['data' => ['product' => $product]]
            );
            $block->append($reviewsBlock, 'product_reviews');
        }
        return $this;
    }

    /**
     * Append messages to block
     *
     * @param Template $block
     * @param Layout $layout
     * @return $this
     */
    protected function appendMessages($block, $layout)
    {
        $messagesBlock = $layout->createBlock(
            Messages::class,
            'aw_ajaxcartpro.ui.messages',
            ['data' => []]
        );
        $block->append($messagesBlock, 'messages');
        return $this;
    }

    /**
     * Render block
     *
     * @param Layout $layout
     * @return string
     */
    abstract public function render($layout);
}
