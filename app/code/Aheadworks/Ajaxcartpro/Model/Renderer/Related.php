<?php
/**
 * Copyright 2018 aheadWorks. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Aheadworks\Ajaxcartpro\Model\Renderer;

use Aheadworks\Ajaxcartpro\Block\Ui\Related as RelatedBlock;
use Magento\Framework\View\Element\Template;
use Magento\Framework\App\RequestInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Aheadworks\Ajaxcartpro\Model\Config;
use Magento\Catalog\Model\Product;

/**
 * Class Related
 * @package Aheadworks\Ajaxcartpro\Model\Renderer
 */
class Related extends AbstractRenderer
{
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $request;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @param RequestInterface $request
     * @param ProductRepositoryInterface $productRepository
     * @param Config $config
     */
    public function __construct(
        RequestInterface $request,
        ProductRepositoryInterface $productRepository,
        Config $config
    ) {
        parent::__construct($config);
        $this->request = $request;
        $this->productRepository = $productRepository;
    }

    /**
     * @inheritdoc
     */
    public function render($layout)
    {
        $relatedBlock = $layout->createBlock(
            RelatedBlock::class,
            'aw_ajaxcartpro.ui.related',
            ['data' => ['product' => $this->getProduct()]]
        );
        return $relatedBlock->toHtml();
    }

    /**
     * Get product
     *
     * @return Product |bool
     */
    private function getProduct()
    {
        $productId = (int)$this->request->getParam('product');
        if ($productId) {
            return $this->productRepository->getById($productId);
        }
        return false;
    }
}
