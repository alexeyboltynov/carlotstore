/**
* Copyright 2018 aheadWorks. All rights reserved.
* See LICENSE.txt for license details.
*/

define([
    'jquery',
    'jquery/ui',
    'magnificPopup'
], function($) {
    "use strict";
    $.widget('awacp.acpReviews', {
        reviewsTabSelector: '[aria-controls="reviews"]',

        /**
         * Creates widget
         * @private
         */
        _create: function () {
            this.element.on('click', $.proxy(this.onReviewsLinkClick, this));
        },

        /**
         * On reviews link click handler
         *
         * @param {Event} event
         */
        onReviewsLinkClick: function (event) {
            $(this.reviewsTabSelector).click();
            $.magnificPopup.close();
        }
    });

    return $.awacp.acpReviews;
});
