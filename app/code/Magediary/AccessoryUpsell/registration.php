<?php
/**
 * @author  Magediary
 * @package Magediary_AccessoryUpsell
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Magediary_AccessoryUpsell',
    __DIR__
);
