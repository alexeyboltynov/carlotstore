<?php
/**
 * @author  Magediary
 * @package Magediary_AccessoryUpsell
 */

namespace Magediary\AccessoryUpsell\Ui\DataProvider\Product\Form\Modifier;

use Magento\Ui\Component\Form\Fieldset;

class Related extends \Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\Related
{
    const DATA_SCOPE_ACCESSORY_UPSELL = 'accessory_upsell';
    const GROUP_RELATED = 'accessory_upsell';

    /**
     * @var string
     */
    private static $previousGroup = 'related';

    /**
     * @var int
     */
    private static $sortOrder = 100;

    /**
     * {@inheritdoc}
     */
    public function modifyMeta(array $meta)
    {
        $meta = array_replace_recursive(
            $meta,
            [
                static::GROUP_RELATED => [
                    'children' => [
                        $this->scopePrefix . static::DATA_SCOPE_ACCESSORY_UPSELL => $this->getAccessoryUpsellFieldset()
                    ],
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'label' => __('Replacement or accessories parts section'),
                                'collapsible' => true,
                                'componentType' => Fieldset::NAME,
                                'dataScope' => static::DATA_SCOPE,
                                'sortOrder' =>
                                    $this->getNextGroupSortOrder(
                                        $meta,
                                        self::$previousGroup,
                                        self::$sortOrder
                                    ),
                            ],
                        ],

                    ],
                ],
            ]
        );

        return $meta;
    }

    /**
     * Prepares config for the Custom type products fieldset
     *
     * @return array
     */
    protected function getAccessoryUpsellFieldset()
    {
        $content = __(
            'Replacement or accessories parts section.'
        );

        return [
            'children' => [
                'button_set' => $this->getButtonSet(
                    $content,
                    __('Add Replacement Products'),
                    $this->scopePrefix . static::DATA_SCOPE_ACCESSORY_UPSELL
                ),
                'modal' => $this->getGenericModal(
                    __('Add Replacement Products'),
                    $this->scopePrefix . static::DATA_SCOPE_ACCESSORY_UPSELL
                ),
                static::DATA_SCOPE_ACCESSORY_UPSELL => $this->getGrid($this->scopePrefix . static::DATA_SCOPE_ACCESSORY_UPSELL),
            ],
            'arguments' => [
                'data' => [
                    'config' => [
                        'additionalClasses' => 'admin__fieldset-section',
                        'label' => __('Replacement Products'),
                        'collapsible' => false,
                        'componentType' => Fieldset::NAME,
                        'dataScope' => '',
                        'sortOrder' => 90,
                    ],
                ],
            ]
        ];
    }

    /**
     * Retrieve all data scopes
     *
     * @return array
     */
    protected function getDataScopes()
    {
        return [
            static::DATA_SCOPE_ACCESSORY_UPSELL
        ];
    }
}
