<?php
/**
 * @author  Magediary
 * @package Magediary_AccessoryUpsell
 */

namespace Magediary\AccessoryUpsell\Block\Pricing;

use Magento\Framework\Pricing\SaleableInterface;

class Render extends \Magento\Framework\Pricing\Render
{

    /**
     * Render price
     *
     * @param  string            $priceCode
     * @param  SaleableInterface $saleableItem
     * @param  array             $arguments
     * @return string
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    public function render($priceCode, SaleableInterface $saleableItem, array $arguments = [])
    {
        $useArguments = array_replace($this->_data, $arguments);

        /**
        * @var \Magento\Framework\Pricing\Render\RendererPool $rendererPool
        */
        $rendererPool = $this->priceLayout->getBlock('render.accessory.upsell.product.prices');
        if (!$rendererPool) {
            throw new \RuntimeException('Wrong Price Rendering layout configuration. Factory block is missed');
        }

        // obtain concrete Price Render
        $priceRender = $rendererPool->createPriceRender($priceCode, $saleableItem, $useArguments);
        return $priceRender->toHtml();
    }
}
