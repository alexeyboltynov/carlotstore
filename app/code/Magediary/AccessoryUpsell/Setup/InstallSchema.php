<?php
/**
 * @author  Magediary
 * @package Magediary_AccessoryUpsell
 */

namespace Magediary\AccessoryUpsell\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;
        $installer->startSetup();

        /**
         * Add entry in product link types
         */
        $data = [
            ['link_type_id' => \Magediary\AccessoryUpsell\Model\Catalog\Product\Link::LINK_TYPE_ACCESSORY_UPSELL, 'code' => 'accessory_upsell']
        ];

        foreach ($data as $bind) {
            $setup->getConnection()
                ->insertForce($setup->getTable('catalog_product_link_type'), $bind);
        }

        /**
         * Add entry in product link attributes
         */
        $data = [
            [
                'link_type_id' => \Magediary\AccessoryUpsell\Model\Catalog\Product\Link::LINK_TYPE_ACCESSORY_UPSELL,
                'product_link_attribute_code' => 'position',
                'data_type' => 'int',
            ]
        ];

        $setup->getConnection()
            ->insertMultiple($setup->getTable('catalog_product_link_attribute'), $data);

        $setup->endSetup();
    }
}
