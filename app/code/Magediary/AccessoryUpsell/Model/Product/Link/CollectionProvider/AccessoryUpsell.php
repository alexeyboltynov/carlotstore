<?php
/**
 * @author  Magediary
 * @package Magediary_AccessoryUpsell
 */

namespace Magediary\AccessoryUpsell\Model\Product\Link\CollectionProvider;

class AccessoryUpsell implements \Magento\Catalog\Model\ProductLink\CollectionProviderInterface
{
    /**
     * {@inheritdoc}
     */
    public function getLinkedProducts(\Magento\Catalog\Model\Product $product)
    {
        $products = $this->getAccessoryUpsellProducts($product);

        if (!isset($products)) {
            return [];
        }

        return $products;
    }

    /**
     * Retrieve array of accessory upsell products
     *
     * @return array
     */
    public function getAccessoryUpsellProducts($product)
    {
        if (!$product->hasAccessoryUpsellProducts()) {
            $products = [];
            $accessoryUpsellProducts = $this->getAccessoryUpsellProductCollection($product);
            foreach ($accessoryUpsellProducts as $p) {
                $products[] = $p;
            }
            $product->setAccessoryUpsellProducts($products);
        }
        return $product->getData('accessory_upsell_products');
    }

    /**
     * Retrieve collection custom type product
     *
     * @return \Magento\Catalog\Model\ResourceModel\Product\Link\Product\Collection
     */
    public function getAccessoryUpsellProductCollection($product)
    {
        $collection = $product->getLinkInstance()->useAccessoryUpsellLinks()->getProductCollection()->setIsStrongMode();
        $collection->setProduct($product);
        return $collection;
    }
}
