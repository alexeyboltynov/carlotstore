<?php
/**
 * @author  Magediary
 * @package Magediary_AccessoryUpsell
 */

namespace Magediary\AccessoryUpsell\Model\CatalogImportExport\Export;

class Product extends \Magento\CatalogImportExport\Model\Export\Product
{
    /**
     * {@inheritdoc}
     */
    protected function setHeaderColumns($customOptionsData, $stockItemRows)
    {
        if (!$this->_headerColumns) {
            parent::setHeaderColumns($customOptionsData, $stockItemRows);

            $this->_headerColumns = array_merge(
                $this->_headerColumns,
                [
                    'accessory_upsell_skus',
                    'accessory_upsell_position'
                ]
            );
        }
    }
}
