<?php
/**
 * @author  Magediary
 * @package Magediary_AccessoryUpsell
 */

namespace Magediary\AccessoryUpsell\Model\Catalog\Product;

class Link extends \Magento\Catalog\Model\Product\Link
{
    const LINK_TYPE_ACCESSORY_UPSELL = 6;

    /**
     * @return \Magento\Catalog\Model\Product\Link $this
     */
    public function useAccessoryUpsellLinks()
    {
        $this->setLinkTypeId(self::LINK_TYPE_ACCESSORY_UPSELL);
        return $this;
    }

    /**
     * Save data for product relations
     *
     * @param  \Magento\Catalog\Model\Product $product
     * @return \Magento\Catalog\Model\Product\Link
     */
    public function saveProductRelations($product)
    {
        parent::saveProductRelations($product);

        $data = $product->getAccessoryUpsellData();
        if ($data) {
            $this->_getResource()->saveProductLinks($product->getId(), $data, self::LINK_TYPE_ACCESSORY_UPSELL);
        }

        return $this;
    }
}
