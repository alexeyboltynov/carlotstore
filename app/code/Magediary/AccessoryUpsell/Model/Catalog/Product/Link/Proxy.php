<?php
/**
 * @author  Magediary
 * @package Magediary_AccessoryUpsell
 */

namespace Magediary\AccessoryUpsell\Model\Catalog\Product\Link;

class Proxy extends \Magento\Catalog\Model\Product\Link\Proxy
{
    /**
     * {@inheritdoc}
     */
    public function useAccessoryUpsellLinks()
    {
        return $this->_getSubject()->useAccessoryUpsellLinks();
    }
}
