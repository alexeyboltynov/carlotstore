# Blue Jalappeno Tiered Pricing Across Multiple Products
Create multibuy discounts across your catalog.
For more information on Tiered Pricing Across Multiple Products, head to www.bluejalappeno.com


Description
-----------
A simple extension that extends on Magento's tier prices.

You can combine products across categories and product types into a logical group.

When any of those products are in the cart together, their quantities count towards a tier discount.

Compatibility
-------------
- Magento >= 2.0

Installation Instructions
-------------------------
For installation from a zip archive

Please follow these instructions exactly as written in order, step by step, or you could see unexpected results.

WARNING: We recommend you backup your Magento installation and database immediately prior to installing any extension. We do not take any responsibility for issues caused due to not taking this precaution. We also recommend you install and test on a non-business critical environment e.g. development or integration server, prior to deploying to your live site

1. After downloading the extension zip file, unzip to a local directory.
2. Copy the 'app' directory from 'src' onto your base Magento installation, overwriting all files. Note - this will not replace any core Magento files.
3. Open a command prompt and navigate to the root directory of your Magento installation
4. Run commands
        bin/magento module:enable Bluejalappeno_TieredPricing
        bin/magento setup:upgrade
5. Remove the contents of the following directories
        var/cache
        var/generation
        var/di
6. If you are using the compiler, recompile
7. Re-index all of your data

Support
-------
If you have any issues with this extension, you can contact us via email at sales@bluejalappeno.com

License
-------
See license files

Copyright
---------
Copyright (c) 2016 Wimbolt Pty Ltd (http://www.BlueJalappeno.com)