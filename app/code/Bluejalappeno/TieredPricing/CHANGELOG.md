# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [20.0.0] - 2016-08-29
### Added
- Initial release version for Magento v2.x

## [20.0.1] - 2016-08-29
### Added
- Code sniffer modifications


## [20.0.2] - 2017-09-18
### Added
- Modified calculations for configurable products
