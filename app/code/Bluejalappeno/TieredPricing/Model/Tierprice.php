<?php
/**
 * BlueJalappeno
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * BlueJalappeno Tiered Pricing Across Multiple Products
 *
 * @category  BlueJalappeno
 * @package   Bluejalappeno_TieredPricing
 * @author    BlueJalappeno Team <sales@bluejalappeno.com>
 * @copyright Copyright (c) 2016 Wimbolt Pty Ltd (http://www.bluejalappeno.com)
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link      www.bluejalappeno.com
 */
/**
 * Copyright � 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/**
 * Calculate Cross Product Tiered Pricing
 */
namespace Bluejalappeno\TieredPricing\Model;

class Tierprice
{
    /**
     * Checkout Session
     *
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;
    /**
     * Session Quote
     *
     * @var \Magento\Backend\Model\Session\Quote
     */
    protected $sessionQuote;
    /**
     * ScopeConfig
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;
    /**
     * Logger
     *
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;
    /*
     * ProductFactory
     *
     * \Magento\Catalog\Model\ProductFactory
     */
    protected $productFactory;

    /**
     * Construct
     *
     * @param \Magento\Checkout\Model\Session                    $checkoutSession
     * @param  \Magento\Backend\Model\Session\Quote              $sessionQuote
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Psr\Log\LoggerInterface                           $logger
     * @param \Magento\Catalog\Model\ProductFactory              $productFactory
     */
    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Backend\Model\Session\Quote $sessionQuote,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Catalog\Model\ProductFactory $productFactory
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->sessionQuote = $sessionQuote;
        $this->scopeConfig = $scopeConfig;
        $this->logger = $logger;
        $this->productFactory = $productFactory;
    }

    /**
     * Calculate cross product tiered pricing
     *
     * @param \Magento\Catalog\Model\Product $product
     *
     * @return boolean
     */
    public function calculateTierPrice(\Magento\Catalog\Model\Product $product)
    {
        $debug = $this->scopeConfig->getValue(
            'catalog/tieredpricing/debug',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        $currentTierId = $product->getCrossTierId();
        if ($currentTierId == '' || $product->getTypeId() == \Magento\Catalog\Model\Product\Type::TYPE_BUNDLE) {
            return false;
        }
        if ($debug) {
            $this->logger->debug(
                'Blue Jalappeno Tiered Pricing :: ',
                ['product' => $product->getSku(), 'cross tier id' => $currentTierId]
            );
        }
        $baseProductId = $product->getId();
        $storeId = $product->getStoreId();
        $finalPrice = $product->getData('final_price');

        // Get array of items in cart
        $quote = $this->checkoutSession->getQuote();
        $items = $quote->getAllItems();

        if (empty($items)) {
            $quote = $this->sessionQuote->getQuote();
            $items =  $quote->getAllItems();
        }

        $tieredItems= [];

        $masterTierId="";
        $masterQty = 0;

        foreach ($items as $item) {
            if ($item->getCrossTierId() == $currentTierId) {
                $masterQty += $item->getQty();
            }
        }
        $tierPrice = $product->getTierPrice($masterQty);
        $finalPrice = min($finalPrice, $tierPrice);

        foreach ($items as $item) {
            $crossTierId="";
            $productId = $item->getProductId();
            $isBundle = $item->getRealProductType() == \Magento\Catalog\Model\Product\Type::TYPE_BUNDLE;
            $isConfigurable = $item->getRealProductType() == 'configurable';
            if ($isBundle || $isConfigurable) {
                continue;
            }
            $qty = $item->getQty();
            if ($item->getParentItem()!=null) {
                if ($item->getParentItem()->getRealProductType() == \Magento\Catalog\Model\Product\Type::TYPE_BUNDLE) {
                    continue;
                }
                if ($item->getParentItem()->getRealProductType() == \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE) {
                    $qty = $item->getParentItem()->getQty();
                }
            }

            $cloneProduct = $this->productFactory->create()->load($productId);
            //  $crossTierId = $item->getProduct()->getCrossTierId();
            $crossTierId = $cloneProduct->getCrossTierId();
            if (!empty($crossTierId)) {
                $found = false;
                $marked = false;
                if ($item->getProductId() == $baseProductId) {
                    $marked=true;
                    if ($item->getRealProductType() != \Magento\Catalog\Model\Product\Type::TYPE_BUNDLE) {
                        $masterTierId=$crossTierId;
                    }
                }
                // lets collate like products together
                foreach ($tieredItems as $key=>$tieredItem) {

                    if ($tieredItem['tier_id']==$crossTierId) {
                        $found = true;
                        $tieredItems[$key]['qty']+=$qty;
                        $tieredItems[$key]['multiple']=true;
                        if ($marked) {
                            $tieredItems[$key]['marked']=true;
                        }
                        break;
                    }
                }
                if (!$found) {
                    $tieredItem=array('tier_id'            => $crossTierId,
                        'qty'             => $qty,
                        'multiple'         => false,
                        'marked'        => $marked);

                    $tieredItems[]=$tieredItem;
                }
            }

        }
        if ($debug) {
            $this->logger->debug(
                'Blue Jalappeno Tiered Pricing tiered items :: ',
                $tieredItems
            );
        }
        if (count($tieredItems) < 1) {
            if ($debug) {
                $this->logger->debug(
                    'Blue Jalappeno Tiered Pricing ',
                    ['No calculation required']
                );
            }
            return false;
        }

        foreach ($tieredItems as $tieredItem) {
		    if ($tieredItem['marked']
		        && $tieredItem['multiple']
		        && $masterTierId==$tieredItem['tier_id']
		    ) {
		        $tierPrice = $product->getTierPrice($tieredItem['qty']);
		        if (is_numeric($tierPrice)) {
		            $finalPrice = min($finalPrice, $tierPrice);
		        }
		        break;
		    } else if ($currentTierId ==$tieredItem['tier_id']) {
		        $tierPrice = $product->getTierPrice($tieredItem['qty']);
		        if (is_numeric($tierPrice)) {
		            $finalPrice = min($finalPrice, $tierPrice);
		        }
		        break;
		    }
        }
        if ($debug) {
            $this->logger->debug(
                'Blue Jalappeno Tiered Pricing ',
                ['final price' => $finalPrice]
            );
        }

        $product->setData('final_price', $finalPrice);
        return true;
    }

}
