<?php
/**
 * BlueJalappeno
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * BlueJalappeno Tiered Pricing Across Multiple Products
 *
 * @category  BlueJalappeno
 * @package   Bluejalappeno_TieredPricing
 * @author    BlueJalappeno Team <sales@bluejalappeno.com>
 * @copyright Copyright (c) 2016 Wimbolt Pty Ltd (http://www.bluejalappeno.com)
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link      www.bluejalappeno.com
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Bluejalappeno_TieredPricing',
    __DIR__
);