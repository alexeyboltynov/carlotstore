<?php
/**
 * BlueJalappeno
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * BlueJalappeno Tiered Pricing Across Multiple Products
 *
 * @category  BlueJalappeno
 * @package   Bluejalappeno_TieredPricing
 * @author    BlueJalappeno Team <sales@bluejalappeno.com>
 * @copyright Copyright (c) 2016 Wimbolt Pty Ltd (http://www.bluejalappeno.com)
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link      www.bluejalappeno.com
 */
/**
 * Calculate Cross Product Tiered Pricing
 */
namespace Bluejalappeno\TieredPricing\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

class CalculateCrossProductTieredPricing implements ObserverInterface
{
    /**
     * Tierprice Service
     *
     * @var \Bluejalappeno\TieredPricing\Model\Tierprice
     */
    protected $tierPriceService;
    /**
     * ScopeConfig
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;
    /**
     *Logger
     *
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * Construct
     *
     * @param \Bluejalappeno\TieredPricing\Model\Tierprice       $tierPriceService
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Psr\Log\LoggerInterface                           $logger
     */
    public function __construct(
        \Bluejalappeno\TieredPricing\Model\Tierprice $tierPriceService,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->tierPriceService = $tierPriceService;
        $this->scopeConfig = $scopeConfig;
        $this->logger = $logger;
    }

    /**
     * Apply cross product tiered pricing
     *
     * @param \Magento\Framework\Event\Observer $observer
     *
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if (!$this->scopeConfig->getValue(
            'catalog/tieredpricing/active',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        )) {
            return $this;
        }
        $product = $observer->getEvent()->getProduct();

        $this->tierPriceService->calculateTierPrice($product);
        return $this;
    }

}
