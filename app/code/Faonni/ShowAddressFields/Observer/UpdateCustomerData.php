<?php

namespace Faonni\ShowAddressFields\Observer;

use Magento\Framework\Event\ObserverInterface;

class UpdateCustomerData implements ObserverInterface
{
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/product.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info("product updated with id and sku = $sku Saved successfully"); 
    }
}