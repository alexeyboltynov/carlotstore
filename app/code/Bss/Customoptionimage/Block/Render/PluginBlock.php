<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_Customoptionimage
 * @author     Extension Team
 * @copyright  Copyright (c) 2018-2019 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */

namespace Bss\Customoptionimage\Block\Render;

use Magento\Framework\UrlInterface;

class PluginBlock extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Bss\Customoptionimage\Helper\ModuleConfig
     */
    protected $moduleConfig;

    /**
     * @var \Magento\Framework\Json\EncoderInterface
     */
    protected $jsonEncoder;

    /**
     * @var \Bss\Customoptionimage\Helper\ImageSaving
     */
    protected $imageSaving;

    /**
     * @var null
     */
    protected $baseMediaUrl = null;

    /**
     * PluginBlock constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
     * @param \Bss\Customoptionimage\Helper\ModuleConfig $moduleConfig
     * @param \Bss\Customoptionimage\Helper\ImageSaving $imageSaving
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Bss\Customoptionimage\Helper\ModuleConfig $moduleConfig,
        \Bss\Customoptionimage\Helper\ImageSaving $imageSaving,
        array $data = []
    ) {
        $this->storeManager = $context->getStoreManager();
        $this->moduleConfig = $moduleConfig;
        $this->jsonEncoder = $jsonEncoder;
        $this->imageSaving = $imageSaving;
        parent::__construct($context, $data);
    }

    /**
     * @inheritdoc
     */
    public function _construct()
    {
        $this->setTemplate('Bss_Customoptionimage::select/image-render.phtml');
    }

    /**
     * @return \Bss\Customoptionimage\Helper\ModuleConfig
     */
    public function getConfigHelper()
    {
        return $this->moduleConfig;
    }

    /**
     * @return string|null
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getBaseMediaUrl()
    {
        if (!$this->baseMediaUrl) {
            $this->baseMediaUrl = $this->storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
        }
        return $this->baseMediaUrl;
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getImageList()
    {
        $result = [];
        $values = $this->getOption()->getValues();
        foreach ($values as $value) {
            $valueData = $value->getData();
            if ($valueData['image_url']) {
                $image = $this->getImageUrl($valueData['image_url']);
                $result[] = [
                    'id' => $valueData['option_type_id'],
                    'url' => $image,
                    'title' => $value['title']
                ];
            }
        }
        return $this->jsonEncoder->encode($result);
    }

    /**
     * @param string $image
     * @param null $imageWidth
     * @param null $imageHeight
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getImageUrl($image, $imageWidth = null, $imageHeight = null)
    {
        if (!$imageWidth) {
            $imageWidth = $this->moduleConfig->getImageX($this->getOption()->getType());
        }
        if (!$imageHeight) {
            $imageHeight = $this->moduleConfig->getImageY($this->getOption()->getType());
        }

        $image = $this->imageSaving->resize($image, $imageWidth, $imageHeight);
        return $image;
    }
}
