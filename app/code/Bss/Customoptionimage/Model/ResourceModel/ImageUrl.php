<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_Customoptionimage
 * @author     Extension Team
 * @copyright  Copyright (c) 2018-2019 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\Customoptionimage\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class ImageUrl extends AbstractDb
{
    /**
     * construct
     */
    public function _construct()
    {
        $this->_init('bss_catalog_product_option_type_image', 'image_id');
    }

    /**
     * @param int $optionId
     * @return bool|mixed
     * @throws \Zend_Db_Statement_Exception
     */
    public function getImageOptionUrl($optionId)
    {
        $connection = $this->getConnection();
        $bssOptionImg = $this->getTable('bss_catalog_product_option_type_image');
        $select = $connection->select()
            ->from(
                $bssOptionImg,
                'image_url'
            )->where('option_type_id = ?', $optionId);
        $data = $this->getConnection()->fetchOne($select);

        return $data;
    }
}
