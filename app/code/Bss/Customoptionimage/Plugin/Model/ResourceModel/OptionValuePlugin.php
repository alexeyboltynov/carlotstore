<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_Customoptionimage
 * @author     Extension Team
 * @copyright  Copyright (c) 2018-2019 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */

namespace Bss\Customoptionimage\Plugin\Model\ResourceModel;

/**
 * Class OptionValuePlugin
 *
 * @package Bss\Customoptionimage\Plugin\Model\ResourceModel
 */
class OptionValuePlugin
{
    /**
     * @var \Bss\Customoptionimage\Helper\ImageSaving
     */
    private $imageSaving;

    /**
     * @var \Bss\Customoptionimage\Model\ImageUrlFactory
     */
    private $imageUrlFactory;

    /**
     * OptionValuePlugin constructor.
     * @param \Bss\Customoptionimage\Helper\ImageSaving $imageSaving
     * @param \Bss\Customoptionimage\Model\ImageUrlFactory $imageUrlFactory
     */
    public function __construct(
        \Bss\Customoptionimage\Helper\ImageSaving $imageSaving,
        \Bss\Customoptionimage\Model\ImageUrlFactory $imageUrlFactory
    ) {
        $this->imageSaving = $imageSaving;
        $this->imageUrlFactory = $imageUrlFactory;
    }

    /**
     * Around Plugin Save Value
     *
     * @param \Magento\Catalog\Model\Product\Option\Value $subject
     * @param callable $proceed
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function aroundSave(
        \Magento\Catalog\Model\Product\Option\Value $subject,
        callable $proceed
    ) {
        $imageUrl = $this->imageSaving->moveImage($subject);
        $proceed();
        $imageUrlModel = $this->imageUrlFactory->create()->getCollection()
        ->getItemByColumnValue('option_type_id', $subject->getOptionTypeId());
        if (!$imageUrlModel) {
            $imageUrlModel = $this->imageUrlFactory->create();
        }
        $imageUrlModel
        ->setOptionTypeId($subject->getOptionTypeId())
        ->setImageUrl($imageUrl)
        ->save();
    }

    /**
     * Around get data
     *
     * @param \Magento\Catalog\Model\Product\Option\Value $subject
     * @param callable $proceed
     * @param string $key
     * @param int $index
     * @return \Magento\Framework\DataObject
     */
    public function aroundGetData(
        \Magento\Catalog\Model\Product\Option\Value $subject,
        callable $proceed,
        $key = '',
        $index = null
    ) {
        $result = $proceed($key, $index);
        $imageUrl = $this->imageUrlFactory->create();
        if ($key === '') {
            if (isset($result['option_type_id']) && !isset($result['image_url'])) {
                $imageData = $imageUrl->getImageOptionUrl($result['option_type_id']);
                $result['image_url'] = $imageData;
            }
        }
        if ($key === 'image_url' && $subject->getData('option_type_id') && !$subject->hasData('image_url')) {
            $imageData = $imageUrl->getImageOptionUrl($subject->getData('option_type_id'));
            return $imageData;
        }
        return $result;
    }

    /**
     * Add data to value
     *
     * @param \Magento\Catalog\Model\Product\Option\Value $subject
     * @param \Magento\Catalog\Model\ResourceModel\Product\Option\Value\Collection $result
     * @return mixed
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function afterGetValuesCollection(
        \Magento\Catalog\Model\Product\Option\Value $subject,
        $result
    ) {
        $result->getSelect()->joinLeft(
            ['opt' => $subject->getCollection()->getTable('bss_catalog_product_option_type_image')],
            'opt.option_type_id=main_table.option_type_id'
        );
        foreach ($result as $value) {
            $newUrl = $this->imageSaving->duplicateImage($value->getImageUrl());
            $value->setImageUrl($newUrl);
        }
        return $result;
    }
}
