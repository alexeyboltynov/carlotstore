/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_Customoptionimage
 * @author     Extension Team
 * @copyright  Copyright (c) 2018-2019 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
define([
    'jquery',
    'underscore',
    'mage/template',
    'priceUtils',
    'jquery/ui'
], function ($, _, mageTemplate) {
    'use strict';

    $.widget('bss.coiStaticImage', {
        _create: function () {
            var $widget = this;
            $widget.updateImage($widget);
            if ($('#bundle-slide').length == 0 || $('#bundle-slide').length == 1) {
                $widget.updateStyle($widget, false);
            }
            $('#bundle-slide').click(function () {
                $widget.updateStyle($widget, true);
                console.log($('#bundle-slide').length);
            });
            $widget.element.on("click", "img", function() {
                var inputElement = $(this).closest('.Bss_image_radio').find('.product-custom-option');
                if (inputElement.attr('type') == 'checkbox') {
                    if (inputElement.prop("checked")) {
                        inputElement.prop("checked", false);
                        $(this).css('border', '0px');
                        inputElement.trigger('change');
                    } else {
                        //inputElement.prop("checked", true);
                        //$(this).css('border', '1px solid red');
                        //inputElement.trigger('change');
                    }
                } else if (inputElement.attr('type') == 'radio') {
                    $($widget.element).find('img').map(function (index, element) {
                        $(element).css('border', '0px');
                    });
                    if (inputElement.prop("checked")) {
                        //$(this).css('border', '1px solid red');
                    } else {
                        //inputElement.prop("checked", true);
                        //$(this).css('border', '1px solid red');
                        //inputElement.trigger('change');
                    }
                }
            });
            $widget.element.on("click", "input", function() {
                var inputElement = $(this).closest('.Bss_image_radio').children('img');
                if ($(this).attr('type') == 'checkbox') {
                    if (!$(this).prop("checked")) {
                        inputElement.css('border', '0px');
                    } else {
                        $(this).prop("checked", true);
                        inputElement.css('border', '1px solid red');
                    }
                } else if ($(this).attr('type') == 'radio') {
                    $($widget.element).find('img').map(function (index, element) {
                        $(element).css('border', '0px');
                    });
                    if (!$(this).prop("checked")) {
                        inputElement.css('border', '0px');
                    } else {
                        inputElement.css('border', '1px solid red');
                    }
                }
            });
        },
        updateImage: function ($widget) {
            $.each($widget.options.imageUrls, function (index, value) {
                var new_image = value.url.replace('/resized/105x105','');
                $widget.element.find('input[value="' + value.id + '"]').parent('.field.choice')
                    .append(mageTemplate('<a class="fancybox_mangoit" rel="group" href="'+new_image+'"><img src="<%- data.url %>" class="mangoit_img" title="<%- data.title %>" alt="" /></a>', {data: value}))
                    .children().wrapAll('<div class="Bss_image_radio"></div>');
            });
            jQuery(".fancybox_mangoit").fancybox({
                    prevEffect      : 'none',
                    nextEffect      : 'none',
                    closeBtn        : true,
                    autoScale        : false,
                    helpers     : {
                        title   : { type : 'inside' },
                        buttons : {}
                    }
                });
        },
        updateStyle: function ($widget, $isBundle) {
            var $element = $widget.element;
            //$element.find('.Bss_image_radio img').height($widget.options.imageHeight).width($widget.options.imageWidth);
            $element.find('.Bss_image_radio').each(function () {
                if ($isBundle) {
                    if ($(this).width() - Number($widget.options.imageWidth) - 90 < $(this).find('.label').width()) {
                        $(this).find('.price-notice').css('display', 'block');
                    }
                }
                var height = Number($widget.options.imageHeight),
                    labelMargin = Number($widget.options.imageHeight)/2 - 4;
                if ($widget.options.priceDisplayType === 3) {
                    var marginDivision = Number($widget.options.imageHeight)/3;
                    labelMargin = marginDivision  - (marginDivision/10 + 4) ;
                }
                if ($(this).find('.label').height() >= Number($widget.options.imageHeight) - 6) {
                    height = $(this).find('label').height();
                    labelMargin = 5;
                    var imgMargin = (height - Number($widget.options.imageHeight))/2 + 5;
                    //$(this).find('img').css('margin-top', imgMargin + 'px');
                }

                //$(this).height(height + 10);
                //$(this).find('input').css('margin-top', height/2 + 6 + 'px');
                //$(this).find('.label').css('margin-left', '40px').css('margin-top', labelMargin + 'px');
            });
        }
    });
    return $.bss.coiStaticImage;
});
