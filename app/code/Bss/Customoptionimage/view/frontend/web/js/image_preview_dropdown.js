/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_Customoptionimage
 * @author     Extension Team
 * @copyright  Copyright (c) 2018-2019 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
define([
    'jquery',
    'underscore',
    'mage/template',
    'priceUtils',
    'jquery/ui'
], function ($) {
    'use strict';

    $.widget('bss.bss_preview', {
        _create: function () {
            var $widget = this;
            var url = $widget.updateImage($widget);
            $widget.cartOptionUpdate($widget, url);
            $widget.updateEventListener($widget, url);
        },
        updateEventListener: function ($widget, url) {
            var viewType = this.options.viewType;

            $widget.element.find('select.product-custom-option:not(.multiselect)').change(function () {
                if (viewType == 0) {
                    var element = $widget.element.find('.Bss_image_preview img');
                    if (typeof url[$(this).val()] == 'string' && url[$(this).val()].length > 0) {
                        element.attr('src', url[$(this).val()]);
                        element.attr('title', $("option[value=" + $(this).val() + "]").html());
                        $widget.element.find('.Bss_image_preview').fadeIn();
                    } else {
                        $widget.element.find('.Bss_image_preview').fadeOut();
                    }
                } else if (viewType == 1) {
                    if (typeof url[$(this).val()] == 'string') {
                        var element = $widget.element.find('#image_preview_' + $(this).val());
                        $widget.element.find('.Bss_image_preview img').css('border','solid 2px #ddd');
                        element.css('border','solid 2px #d33');
                    } else {
                        $widget.element.find('.Bss_image_preview img').css('border','solid 2px #ddd');
                    }
                }
            });
            $widget.element.on("click", "img", function() {
                var imageId = $(this).attr('id').split('_'),
                    optionId = imageId[2],
                    dropdown = $widget.element.find('.product-custom-option');
                if (dropdown.val() == optionId) {
                    dropdown.val('').trigger('change');
                } else {
                    dropdown.val(optionId).trigger('change');
                }
            });
        },
        updateImage: function ($widget) {
            var result = [];
            $.each($widget.options.imageUrls, function (index, image) {
                result[image.id] = image.url;
            });
            return result;
        },
        cartOptionUpdate: function ($widget, url) {
            var viewType = this.options.viewType,
                width = this.options.imageWidth,
                height = this.options.imageHeight;

            $('.Bss_image_preview img').css('width', width + 'px')
                .css('height', height + 'px');
            if (this.options.viewType == 1) {
                $('.Bss_image_preview').fadeIn();
            }

            var dropdownCartOptions = $widget.element.find('select.product-custom-option:not(.multiselect)');

            if (viewType == 0) {
                var element = $widget.element.find('.Bss_image_preview img');
                if (typeof url[$(dropdownCartOptions).val()] == 'string' && url[$(dropdownCartOptions).val()].length > 0) {
                    element.attr('src', url[$(dropdownCartOptions).val()]);
                    element.attr('title', $("option[value=" + $(dropdownCartOptions).val() + "]").html());
                    $widget.element.find('.Bss_image_preview').fadeIn();
                } else {
                    $widget.element.find('.Bss_image_preview').fadeOut();
                }
            } else if (viewType == 1) {
                if (typeof url[$(dropdownCartOptions).val()] == 'string') {
                    var element = $widget.element.find('#image_preview_' + $(dropdownCartOptions).val());
                    $widget.element.find('.Bss_image_preview img').css('border','solid 2px #ddd');
                    element.css('border','solid 2px #d33');
                } else {
                    $widget.element.find('.Bss_image_preview img').css('border','solid 2px #ddd');
                }
            }
        }
    });
    return $.bss.bss_preview;
});
