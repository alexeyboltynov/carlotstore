<?php
namespace Bss\Customoptionimage\Setup;

use Bss\Customoptionimage\Model\ImageUrlFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var ImageUrlFactory
     */
    private $imageUrlFactory;

    /**
     * UpgradeData constructor.
     * @param ImageUrlFactory $imageUrlFactory
     */
    public function __construct(
        ImageUrlFactory $imageUrlFactory
    ) {
        $this->imageUrlFactory = $imageUrlFactory;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.6') < 0) {
            $this->rebuildUrl();
        }

        $setup->endSetup();
    }

    /**
     * {@inheritdoc}
     */
    public function rebuildUrl()
    {
        $this->imageUrlFactory->create()->getCollection()->walk('rebuildUrl');
    }
}
