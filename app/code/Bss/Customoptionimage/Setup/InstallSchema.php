<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_Customoptionimage
 * @author     Extension Team
 * @copyright  Copyright (c) 2018-2019 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */

namespace Bss\Customoptionimage\Setup;

class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{
    /**
     * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
     * @param \Magento\Framework\Setup\ModuleContextInterface $context
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function install(
        \Magento\Framework\Setup\SchemaSetupInterface $setup,
        \Magento\Framework\Setup\ModuleContextInterface $context
    ) {
        $installer = $setup;
        $installer->startSetup();
        if (!$installer->tableExists('bss_catalog_product_option_type_image')) {
            $table = $installer->getConnection()
                ->newTable(
                    $installer->getTable('bss_catalog_product_option_type_image')
                )
                ->addColumn(
                    'option_type_image_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    ['unsigned' => true, 'nullable' => false],
                    'Option Type Image ID'
                )
                ->addColumn(
                    'option_type_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    ['primary' => true, 'unsigned' => true, 'nullable' => false],
                    'Option Type ID'
                )
                ->addColumn(
                    'image_url',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    ['nullable' => true, 'default' => null],
                    'Url'
                )->addIndex(
                    $installer->getIdxName('bss_catalog_product_option_type_image', ['option_type_id']),
                    ['option_type_id']
                )
                ->setComment(
                    'Catalog Product Option Type Image Url'
                );
                $installer->getConnection()->createTable($table);
        }
        $installer->endSetup();
    }
}
