<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_Customoptionimage
 * @author     Extension Team
 * @copyright  Copyright (c) 2018-2019 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\Customoptionimage\Observer\Adminhtml;

use \Magento\Framework\Event\ObserverInterface;
use \Magento\Framework\Event\Observer as EventObserver;
use Magento\Ui\Component\Form\Field;
use Magento\Ui\Component\Form\Element\Checkbox;
use Magento\Ui\Component\Form\Element\DataType\Text;
use Magento\Ui\Component\Form\Element\DataType\Media;
use Magento\Framework\UrlInterface;

class AddBackendField implements ObserverInterface
{
    const FIELD_UPLOAD_IMAGE_PREVIEW = 'image_url';
    const FIELD_UPLOAD_IMAGE_BUTTON = 'bss_image_button';

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * AddBackendField constructor.
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param UrlInterface $urlBuilder
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        UrlInterface $urlBuilder
    ) {
        $this->storeManager = $storeManager;
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * @param EventObserver $observer
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $observer->getChild()->addData($this->getCustomImageField());
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function getCustomImageField()
    {
        return [
            230 => ['index' => static::FIELD_UPLOAD_IMAGE_PREVIEW, 'field' => $this->getImagePreviewFieldConfig(230)],
            240 => ['index' => static::FIELD_UPLOAD_IMAGE_BUTTON, 'field' => $this->getUploadButtonFieldConfig(240)]
        ];
    }

    /**
     * @param $sortOrder
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function getImagePreviewFieldConfig($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label' => __('Image'),
                        'componentType' => Field::NAME,
                        'component' => 'Bss_Customoptionimage/js/image_preview',
                        'elementTmpl' => 'Bss_Customoptionimage/image-preview',
                        'dataScope' => static::FIELD_UPLOAD_IMAGE_PREVIEW,
                        'dataType' => Text::NAME,
                        'formElement' => Checkbox::NAME,
                        'sortOrder' => $sortOrder,
                        'mediaUrl' => $this->storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA),
                        'valueMap' => [
                            'true' => '1',
                            'false' => ''
                        ]
                    ],
                ],
            ],
        ];
    }

    /**
     * @param $sortOrder
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function getUploadButtonFieldConfig($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'formElement' => 'fileUploader',
                        'componentType' => 'file',
                        'component' => 'Bss_Customoptionimage/js/upload_field',
                        'elementTmpl' => 'Bss_Customoptionimage/upload-field',
                        'visible' => true,
                        'dataType' => Media::NAME,
                        'required' => false,
                        'label' => __('Upload'),
                        'dataScope' => static::FIELD_UPLOAD_IMAGE_BUTTON,
                        'sortOrder' => $sortOrder,
                        'baseUrl' => $this->urlBuilder->getUrl('bss_coi/json/uploader'),
                        'mediaUrl' => $this->storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA)
                    ],
                ],
            ],
        ];
    }
}
