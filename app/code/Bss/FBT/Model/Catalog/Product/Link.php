<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_FBT
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\FBT\Model\Catalog\Product;

class Link extends \Magento\Catalog\Model\Product\Link
{
    
    const LINK_TYPE_FBT = 13;
    const LINK_TYPE_ACCESSORY_UPSELL = 6;

    public function useFbtLinks()
    {
        $this->setLinkTypeId(self::LINK_TYPE_FBT);
        return $this;
    }

    public function saveFbts($product)
    {
        parent::saveFbts($product);

        $data = $product->getFbtData();
        if(!is_null($data)) {
            $this->_getResource()->saveProductLinks($product, $data, self::LINK_TYPE_FBT);
        }
    }


    /**
     * @return \Magento\Catalog\Model\Product\Link $this
     */
    public function useAccessoryUpsellLinks()
    {
        $this->setLinkTypeId(self::LINK_TYPE_ACCESSORY_UPSELL);
        return $this;
    }

    /**
     * Save data for product relations
     *
     * @param  \Magento\Catalog\Model\Product $product
     * @return \Magento\Catalog\Model\Product\Link
     */
    public function saveProductRelations($product)
    {
        parent::saveProductRelations($product);

        $data = $product->getAccessoryUpsellData();
        if ($data) {
            $this->_getResource()->saveProductLinks($product->getId(), $data, self::LINK_TYPE_ACCESSORY_UPSELL);
        }

        return $this;
    }
}