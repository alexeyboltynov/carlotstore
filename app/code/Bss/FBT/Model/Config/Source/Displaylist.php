<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_FBT
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\FBT\Model\Config\Source;

class Displaylist implements \Magento\Framework\Option\ArrayInterface {

    public function toOptionArray() {
        return [
                ['value' => 0, 'label' => __('Style 1')], 
                ['value' => 1, 'label' => __('Style 2')]
                ];
    }

    public function toArray() {
        return [0 => __('Style 1'), 1 => __('Style 2')];
    }
}