<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_FBT
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\FBT\Model;

class Product extends \Magento\Catalog\Model\Product
{
    public function getFbtProducts() 
    {
        if (!$this->hasFbtProducts()) {
            $products = [];
            foreach ($this->getFbtProductCollection() as $product) {
                $products[] = $product;
            }
            $this->setFbtProducts($products);
        }
        return $this->getData('fbt_products');
    }

    public function getFbtIds() 
    {
        if (!$this->hasFbtbProductIds()) {
            $ids = [];
            foreach ($this->getFbtProducts() as $product) {
                $ids[] = $product->getId();
            }
            $this->setFbtProductIds($ids);
        }
        return $this->getData('fbt_product_ids');
    }

    public function getFbtProductCollection() 
    {
        $collection = $this->getLinkInstance()->useFbtLinks()->getProductCollection()->setIsStrongMode();
        $collection->setProduct($this);
        return $collection;
    }

    public function getFbtLinkCollection() 
    {
        $collection = $this->getLinkInstance()->useFbtLinks()->getLinkCollection();
        $collection->setProduct($this);
        $collection->addLinkTypeIdFilter();
        $collection->addProductIdFilter();
        $collection->joinAttributes();
        return $collection;
    }
    
}