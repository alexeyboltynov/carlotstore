define([
    'jquery',
    'underscore',
    "mage/template",
    "mage/translate",
    'mage/mage',
    "Bss_FBT/js/owl.carousel.min",
    "Bss_FBT/js/jquery.fancybox",
    "domReady!"
], function ($, _,mageTemplate) {
    'use strict';
    $.widget('mage.FBT', {
        options: {
            urlAddToCart:'',
            js_array:[],
            displaylist:'',
            shpreview:'',
            items:'',
            slideSpeed:'',
            autoPlay:''
        },

        _init: function () {
            this._createSlide();
        },

        _create: function () {
            var $widget = this;
            var showstick = this.options.showStick;
            var showcheckbox = this.options.showCheckbox;
            // remove redirect-url
            $('.fbt-product-list .product-reviews-summary').each(function(){
                if($(this).hasClass('empty')) $(this).remove();
            })
            $widget._EventListener();

            $('.option-er-pu input.bundle.option,.option-er-pu select.bundle.option').trigger('keyup');
        },

        _EventListener: function () {

            var $widget = this;

            $(document).on('click', '.fbt-product-select', function () {
                return $widget._OnClick($(this));
            });

            $(document).on('click', '.fbt-add-all-product',function(){
                 return $widget._OnClickSelectAll($(this));
            })

            $(document).on('input', '.fbt-qty', function () {
                return $widget._InputChange($(this));
            });

            $('body').on('click','.fbt-product-list .tocart,.fbt-product-list .fbtaddtocart,.fbt-product-list .fbtaddtowishlist,#product-addtocart-button-er-fbt',function (e) {
                e.preventDefault();
                return $widget._AddToCart($(this));
            })

            // popup
            var decimalSymbol = $('#currency-add').val();
            var priceIncludesTax = $widget.options.priceIncludesTax;

            $('body').on("change paste keyup",'.option-er-pu input.product-custom-option,.option-er-pu select.product-custom-option , .option-er-pu textarea.product-custom-option', function () {
                var productid = $(this).parents('.info-er-pu').find('.price-box').data('product-id');
                var ratetax = $('#rate_' + productid).val();
                return $widget._ReloadPriceCustomOption($(this), productid, decimalSymbol, ratetax, priceIncludesTax);
            });
            // bundel product
            $('body').on("change paste keyup",'.option-er-pu input.bundle.option,.option-er-pu select.bundle.option', function () {
                var productid = $(this).parents('.info-er-pu').find('.price-box').data('product-id');
                var ratetax = $('#rate_' + productid).val();
                return $widget._ReloadPriceBundel($(this),productid, decimalSymbol, ratetax, priceIncludesTax);
            });
        },

        _OnClick: function ($this) {
            var $widget = this;
            var displaylist = $widget.options.displaylist;
            var shpreview = $widget.options.shpreview;
            if (displaylist == 1) {
                if ($('.fbt-product-select').length > 1) {
                    if($this.is(':checked')) {
                        $('.fbt_' + $this.val()).show();
                        $widget.reinitcarousel(true,$this.val());
                    } else {
                        $('.fbt_' + $this.val()).hide();
                        $widget.reinitcarousel(false,$this.val());
                    }
                }
            } else {
                $widget.preview();
            }
        },

        _OnClickSelectAll: function ($this) {
            if($this.is(':checked')) {
                $this.prop( "checked", true );
                $('.fbt-product-select').each(function(){
                    if (!$(this).is(':checked')) {
                        $(this).trigger('click');
                    }
                })
            } else {
                $this.prop( "checked", false );
                $('.fbt-product-select').each(function(){
                    if ($(this).is(':checked')) {
                        $(this).trigger('click');
                    }
                })
            }
        },

        _InputChange: function ($this) {
            var $widget = this;
            var shpreview = $widget.options.shpreview;
            if (shpreview == 1) {
                $widget.preview(shpreview);
            }
        },

        _RenderOption: function (form, product_id) {
            $('#product_' + product_id).parent().find('input').each(function () {
                if ($(this).attr('name') !='uenc' && $(this).attr('name') !='form_key' && $(this).attr('name') !='product') {
                    var name = product_id + '_' + $(this).attr('name');
                    if ($(this).attr('name') == 'product-select[]' && ($(form).find('#product_' + product_id).length == 0 || $(this).parents('.wishlist').length)) {
                            $(this).clone().prependTo($(form).find(".add-option")).addClass('vls_' + product_id).val($(this).val());
                    } else {
                        if ($(form).find('input[name="'+ name +'"]').length == 0) {
                            $(this).clone().prependTo($(form).find(".add-option")).addClass('vls_' + product_id).attr('name', name).val($(this).val());
                        }
                    }
                }
            })
            $('#product_' + product_id).parent().find('textarea').each(function () {
                var name = product_id + '_' + $(this).attr('name');
                if ($(form).find('textarea[name="'+ name +'"]').length == 0) {
                    $(this).clone().prependTo($(form).find(".add-option")).addClass('vls_' + product_id).attr('name' ,name).val($(this).val());
                }
            })
            $('#product_' + product_id).parent().find('select').each(function () {
                var name = product_id + '_' + $(this).attr('name');
                if (!$(form).find('select[name="'+ name +'"]').length == 0) {
                    $(this).clone().prependTo($(form).find(".add-option")).addClass('vls_' + product_id).attr('name', name).val($(this).val());
                }
            })
        },

        preview: function () {
            var html = '';
            var preview = '';
            $('.products-fbt .item').each(function(){
                if ($(this).find('.fbt-product-select').is(':checked')) {
                   preview += '<tr><td>'+ $(this).find('.product-item-name a').text() +'</td><td>' + $(this).find('.fbt-qty').val()+'</td><td>'+ $(this).find('.price-box').html() +'</td></tr>'; 
                }
            })
            if (preview !='') {
                html = '<tr><td>' + $.mage.__('Products Name') + '</td><td>'+ $.mage.__('Qty') + '</td><td>'+ $.mage.__('Unit Price') + '</td></tr>' ;
            }
            html += preview; 
            $('.fbt-preview2 tbody').html(html);
        },

        _createSlide: function () {
            var $widget = this;
            var owl = $('.fbt .product-items');
            owl.owlCarousel({
                items : $widget.options.items,
                stagePadding: 50,
                margin:10,
                itemsCustom : false,
                itemsDesktop : [1199,$widget.options.items],
                itemsTablet: [768,2],
                itemsTabletSmall: false,
                itemsMobile : [479,1],
                singleItem : false,
                itemsScaleUp : false,
                slideSpeed : $widget.options.slideSpeed,
                paginationSpeed : 800,
                rewindSpeed : 1000,
                navigation : true,
                rewindNav : true,
                scrollPerPage : false,
                pagination : false,
                paginationNumbers: false,
                autoPlay : $widget.options.autoPlay
            })
        },

        reinitcarousel : function(rm,v)
        {
            var js_array = this.options.js_array;

            if (!rm) {
                $('.fbt .product-items .fbt_' + v).clone().appendTo('.slide-n-fbt');
                $('.fbt .product-items .fbt_' + v).remove();
            }else{
                $('.slide-n-fbt .fbt_' + v).clone().appendTo('.fbt .product-items');
                $('.slide-n-fbt .fbt_' + v).remove();
            }
            $('.fbt .owl-wrapper .owl-item').each(function(){
                if($(this).find('li').length == 0 ) $(this).remove();  
            })
            var owl = $('.fbt .product-items');
            owl.data('owlCarousel').destroy();
            owl.owlCarousel({
            // Most important owl features
                items : this.options.items,
                stagePadding: 50,
                margin:10,
                itemsCustom : false,
                itemsDesktop : [1199,this.options.items],
                itemsDesktopSmall : [980,3],
                itemsTablet: [768,2],
                itemsTabletSmall: false,
                itemsMobile : [479,2],
                singleItem : false,
                itemsScaleUp : false,
             
                //Basic Speeds
                // slideSpeed : ,
                paginationSpeed : this.options.slideSpeed,
                rewindSpeed : 1000,
             
                //Autoplay
                autoPlay : this.options.autoPlay,
                stopOnHover : true,
             
                // Navigation
                navigation : true,
                navigationText : ["",""],
                rewindNav : true,
                scrollPerPage : false,
             
                //Pagination
                pagination : false,
                paginationNumbers: false
            })

            if ($('.fbt .owl-item li').length > 1) {
                $.each(js_array,function(index,value){
                   $('.fbt .owl-wrapper').append($('.fbt .owl-item .li'+ value).parent());
                })
            }
        },
        
        // Popup
        _ReloadPriceCustomOption: function ($this, productId, decimalSymbol, tax, priceIncludesTax) {
            var priceplus = 0;
            var allselected = '';
            var itemp = $('.er-pu-'+ productId);
            $(itemp).find('input').each(function () {
                if ($(this).attr('type') == 'checkbox' || $(this).attr('type') == 'radio') {
                    if ($(this).is(':checked')) {
                        if ($(this).attr('price') > 0) {
                            priceplus += parseFloat($(this).attr('price'));
                        }
                    }
                }

                if ($(this).attr('type') == 'text' || $(this).attr('type') == 'time' ) {
                    if ($(this).val() != '') {
                        if ($(this).attr('price') > 0) {
                            priceplus += parseFloat($(this).attr('price'));
                        }
                    }
                }
                if ($(this).attr('type') == 'file') {
                }
            })

            $(itemp).find('select').each(function () {
                if ($(this).is("select[multiple]")) {
                    if ($(this).val() != '') {
                        $(this).find('option:selected').each(function () {
                            if ($(this).attr('price') > 0) {
                                priceplus += parseFloat($(this).attr('price'));
                            }
                        });
                    }
                } else if ($(this).hasClass('datetime-picker')) {
                    allselected = 1;
                    $(this).parent().find('select').each(function () {
                        if ($(this).val() == '') {
                            allselected = 0;
                        }
                    })
                    if (allselected == 1) {
                        if ($('option:selected', this).attr('price') > 0) {
                            priceplus += parseFloat($('option:selected', this).attr('price'));
                        }
                    }
                } else {
                    if ($(this).val() != '') {
                        if ($('option:selected', this).attr('price') > 0) {
                            priceplus += parseFloat($('option:selected', this).attr('price'));
                        }
                    }
                }
            })

            $(itemp).find('textarea.product-custom-option').each(function () {
                if ($(this).val() != '') {
                    if ($(this).attr('price') > 0) {
                        priceplus += parseFloat($(this).attr('price'));
                    }
                }
            })

            var finalPrice =  $this.parents('.info-er-pu').find('.fixed-price-ad-pu span.finalPrice').text();
            finalPrice = finalPrice.replace(decimalSymbol, '');
            finalPrice = parseFloat(finalPrice);
            var basePrice =  $this.parents('.info-er-pu').find('.fixed-price-ad-pu span.basePrice').text();
            basePrice = basePrice.replace(decimalSymbol, '');
            basePrice = parseFloat(basePrice);
            var oldPrice =  $this.parents('.info-er-pu').find('.fixed-price-ad-pu span.oldPrice').text();
            oldPrice = oldPrice.replace(decimalSymbol, '');
            oldPrice = parseFloat(oldPrice);
            
            if (priceIncludesTax == '1') {
                if (tax && tax > 0) {
                    basePrice = (parseFloat(basePrice + priceplus) - (parseFloat(basePrice + priceplus)*(1- 1/(1+parseFloat(tax))))).toFixed(2);
                    finalPrice = finalPrice + priceplus;
                    oldPrice = oldPrice + priceplus;
                } else {
                    basePrice = basePrice + priceplus;;
                    finalPrice = finalPrice + priceplus;
                    oldPrice = oldPrice + priceplus;
                }
            } else {
                if (tax && tax > 0) {
                    finalPrice = (parseFloat(finalPrice + priceplus) + (parseFloat(priceplus)*(parseFloat(tax)))).toFixed(2);
                    basePrice = basePrice + priceplus;
                    oldPrice = oldPrice + priceplus; 
                } else {
                    basePrice = basePrice + priceplus;;
                    finalPrice = finalPrice + priceplus;
                    oldPrice = oldPrice + priceplus;
                }
            }

            $this.parents('.info-er-pu').find('.price-box .price-container> span[data-price-type="finalPrice"] > .price').text($('#currency-add').val() + parseFloat(finalPrice).toFixed(2));

            $this.parents('.info-er-pu').find('.price-box .price-container> span[data-price-type="basePrice"] > .price').text($('#currency-add').val() + parseFloat(basePrice).toFixed(2));

            $this.parents('.info-er-pu').find('.price-box .price-container> span[data-price-type="oldPrice"] > .price').text($('#currency-add').val() + parseFloat(oldPrice).toFixed(2));
            // return priceplus;
        },

        _ReloadPriceBundel: function ($this, productId, decimalSymbol, tax, priceIncludesTax) {
            var price = 0;
            var price_ect = 0;
            var itemp = $('.er-pu-'+ productId);
            $(itemp).find('input').each(function () {
                if ($(this).attr('type') == 'checkbox' || $(this).attr('type') == 'radio') {
                    if ($(this).is(':checked')) {
                        if ($(this).attr('price') > 0) {
                            price += parseFloat($(this).attr('price'));
                            if (tax && tax > 0) {
                                price_ect += parseFloat((parseFloat($(this).attr('price')) - (parseFloat($(this).attr('price'))*(1- 1/(1+parseFloat(tax))))).toFixed(2));
                            }
                        }
                    }
                }
            })

            $(itemp).find('select').each(function () {
                if ($(this).is("select[multiple]")) {
                    if ($(this).val() != '') {
                        $(this).find('option:selected').each(function () {
                            if ($(this).attr('price') > 0) {
                                price += parseFloat($(this).attr('price'));
                                if (tax && tax > 0) {
                                    price_ect += parseFloat((parseFloat($(this).attr('price')) - (parseFloat($(this).attr('price'))*(1- 1/(1+parseFloat(tax))))).toFixed(2));
                                }
                            }
                        });
                    }
                } else {
                    if ($(this).val() != '') {
                        if ($('option:selected', this).attr('price') > 0) {
                            price += parseFloat($('option:selected', this).attr('price'));
                            if (tax && tax > 0) {
                                price_ect += parseFloat((parseFloat($('option:selected', this).attr('price')) - (parseFloat($('option:selected', this).attr('price'))*(1- 1/(1+parseFloat(tax))))).toFixed(2));
                            }
                        }
                    }
                }
            })

            $this.parents('.info-er-pu').find('.price-box .price-container> span[data-price-type="maxPrice"] > .price').text($('#currency-add').val() + (parseFloat(price)).toFixed(2))

            $this.parents('.info-er-pu').find('.price-box .price-container> span[data-price-type="basePrice"] > .price').text($('#currency-add').val() + (parseFloat(price_ect)).toFixed(2))

            $this.parents('.info-er-pu').find('.price-box .price-container> span[data-price-type="oldPrice"] > .price').text($('#currency-add').val() + (parseFloat(price)).toFixed(2))
        },

        _AddToCart: function ($this) {
            var $widget = this;
            var form = $this.parents('form').get(0);
            var addUrl = $widget.options.urlAddToCart;
            var data = '';
            var qty = 1;
            var product_id = '';
            if ($this.hasClass('tocart')) {
                qty = $this.parents('.item').find('.ip-fbt .fbt-qty').val();
                product_id = $this.parents('.item').find('.ip-fbt .fbt-product-select').val();
                if (Math.floor(product_id) == product_id && $.isNumeric(product_id)) {
                    data ='product=' + product_id + '&qty=' + qty;
                    $widget._sendAjax(addUrl, data);
                    return false;
                }
            }
            if ($this.hasClass('fbtaddtocart')) {
                form = $('#fbt-products');
                data = $(form).serialize();
                addUrl = $(form).attr('action');
                $widget._sendAjax(addUrl, data);
                return false;
            }
            if ($this.hasClass('fbtaddtowishlist')) {
                form = $('#fbt-products');
                data = $(form).serialize();
                addUrl = addUrl.replace('cart','wishlist');
                $(form).attr('action',addUrl);
                $(form).submit();
                return false;
            }
            // popup
            if ($this.hasClass('fbtaddtocartpopup')) {
                var dataForm = $('#product_fbt_form_popup');
                dataForm.mage('validation', {});
                form = $this.parents('form').get(0);
                if ($(dataForm).valid()) {
                    $('.fancybox-opened').css('zIndex', '1');
                    addUrl = $(form).attr('action');
                    data = $(form).serialize();
                    $widget._sendAjax(addUrl, data);
                }
                return false;
            }
        },

        _sendAjax: function (addUrl, data) {
            var $widget = this;
            $.fancybox.showLoading();
            $.fancybox.helpers.overlay.open({parent: 'body'});
            $.ajax({
                type: 'post',
                url: addUrl,
                data: data,
                dataType: 'json',
                success: function (data) {
                    if (data.popup) {
                        $('#bss_fbt_cart_popup').html(data.popup);
                        $('#bss_fbt_cart_popup').trigger('contentUpdated');
                        $.fancybox({
                            href: '#bss_fbt_cart_popup',
                            modal: false,
                            helpers: {
                                overlay: {
                                    locked: false
                                }
                            },
                            afterClose: function () {
                            }
                        });
                    }else{
                        $.fancybox.hideLoading();
                        $('.fancybox-overlay').hide();
                        return false;
                    }
                },
                error: function () {
                    // window.location.href = '';
                }
            });
        }
    });
    return $.mage.FBT;
});
