require([
    'jquery',
    'jquery/ui',
    'mage/adminhtml/events'
], function($) {
    $(function () {

        // copy checked value from hidden field
        $('#fbt_general_item_type_inherit').prop('checked', $('#fbt_general_sort_item_type_inherit').prop('checked'));
        disableSort();

        // trigger click checkbox
        $('#fbt_general_item_type_inherit').change(function() {
            $('#fbt_general_sort_item_type_inherit').trigger('click');
            disableSort();
        });

        //disable sort
        function disableSort() {
            if ($('#fbt_general_item_type_inherit').prop('checked')) {
                $('ul#sort-fbt').addClass('disable').sortable({
                    disabled: true
                });
            } else {
                $('ul#sort-fbt').removeClass('disable').sortable({
                    disabled: false
                });
            }
        }

        $('ul#sort-fbt').sortable({
            connectWith: "ul",
            receive: function(event, ui) {
                ui.item.attr('id', ui.item.attr('id').replace(ui.sender.data('list'), $(this).data('list')));
            },
            update: function(event, ui) {
                var sortable = [
                    $('#sort-fbt').sortable('serialize'),
                ];
                $('#fbt_general_sort_item_type').val( sortable.join('&') );
            }
        })
            .disableSelection();
    });
});