<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_FBT
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\FBT\Block\Product\ProductList;

use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\Framework\View\Element\AbstractBlock;

class Fbt extends \Magento\Catalog\Block\Product\AbstractProduct implements \Magento\Framework\DataObject\IdentityInterface
{

    /**
     * @var $_itemCollection
     */
    protected $_itemCollection;

    /**
     * @var $fbt_itemCollection
     */
    protected $fbt_itemCollection;

    /**
     * @var \Magento\Catalog\Model\Product\Visibility
     */
    protected $_catalogProductVisibility;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $timezone;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $productCollectionFactory;

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\Item\CollectionFactory
     */
    protected $itemCollectionFactory;

    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

    /**
     * @var \Bss\FBT\Helper\Data
     */
    protected $helper;

    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility,
        \Magento\Framework\Stdlib\DateTime\DateTime $timezone,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Sales\Model\ResourceModel\Order\Item\CollectionFactory $itemCollectionFactory,
        \Magento\Framework\Module\Manager $moduleManager,
        \Bss\FBT\Helper\Data $helper,
        array $data = []
    ) {
        $this->_catalogProductVisibility = $catalogProductVisibility;
        $this->timezone = $timezone;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->itemCollectionFactory = $itemCollectionFactory;
        $this->moduleManager = $moduleManager;
        $this->helper = $helper;
        parent::__construct(
            $context,
            $data
        );
    }

    protected function _prepareData()
    {
        $product = $this->_coreRegistry->registry('product');
        /* @var $product \Magento\Catalog\Model\Product */
        $this->_itemCollection = $product->getFbtProductCollection()->addAttributeToSelect(
            'required_options'
        )->setPositionOrder()->addStoreFilter();

        if ($this->moduleManager->isEnabled('Magento_Checkout')) {
            $this->_addProductAttributesAndPrices($this->_itemCollection);
        }
        $this->_itemCollection->setVisibility($this->_catalogProductVisibility->getVisibleInCatalogIds());

        $this->_itemCollection->load();

        foreach ($this->_itemCollection as $product) {
            $product->setDoNotUseCategoryId(true);
        }

        return $this;
    }

    protected function _beforeToHtml()
    {
        $this->_prepareData();
        return parent::_beforeToHtml();
    }

    public function getItems()
    {
        return $this->_itemCollection;
    }

    public function getItemsFbt()
    {
        $productIds = $order_ids = [];
        $product = $this->_coreRegistry->registry('product');
        $sortableString = $this->helper->getSortItemType();
        if (!$sortableString) {
            $sortableString = 'sort[]=0&sort[]=1&sort[]=2&sort[]=3&sort[]=4';
        }
        $sortable = null;
        parse_str($sortableString, $sortable);
        foreach ($sortable['sort'] as $value) {

           if ($value == 4) {
               # Auto
                $startdate = $this->helper->getStartDate();
                $dateStart = date('Y-m-d' . ' 00:00:00', strtotime(str_replace('/','-',$startdate)));
                $dateEnd = date('Y-m-d' . ' 23:59:59', strtotime($this->timezone->gmtDate()));
                
                $item_collection = $this->itemCollectionFactory->create();
                $item_collection->addFieldToSelect('created_at')
                                ->addFieldToSelect('product_id')
                                ->addFieldToSelect('order_id')
                                ->addFieldToSelect('parent_item_id');
                if ($startdate) {
                    $item_collection->addFieldToFilter('created_at', ['from' => $dateStart, 'to' => $dateEnd]);
                }
                $item_collection->addFieldToFilter('parent_item_id', ['null' => true]);
                if ($item_collection->getSize() > 0) {
                    foreach ($item_collection as  $item) {
                        if ($item->getProductId() == $product->getId()) {
                             $order_ids[] = $item->getOrderId();
                        }
                    }
                }

                $item_collection2  = $this->itemCollectionFactory->create();
                $item_collection2->addFieldToFilter('order_id', ['in' => $order_ids]);
                $item_collection2->addFieldToFilter('parent_item_id', ['null' => true]);
                if ($item_collection2->getSize() > 0) {
                    foreach ($item_collection2 as  $key => $_item) {
                        if ($_item->getProductId() == $product->getId()) {
                            continue;
                        }
                        if (isset($productIds[$_item->getProductId()])) {
                            $productIds[$_item->getProductId()] += $_item->getQtyOrdered();
                        }else{
                            $productIds[$_item->getProductId()] = $_item->getQtyOrdered();
                        }
                    }
                }
                arsort($productIds);
                foreach ($productIds as $productId => $qty) {
                    $productIds[$productId] = $productId;
                }
                $collection = $this->productCollectionFactory->create();
                $collection->addAttributeToSelect('*');
                $collection->addFieldToFilter('entity_id', ['in' => $productIds]);
                $this->fbt_itemCollection = $collection;
                $this->fbt_itemCollection->setVisibility($this->_catalogProductVisibility->getVisibleInCatalogIds());
                $this->fbt_itemCollection->load();
                if ($this->fbt_itemCollection->getSize() < 1) {
                    $this->fbt_itemCollection->getSelect()->order(new \Zend_Db_Expr('FIELD(entity_id, ' . implode(',', $productIds).')'));
                    $this->fbt_itemCollection->setPageSize(25);
                    continue;
                }else{
                    break;
                }
            }else{
                if ($value == 0) {
                   # Related Products
                    $this->fbt_itemCollection = $product->getRelatedProductCollection()->addAttributeToSelect('required_options')->setPositionOrder()->addStoreFilter();
                }elseif ($value == 1) {
                   # Up-Sell Products
                   $this->fbt_itemCollection = $product->getUpSellProductCollection()->setPositionOrder()->addStoreFilter();
                }
                elseif ($value == 2) {
                   # Cross-Sell Products
                    $this->fbt_itemCollection = $product->getCrossSellProductCollection()->addAttributeToSelect($this->_catalogConfig->getProductAttributes())->setPositionOrder()->addStoreFilter();
                }
                elseif ($value == 3){
                   # Frequently Bought Together Products
                    $this->fbt_itemCollection = $product->getFbtProductCollection()->addAttributeToSelect('required_options')->setPositionOrder()->addStoreFilter();
                }

                if ($this->moduleManager->isEnabled('Magento_Checkout')) {
                    $this->_addProductAttributesAndPrices($this->fbt_itemCollection);
                }
                $this->fbt_itemCollection->setVisibility($this->_catalogProductVisibility->getVisibleInCatalogIds());
                
                $this->fbt_itemCollection->load();
                if ($this->fbt_itemCollection->getSize() < 1) {
                    continue;
                }else{
                    break;
                }
            }
        }
        return $this->fbt_itemCollection;

    }

    public function getIdentities()
    {
        $identities = [];
        foreach ($this->getItems() as $item) {
            $identities = array_merge($identities, $item->getIdentities());
        }
        return $identities;
    }

    public function canItemsAddToCart()
    {
        foreach ($this->getItems() as $item) {
            if (!$item->isComposite() && $item->isSaleable() && !$item->getRequiredOptions()) {
                return true;
            }
        }
        return false;
    }
    public function getCurrentProduct(){
        return $this->_coreRegistry->registry('product');
    }
}
