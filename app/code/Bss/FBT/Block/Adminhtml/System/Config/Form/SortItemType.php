<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_FBT
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */

namespace Bss\FBT\Block\Adminhtml\System\Config\Form;

use \Magento\Backend\Block\Template;
use Bss\FBT\Helper\Data;

/**
 * Class SortItemType
 * @package Bss\FBT\Block\Adminhtml\System\Config\Form
 */
class SortItemType extends \Magento\Config\Block\System\Config\Form\Field
{
    /**
     * @var Data
     */
    protected $helper;

    /**
     * SortItemType constructor.
     * @param Template\Context $context
     * @param Data $helper
     * @param array $data
     */
    public function __construct(Template\Context $context, Data $helper, array $data = [])
    {
        $this->helper = $helper;
        $this->setTemplate('system/config/sortitemtype.phtml');
        parent::__construct($context, $data);
    }

    /**
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        return $this->toHtml();
    }

    /**
     * @return array
     */
    public function getItemType()
    {
        return ['0' => __('Related Products'),
            '1' => __('Up-Sell Products'),
            '2' => __('Cross-Sell Products'),
            '3' => __('Frequently Bought Together Products'),
            '4' => __('Real Data')
        ];
    }

    /**
     * @return mixed
     */
    public function sortItemType()
    {
        $store =  null;
        $storeManager = \Magento\Framework\App\ObjectManager::getInstance()->get('\Magento\Store\Model\StoreManagerInterface');
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        $store = $storeManager->getStore($storeId);
        return $this->helper->getSortItemType($store);
    }
}