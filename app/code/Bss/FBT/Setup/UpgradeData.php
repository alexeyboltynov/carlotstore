<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_FBT
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\FBT\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

class UpgradeData implements UpgradeDataInterface
{

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {   
        $collection_link = \Magento\Framework\App\ObjectManager::getInstance()->get(\Magento\Catalog\Model\ResourceModel\Product\Link\Collection::class);
        $collection_link->addFieldToFilter('link_type_id', ['eq' => \Bss\FBT\Model\Catalog\Product\Link::LINK_TYPE_FBT]);
        if ($collection_link->getSize() === 0) {
            $setup->startSetup();
            $productLinkTable = 'catalog_product_link';
            $setup->getConnection()
                ->addColumn(
                        $setup->getTable($productLinkTable),
                        'store_id',
                        [
                            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                            'nullable' => false,
                            'default' => 0,
                            'comment' => 'Store Id'
                        ]
                );

            $data = [
                ['link_type_id' => \Bss\FBT\Model\Catalog\Product\Link::LINK_TYPE_FBT, 'code' => 'fbt']
            ];

            foreach ($data as $bind) {
                $setup->getConnection()
                    ->insertForce($setup->getTable('catalog_product_link_type'), $bind);
            }

            $data = [
                [
                    'link_type_id' => \Bss\FBT\Model\Catalog\Product\Link::LINK_TYPE_FBT,
                    'product_link_attribute_code' => 'position',
                    'data_type' => 'int',
                ]
            ];

            $setup->getConnection()
                ->insertMultiple($setup->getTable('catalog_product_link_attribute'), $data);
            
            $setup->endSetup();
        }
    }
}