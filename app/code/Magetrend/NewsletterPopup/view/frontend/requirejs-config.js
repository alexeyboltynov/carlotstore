/**
 * Copyright © 2016 MB Vienas bitas. All rights reserved.
 * @website    www.magetrend.com
 * @package    Newsletter Popup Pro from M2
 * @author     Edvinas Stulpinas <edwin@magetrend.com>
 */

var config = {
    map: {
        '*': {
            newsletterPopup: 'Magetrend_NewsletterPopup/js/newsletterpopup',
            newsletterCms: 'Magetrend_NewsletterPopup/js/cms',
        }
    }
};