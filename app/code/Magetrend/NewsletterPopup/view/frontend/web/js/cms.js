/**
 * Copyright © 2016 MB Vienas bitas. All rights reserved.
 * @website    www.magetrend.com
 * @package    Newsletter Popup Pro from M2
 * @author     Edvinas Stulpinas <edwin@magetrend.com>
 */

define(['jquery'], function ($) {
        'use strict';

    var newsletterCms = function() {

        var config = {};

        var processingFlag = false;

        var buttonText = '';

        var init = function(options) {
            config = {
                openSelector: '#esnp_button',
                formSelector: '#esns_box_subscribe_form',
                emailFieldSelector: '#esns_email',
                submitButtonSelector: '#esns_submit',
                errorContainerSelector: '#esns_box_subscribe_response_error',
                successContainerSelector: '#esns_box_subscribe_response_success',
                couponSelector: '#esns_box_subscribe_response_coupon',
                couponCodeSelector: '#esns_box_subscribe_response_coupon span',
                autoStart:  true,
                delayTime:  0,
                cookieLifeTime: 365,
                cookieName: 'mtns_',
                actionUrl: 'newsletterpopup/subscriber/subscribe',
                autoPosition: true,
                translate: {
                    'wait': 'Wait...'
                },
                layerClose: true,
                campaign_id: 0,
                popup_id: 0
            };
            $.extend(config, options );
            setup();
        };

        var setup = function() {
            setupFormSubmit();

            //press enter and submit
            $(config.emailFieldSelector).keypress(function (e) {
                if (e.which == 13) {
                    $(config.submitButtonSelector).trigger('click');
                    return false;
                }
            });
        };


        var setupFormSubmit = function() {
            var emailField = $(config.emailFieldSelector);
            var submitButton = $(config.submitButtonSelector);

            submitButton.click(function(e){
                if (!validateForm()) {
                    return false;
                }

                if (!startProcess()) {
                    //return;
                }
                showLoading();

                var data = getFormData();

                $.ajax({
                    url: config.actionUrl,
                    type: 'POST',
                    dataType: 'json',
                    data: data,
                    success: function(resp) {
                        if (resp.errorMsg) {
                            showErrorMsg(resp.errorMsg);
                        } else {
                            hideSubscriptionForm();
                            if (resp.couponCode && $(config.couponSelector).length > 0) {
                                showSuccessMsg(resp.successMsg, resp.couponCode);
                            } else {
                                showSuccessMsg(resp.successMsg);
                                setTimeout(function() {
                                    popup.close();
                                }, 6000);
                            }

                            setTimeout(function() {
                                triggerButton.hide();
                            }, 1000);
                        }
                        hideLoading();
                        endProcess();
                        return false;
                    }
                });
            });
        };

        var getFormData = function() {

            var data = {
                popup_id: config.popup_id,
                campaign_id: config.campaign_id
            };

            if ($(config.emailFieldSelector).length > 0) {
                data['email'] = $(config.emailFieldSelector).val();
            }

            $('.esns-additional-field').each(function() {
                var name = $(this).attr('name');
                if ($(this).attr('type') == 'checkbox' && !$(this).is(':checked')) {
                    data[name] = 0;
                } else {
                    data[name] = $(this).val();
                }
            });

            return data;
        };


        var validateForm = function() {
            var error = 0;

            var fieldList = $('.mt-validator-required');

            for (var i = fieldList.length - 1; i >= 0; i--) {
                var field = $(fieldList[i]);
                if (field.val().length == 0 || field.data('field-name') == field.val()) {
                    showErrorMsg(translate('field')+ ' "'+field.data('field-name')+'" '+translate('is_required'));
                    error = 1;
                } else if (field.is(':checkbox') && field.is(':checked') == false) {
                    showErrorMsg(translate('field')+ ' "'+field.data('field-name')+'" '+translate('must_be_checked'));
                    error = 1;
                }
            }

            if (error == 0) {
                $('.mt-validator-email').each(function() {
                    var field = $(this);
                    var tmp = field.val().split('@');
                    if (tmp.length < 2 || tmp[1].split('.').length < 2) {
                        showErrorMsg(translate('error_email_not_valid'));
                        error = 1;
                    }
                });
            }

            return error == 0;
        };

        var translate = function($key) {
            if (config.translate[$key]) {
                return config.translate[$key];
            }
            return $key;
        };

        var startProcess = function() {
            if (processingFlag == true)
                return false;
            processingFlag = true;
            return true;
        };

        var endProcess = function() {
            processingFlag = false;
        };

        var showLoading = function() {
            if (buttonText == '') {
                buttonText =  $(config.submitButtonSelector).text();
            }
            $(config.submitButtonSelector).text(config.translate.wait);
        };

        var hideLoading = function() {
            $(config.submitButtonSelector).text(buttonText);
        };

        var showErrorMsg = function(msg) {
            hideSuccessMsg();
            $(config.errorContainerSelector).html(msg).css('display', 'inline-block');
        };

        var hideErrorMsg = function() {
            $(config.errorContainerSelector).hide();
        };

        var showSuccessMsg = function(msg, couponCode) {
            hideErrorMsg();
            if (couponCode) {
                $(config.couponCodeSelector).html(couponCode);
                $(config.couponSelector).show();
                $('#esns_box_layer').addClass('esns-success-has-coupon');
            }
            $(config.successContainerSelector).html(msg).show();
            $('#esns_box_layer').addClass('esns-success-true');
        };

        var hideSuccessMsg = function() {
            $(config.successContainerSelector).hide();
            $(config.couponSelector).hide();
            $('#esns_box_layer').removeClass('esns-success-true');
        };

        var hideSubscriptionForm = function() {
            $(config.formSelector).hide();
        };

        return {
            init: init
        };
    }();

    $.widget('mage.newsletterCms', {
        options: {},
        _create: function() {
            newsletterCms.init(this.options);
        }
    });
    return $.mage.newsletterCms;
});
