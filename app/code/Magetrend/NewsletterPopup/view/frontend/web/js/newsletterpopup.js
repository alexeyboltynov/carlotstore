/**
 * Copyright © 2016 MB Vienas bitas. All rights reserved.
 * @website    www.magetrend.com
 * @package    Newsletter Popup Pro from M2
 * @author     Edvinas Stulpinas <edwin@magetrend.com>
 */

define(['jquery'], function ($) {
        'use strict';

    var newsletterPopup = function() {

        var config = {};

        var processingFlag = false;

        var buttonText = '';

        var init = function(options) {
            config = {
                openSelector: '#esnp_button',
                forceOpenSelector: '.mt-np-popup-open-force',
                formSelector: '#esns_box_subscribe_form',
                emailFieldSelector: '#esns_email',
                submitButtonSelector: '#esns_submit',
                errorContainerSelector: '#esns_box_subscribe_response_error',
                successContainerSelector: '#esns_box_subscribe_response_success',
                couponSelector: '#esns_box_subscribe_response_coupon',
                couponCodeSelector: '#esns_box_subscribe_response_coupon span',
                autoStart:  true,
                delayTime:  0,
                cookieLifeTime: 365,
                cookieName: 'mtns_',
                actionUrl: 'newsletterpopup/subscriber/subscribe',
                autoPosition: true,
                translate: {
                    'wait': 'Wait...'
                },
                layerClose: true,
                campaign_id: 0,
                popup_id: 0
            };
            $.extend(config, options );

            if (!isSubscriber()) {
                cookie.setLifeTime(config.cookieLifeTime);
                triggerButton.init(config);
            }

            popup.init(config);
            setup();
        };

        var setup = function() {
            decoratePopup();
            setupPopup();
            setupFormSubmit();

            $(config.openSelector).click(function(){
                popup.open();
                cookie.setCookie(config.cookieName, 1);
            });

            $(config.forceOpenSelector).click(function(e){
                e.preventDefault();
                popup.open();
                cookie.setCookie(config.cookieName, 1);
                console.log('OK');
                return false;
            });


            //press enter and submit
            $(config.emailFieldSelector).keypress(function (e) {
                if (e.which == 13) {
                    $(config.submitButtonSelector).trigger('click');
                    return false;
                }
            });
        };

        var decoratePopup = function () {
            $('.esns_theme_orange #esns_box_block_1 span').css('color', '#'+config.color1);
        };

        var setupPopup = function() {
            if (isSubscriber()) {
                return false;
            }
            //popup delay
            setTimeout(function() {
                if (cookie.getCookie(config.cookieName) != 1) {
                    cookie.setCookie(config.cookieName, 1);
                    if (config.autoStart) {
                        popup.open();
                    }
                }
            }, config.delayTime);
        };

        var setupFormSubmit = function() {
            var emailField = $(config.emailFieldSelector);
            var submitButton = $(config.submitButtonSelector);

            submitButton.click(function(e){
                if (!validateForm()) {
                    return false;
                }

                if (!startProcess()) {
                    //return;
                }
                showLoading();

                var data = getFormData();

                $.ajax({
                    url: config.actionUrl,
                    type: 'POST',
                    dataType: 'json',
                    data: data,
                    success: function(resp) {
                        if (resp.errorMsg) {
                            showErrorMsg(resp.errorMsg);
                        } else {
                            hideSubscriptionForm();
                            if (resp.couponCode && $(config.couponSelector).length > 0) {
                                showSuccessMsg(resp.successMsg, resp.couponCode);
                            } else {
                                showSuccessMsg(resp.successMsg);
                                setTimeout(function() {
                                    popup.close();
                                }, 6000);
                            }

                            setTimeout(function() {
                                triggerButton.hide();
                            }, 1000);
                        }
                        hideLoading();
                        endProcess();
                        return false;
                    }
                });
            });
        };

        var getFormData = function() {

            var data = {
                popup_id: config.popup_id,
                campaign_id: config.campaign_id
            };

            if ($(config.emailFieldSelector).length > 0) {
                data['email'] = $(config.emailFieldSelector).val();
            }

            $('.esns-additional-field').each(function() {
                var name = $(this).attr('name');
                if ($(this).attr('type') == 'checkbox' && !$(this).is(':checked')) {
                    data[name] = 0;
                } else {
                    data[name] = $(this).val();
                }
            });

            return data;
        };


        var validateForm = function() {
            var error = 0;

            var fieldList = $('#esns_background_layer .mt-validator-required');

            for (var i = fieldList.length - 1; i >= 0; i--) {
                var field = $(fieldList[i]);
                if (field.val().length == 0 || field.data('field-name') == field.val()) {
                    showErrorMsg(translate('field')+ ' "'+field.data('field-name')+'" '+translate('is_required'));
                    error = 1;
                } else if (field.is(':checkbox') && field.is(':checked') == false) {
                    if (field.data('error-message')) {
                        var customErrorMessage = field.data('error-message');

                        if (customErrorMessage.is_required) {
                            showErrorMsg(customErrorMessage.is_required);
                        } else {
                            showErrorMsg(translate('field')+ ' "'+field.data('field-name')+'" '+translate('must_be_checked'));
                        }
                    } else {
                        showErrorMsg(translate('field')+ ' "'+field.data('field-name')+'" '+translate('must_be_checked'));
                    }

                    error = 1;
                }
            }

            if (error == 0) {
                $('.mt-validator-email').each(function() {
                    var field = $(this);
                    var tmp = field.val().split('@');
                    if (tmp.length < 2 || tmp[1].split('.').length < 2) {
                        showErrorMsg(translate('error_email_not_valid'));
                        error = 1;
                    }
                });
            }

            return error == 0;
        };

        var translate = function($key) {
            if (config.translate[$key]) {
                return config.translate[$key];
            }
            return $key;
        };

        var startProcess = function() {
            if (processingFlag == true)
                return false;
            processingFlag = true;
            return true;
        };

        var endProcess = function() {
            processingFlag = false;
        };

        var showLoading = function() {
            if (buttonText == '') {
                buttonText =  $(config.submitButtonSelector).text();
            }
            $(config.submitButtonSelector).text(config.translate.wait);
        };

        var hideLoading = function() {
            $(config.submitButtonSelector).text(buttonText);
        };

        var showErrorMsg = function(msg) {
            hideSuccessMsg();
            $(config.errorContainerSelector).html(msg).css('display', 'inline-block');
        };

        var hideErrorMsg = function() {
            $(config.errorContainerSelector).hide();
        };

        var showSuccessMsg = function(msg, couponCode) {
            hideErrorMsg();
            if (couponCode) {
                $(config.couponCodeSelector).html(couponCode);
                $(config.couponSelector).show();
                $('#esns_box_layer').addClass('esns-success-has-coupon');
            }
            $(config.successContainerSelector).html(msg).show();
            $('#esns_box_layer').addClass('esns-success-true');
            valingMiddle();
        };

        var hideSuccessMsg = function() {
            $(config.successContainerSelector).hide();
            $(config.couponSelector).hide();
            $('#esns_box_layer').removeClass('esns-success-true');
        };

        var hideSubscriptionForm = function() {
            $(config.formSelector).hide();
        };

        var isSubscriber = function () {
            if (cookie.getCookie(config.subscriberCookieName) == '1') {
                return true;
            }
            return false;
        };

        var valingMiddle = function () {
            if (!$('.esns-valing-middle')) {
                return;
            }
            var contentH = $('.esns-valing-middle').height();
            var boxH = $(config.boxSelector).height();
            $('.esns-valing-middle').css('margin-top', ((contentH-boxH)/2) + 'px');
        };

        return {
            init: init
        };
    }();

    var popup = (function() {
        var isOpen = false;

        var bgLayer = null;

        var popupBox = null;

        var lastTopPosition = 0;

        var config = {};

        var init = function(settings) {
            config = {
                openSelector:           '#esnp_button',
                closeSelector:          '#esns_box_close',
                backgroundSelector:     '#esns_background_layer',
                boxSelector:            '#esns_box_layer',
                layerClose:             true,
                autoPosition:           true
            };
            $.extend(config, settings );

            setup();
        };

        var setup = function() {

            bgLayer = $(config.backgroundSelector);
            popupBox = $(config.boxSelector);

            if (config.autoPosition) {
                $(document).scroll(function() {
                    eventScroll();
                });

                $(window).resize(function() {
                    eventResize();
                });
            }

            $(config.closeSelector+', .esns-close').click(function(){
                close();
            });

            if (config.layerClose) {
                $(config.backgroundSelector).click(function(e) {
                    if ('#'+e.target.id == config.backgroundSelector) {
                        close();
                    }
                });
            }
        };

        var open = function() {
            if(!isOpen) {
                bgLayer.fadeIn();
                bgLayer.css('height', $(document).height()+'px');
                popupBox.css('margin-top', getTopPosition()+'px');
                isOpen = true;
            }
        };

        var close = function() {
            if (isOpen) {
                bgLayer.fadeOut();
                isOpen = false;
            }
        };

        var getTopPosition = function() {
            var scrollTop = jQuery(document).scrollTop();
            var windowH = jQuery(window).height();
            var boxH = popupBox.height();
            var boxTop = 0;
            if (windowH <= boxH) {
                boxTop = scrollTop;
            } else {
                boxTop = scrollTop + ((windowH - boxH ) /2);
            }
            return boxTop;
        };

        var eventScroll = function()
        {
            var windowH = $(window).height();

            var boxH = popupBox.height();
            var scrollTop = $(document).scrollTop();
            var diff = Math.abs(lastTopPosition - scrollTop);
            if (windowH <= boxH) {
                return;
            }

            if (diff > 150
                || scrollTop == 0
                || scrollTop + $(window).height() == $(document).height()
            ) {
                lastTopPosition = scrollTop;
                popupBox.css('margin-top', getTopPosition()+'px');
                bgLayer.css('height', $(document).height()+'px');
            }
        };

        var eventResize = function() {
            var windowH = $(window).height();
            var boxH = popupBox.height();
            if (windowH <= boxH) {
                return;
            }
            popupBox.css('margin-top', getTopPosition()+'px');
        };

        return {
            init:   init,
            open:   open,
            close:  close
        };
    })();

    var triggerButton = (function() {
        var config = {};

        var init = function(settings) {
            config = {
                buttonSelector: '#esnp_button',
                cookieName: 'es_newssubscriber'
            };

            $.extend(config, settings);
            setup();
        };

        var setup = function() {
            show();
        };

        var show = function() {
            $(config.buttonSelector).show();
        };

        var hide = function() {
            $(config.buttonSelector).hide();
        };

        return {
            init: init,
            hide: hide,
            show: show,
        };
    })();

    var cookie = (function() {
        var lifeTime = 0;

        var setLifeTime = function(days) {
            lifeTime = days;
        };

        var setCookie = function(key, value) {
            var date = new Date();
            date.setTime(date.getTime()+(lifeTime*60*1000));
            var expires = "; expires="+date.toGMTString();
            document.cookie = escape(key)+"="+escape(value)+expires+"; path=/";
        };

        var getCookie = function(key) {
            var nameEQ = escape(key) + "=";
            var ca = document.cookie.split(';');
            for(var i=0;i < ca.length;i++) {
                var c = ca[i];
                while (c.charAt(0)==' ') c = c.substring(1,c.length);
                if (c.indexOf(nameEQ) == 0) return unescape(c.substring(nameEQ.length,c.length));
            }
            return null;
        };

        return {
            setLifeTime: setLifeTime,
            getCookie: getCookie,
            setCookie: setCookie
        };

    })();

    $.widget('mage.newsletterPopup', {
        options: {},
        _create: function() {
            newsletterPopup.init(this.options);
        }
    });
    return $.mage.newsletterPopup;
});
