<?php
/**
 * Copyright © 2016 MB Vienas bitas. All rights reserved.
 * @website    www.magetrend.com
 * @package    Newsletter Popup Pro from M2
 * @author     Edvinas Stulpinas <edwin@magetrend.com>
 */

namespace Magetrend\NewsletterPopup\Model;

use \Magento\Framework\Model\AbstractModel;

class Field extends AbstractModel
{

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $_resourceConnection;

    /**
     * @var string
     */
    private $__columnPrefix = 'subscriber_';

    /**
     * @var string
     */
    private $__dbTable = 'newsletter_subscriber';

    /**
     * Field constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\App\ResourceConnection $resource
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $abstractResource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Framework\Model\ResourceModel\AbstractResource $abstractResource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->_resourceConnection = $resource;
        parent::__construct(
            $context,
            $registry,
            $abstractResource,
            $resourceCollection,
            $data
        );
    }

    protected function _construct()
    {
        $this->_init('Magetrend\NewsletterPopup\Model\ResourceModel\Field');
    }

    public function save()
    {
        $this->isValid();
        $connection = $this->getConnection();
        $tableName = $this->_getResource()->getTable($this->__dbTable);
        $tableColumns = $connection->describeTable($tableName);
        if (!isset($tableColumns[$this->getColumnName()])) {
            $this->createColumn();
        }
        return parent::save();
    }

    public function isValid()
    {
        $fieldName = $this->getName();
        if (empty($fieldName) || !ctype_alpha($fieldName)) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('Bad field name. It must be not empty and only alpha')
            );
        }

        $fieldType = $this->getType();
        if (empty($fieldType)) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Bad field type'));
        }
    }

    public function getColumnName()
    {
        return $this->__columnPrefix.$this->getName();
    }

    protected function createColumn()
    {
        $db = $this->getConnection();
        $columnName = $this->getColumnName();
        $tableName = $this->_getResource()->getTable($this->__dbTable);

        if ($this->getType() == 'checkbox') {
            $db->addColumn($tableName, $columnName, [
                'TYPE'      => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                'LENGTH'    => 1,
                'COMMENT'   => 'Additional field'
            ]);
        } else {
            $db->addColumn($tableName, $columnName, [
                'TYPE'      => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'COMMENT'   => 'Additional field'
            ]);
        }

        return true;
    }

    /**
     * Get connection
     *
     * @return \Magento\Framework\App\ResourceConnection
     * @codeCoverageIgnore
     */
    public function getConnection()
    {
        return $this->_resourceConnection->getConnection();
    }
}
