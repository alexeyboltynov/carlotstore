<?php
/**
 * Copyright © 2016 MB Vienas bitas. All rights reserved.
 * @website    www.magetrend.com
 * @package    Newsletter Popup Pro from M2
 * @author     Edvinas Stulpinas <edwin@magetrend.com>
 */

namespace Magetrend\NewsletterPopup\Model\ResourceModel\FieldOption;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    protected function _construct()
    {
        $this->_init(
            'Magetrend\NewsletterPopup\Model\FieldOption',
            'Magetrend\NewsletterPopup\Model\ResourceModel\FieldOption'
        );
    }

    public function setFieldFilter($fieldId)
    {
        $this->addFieldToFilter('field_id', $fieldId);
        return $this;
    }
}
