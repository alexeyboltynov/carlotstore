<?php
/**
 * Copyright © 2016 MB Vienas bitas. All rights reserved.
 * @website    www.magetrend.com
 * @package    Newsletter Popup Pro from M2
 * @author     Edvinas Stulpinas <edwin@magetrend.com>
 */

namespace Magetrend\NewsletterPopup\Model\ResourceModel\Campaign;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'Magetrend\NewsletterPopup\Model\Campaign',
            'Magetrend\NewsletterPopup\Model\ResourceModel\Campaign'
        );
    }

    /**
     * @param string $pageId
     * @return $this
     */
    public function addPageIdFilter($pageId)
    {
        $this->getSelect()
            ->join(
                ['pg' => $this->getTable('mt_np_campaign_page')],
                "main_table.entity_id = pg.campaign_id AND (pg.page_id='{$pageId}' OR pg.page_id='all' OR page_id = 's_{$pageId}')",
                ['']
            );

        return $this;
    }

    public function addStoreIdFilter($storeId)
    {
        $this->getSelect()
            ->join(
                ['st' => $this->getTable('mt_np_campaign_store')],
                "main_table.entity_id = st.campaign_id AND (st.store_id='{$storeId}' OR st.store_id=0)",
                ['']
            );
        return $this;
    }

    public function addPopupRelation()
    {
        $this->getSelect()
            ->joinLeft(
                ['p' => $this->getTable('mt_np_popup')],
                "main_table.popup_id = p.entity_id",
                ['popup_name' => 'p.name']
            );
        return $this;
    }
}
