<?php
/**
 * Copyright © 2016 MB Vienas bitas. All rights reserved.
 * @website    www.magetrend.com
 * @package    Newsletter Popup Pro from M2
 * @author     Edvinas Stulpinas <edwin@magetrend.com>
 */

namespace Magetrend\NewsletterPopup\Model\ResourceModel\CampaignStore;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    protected function _construct()
    {
        $this->_init(
            'Magetrend\NewsletterPopup\Model\CampaignStore',
            'Magetrend\NewsletterPopup\Model\ResourceModel\CampaignStore'
        );
    }

    public function setCampaignFilter($campaignId)
    {
        $this->addFieldToFilter('campaign_id', $campaignId);
        return $this;
    }
}
