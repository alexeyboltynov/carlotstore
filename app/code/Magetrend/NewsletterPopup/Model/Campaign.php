<?php
/**
 * Copyright © 2016 MB Vienas bitas. All rights reserved.
 * @website    www.magetrend.com
 * @package    Newsletter Popup Pro from M2
 * @author     Edvinas Stulpinas <edwin@magetrend.com>
 */

namespace Magetrend\NewsletterPopup\Model;

use Magento\Framework\App\Request\Http;

class Campaign extends \Magento\Framework\Model\AbstractModel
{
    const CATEGORY_ATTRIBUTE_CODE = 'mtnp_campaign';

    const PRODUCT_ATTRIBUTE_CODE = 'mtnp_campaign';
    /**
     * @var ResourceModel\CampaignPage\CollectionFactory
     */
    protected $pCollectionFactory;

    /**
     * @var ResourceModel\CampaignStore\CollectionFactory
     */
    protected $_sCollectionFactory;

    /**
     * @var ResourceModel\CampaignPage\CollectionFactory
     */
    protected $_pCollectionFactory;

    /**
     * @var ResourceModel\Campaign\CollectionFactory
     */
    protected $_campaignCollectionFactory;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var \Magetrend\NewsletterPopup\Model\Campaign|null
     */
    private $__currentCampaign = null;

    /**
     * @var \Magetrend\NewsletterPopup\Model\Popup|null
     */
    private $__currentPopup = null;

    /**
     * @var \Magetrend\NewsletterPopup\Model\Popup|null
     */
    private $__popup = null;

    /**
     * @var \Magetrend\NewsletterPopup\Helper\Data
     */
    protected $_helper;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var Http
     */
    public $request;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    public $url;

    /**
     * Campaign constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param ResourceModel\CampaignPage\CollectionFactory $pCollection
     * @param ResourceModel\CampaignStore\CollectionFactory $sCollection
     * @param ResourceModel\Campaign\CollectionFactory $campaignCollection
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magetrend\NewsletterPopup\Helper\Data $helper
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param Http $request,
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magetrend\NewsletterPopup\Model\ResourceModel\CampaignPage\CollectionFactory $pCollection,
        \Magetrend\NewsletterPopup\Model\ResourceModel\CampaignStore\CollectionFactory $sCollection,
        \Magetrend\NewsletterPopup\Model\ResourceModel\Campaign\CollectionFactory $campaignCollection,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magetrend\NewsletterPopup\Helper\Data $helper,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        Http $request,
        \Magento\Framework\UrlInterface $url,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->_helper = $helper;
        $this->_pCollectionFactory = $pCollection;
        $this->_sCollectionFactory = $sCollection;
        $this->_campaignCollectionFactory = $campaignCollection;
        $this->_objectManager = $objectManager;
        $this->_storeManager = $storeManager;
        $this->request = $request;
        $this->url = $url;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magetrend\NewsletterPopup\Model\ResourceModel\Campaign');
    }

    /**
     * @return $this
     */
    public function getPageIdsCollection()
    {
        $collection = $this->_pCollectionFactory->create()
            ->setCampaignFilter($this->getId());
        return $collection;
    }

    /**
     * @return array
     */
    public function getPageIdsAsArray()
    {
        $dataArray = [];
        $collection = $this->getPageIdsCollection();
        if ($collection->getSize() > 0) {
            foreach ($collection as $item) {
                $dataArray[] = $item->getPageId();
            }
        }
        return $dataArray;
    }

    /**
     * @return $this
     */
    public function getStoreIdsCollection()
    {
        $collection = $this->_sCollectionFactory->create()
            ->setCampaignFilter($this->getId());
        return $collection;
    }

    /**
     * @return array
     */
    public function getStoreIdsAsArray()
    {
        $dataArray = [];
        $collection = $this->getStoreIdsCollection();
        if ($collection->getSize() > 0) {
            foreach ($collection as $item) {
                $dataArray[] = $item->getStoreId();
            }
        }
        return $dataArray;
    }

    /**
     * Delete campaign object
     * @return $this
     */
    public function delete()
    {
        $this->deleteCampaignPageCollection();
        $this->deleteCampaignStoreCollection();
        return parent::delete();
    }

    /**
     * Save campaign object
     * @return $this
     */
    public function save()
    {
        $pageIds = $this->getData('page_ids');
        $soreIds = $this->getData('store_ids');
        parent::save();
        $this->saveCampaignPageCollection($pageIds);
        $this->saveCampaignStoreCollection($soreIds);

        return $this;
    }

    /**
     * It will delete all campaign-page relation objects
     */
    public function deleteCampaignPageCollection()
    {
        $this->_pCollectionFactory->create()
            ->setCampaignFilter($this->getId())
            ->walk('delete');
    }

    /**
     * It will delete all campaign-store relation objects
     */
    public function deleteCampaignStoreCollection()
    {
        $this->_sCollectionFactory->create()
            ->setCampaignFilter($this->getId())
            ->walk('delete');
    }

    /**
     * @param $data
     */
    public function saveCampaignPageCollection($data)
    {
        $this->deleteCampaignPageCollection();

        if (count($data) > 0) {
            $campaignId = $this->getId();
            foreach ($data as $id) {
                $this->createCampaignPage($campaignId, $id);
            }
        }
    }

    /**
     * @param int $campaignId
     * @param string $pageId
     */
    public function createCampaignPage($campaignId, $pageId)
    {
        $campaignPage = $this->_objectManager->create('Magetrend\NewsletterPopup\Model\CampaignPage');
        $campaignPage->setCampaignId($campaignId);
        $campaignPage->setPageId($pageId);
        $campaignPage->save();
    }

    /**
     * @param $data
     */
    public function saveCampaignStoreCollection($data)
    {
        $this->deleteCampaignStoreCollection();
        if (count($data) > 0) {
            $campaignId = $this->getId();
            foreach ($data as $id) {
                $this->createCampaignStore($campaignId, $id);
            }
        }
    }

    /**
     * @param int $campaignId
     * @param int $storeId
     */
    public function createCampaignStore($campaignId, $storeId)
    {
        $campaignPage = $this->_objectManager->create('Magetrend\NewsletterPopup\Model\CampaignStore');
        $campaignPage->setCampaignId($campaignId);
        $campaignPage->setStoreId($storeId);
        $campaignPage->save();
    }

    /**
     * Returns current page campaign
     * @return bool|null
     */
    public function getCurrentCampaign()
    {
        if ($this->__currentCampaign == null) {
            $storeId = $this->_storeManager->getStore()->getId();
            $pageId = $this->_helper->getCurrentPageId();
            $campaignCollection = $this->_campaignCollectionFactory->create()
                ->addFieldToFilter('is_active', 1)
                ->addPageIdFilter($pageId)
                ->addStoreIdFilter($storeId)

                ->setPageSize(1)
                ->setCurPage(1);

            if ($campaignCollection->getSize() > 0) {
                foreach ($campaignCollection as $campaignModel) {
                    if ($this->isAvailable($campaignModel)) {
                        $this->__currentCampaign = $campaignModel;
                        break;
                    }
                }
            } else {
                $this->__currentCampaign = false;
            }
        }
        return $this->__currentCampaign;
    }

    /**
     * Returns current campaign popup
     * @return bool|null
     */
    public function getCurrentPopup()
    {
        if ($this->__currentPopup == null) {
            $campaign = $this->getCurrentCampaign();
            if ($campaign) {
                $this->__currentPopup = $campaign->getPopup();
            } else {
                $this->__currentPopup = false;
            }
        }

        return $this->__currentPopup;
    }

    /**
     * Returns popup model
     * @return \Magetrend\NewsletterPopup\Model\Popup|null
     */
    public function getPopup()
    {
        if ($this->__popup == null) {
            $popup = $this->_objectManager->create('Magetrend\NewsletterPopup\Model\Popup');
            $popup->load($this->getPopupId());
            $this->__popup = $popup;
        }

        return $this->__popup;
    }

    /**
     * Returns cookie lifetime
     * @return float
     */
    public function getCookieLifetime()
    {
        if ($this->_helper->getIsDevelopmentMode()) {
            return 0.000001;
        }

        return parent::getCookieLifetime();
    }

    /**
     * Returns cookie name
     * @return string
     */
    public function getCookieName()
    {
        if ($this->_helper->getIsDevelopmentMode()) {
            return 'npdev-'.time();
        }

        $cookiePrefix = $this->_helper->getCookiePrefix();
        $cookieSuffix = '';
        if (empty($cookiePrefix)) {
            $cookiePrefix = 'mtns_';
        }

        if ($this->getMethod() == \Magetrend\NewsletterPopup\Model\Config\Source\Method::METHOD_PAGE) {
            $cookieSuffix = '_'.md5($this->url->getCurrentUrl());
        }

        return $cookiePrefix.$this->getId().$cookieSuffix;
    }

    /**
     * Check for availability
     *
     * @param $campaign
     * @return bool
     */
    public function isAvailable($campaign)
    {
        $campaignParams = $campaign->getParams();
        if (!empty($campaignParams)) {
            $campaignParams = explode('&', $campaignParams);
            $params = $this->request->getParams();
            foreach ($campaignParams as $param) {
                $param = explode('=', $param);
                //@codingStandardsIgnoreStart
                if (count($param) != 2) {
                    continue;
                }
                //@codingStandardsIgnoreEnd

                if (isset($params[$param[0]]) && $params[$param[0]] == $param[1]) {
                    continue;
                }
                return false;
            }
        }

        $pageIds = $campaign->getPageIdsAsArray();
        $currentPageId = $this->_helper->getCurrentPageId();

        if (in_array('s_category_page', $pageIds)) {
            if (!$campaign->isAvailableOnCategory() && $currentPageId == 'category_page') {
                return false;
            }
        }

        if (in_array('s_product_page', $pageIds) && $currentPageId == 'product_page') {
            if (!$campaign->isAvailableOnProduct()) {
                return false;
            }
        }

        return true;
    }

    public function isAvailableOnCategory()
    {
        $currentCategory = $this->_registry->registry('current_category');
        if ($currentCategory && $currentCategory->getData(self::CATEGORY_ATTRIBUTE_CODE) == $this->getId()) {
            return true;
        }

        return false;
    }

    public function isAvailableOnProduct()
    {
        $currentProduct = $this->_registry->registry('current_product');
        if ($currentProduct && $currentProduct->getData(self::PRODUCT_ATTRIBUTE_CODE) == $this->getId()) {
            return true;
        }
        return false;
    }
}
