<?php
/**
 * Copyright © 2016 MB Vienas bitas. All rights reserved.
 * @website    www.magetrend.com
 * @package    Newsletter Popup Pro from M2
 * @author     Edvinas Stulpinas <edwin@magetrend.com>
 */

namespace Magetrend\NewsletterPopup\Model;

class Popup extends \Magento\Framework\Model\AbstractModel
{
    const TYPE_NEWSLETTER_SUBSCRIPTION = 'newsletter_subscription_form';

    const TYPE_STATIC_BLOCK = 'static_block';

    const DISCOUNT_CODE_FIELD = 'np_discount_code';

    /**
     * @var ResourceModel\Field\CollectionFactory
     */
    protected $_fieldCollectionFactory;

    /**
     * @var ResourceModel\FieldOption\CollectionFactory
     */
    protected $_fieldOptionCollectionFactory;

    /**
     * @var \Magento\SalesRule\Model\Rule
     */
    protected $_rule;

    /**
     * @var
     */
    protected $_massCodeGeneratorFactory;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var null
     */
    protected $_additionalFields = null;

    /**
     * @var
     */
    protected $_massGenerator;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    public $date;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    public $jsonHelper;

    /**
     * Popup constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param ResourceModel\Field\CollectionFactory $fieldCollection
     * @param ResourceModel\FieldOption\CollectionFactory $fieldOptionCollection
     * @param \Magento\SalesRule\Model\Rule $rule
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magetrend\NewsletterPopup\Model\ResourceModel\Field\CollectionFactory $fieldCollection,
        \Magetrend\NewsletterPopup\Model\ResourceModel\FieldOption\CollectionFactory $fieldOptionCollection,
        \Magento\SalesRule\Model\Rule $rule,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->_rule = $rule;
        $this->_fieldCollectionFactory = $fieldCollection;
        $this->_fieldOptionCollectionFactory = $fieldOptionCollection;
        $this->_objectManager = $objectManager;
        $this->date = $dateTime;
        $this->jsonHelper = $jsonHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    protected function _construct()
    {
        $this->_init('Magetrend\NewsletterPopup\Model\ResourceModel\Popup');
    }

    public function save()
    {
        parent::save();
        $fieldData = $this->getData('field');
        $this->saveAdditionalFieldCollection($fieldData);
    }

    public function saveAdditionalFieldCollection($fieldData)
    {
        if (isset($fieldData['options'])) {
            $data = $fieldData['options'];
            if (count($data) > 0) {
                foreach ($data as $fieldData) {
                    if (isset($fieldData['is_delete']) && $fieldData['is_delete'] == 1) {
                        $this->deleteField($fieldData['id']);
                    } else {
                        $this->saveField($fieldData);
                    }
                }
            }
        }
    }

    public function saveField($fieldData)
    {
        $newFieldId = $this->createField($fieldData, $this->getId());
        if (isset($fieldData['previous_group']) && $fieldData['previous_group'] == 'select') {
            if (isset($fieldData['values'])) {
                foreach ($fieldData['values'] as $fieldOption) {
                    if ($fieldOption['is_delete'] == 1) {
                        $this->deleteOption($fieldOption['option_type_id']);
                    } else {
                        $this->createOption($newFieldId, $fieldOption);
                    }
                }
            }
        }
    }

    public function createOption($fieldId, $postData)
    {
        $option = $this->_objectManager->create('Magetrend\NewsletterPopup\Model\FieldOption');
        if (isset($postData['option_type_id']) && $postData['option_type_id'] > 0) {
            $option->load($postData['option_type_id']);
        }

        $option->setData('field_id', $fieldId);
        $option->setData('value', $postData['value']);
        $option->setData('label', $postData['label']);
        $option->setData('position', $postData['sort_order']);
        $option->save();
    }

    public function createField($postData, $popupId)
    {
        $field = $this->_objectManager->create('Magetrend\NewsletterPopup\Model\Field');
        if (isset($postData['id']) && $postData['option_id'] > 0) {
            $field->load($postData['id']);
        }

        if (!isset($postData['error_message'])) {
            $postData['error_message'] = [];
        }
        $postData['error_message'] = $this->jsonHelper->jsonEncode($postData['error_message']);

        $field->setData('popup_id', $popupId);
        $field->setData('name', $postData['name']);
        $field->setData('type', $postData['type']);
        $field->setData('position', $postData['sort_order']);
        $field->setData('is_required', $postData['is_require']);
        $field->setData('after_email_field', $postData['after_email_field']);
        $field->setData('default_value', isset($postData['default_value'])?$postData['default_value']:'');
        $field->setData('frontend_label', isset($postData['frontend_label'])?$postData['frontend_label']:'');
        $field->setData('error_message', $postData['error_message']);
        $field->setData('label', $postData['label']);
        $field->save();
        return $field->getId();
    }

    public function deleteField($fieldId)
    {
        $field = $this->_objectManager->create('Magetrend\NewsletterPopup\Model\Field');
        $field->load($fieldId);
        $field->delete();

        $this->_objectManager
            ->create('Magetrend\NewsletterPopup\Model\ResourceModel\FieldOption\Collection')
            ->setFieldFilter($field->getId())
            ->walk('delete');

        return true;
    }

    public function deleteOption($optionId)
    {
        $option = $this->_objectManager->create('Magetrend\NewsletterPopup\Model\FieldOption');
        $option->load($optionId);
        $option->delete();

        return true;
    }

    public function delete()
    {
        $this->deleteFieldCollection();
        parent::delete();
    }

    public function deleteFieldCollection()
    {
        $collection = $this->_fieldCollectionFactory->create()
            ->setPopupFilter($this->getId());
        if ($collection->getSize() > 0) {
            foreach ($collection as $item) {
                $this->deleteField($item->getId());
            }
        }
    }

    public function isStaticBlockPopup()
    {
        if ($this->getContentType() != self::TYPE_STATIC_BLOCK) {
            return false;
        }

        return true;
    }

    public function getColor($key)
    {
        $color = $this->getData('color_'.$key);
        return '#'.str_replace('#', '', $color);
    }

    public function getAdditionalFields()
    {
        if ($this->_additionalFields == null) {
            $collection = $this->_fieldCollectionFactory->create()
                ->setPopupFilter($this->getId())
                ->joinOptionCollection()
                ->sortByPositionCollection();

            $this->_additionalFields = $collection->getGroupedData();
        }
        return $this->_additionalFields;
    }

    public function getUniqueDiscountCode()
    {
        if (!$this->getId() || !is_numeric($this->getCouponRuleId())) {
            return '';
        }

        $rule = $this->_rule->load($this->getCouponRuleId());
        if (!$rule->getId()) {
            return '';
        }

        if ($rule->getUseAutoGeneration() == 0) {
            return $rule->getCouponCode();
        }

        $codeGenerator = $this->_objectManager->get('Magento\SalesRule\Model\Coupon\Massgenerator');
        $codeGenerator->setData('qty', 1);
        $codeGenerator->setData('rule_id', $rule->getId());
        $codeGenerator->setData('length', $this->getCouponLength());
        $codeGenerator->setData('format', $this->getCouponFormat());
        $codeGenerator->setData('prefix', $this->getCouponPrefix());
        $codeGenerator->setData('suffix', $this->getCouponSuffix());
        $codeGenerator->setData('dash', $this->getCouponDash());
        $codeGenerator->setData('uses_per_coupon', 1);
        $codeGenerator->setData('uses_per_customer', 1);

        $codeGenerator->generatePool();
        $latestCoupon = max($rule->getCoupons());

        $expireInDays = $this->getData('coupon_expire_in_days');
        if (is_numeric($expireInDays) && $expireInDays > 0) {
            $latestCoupon->setData(
                'expiration_date',
                $this->date->gmtDate('Y-m-d H:i:s', time() + 3600 * 24 * $expireInDays)
            )->save();
        } elseif ($rule->getToDate()) {
            $latestCoupon->setData(
                'expiration_date',
                $rule->getToDate()
            )->save();
        }

        return $latestCoupon->getCode();
    }

    public function addDefaultData()
    {
        if ($this->getId()) {
            return;
        }

        $template = !empty($this->getTheme())?$this->getTheme():'';

        $this->addData([
            'text_1' => __('Subscribe & get <span>$5</span> discount!'),
            'text_2' => __('get the first offers and events from our store'),
            'text_3' => __(
                'Would you like to be one of the first to receive exclusive information '.
                'about the latest collections, offers and events from our store? Then just subscribe to '.
                'our free newsletter now and get a voucher to the value of 5 € for your next purchase '.
                'in our Online-Shop.'
            ),
            'text_4' => __('More ways to stay connected:'),
            'icon' => 'gift_card',
            'color_1' => '#ffffff',
            'color_2' => '#000000',
            'color_3' => '#000000',
            'color_4' => '#000000',
            'color_5' => '#ffffff',
        ]);

        if ($template == 'orange') {
            $this->setData('color_1', '#fa814c');
        }

        if ($template == 'osom') {
            $this->setData('text_1', __('SUBSCRIBE OUR NEWSLETTER!'))
                ->setData('text_2', __('and get $5 discount!'));
        }
    }

}
