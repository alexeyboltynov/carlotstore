<?php
/**
 * Copyright © 2016 MB Vienas bitas. All rights reserved.
 * @website    www.magetrend.com
 * @package    Newsletter Popup Pro from M2
 * @author     Edvinas Stulpinas <edwin@magetrend.com>
 */

namespace Magetrend\NewsletterPopup\Model\Config\Source\Field;

class Type implements \Magento\Framework\Option\ArrayInterface
{

    public function getAll()
    {
        return [
            [
                'label' => 'Text',
                'renderer' => '\Magetrend\NewsletterPopup\Block\Adminhtml\Popup\Edit\Tab\Field\Type\Text',
                'name' => 'text',
                'types' => [
                    ['value' => 'field',  'label' => __('Text Field')],
                    ['value' => 'area',    'label' => __('Textarea')],
                ]
            ],
            [
                'name' => 'select',
                'label' => 'Select',
                'renderer' => '\Magetrend\NewsletterPopup\Block\Adminhtml\Popup\Edit\Tab\Field\Type\Select',
                'types' => [
                    ['value' => 'drop_down',    'label' => __('Drop Down')],
                ]
            ],
            [
                'name' => 'checkbox',
                'label' => 'Checkbox',
                'renderer' => '\Magetrend\NewsletterPopup\Block\Adminhtml\Popup\Edit\Tab\Field\Type\Checkbox',
                'types' => [
                    ['value' => 'checkbox',    'label' => __('Checkbox')],
                ]
            ],
            [
                'name' => 'hidden',
                'label' => 'Hidden',
                'renderer' => '\Magetrend\NewsletterPopup\Block\Adminhtml\Popup\Edit\Tab\Field\Type\Hidden',
                'types' => [
                    ['value' => 'hidden',    'label' => __('Hidden')],
                ]
            ],
        ];
    }
    public function toOptionArray()
    {
        $groups = [['value' => '', 'label' => __('-- Please select --')]];

        foreach ($this->getAll() as $option) {
            $types = [];
            foreach ($option['types'] as $type) {
                $types[] = ['label' => __($type['label']), 'value' => $type['value']];
            }
            if ($types) {
                $groups[] = ['label' => __($option['label']), 'value' => $types];
            }
        }

        return $groups;
    }
}
