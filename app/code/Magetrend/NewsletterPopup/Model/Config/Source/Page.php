<?php
/**
 * Copyright © 2016 MB Vienas bitas. All rights reserved.
 * @website    www.magetrend.com
 * @package    Newsletter Popup Pro from M2
 * @author     Edvinas Stulpinas <edwin@magetrend.com>
 */

namespace Magetrend\NewsletterPopup\Model\Config\Source;

class Page implements \Magento\Framework\Option\ArrayInterface
{
    protected $_collectionFactory;

    public function __construct(
        \Magento\Cms\Model\ResourceModel\Page\CollectionFactory $collectionFactory
    ) {
        $this->_collectionFactory = $collectionFactory;
    }
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = $this->toArray();
        $optionsArray = [];
        foreach ($options as $value => $label) {
            $optionsArray[] = ['value' => $value,  'label' => $label];
        }

        return $optionsArray;
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {

        $values = [
            'all'               => __('All Pages'),
            'product_page'      => __('Product Page'),
            'category_page'     => __('Category Page'),
            's_product_page'    => __('Specific Product Page'),
            's_category_page'   => __('Specific Category Page'),
            'cart_page'         => __('Cart'),
            'checkout_page'     => __('Checkout')
        ];

        $collection = $this->_collectionFactory->create();
        if ($collection->getSize() > 0) {
            foreach ($collection as $item) {
                $values['cms_page_'.$item->getId()] = $item->getTitle();
            }
        }

        return $values;
    }
}
