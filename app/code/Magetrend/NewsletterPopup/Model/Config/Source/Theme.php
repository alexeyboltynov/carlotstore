<?php
/**
 * Copyright © 2016 MB Vienas bitas. All rights reserved.
 * @website    www.magetrend.com
 * @package    Newsletter Popup Pro from M2
 * @author     Edvinas Stulpinas <edwin@magetrend.com>
 */

namespace Magetrend\NewsletterPopup\Model\Config\Source;

class Theme implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'basic',    'label' => __('Default')],
            //['value' => 'label',    'label' => __('Label')],
            ['value' => 'clear',    'label' => __('Clear')],
            ['value' => 'orange',   'label' => __('Orange')],
            ['value' => 'osom',   'label' => __('Osom')],
        ];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {

        return [
            'basic'     => __('Default'),
            //'label'     => __('Label'),
            'clear'     => __('Clear'),
            'orange'    => __('Orange'),
            'osom'    => __('Osom'),
        ];
    }
}
