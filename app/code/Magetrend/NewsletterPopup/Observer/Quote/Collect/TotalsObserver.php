<?php
/**
 * MB "Vienas bitas" (Magetrend.com)
 *
 * @category MageTrend
 * @package  Magetend/NewsletterPopup
 * @author   Edvinas Stulpinas <edwin@magetrend.com>
 * @license  http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link     http://www.magetrend.com/magento-2-newsletter-popup-extension
 */

namespace Magetrend\NewsletterPopup\Observer\Quote\Collect;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;

class TotalsObserver implements ObserverInterface
{
    /**
     * @var \Magento\SalesRule\Model\CouponFactory
     */
    public $couponFactory;

    /**
     * @var \Magetrend\NewsletterPopup\Helper\Data
     */
    public $helper;

    /**
     * TotalsObserver constructor.
     * @param \Magento\SalesRule\Model\CouponFactory $couponFactory
     * @param \Magetrend\NewsletterPopup\Helper\Data $helper
     */
    public function __construct(
        \Magento\SalesRule\Model\CouponFactory $couponFactory,
        \Magetrend\NewsletterPopup\Helper\Data $helper
    ) {
        $this->couponFactory = $couponFactory;
        $this->helper = $helper;
    }

    /**
     * @param Observer $observer
     * @return $this
     */
    public function execute(Observer $observer)
    {
        $quote = $observer->getQuote();
        $couponCode = $quote->getCouponCode();
        if (empty($couponCode)) {
            return $this;
        }

        $coupon = $this->couponFactory->create();
        $coupon->load($couponCode, 'code');
        if ($coupon->getId()) {
            if (strtotime($coupon->getExpirationDate()) < time()) {
                $quote->setCouponCode('');
            }
        }

        return $this;
    }
}
