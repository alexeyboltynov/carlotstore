<?php
/**
 * MB "Vienas bitas" (Magetrend.com)
 *
 * @category MageTrend
 * @package  Magetend/NewsletterPopup
 * @author   Edvinas Stulpinas <edwin@magetrend.com>
 * @license  http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link     http://www.magetrend.com/magento-2-newsletter-popup-extension
 */

namespace Magetrend\NewsletterPopup\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use \Magento\Checkout\Model\Session as CheckoutSession;

class Coupon implements ObserverInterface
{
    /**
     * @var \Magento\Framework\Stdlib\CookieManagerInterface
     */
    public $cookieManager;

    /**
     * @var \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory
     */
    public $cookieMetadataFactory;

    /**
     * @var CheckoutSession
     */
    public $checkoutSession;

    /**
     * Coupon Observer constructor.
     * @param \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager
     * @param \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory
     * @param CheckoutSession $checkoutSession
     */
    public function __construct(
        \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager,
        \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory,
        CheckoutSession $checkoutSession
    ) {
        $this->cookieManager = $cookieManager;
        $this->cookieMetadataFactory = $cookieMetadataFactory;
        $this->checkoutSession = $checkoutSession;
    }

    public function execute(Observer $observer)
    {

        $couponCode = $this->cookieManager->getCookie('mtns-c', false);
        if (!$couponCode || empty($couponCode)) {
            return;
        }

        $this->checkoutSession->getQuote()
            ->setCouponCode(base64_decode($couponCode));

        $cookieMetadata = $this->cookieMetadataFactory->createPublicCookieMetadata()
            ->setHttpOnly(false)
            ->setDuration(1)
            ->setPath('/');
        $this->cookieManager->setPublicCookie('mtns-c', null, $cookieMetadata);
    }
}
