<?php
/**
 * Copyright © 2016 MB Vienas bitas. All rights reserved.
 * @website    www.magetrend.com
 * @package    Newsletter Popup Pro from M2
 * @author     Edvinas Stulpinas <edwin@magetrend.com>
 */

namespace Magetrend\NewsletterPopup\Plugin\Newsletter\Block;

class Subscribe
{
    /**
     * @var \Magetrend\NewsletterPopup\Model\CampaignFactory
     */
    public $campaignFactory;

    /**
     * @var \Magetrend\NewsletterPopup\Helper\Data
     */
    public $moduleHelper;

    /**
     * @var \Magetrend\NewsletterPopup\Block\Popup\Theme
     */
    public $themeBlock;

    /**
     * Subscribe block plugin
     * @param \Magetrend\NewsletterPopup\Model\CampaignFactory $campaignFactory
     * @param \Magetrend\NewsletterPopup\Block\Popup\Theme $themeBlock
     * @param \Magetrend\NewsletterPopup\Helper\Data $moduleHelper
     */
    public function __construct(
        \Magetrend\NewsletterPopup\Model\CampaignFactory $campaignFactory,
        \Magetrend\NewsletterPopup\Block\Popup\Theme $themeBlock,
        \Magetrend\NewsletterPopup\Helper\Data $moduleHelper
    ) {
        $this->campaignFactory = $campaignFactory;
        $this->moduleHelper = $moduleHelper;
        $this->themeBlock = $themeBlock;
    }

    /**
     * @param \Magento\Newsletter\Block\Subscribe $subscribeBlock
     * @param $results
     * @return mixed
     */
    public function afterGetTemplate($subscribeBlock, $results)
    {
        if (!$this->moduleHelper->showAdditioanlFields()
            || !$this->moduleHelper->isActiveDefault()
            || !$this->moduleHelper->getDefaultCampaignId()
        ) {
            return $results;
        }

        $campaign = $this->campaignFactory->create()
            ->load($this->moduleHelper->getDefaultCampaignId());

        if (!$campaign->getId()) {
            return $results;
        }

        /**
         * @var \Magetrend\NewsletterPopup\Model\Popup $popup
         */
        $popup = $campaign->getPopup();

        $subscribeBlock->setData('additional_fields', $popup->getAdditionalFields());
        $subscribeBlock->setData('theme_block', $this->themeBlock);
        return 'Magetrend_NewsletterPopup::newsletter/subscribe.phtml';
        //return 'Magento_Newsletter::subscribe.phtml';
    }
}
