<?php
/**
 * Copyright © 2016 MB Vienas bitas. All rights reserved.
 * @website    www.magetrend.com
 * @package    Newsletter Popup Pro from M2
 * @author     Edvinas Stulpinas <edwin@magetrend.com>
 */

namespace Magetrend\NewsletterPopup\Plugin\Newsletter\Block\Adminhtml\Subscriber;

use \Magento\Newsletter\Block\Adminhtml\Subscriber\Grid;

class GridPlugin
{

    protected $_ignoreAdditionalField = [
        'firstname' => 1,
        'lastname' => 1
    ];

    protected $_updateRenderer = [
        'firstname' => 'Magetrend\NewsletterPopup\Block\Adminhtml\Newsletter\Subscriber\Grid\Column\Renderer\Firstname',
        'lastname' => 'Magetrend\NewsletterPopup\Block\Adminhtml\Newsletter\Subscriber\Grid\Column\Renderer\Lastname'
    ];

    private $__columnCount = 4;

    protected $_fieldCollectionFactory;

    protected $_npHelper;

    private $__fieldCollection = null;

    public function __construct(
        \Magetrend\NewsletterPopup\Model\ResourceModel\Field\CollectionFactory $collectionFactory,
        \Magetrend\NewsletterPopup\Helper\Data $helper
    ) {
        $this->_npHelper = $helper;
        $this->_fieldCollection = $collectionFactory;
    }

    public function beforeToHtml(Grid $subject)
    {
        $this->addAdditionFieldColumns($subject);
        $this->updateRenderer($subject, 'firstname');
        $this->updateRenderer($subject, 'lastname');
    }

    protected function addAdditionFieldColumns(Grid $subject)
    {
        if (!$this->_npHelper->isActive()) {
            return false;
        }

        $fieldCollection = $this->getFieldCollection();
        if (count($fieldCollection) > 0) {
            $addedFields = [];
            foreach ($fieldCollection as $field) {
                if (!isset($addedFields[$field->getName()])) {
                    $this->addAdditionalColumn($subject, $field->getLabel(), $field->getName(), $field->getType());
                    $addedFields[$field->getName()] = 1;
                }
            }
        }

        $this->addAdditionalColumn(
            $subject,
            'NP Discount Code',
            \Magetrend\NewsletterPopup\Model\Popup::DISCOUNT_CODE_FIELD
        );

        $columnBlock = $subject->getLayout()
            ->createBlock('Magento\Backend\Block\Widget\Grid\Column')
            ->setData(
                [
                    'header' => __('Created At'),
                    'index' => 'created_at',
                    'type' => 'date',
                    'header_css_class' => 'col-id',
                    'column_css_class' => 'col-id',
                ]
            );

        $columnSet = $subject->getColumnSet();
        $columnSet->insert(
            $columnBlock,
            ++$this->__columnCount,
            'created_at'
        );

        return true;
    }

    /**
     * @param Grid $subject
     * @param $name
     */
    private function updateRenderer(Grid $subject, $name)
    {
        if (!isset($this->_updateRenderer[$name])) {
            return;
        }
        $columnList = $subject->getColumnSet()->getColumns();

        if (!isset($columnList[$name])) {
            return;
        }
        $column = $columnList[$name];
        $column->setRendererType($name, $this->_updateRenderer[$name]);
        $column->setType($name);
    }

    /**
     * Add additional fields columns
     * @param Grid $subject
     * @param $label
     * @param $name
     * @param $type
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function addAdditionalColumn(Grid $subject, $label, $name, $type = 'text')
    {

        if (isset($this->_ignoreAdditionalField[$name])) {
            return;
        }
        $index = 'subscriber_'.$name;
        if ($name == \Magetrend\NewsletterPopup\Model\Popup::DISCOUNT_CODE_FIELD) {
            $index = $name;
        }

        $options = [
            'header' => __($label),
            'index' => $index,
            'header_css_class' => 'col-id',
            'column_css_class' => 'col-id',
        ];

        if ($type == 'checkbox') {
            $options['type'] = 'options';
            $options['options'] = [
                [
                    'label' => (string)__('Not Checked'),
                    'value' => 0,
                ],
                [
                    'label' => (string)__('Not Checked'),
                    'value' => '',
                ],
                [
                    'label' => (string)__('Checked'),
                    'value' => 1,
                ]
            ];
        }

        $columnBlock = $subject->getLayout()
            ->createBlock('Magento\Backend\Block\Widget\Grid\Column')
            ->setData($options);

        $columnSet = $subject->getColumnSet();
        $columnSet->insert(
            $columnBlock,
            ++$this->__columnCount,
            $name
        );
    }

    /**
     * Returns Additional fields collection
     * @return \Magetrend\NewsletterPopup\Model\ResourceModel\Field\Collection|null
     */
    public function getFieldCollection()
    {
        if ($this->__fieldCollection === null) {
            $collection = $this->_fieldCollection->create();
            $this->__fieldCollection = $collection;
        }

        return $this->__fieldCollection;
    }
}
