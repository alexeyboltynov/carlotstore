<?php
/**
 * Copyright © 2016 MB Vienas bitas. All rights reserved.
 * @website    www.magetrend.com
 * @package    Newsletter Popup Pro from M2
 * @author     Edvinas Stulpinas <edwin@magetrend.com>
 */

namespace Magetrend\NewsletterPopup\Plugin\Newsletter\Model;

use Magento\Newsletter\Model\Subscriber;

class SubscriberPlugin
{
    /**
     * @var \Magento\Framework\Event\ManagerInterface
     */
    protected $_subscriberModel;

    /**
     * SubscriberPlugin constructor.
     * @param \Magetrend\NewsletterPopup\Model\Subscriber $subscriber
     */
    public function __construct(
        \Magetrend\NewsletterPopup\Model\Subscriber $subscriber
    ) {
        $this->_subscriberModel = $subscriber;
    }

    /**
     * Assign additional fields data and coupon code for subscriber object
     * @param $subscriber
     * @param $email
     * @return array
     */
    public function beforeSubscribe($subscriber, $email)
    {
        $this->_subscriberModel->beforeSubscribe($subscriber);
        return [$email];
    }

    /**
     * Assign additional fields data and coupon code for subscriber object then subscriber is customer
     * @param $subscriber
     * @param $customerId
     * @return array
     */
    public function beforeSubscribeCustomerById($subscriber, $customerId)
    {
        $this->_subscriberModel->beforeSubscribeCustomerById($subscriber);
        return [$customerId];
    }

    public function afterConfirm($subscriber, $wasConfirmed)
    {
        if ($wasConfirmed) {
            $subscriber->sendConfirmationSuccessEmail();
        }

        return $wasConfirmed;
    }
}
