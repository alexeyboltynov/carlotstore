<?php
/**
 * Copyright © 2016 MB Vienas bitas. All rights reserved.
 * @website    www.magetrend.com
 * @package    Newsletter Popup Pro from M2
 * @author     Edvinas Stulpinas <edwin@magetrend.com>
 */
namespace Magetrend\NewsletterPopup\Plugin\SalesRule\Block\Adminhtml\Promo\Quote\Edit\Tab\Coupons;

use Magento\SalesRule\Block\Adminhtml\Promo\Quote\Edit\Tab\Coupons\Grid;

class GridPlugin
{
    /**
     * @var \Magetrend\Dao\Helper\Data
     */
    public $helper;

    /**
     * @var \Magento\Framework\Registry
     */
    public $coreRegistry;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    public $storeManager;

    /**
     * GridPlugin constructor.
     * @param \Magetrend\NewsletterPopup\Helper\Data $helper
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magetrend\NewsletterPopup\Helper\Data $helper,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->helper = $helper;
        $this->coreRegistry = $coreRegistry;
        $this->storeManager = $storeManager;
    }

    /**
     * Add columns
     *
     * @param Grid $subject
     */
    public function beforeToHtml(Grid $subject)
    {
        $this->addColumns($subject);
    }

    /**
     * Add additional column in the grid
     * @param Grid $subject
     * @return bool
     */
    public function addColumns(Grid $subject)
    {
        if (!$this->helper->isActive()) {
            return false;
        }

        if (!$subject->getColumn('expiration_date')) {
            $subject->addColumnAfter(
                'expiration_date',
                [
                    'header' => __('Expiration Date'),
                    'index' => 'expiration_date',
                    'type' => 'datetime',
                    'align' => 'center',
                    'width' => '140'
                ],
                'created_at'
            );
        }
        return true;
    }
}
