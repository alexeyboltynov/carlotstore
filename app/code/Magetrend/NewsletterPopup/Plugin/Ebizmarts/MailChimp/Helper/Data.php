<?php
/**
 * Copyright © 2016 MB Vienas bitas. All rights reserved.
 * @website    www.magetrend.com
 * @package    Newsletter Popup Pro from M2
 * @author     Edvinas Stulpinas <edwin@magetrend.com>
 */

namespace Magetrend\NewsletterPopup\Plugin\Ebizmarts\MailChimp\Helper;

class Data
{
    /**
     * @var \Magetrend\NewsletterPopup\Helper\Data
     */
    public $helper;

    /**
     * McapiPlugin constructor.
     * @param \Magetrend\NewsletterPopup\Helper\Data $helper
     */
    public function __construct(
        \Magetrend\NewsletterPopup\Helper\Data $helper
    ) {
        $this->helper = $helper;
    }

    public function afterGetMergeVarsByCustomer($subject, $mergedVars)
    {
        return $this->helper->addMergeVars($mergedVars);
    }

    public function afterGetMergeVarsBySubscriber($subject, $mergedVars)
    {
        return $this->helper->addMergeVars($mergedVars);
    }
}
