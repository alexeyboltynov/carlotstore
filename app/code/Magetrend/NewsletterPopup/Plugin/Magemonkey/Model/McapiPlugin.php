<?php
/**
 * Copyright © 2016 MB Vienas bitas. All rights reserved.
 * @website    www.magetrend.com
 * @package    Newsletter Popup Pro from M2
 * @author     Edvinas Stulpinas <edwin@magetrend.com>
 */

namespace Magetrend\NewsletterPopup\Plugin\MageMonkey\Model;

use Magento\Newsletter\Model\Subscriber;

class McapiPlugin
{

    /**
     * @var \Magento\Framework\Event\ManagerInterface
     */
    protected $_eventManager;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_registry;

    /**
     * @var \Magetrend\NewsletterPopup\Helper\Data
     */
    protected $_helper;

    /**
     * McapiPlugin constructor.
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfigInterface
     * @param \Magetrend\NewsletterPopup\Helper\Data $helper
     */
    public function __construct(
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfigInterface,
        \Magetrend\NewsletterPopup\Helper\Data $helper
    ) {
        $this->_registry = $registry;
        $this->_scopeConfig = $scopeConfigInterface;
        $this->_helper = $helper;
    }

    public function beforeListCreateMember($api, $listId, $memberData)
    {
        if (!$this->_helper->isActive()) {
            return;
        }

        $additionalData = $this->_registry->registry('newsletterpopup_additional_data');
        $couponCode = $this->_registry->registry('newsletterpopup_coupon_code');
        if (!empty($couponCode)) {
            $fieldName = \Magetrend\NewsletterPopup\Model\Popup::DISCOUNT_CODE_FIELD;
            $additionalData[$fieldName] = $couponCode;
        }

        $mergeVars = unserialize($this->_scopeConfig->getValue(
            \Ebizmarts\MageMonkey\Helper\Data::XML_PATH_MAPPING,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            null
        ));
        $memberData = json_decode($memberData, true);
        if (count($mergeVars)) {
            foreach ($mergeVars as $mergeVar) {
                if (isset($additionalData[$mergeVar['magento']])
                    && !empty($additionalData[$mergeVar['magento']])
                ) {
                    $memberData['merge_fields'][$mergeVar['mailchimp']] = $additionalData[$mergeVar['magento']];
                }
            }
        }

        return [$listId, json_encode($memberData)];
    }
}
