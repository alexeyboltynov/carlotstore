<?php
/**
 * Copyright © 2016 MB Vienas bitas. All rights reserved.
 * @website    www.magetrend.com
 * @package    Newsletter Popup Pro from M2
 * @author     Edvinas Stulpinas <edwin@magetrend.com>
 */

namespace Magetrend\NewsletterPopup\Controller\Adminhtml\Campaign;

class Index extends \Magetrend\NewsletterPopup\Controller\Adminhtml\Campaign
{
    /**
     * Newsletter subscribers page
     *
     * @return void
     */
    public function execute()
    {
        if ($this->getRequest()->getParam('ajax')) {
            $this->_forward('grid');
            return;
        }

        $this->_view->loadLayout();

        $this->_setActiveMenu('Magetrend_NewsletterPopup::popup_index');
        $this->_view->getPage()->getConfig()->getTitle()->prepend(__('Newsletter Popup Campaign'));

        $this->_addBreadcrumb(__('Newsletter Popup '), __('Newsletter Popup '));
        $this->_addBreadcrumb(__('Campaign'), __('Campaign'));

        $this->_view->renderLayout();
    }
}
