<?php
/**
 * Copyright © 2016 MB Vienas bitas. All rights reserved.
 * @website    www.magetrend.com
 * @package    Newsletter Popup Pro from M2
 * @author     Edvinas Stulpinas <edwin@magetrend.com>
 */

namespace Magetrend\NewsletterPopup\Controller\Adminhtml\Popup;

class Index extends \Magetrend\NewsletterPopup\Controller\Adminhtml\Popup
{
    /**
     * Newsletter subscribers page
     *
     * @return void
     */
    public function execute()
    {
        if ($this->getRequest()->getParam('ajax')) {
            $this->_forward('grid');
            return;
        }

        $this->_view->loadLayout();

        $this->_setActiveMenu('Magetrend_NewsletterPopup::popup_index');
        $this->_view->getPage()->getConfig()->getTitle()->prepend(__('Newsletter Popups'));

        $this->_addBreadcrumb(__('Newsletter'), __('Newsletter'));
        $this->_addBreadcrumb(__('Subscribers'), __('Popups'));

        $this->_view->renderLayout();
    }
}
