<?php
/**
 * Copyright © 2016 MB Vienas bitas. All rights reserved.
 * @website    www.magetrend.com
 * @package    Newsletter Popup Pro from M2
 * @author     Edvinas Stulpinas <edwin@magetrend.com>
 */

namespace Magetrend\NewsletterPopup\Controller\Subscriber;

use \Magento\Framework\Exception\LocalizedException;
use Magento\TestFramework\Event\Magento;

class Subscribe extends \Magento\Newsletter\Controller\Subscriber
{

    /**
     * @var \Magento\Customer\Api\AccountManagementInterface
     */
    protected $customerAccountManagement;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $_resultJsonFactory;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @var \Magetrend\NewsletterPopup\Helper\Data
     */
    protected $_helper;

    /**
     * Subscribe constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Newsletter\Model\SubscriberFactory $subscriberFactory
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Registry $registry
     * @param \Magetrend\NewsletterPopup\Helper\Data $helper
     * @param \Magento\Customer\Model\Url $customerUrl
     * @param \Magento\Customer\Api\AccountManagementInterface $customerAccountManagement
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Newsletter\Model\SubscriberFactory $subscriberFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Registry $registry,
        \Magetrend\NewsletterPopup\Helper\Data $helper,
        \Magento\Customer\Model\Url $customerUrl,
        \Magento\Customer\Api\AccountManagementInterface $customerAccountManagement,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        $this->_helper = $helper;
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_coreRegistry = $registry;
        $this->customerAccountManagement = $customerAccountManagement;
        parent::__construct(
            $context,
            $subscriberFactory,
            $customerSession,
            $storeManager,
            $customerUrl
        );
    }

    /**
     * Validates that the email address isn't being used by a different account.
     *
     * @param string $email
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return void
     */
    protected function validateEmailAvailable($email)
    {
        $websiteId = $this->_storeManager->getStore()->getWebsiteId();
        if ((
                $this->_customerSession->getCustomerDataObject()->getEmail() !== $email
                && !$this->customerAccountManagement->isEmailAvailable($email, $websiteId))
            || $this->_isSubscribed($email, $websiteId)

        ) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __($this->_helper->translate('email_already_exist'))
            );
        }
    }

    /**
     * Validates that if the current user is a guest, that they can subscribe to a newsletter.
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return void
     */
    protected function validateGuestSubscription()
    {
        if ($this->_objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')
                ->getValue(
                    \Magento\Newsletter\Model\Subscriber::XML_PATH_ALLOW_GUEST_SUBSCRIBE_FLAG,
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                ) != 1
            && !$this->_customerSession->isLoggedIn()
        ) {
            throw new \Magento\Framework\Exception\LocalizedException(__(
                $this->_helper->translate('only_for_customer'),
                $this->_customerUrl->getRegisterUrl()
            ));
        }
    }

    /**
     * Validates the format of the email address
     *
     * @param string $email
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return void
     */
    protected function validateEmailFormat($email)
    {
        if (!\Zend_Validate::is($email, 'EmailAddress')) {
            throw new \Magento\Framework\Exception\LocalizedException(__(
                $this->_helper->translate('error_email_not_valid')
            ));
        }
    }

    /**
     * New subscription action
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return string
     */
    public function execute()
    {
        $response = [
            'errorMsg' => '',
            'successMsg' => '',
            'couponCode' => ''
        ];

        if ($this->getRequest()->isPost()
            && $this->getRequest()->getPost('email')
            && $this->getRequest()->getPost('popup_id')
        ) {
            $email = (string)$this->getRequest()->getPost('email');
            $popupId = $this->getRequest()->getPost('popup_id');

            try {
                $this->validateEmailFormat($email);
                $this->validateGuestSubscription();
                $this->validateEmailAvailable($email);
                $popup = $this->_getPopup($popupId);
                $status = $this->_subscriberFactory->create()->subscribe($email);

                if ($popup->getShowInPopup()) {
                    $code = $this->_coreRegistry->registry('newsletterpopup_coupon_code');
                    if (!empty($code)) {
                        $response['couponCode'] = $code;
                    }
                }

                if ($status == \Magento\Newsletter\Model\Subscriber::STATUS_NOT_ACTIVE) {
                    $response['successMsg'] = __($this->_helper->translate('success_message_need_to_confirm'));
                } else {
                    $response['successMsg'] = __($this->_helper->translate('success_message'));
                }
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $response['errorMsg'] = __('%1', $e->getMessage());
            } catch (\Exception $e) {
                $response['errorMsg'] = __($this->_helper->translate('error_with_subscription'));
                $response['errorMsg'] = __($e->getMessage());
            }
        }

        $resultJson = $this->_resultJsonFactory->create();
        return $resultJson->setData($response);
    }

    protected function _getPopup($popupId)
    {
        if (!is_numeric($popupId)) {
            throw new LocalizedException(__('Popup id is not valid'));
        }

        $popup = $this->_objectManager->create('Magetrend\NewsletterPopup\Model\Popup')->load($popupId);

        if (!$popup->getId()) {
            throw new LocalizedException(__('Popup is no longer available'));
        }

        return $popup;
    }

    protected function _isSubscribed($email, $websiteId)
    {
        $subscriber = $this->_objectManager->create('Magento\Newsletter\Model\Subscriber')
            ->setWebsiteId($websiteId)
            ->loadByEmail($email);

        if (!$subscriber->getId()
            || $subscriber->getStatus() != \Magento\Newsletter\Model\Subscriber::STATUS_SUBSCRIBED
        ) {
            return false;
        }

        return true;
    }
}
