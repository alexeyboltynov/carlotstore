<?php
/**
 * Copyright © 2016 MB Vienas bitas. All rights reserved.
 * @website    www.magetrend.com
 * @package    Newsletter Popup Pro from M2
 * @author     Edvinas Stulpinas <edwin@magetrend.com>
 */

namespace Magetrend\NewsletterPopup\Helper;

use Magento\Store\Model\ScopeInterface;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const XML_PATH_NEWSLETTE_RPOPUP_GENERAL_IS_ACTIVE = 'newsletterpopup/general/is_active';

    const XML_PATH_TRANSLATE = 'newsletterpopup/translate';

    const XML_PATH_COOKIE_NAME = 'newsletterpopup/general/cookie_name';

    const XML_PATH_GOD_MODE = 'newsletterpopup/general/dev';

    const XML_PATH_NEWSLETTE_RPOPUP_DEFAULT_IS_ACTIVE = 'newsletterpopup/default/is_active';

    const XML_PATH_NEWSLETTE_RPOPUP_DEFAULT_CAMPAIGN = 'newsletterpopup/default/campaign';

    const XML_PATH_SHOW_ADDITIOANL_FIELDS = 'newsletterpopup/default/show_additional_fields';

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var
     */
    protected $_productHelper;

    /**
     * @var \Magento\Catalog\Helper\Data
     */
    protected $_catalogHelper;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Framework\Stdlib\CookieManagerInterface
     */
    protected $_cookieManager;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_registry;

    /**
     * Data constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Store\Model\StoreManagerInterface $storeManagerInterface
     * @param \Magento\Framework\Stdlib\CookieManagerInterface $cookieManagerInterface
     * @param \Magento\Catalog\Helper\Data $catalogHelper
     * @param \Magento\Framework\Registry $registry
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\ObjectManagerInterface  $objectManager,
        \Magento\Store\Model\StoreManagerInterface $storeManagerInterface,
        \Magento\Framework\Stdlib\CookieManagerInterface $cookieManagerInterface,
        \Magento\Catalog\Helper\Data  $catalogHelper,
        \Magento\Framework\Registry $registry
    ) {
        $this->_objectManager = $objectManager;
        $this->_storeManager = $storeManagerInterface;
        $this->_catalogHelper = $catalogHelper;
        $this->_cookieManager = $cookieManagerInterface;
        $this->_registry = $registry;

        parent::__construct($context);
    }

    /**
     * @param null $store
     * @return bool
     */
    public function isActive($store = null)
    {
        if ($this->scopeConfig->getValue(
            self::XML_PATH_NEWSLETTE_RPOPUP_GENERAL_IS_ACTIVE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        )) {
            return true;
        }
        return false;
    }

    /**
     * @param null $store
     * @return bool
     */
    public function isActiveDefault($store = null)
    {
        if ($this->_registry->registry('newsletterpopup_disable') == 1) {
            return false;
        };

        if ($this->scopeConfig->getValue(
            self::XML_PATH_NEWSLETTE_RPOPUP_DEFAULT_IS_ACTIVE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        )) {
            return true;
        }
        return false;
    }

    /**
     * @param null $store
     * @return bool
     */
    public function getDefaultCampaignId($store = null)
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_NEWSLETTE_RPOPUP_DEFAULT_CAMPAIGN,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * @param $keyWord
     * @return mixed
     */
    public function translate($keyWord)
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_TRANSLATE.'/'.$keyWord,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return mixed
     */
    public function getTranslation()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_TRANSLATE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCookiePrefix()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_COOKIE_NAME,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return bool
     */
    public function getIsDevelopmentMode()
    {
        $isDevelopment = $this->scopeConfig->getValue(
            self::XML_PATH_GOD_MODE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        return $isDevelopment == 1;
    }

    /**
     * @return bool
     */
    public function showAdditioanlFields()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_SHOW_ADDITIOANL_FIELDS,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        ) == 1;
    }

    /**
     * @return string
     */
    public function getCurrentPageId()
    {
        $pageId = 'all';
        if ($this->_getRequest()->getRouteName() == 'cms') {
            $cmsPage = $this->_objectManager->get('Magento\Cms\Model\Page');
            $pageId = 'cms_page_'.$cmsPage->getId();
        } else {
            $product = $this->_catalogHelper->getProduct();
            if ($product && $product->getId()) {
                $pageId = 'product_page';
            } else {
                $category = $this->_catalogHelper->getCategory();
                if ($category && $category->getId()) {
                    $pageId = 'category_page';
                } else {
                    $request = $this->_getRequest();
                    $module = $request->getModuleName();
                    $controller = $request->getControllerName();
                    $action = $request->getActionName();

                    if ($module == 'checkout' && $controller == 'cart' && $action == 'index') {
                        $pageId = 'cart_page';
                    } elseif ($module == 'checkout' && $controller == 'index' && $action == 'index') {
                        $pageId = 'checkout_page';
                    }
                }
            }
        }

        return $pageId;
    }

    public function getSubscriberCookieName()
    {
        $storeId = $this->_storeManager->getStore()->getId();
        return $this->getCookiePrefix().'_'.$storeId.'__1';
    }

    public function isSubscriber()
    {
        $cookieName = $this->getSubscriberCookieName();
        if (!$this->_cookieManager->getCookie($cookieName, false)) {
            return false;
        }

        return true;
    }

    /**
     * Add additional data to array, used for mailchimp
     * @param $mergeVars
     */
    public function addMergeVars($mergeVars)
    {
        if (!$this->isActive()) {
            return;
        }

        $additionalData = $this->_registry->registry('newsletterpopup_additional_data');
        $couponCode = $this->_registry->registry('newsletterpopup_coupon_code');

        if (!empty($couponCode)) {
            $fieldName = \Magetrend\NewsletterPopup\Model\Popup::DISCOUNT_CODE_FIELD;
            $additionalData['COUPON'] = $couponCode;
        }

        if (!empty($additionalData)) {
            foreach ($additionalData as $key => $value) {
                if ($key == 'firstname') {
                    $key = 'FNAME';
                }
                if ($key == 'lastname') {
                    $key = 'LNAME';
                }
                $mergeVars[strtoupper($key)] = $value;
            }
        }

        return $mergeVars;
    }
}
