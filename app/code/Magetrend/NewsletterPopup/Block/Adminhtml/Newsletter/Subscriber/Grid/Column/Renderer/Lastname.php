<?php
/**
 * Copyright © 2016 MB Vienas bitas. All rights reserved.
 * @website    www.magetrend.com
 * @package    Newsletter Popup Pro from M2
 * @author     Edvinas Stulpinas <edwin@magetrend.com>
 */

namespace Magetrend\NewsletterPopup\Block\Adminhtml\Newsletter\Subscriber\Grid\Column\Renderer;

use Magento\Framework\DataObject;

class Lastname extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\Text
{
    public function render(DataObject $row)
    {
        if ($row->getData('customer_lastname') == '' && $row->getData('subscriber_lastname') != '') {
            return $row->getData('subscriber_lastname');
        }

        return parent::render($row);
    }
}
