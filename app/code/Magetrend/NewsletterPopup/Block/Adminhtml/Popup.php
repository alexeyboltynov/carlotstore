<?php
/**
 * Copyright © 2016 MB Vienas bitas. All rights reserved.
 * @website    www.magetrend.com
 * @package    Newsletter Popup Pro from M2
 * @author     Edvinas Stulpinas <edwin@magetrend.com>
 */

namespace Magetrend\NewsletterPopup\Block\Adminhtml;

use Magento\Core\Model\ObjectManager;

class Popup extends \Magento\Backend\Block\Widget\Grid\Container
{
    protected $_contentType;

    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magetrend\NewsletterPopup\Model\Config\Source\Type $contentType,
        array $data = []
    ) {
        $this->_contentType = $contentType;
        parent::__construct($context, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_controller = 'popup_index';
        $this->_headerText = __('Manage Newsletter Popup');
        parent::_construct();
    }

    protected function _prepareLayout()
    {
        $this->removeButton('add');
        $this->addButton('add', [
            'id' => 'add_new_blog_post',
            'label' => '&nbsp;'.__('Add New Popup').'&nbsp;&nbsp;&nbsp;&nbsp;',
            'class' => 'add',
            'button_class' => '',
            'class_name' => 'Magento\Backend\Block\Widget\Button\SplitButton',
            'options' => $this->_getAddButtonOptions(),
        ]);
        return parent::_prepareLayout();
    }

    protected function _getAddButtonOptions()
    {
        $splitButtonOptions = [];
        $options = $this->_contentType->toArray();
        foreach ($options as $key => $value) {
            $splitButtonOptions[] = [
                'label' => __($value),
                'onclick' => "setLocation('" . $this->_getCreateUrl($key) . "')"
            ];
        }

        return $splitButtonOptions;
    }

    protected function _getCreateUrl($key)
    {
        return $this->getUrl(
            'newsletterPopup/*/new',
            ['content_type' => $key]
        );
    }
}
