<?php
/**
 * Copyright © 2016 MB Vienas bitas. All rights reserved.
 * @website    www.magetrend.com
 * @package    Newsletter Popup Pro from M2
 * @author     Edvinas Stulpinas <edwin@magetrend.com>
 */

namespace Magetrend\NewsletterPopup\Block\Adminhtml\Campaign\Edit\Tab;

use \Magento\Backend\Block\Widget\Form\Generic;
use \Magento\Backend\Block\Widget\Tab\TabInterface;

class Popup extends Generic implements TabInterface
{
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    /**
     * @var \Magento\Cms\Model\Wysiwyg\Config
     */
    protected $_wysiwygConfig;

    protected $_yesNo;

    protected $_theme;

    /**
     * @var \Magetrend\NewsletterPopup\Model\Config\Source\Method
     */
    public $methodConfig;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        \Magento\Config\Model\Config\Source\Yesno $yesNo,
        \Magetrend\NewsletterPopup\Model\Config\Source\Page $pageSource,
        \Magetrend\NewsletterPopup\Model\Config\Source\Popup $popup,
        \Magetrend\NewsletterPopup\Model\Config\Source\Method $methodConfig,
        array $data = []
    ) {
        $this->_systemStore = $systemStore;
        $this->_popup = $popup;
        $this->_yesNo = $yesNo;
        $this->_pageSource = $pageSource;
        $this->methodConfig = $methodConfig;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('newsletterpopup_campaign');
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('page_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Popup Settings')]);

        $fieldset->addField(
            'popup_id',
            'select',
            [
                'name' => 'popup_id',
                'label' =>  __('Popup'),
                'title' =>  __('Popup'),
                'value' => '',
                'required' => true,
                'options' => $this->_popup->toArray(),
            ]
        );

        $fieldset->addField(
            'popup_delay',
            'text',
            [
                'name' => 'popup_delay',
                'label' => __('Popup Delay Time'),
                'title' => __('Popup Delay Time'),
                'required' => false,
                'disabled' => false,
                'value' => 7
            ]
        );

        $fieldset->addField(
            'layer_close',
            'select',
            [
                'name' => 'layer_close',
                'label' =>  __('Close after Click on Popup Layer'),
                'title' =>  __('Close after Click on Popup Layer'),
                'value' => 1,
                'options' => $this->_yesNo->toArray(),
            ]
        );

        $fieldset->addField(
            'cookie_lifetime',
            'text',
            [
                'name' => 'cookie_lifetime',
                'label' => __('Show again after X minutes'),
                'title' => __('Show again after X minutes'),
                'required' => false,
                'disabled' => false,
                'value' => '365'
            ]
        );

        $fieldset->addField(
            'method',
            'select',
            [
                'name' => 'method',
                'label' =>  __('Show popup once per'),
                'title' =>  __('Show popup once per'),
                'value' => '',
                'required' => true,
                'options' => $this->methodConfig->toArray(),
                'note' => __('Default: Store')
            ]
        );

        if ($model->getId()) {
            $form->setValues($model->getData());
        }
        $this->setForm($form);
        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Popup Settings');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Popup Settings');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}
