<?php
/**
 * Copyright © 2016 MB Vienas bitas. All rights reserved.
 * @website    www.magetrend.com
 * @package    Newsletter Popup Pro from M2
 * @author     Edvinas Stulpinas <edwin@magetrend.com>
 */

namespace Magetrend\NewsletterPopup\Block\Adminhtml;

use Magento\Core\Model\ObjectManager;

class Campaign extends \Magento\Backend\Block\Widget\Grid\Container
{
    protected $_contentType;

    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magetrend\NewsletterPopup\Model\Config\Source\Type $contentType,
        array $data = []
    ) {
        $this->_contentType = $contentType;
        parent::__construct($context, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_controller = 'campaign_index';
        $this->_headerText = __('Manage Newsletter Popup Campaign');
        $this->_addButtonLabel = __('Create New Campaign');
        parent::_construct();
    }

    protected function _getCreateUrl($key)
    {
        return $this->getUrl(
            'newsletterPopup/*/new',
            ['content_type' => $key]
        );
    }
}
