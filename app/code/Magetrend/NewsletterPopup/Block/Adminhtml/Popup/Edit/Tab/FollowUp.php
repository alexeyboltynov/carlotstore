<?php
/**
 * MB "Vienas bitas" (Magetrend.com)
 *
 * @category MageTrend
 * @package  Magetend/NewsletterPopup
 * @author   Edvinas Stulpinas <edwin@magetrend.com>
 * @license  http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link     http://www.magetrend.com/magento-2-newsletter-popup-extension
 */

namespace Magetrend\NewsletterPopup\Block\Adminhtml\Popup\Edit\Tab;

use \Magento\Backend\Block\Widget\Form\Generic;
use \Magento\Backend\Block\Widget\Tab\TabInterface;

class FollowUp extends Generic implements TabInterface
{
    /**
     * @var \Magento\Config\Model\Config\Source\Yesno
     */
    public $yesNo;

    /**
     * @var \Magento\Config\Model\Config\Source\Email\Template
     */
    public $emailTemplateSource;

    /**
     * FollowUp constructor.
     *
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Config\Model\Config\Source\Email\Template $emailTemplateSource
     * @param \Magento\Config\Model\Config\Source\Yesno $yesno
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Config\Model\Config\Source\Email\Template $emailTemplateSource,
        \Magento\Config\Model\Config\Source\Yesno $yesno,
        array $data = []
    ) {
        $this->yesNo = $yesno;
        $this->emailTemplateSource = $emailTemplateSource;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('newsletterpopup_popup');
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('page_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Follow Up Email')]);
        $fieldset->addField(
            'followup_is_active',
            'select',
            [
                'name' => 'followup_is_active',
                'label' =>  __('Is Active'),
                'title' =>  __('Is Active'),
                'value' => 0,
                'options' => $this->yesNo->toArray(),
                'note' =>  __('Send follow up email if coupon was not used yet'),
            ]
        );

        $this->emailTemplateSource->setPath('newsletterpopup_followup_email_template');
        $options = $this->emailTemplateSource->toOptionArray();
        $optionArray = [];
        foreach ($options as $option) {
            $optionArray[$option['value']] = $option['label'];
        }
        $fieldset->addField(
            'followup_email_template',
            'select',
            [
                'name' => 'followup_email_template',
                'label' =>  __('Email Template'),
                'title' =>  __('Email Template'),
                'options' => $optionArray,
            ]
        );

        $fieldset->addField(
            'followup_delay',
            'text',
            [
                'name' => 'followup_delay',
                'label' => __('Send after X hours'),
                'title' => __('Send after X hours'),
                'required' => false,
                'disabled' => false,
                'value' => 12
            ]
        );

        $fieldset->addField(
            'followup_sender_name',
            'text',
            [
                'name' => 'followup_sender_name',
                'label' => __('Sender Name'),
                'title' => __('Sender Name'),
                'required' => false,
                'disabled' => false,
            ]
        );

        $fieldset->addField(
            'followup_sender_email',
            'text',
            [
                'name' => 'followup_sender_email',
                'label' => __('Sender Email'),
                'title' => __('Sender Email'),
                'required' => false,
                'disabled' => false,
            ]
        );

        if ($model->getId()) {
            $form->setValues($model->getData());
        }
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Discount Coupon');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return $this->getTabLabel();
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}
