<?php
/**
 * Copyright © 2016 MB Vienas bitas. All rights reserved.
 * @website    www.magetrend.com
 * @package    Newsletter Popup Pro from M2
 * @author     Edvinas Stulpinas <edwin@magetrend.com>
 */

namespace Magetrend\NewsletterPopup\Block\Adminhtml\Popup\Edit\Tab;

use \Magento\Backend\Block\Widget\Tab\TabInterface;
use \Magento\Backend\Block\Widget\Form\Generic;

class Button extends Generic implements TabInterface
{
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    /**
     * @var \Magento\Config\Model\Config\Source\Yesno
     */
    protected $_yesNo;

    /**
     * @var \Magetrend\NewsletterPopup\Model\Config\Source\Position
     */
    protected $_position;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        \Magento\Config\Model\Config\Source\Yesno $yesno,
        \Magetrend\NewsletterPopup\Model\Config\Source\Position $position,
        array $data = []
    ) {
        $this->_systemStore = $systemStore;
        $this->_position = $position;
        $this->_yesNo = $yesno;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('newsletterpopup_popup');

        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('page_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => $this->getTabLabel()]);

        $fieldset->addField(
            'button_is_active',
            'select',
            [
                'name' => 'button_is_active',
                'label' =>  __('Is Active'),
                'title' =>  __('Is Active'),
                'value' => 0,
                'options' => $this->_yesNo->toArray(),
            ]
        );

        $fieldset->addField(
            'button_selector',
            'text',
            [
                'name' => 'button_selector',
                'label' => __('Trigger Button HTML selector'),
                'title' => __('Trigger Button HTML selector'),
                'note' => __('E.g. .popup-open-button, #trigger button'),
                'required' => false,
                'disabled' => false,
                'value' => '.mt-np-popup-open'
            ]
        );

        $fieldset->addField(
            'button_text',
            'text',
            [
                'name' => 'button_text',
                'label' => __('Button Text'),
                'title' => __('Button Text'),
                'required' => false,
                'disabled' => false,
                'value' => __("Your Free Voucher is Here!")
            ]
        );

        $fieldset->addField(
            'button_only',
            'select',
            [
                'name' => 'button_only',
                'label' =>  __('Button Only'),
                'title' =>  __('Button Only'),
                'note' =>  __('If yes, popup will not show automatically'),
                'value' => 0,
                'options' => $this->_yesNo->toArray(),
            ]
        );

        $fieldset->addField(
            'button_position',
            'select',
            [
                'name' => 'button_position',
                'label' =>  __('Button Position'),
                'title' =>  __('Button Position'),
                'value' => 'leftcenter',
                'options' => $this->_position->toArray(),

            ]
        );

        $fieldset->addField(
            'button_top',
            'text',
            [
                'name' => 'button_top',
                'label' =>  __('Padding Top'),
                'title' =>  __('Padding Top'),
                'value'=> '150px'
            ]
        );

        if ($model->getId()) {
            $form->setValues($model->getData());
        }
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Trigger Button Settings');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return $this->getTabLabel();
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}
