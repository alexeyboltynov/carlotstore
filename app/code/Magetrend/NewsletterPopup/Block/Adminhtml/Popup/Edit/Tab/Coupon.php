<?php
/**
 * Copyright © 2016 MB Vienas bitas. All rights reserved.
 * @website    www.magetrend.com
 * @package    Newsletter Popup Pro from M2
 * @author     Edvinas Stulpinas <edwin@magetrend.com>
 */

namespace Magetrend\NewsletterPopup\Block\Adminhtml\Popup\Edit\Tab;

use \Magento\Backend\Block\Widget\Form\Generic;
use \Magento\Backend\Block\Widget\Tab\TabInterface;

class Coupon extends Generic implements TabInterface
{
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    /**
     * @var \Magetrend\NewsletterPopup\Model\Config\Source\Format
     */
    protected $_codeFormat;

    /**
     * @var \Magento\Config\Model\Config\Source\Yesno
     */
    protected $_yesNo;

    /**
     * @var \Magetrend\NewsletterPopup\Model\Config\Source\Rule
     */
    protected $_priceRuleSource;

    /**
     * Coupon constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magetrend\NewsletterPopup\Model\Config\Source\Format $format
     * @param \Magetrend\NewsletterPopup\Model\Config\Source\Rule $rule
     * @param \Magento\Config\Model\Config\Source\Yesno $yesno
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magetrend\NewsletterPopup\Model\Config\Source\Format $format,
        \Magetrend\NewsletterPopup\Model\Config\Source\Rule $rule,
        \Magento\Config\Model\Config\Source\Yesno $yesno,
        array $data = []
    ) {
        $this->_codeFormat = $format;
        $this->_yesNo = $yesno;
        $this->_priceRuleSource = $rule;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('newsletterpopup_popup');
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('page_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Discount Coupon')]);
        $fieldset->addField(
            'coupon_is_active',
            'select',
            [
                'name' => 'coupon_is_active',
                'label' =>  __('Is Active'),
                'title' =>  __('Is Active'),
                'value' => 0,
                'options' => $this->_yesNo->toArray(),
            ]
        );

        $fieldset->addField(
            'show_in_popup',
            'select',
            [
                'name' => 'show_in_popup',
                'label' => __('Show in Popup'),
                'title' => __('Show in Popup'),
                'note' =>  __('Show Coupon Code in Popup after Subscription.'),
                'value' => 0,
                'options' => $this->_yesNo->toArray(),
            ]
        );

        $fieldset->addField(
            'apply_to_cart',
            'select',
            [
                'name' => 'apply_to_cart',
                'label' => __('Apply to Cart'),
                'title' => __('Apply to Cart'),
                'note' =>  __('Apply discount code for cart automatically'),
                'value' => 0,
                'options' => $this->_yesNo->toArray(),
            ]
        );

        $fieldset->addField(
            'coupon_rule_id',
            'select',
            [
                'name' => 'coupon_rule_id',
                'label' =>  __('Cart Price Rule'),
                'title' =>  __('Cart Price Rule'),
                'note' =>  __('Create new rule: Marketing >> Cart Price Rules >> Add New Rule'),
                'options' => $this->_priceRuleSource->toArray(),
            ]
        );

        $fieldset->addField(
            'coupon_expire_in_days',
            'text',
            [
                'name' => 'coupon_expire_in_days',
                'label' => __('Coupon lifetime (in days)'),
                'title' => __('Coupon lifetime (in days)'),
                'required' => false,
                'disabled' => false,
                'value' => 3,
                'note' =>  __('How many days coupon code will be valid'),

            ]
        );

        $fieldset->addField(
            'coupon_length',
            'text',
            [
                'name' => 'coupon_length',
                'label' => __('Code Length'),
                'title' => __('Code Length'),
                'required' => false,
                'disabled' => false,
                'value' => 8
            ]
        );

        $fieldset->addField(
            'coupon_format',
            'select',
            [
                'name' => 'coupon_format',
                'label' =>  __('Code Format'),
                'title' =>  __('Code Format'),
                'value' => 'alphanum',
                'options' => $this->_codeFormat->toArray(),
            ]
        );

        $fieldset->addField(
            'coupon_prefix',
            'text',
            [
                'name' => 'coupon_prefix',
                'label' => __('Code Prefix'),
                'title' => __('Code Prefix'),
                'required' => false,
                'disabled' => false,
                'value' => 'NS-'
            ]
        );

        $fieldset->addField(
            'coupon_suffix',
            'text',
            [
                'name' => 'coupon_suffix',
                'label' => __('Code Suffix'),
                'title' => __('Code Suffix'),
                'required' => false,
                'disabled' => false,
            ]
        );

        $fieldset->addField(
            'coupon_dash',
            'text',
            [
                'name' => 'coupon_dash',
                'label' => __('Dash'),
                'title' => __('Dash'),
                'note' => __('Add Dash Every Time after X Symbols'),
                'required' => false,
                'disabled' => false,
                'value' => 4
            ]
        );

        if ($model->getId()) {
            $form->setValues($model->getData());
        }
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Discount Coupon');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return $this->getTabLabel();
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}
