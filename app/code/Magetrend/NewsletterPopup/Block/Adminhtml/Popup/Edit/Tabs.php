<?php
/**
 * MB "Vienas bitas" (Magetrend.com)
 *
 * @category MageTrend
 * @package  Magetend/NewsletterPopup
 * @author   Edvinas Stulpinas <edwin@magetrend.com>
 * @license  http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link     http://www.magetrend.com/magento-2-newsletter-popup-extension
 */
namespace Magetrend\NewsletterPopup\Block\Adminhtml\Popup\Edit;

/**
 * Admin page left menu
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * Tabs constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
     * @param \Magento\Backend\Model\Auth\Session $authSession
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Backend\Model\Auth\Session $authSession,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $jsonEncoder, $authSession, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('popup_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Newsletter Popup Information'));
    }

    /**
     * @return $this
     */
    protected function _beforeToHtml()
    {
        $contentType = $this->_request->getParam('content_type');
        if (empty($contentType)) {
            $model = $this->_coreRegistry->registry('newsletterpopup_popup');
            $contentType = $model->getContentType();
        }

        if ($contentType == \Magetrend\NewsletterPopup\Model\Popup::TYPE_NEWSLETTER_SUBSCRIPTION) {
            $this->addGeneralTab();
            $this->addPopupTab();
            $this->addCouponTab();
            $this->followUpTab();
            $this->addFieldTab();
            $this->addThemeTab();
            $this->addLinkTab();
            $this->addButtonTab();
        }

        if ($contentType == \Magetrend\NewsletterPopup\Model\Popup::TYPE_STATIC_BLOCK) {
            $this->addGeneralTab();
            $this->addPopupTab();
            $this->addCmsTab();
            $this->addButtonTab();
        }

        return parent::_beforeToHtml();
    }

    /**
     * @throws \Exception
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function addGeneralTab()
    {
        $this->addTab(
            'general_section',
            [
                'label' => __('General Settings'),
                'title' => __('General Settings'),
                'active' => true,
                'content' => $this->getLayout()->createBlock(
                    'Magetrend\NewsletterPopup\Block\Adminhtml\Popup\Edit\Tab\General'
                )->toHtml()
            ]
        );
    }

    /**
     * @throws \Exception
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function addPopupTab()
    {
        $this->addTab(
            'popup_section',
            [
                'label' => __('Popup Settings'),
                'title' => __('Popup Settings'),
                'content' => $this->getLayout()->createBlock(
                    'Magetrend\NewsletterPopup\Block\Adminhtml\Popup\Edit\Tab\Popup'
                )->toHtml()
            ]
        );
    }

    /**
     * @throws \Exception
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function addFieldTab()
    {

        $this->addTab(
            'field_section',
            [
                'label' => __('Additional Fields'),
                'title' => __('Additional Fields'),
                'content' => $this->getLayout()->createBlock(
                    'Magetrend\NewsletterPopup\Block\Adminhtml\Popup\Edit\Tab\Field'
                )->toHtml()
            ]
        );
    }

    /**
     * @throws \Exception
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function addThemeTab()
    {
        if (!empty($this->_request->getParam('content_type', ''))) {
            return;
        }

        $this->addTab(
            'theme_section',
            [
                'label' => __('Theme'),
                'title' => __('Theme'),
                'content' => $this->getLayout()->createBlock(
                    'Magetrend\NewsletterPopup\Block\Adminhtml\Popup\Edit\Tab\Theme'
                )->toHtml()
            ]
        );
    }

    /**
     * @throws \Exception
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function addLinkTab()
    {
        $this->addTab(
            'link_section',
            [
                'label' => __('Social Links'),
                'title' => __('Social Links'),
                'content' => $this->getLayout()
                    ->createBlock('Magetrend\NewsletterPopup\Block\Adminhtml\Popup\Edit\Tab\Link')
                    ->toHtml()
            ]
        );
    }

    /**
     * @throws \Exception
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function addCouponTab()
    {
        $this->addTab(
            'coupon_section',
            [
                'label' => __('Discount Coupon'),
                'title' => __('Discount Coupon'),
                'content' => $this->getLayout()->createBlock(
                    'Magetrend\NewsletterPopup\Block\Adminhtml\Popup\Edit\Tab\Coupon'
                )->toHtml()
            ]
        );
    }

    /**
     * @throws \Exception
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function followUpTab()
    {
        $this->addTab(
            'reminder_section',
            [
                'label' => __('Follow Up Email'),
                'title' => __('Follow Up Email'),
                'content' => $this->getLayout()->createBlock(
                    'Magetrend\NewsletterPopup\Block\Adminhtml\Popup\Edit\Tab\FollowUp'
                )->toHtml()
            ]
        );
    }

    /**
     * @throws \Exception
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function addCmsTab()
    {
        $this->addTab(
            'cms_section',
            [
                'label' => __('Static Block Settings'),
                'title' => __('Static Block Settings'),
                'content' => $this->getLayout()->createBlock(
                    'Magetrend\NewsletterPopup\Block\Adminhtml\Popup\Edit\Tab\Cms'
                )->toHtml()
            ]
        );
    }

    /**
     * @throws \Exception
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function addButtonTab()
    {
        $this->addTab(
            'button_section',
            [
                'label' => __('Trigger Button'),
                'title' => __('Trigger Button'),
                'content' => $this->getLayout()->createBlock(
                    'Magetrend\NewsletterPopup\Block\Adminhtml\Popup\Edit\Tab\Button'
                )->toHtml()
            ]
        );
    }
}
