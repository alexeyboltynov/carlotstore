<?php
/**
 * Copyright © 2016 MB Vienas bitas. All rights reserved.
 * @website    www.magetrend.com
 * @package    Newsletter Popup Pro from M2
 * @author     Edvinas Stulpinas <edwin@magetrend.com>
 */

namespace Magetrend\NewsletterPopup\Block\Adminhtml\Popup\Edit\Tab;

use \Magento\Backend\Block\Widget\Form\Generic;
use \Magento\Backend\Block\Widget\Tab\TabInterface;

class Theme extends Generic implements TabInterface
{
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    /**
     * @var \Magetrend\NewsletterPopup\Model\Config\Source\Icon
     */
    protected $_iconSource;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        \Magetrend\NewsletterPopup\Model\Config\Source\Icon $icon,
        array $data = []
    ) {
        $this->_systemStore = $systemStore;
        $this->_iconSource = $icon;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('newsletterpopup_popup');
        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('page_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Content & Colors')]);

        switch ($model->getTheme()) {
            case 'default':
                $this->addTitleField($fieldset);
                $this->addSubTitleField($fieldset);
                $this->addContentField($fieldset);
                break;
            case 'label':
                $this->addTitleField($fieldset);
                $this->addSubTitleField($fieldset);
                $this->addContentField($fieldset);
                break;
            case 'clear':
                $this->addTitleField($fieldset);
                $this->addSubTitleField($fieldset);
                $this->addFooterTextField($fieldset);
                $this->addPrimaryColorField($fieldset);
                break;

            case 'osom':
                $this->addTitleField($fieldset);
                $this->addSubTitleField($fieldset);
                $this->addFooterTextField($fieldset);
                $this->addColor1Field($fieldset);
                $this->addColor2Field($fieldset);
                $this->addColor3Field($fieldset);
                $this->addColor4Field($fieldset);
                $this->addColor5Field($fieldset);
                $this->addImageField($fieldset);
                break;
            case 'orange':
                $this->addTitleField($fieldset);
                $this->addSubTitleField($fieldset);
                $this->addFooterTextField($fieldset);
                $this->addPrimaryColorField($fieldset);
                $this->addIconField($fieldset);
                break;
            default:
                $this->addTitleField($fieldset);
                $this->addSubTitleField($fieldset);
                $this->addContentField($fieldset);
                $this->addFooterTextField($fieldset);
                $this->addPrimaryColorField($fieldset);
                $this->addIconField($fieldset);
                break;
        }

        if ($model->getId()) {
            $form->setValues($model->getData());
        }
        $this->setForm($form);

        return parent::_prepareForm();
    }

    public function addTitleField($fieldset)
    {
        $fieldset->addField(
            'text_1',
            'text',
            [
                'name' => 'text_1',
                'label' => __('Title'),
                'title' => __('Title'),
                'required' => false,
                'disabled' => false,
                'value' => 'Subscribe & get <span>$5</span> discount!'
            ]
        );
    }

    public function addIconField($fieldset)
    {
        $fieldset->addField(
            'icon',
            'select',
            [
                'name' => 'icon',
                'label' =>  __('Icon'),
                'title' =>  __('Icon'),
                'value' => 'gift_card',
                'options' => $this->_iconSource->toArray(),
            ]
        );
    }

    public function addPrimaryColorField($fieldset)
    {
        $fieldset->addField(
            'color_1',
            'text',
            [
                'name' => 'color_1',
                'label' => __('Primary Color'),
                'title' => __('Primary Color'),
                'required' => false,
                'disabled' => false,
                'class'  => 'jscolor {hash:true,refine:false}',
                'value' => 'fa814c'

            ]
        );
    }

    public function addColor1Field($fieldset)
    {
        $fieldset->addField(
            'color_1',
            'text',
            [
                'name' => 'color_1',
                'label' => __('Background Color'),
                'title' => __('Background Color'),
                'required' => false,
                'disabled' => false,
                'class'  => 'jscolor {hash:true,refine:false}',
                'value' => '#ffffff'

            ]
        );
    }

    public function addColor2Field($fieldset)
    {
        $fieldset->addField(
            'color_2',
            'text',
            [
                'name' => 'color_2',
                'label' => __('Title Text Color'),
                'title' => __('Title Text Color'),
                'required' => false,
                'disabled' => false,
                'class'  => 'jscolor {hash:true,refine:false}',
                'value' => '#000000'

            ]
        );
    }

    public function addColor3Field($fieldset)
    {
        $fieldset->addField(
            'color_3',
            'text',
            [
                'name' => 'color_3',
                'label' => __('Subtitle Text Color'),
                'title' => __('Subtitle Text Color'),
                'required' => false,
                'disabled' => false,
                'class'  => 'jscolor {hash:true,refine:false}',
                'value' => '#000000'

            ]
        );
    }

    public function addColor4Field($fieldset)
    {
        $fieldset->addField(
            'color_4',
            'text',
            [
                'name' => 'color_4',
                'label' => __('Button Background Color'),
                'title' => __('Button Background Color'),
                'required' => false,
                'disabled' => false,
                'class'  => 'jscolor {hash:true,refine:false}',
                'value' => '#000000'

            ]
        );
    }

    public function addColor5Field($fieldset)
    {
        $fieldset->addField(
            'color_5',
            'text',
            [
                'name' => 'color_5',
                'label' => __('Button Text Color'),
                'title' => __('Button Text Color'),
                'required' => false,
                'disabled' => false,
                'class'  => 'jscolor {hash:true,refine:false}',
                'value' => '#ffffff'

            ]
        );
    }

    public function addFooterTextField($fieldset)
    {
        $fieldset->addField(
            'text_4',
            'text',
            [
                'name' => 'text_4',
                'label' => __('Footer Text'),
                'title' => __('Footer Text'),
                'required' => false,
                'disabled' => false,
                'value' => 'More ways to stay connected:'
            ]
        );
    }

    public function addContentField($fieldset)
    {
        $fieldset->addField(
            'text_3',
            'textarea',
            [
                'name' => 'text_3',
                'label' => __('Content Text'),
                'title' => __('Content Text'),
                'required' => false,
                'disabled' => false,
                'value' => 'Would you like to be one of the first to receive exclusive information
                about the latest collections, offers and events from our store? Then just subscribe to
                our free newsletter now and get a voucher to the value of 5 € for your next purchase
                in our Online-Shop.'

            ]
        );
    }

    public function addSubTitleField($fieldset)
    {
        $fieldset->addField(
            'text_2',
            'text',
            [
                'name' => 'text_2',
                'label' => __('Subtitle'),
                'title' => __('Subtitle'),
                'required' => false,
                'disabled' => false,
                'value' => 'get the first offers and events from our store'
            ]
        );
    }

    public function addImageField($fieldset)
    {
        $fieldset->addField(
            'bg_1',
            'image',
            [
                'name' => 'bg_1',
                'label' => __('Popup Image'),
                'title' => __('Popup Image'),
                'required' => false,
                'disabled' => false,
                'value' => '',
                'note' =>  __('Size: 300px x 450px'),
            ]
        );
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Content & Colors');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Content & Colors');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}
