<?php
/**
 * Copyright © 2016 MB Vienas bitas. All rights reserved.
 * @website    www.magetrend.com
 * @package    Newsletter Popup Pro from M2
 * @author     Edvinas Stulpinas <edwin@magetrend.com>
 */

namespace Magetrend\NewsletterPopup\Block\Adminhtml\Popup\Edit\Tab;

use \Magento\Backend\Block\Widget\Form\Generic;
use \Magento\Backend\Block\Widget\Tab\TabInterface;

class Link extends Generic implements TabInterface
{
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    /**
     * Link constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magetrend\NewsletterPopup\Model\Config\Source\Icon $icon
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magetrend\NewsletterPopup\Model\Config\Source\Icon $icon,
        array $data = []
    ) {
        $this->_iconSource = $icon;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('newsletterpopup_popup');
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('page_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Social Links')]);
        $fieldset->addField(
            'link_facebook',
            'text',
            [
                'name' => 'link_facebook',
                'label' => __('Facebook'),
                'title' => __('Facebook'),
                'required' => false,
                'disabled' => false
            ]
        );

        $fieldset->addField(
            'link_twitter',
            'text',
            [
                'name' => 'link_twitter',
                'label' => __('Twitter'),
                'title' => __('Twitter'),
                'required' => false,
                'disabled' => false
            ]
        );

        $fieldset->addField(
            'link_pinterest',
            'text',
            [
                'name' => 'link_pinterest',
                'label' => __('Pinterest'),
                'title' => __('Pinterest'),
                'required' => false,
                'disabled' => false
            ]
        );

        $fieldset->addField(
            'link_gplus',
            'text',
            [
                'name' => 'link_gplus',
                'label' => __('Google Plus'),
                'title' => __('Google Plus'),
                'required' => false,
                'disabled' => false
            ]
        );

        $fieldset->addField(
            'link_instagram',
            'text',
            [
                'name' => 'link_instagram',
                'label' => __('Instagram'),
                'title' => __('Instagram'),
                'required' => false,
                'disabled' => false
            ]
        );

        $fieldset->addField(
            'link_tumblr',
            'text',
            [
                'name' => 'link_tumblr',
                'label' => __('Tumblr'),
                'title' => __('Tumblr'),
                'required' => false,
                'disabled' => false
            ]
        );

        $fieldset->addField(
            'link_linkedin',
            'text',
            [
                'name' => 'link_linkedin',
                'label' => __('Linked In'),
                'title' => __('Linked In'),
                'required' => false,
                'disabled' => false
            ]
        );

        $fieldset->addField(
            'link_youtube',
            'text',
            [
                'name' => 'link_youtube',
                'label' => __('Youtube'),
                'title' => __('Youtube'),
                'required' => false,
                'disabled' => false
            ]
        );

        if ($model->getId()) {
            $form->setValues($model->getData());
        }
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Social Links');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Social Links');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}
