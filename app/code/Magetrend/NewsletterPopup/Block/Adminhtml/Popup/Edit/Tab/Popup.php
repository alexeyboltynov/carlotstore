<?php
/**
 * Copyright © 2016 MB Vienas bitas. All rights reserved.
 * @website    www.magetrend.com
 * @package    Newsletter Popup Pro from M2
 * @author     Edvinas Stulpinas <edwin@magetrend.com>
 */

namespace Magetrend\NewsletterPopup\Block\Adminhtml\Popup\Edit\Tab;

use \Magento\Backend\Block\Widget\Form\Generic;
use \Magento\Backend\Block\Widget\Tab\TabInterface;

class Popup extends Generic implements TabInterface
{
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    /**
     * @var \Magento\Cms\Model\Wysiwyg\Config
     */
    protected $_wysiwygConfig;

    /**
     * @var \Magento\Config\Model\Config\Source\Yesno
     */
    protected $_yesNo;

    /**
     * @var \Magetrend\NewsletterPopup\Model\Config\Source\Theme
     */
    protected $_theme;

    /**
     * Popup constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param \Magento\Config\Model\Config\Source\Yesno $yesNo
     * @param \Magetrend\NewsletterPopup\Model\Config\Source\Page $pageSource
     * @param \Magetrend\NewsletterPopup\Model\Config\Source\Theme $theme
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        \Magento\Config\Model\Config\Source\Yesno $yesNo,
        \Magetrend\NewsletterPopup\Model\Config\Source\Page $pageSource,
        \Magetrend\NewsletterPopup\Model\Config\Source\Theme $theme,
        array $data = []
    ) {
        $this->_systemStore = $systemStore;
        $this->_theme = $theme;
        $this->_yesNo = $yesNo;
        $this->_pageSource = $pageSource;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('newsletterpopup_popup');
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('page_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Popup Settings')]);

        $contentType = $this->_request->getParam('content_type');
        if (empty($contentType)) {
            $model = $this->_coreRegistry->registry('newsletterpopup_popup');
            $contentType = $model->getContentType();
        }

        if ($contentType == \Magetrend\NewsletterPopup\Model\Popup::TYPE_NEWSLETTER_SUBSCRIPTION) {
            $fieldset->addField(
                'theme',
                'select',
                [
                    'name' => 'theme',
                    'label' =>  __('Theme'),
                    'title' =>  __('Theme'),
                    'value' => 1,
                    'options' => $this->_theme->toArray(),
                ]
            );
        }

        $fieldset->addField(
            'mobile_auto_position',
            'select',
            [
                'name' => 'mobile_auto_position',
                'label' => __('Disable Auto Position on Mobile Devices'),
                'title' => __('Disable Auto Position on Mobile Devices'),
                'required' => false,
                'disabled' => false,
                'value' => 0,
                'options' => $this->_yesNo->toArray(),
                'note' => __('This option should be on if your store design is not response.')
            ]
        );

        if ($model->getId()) {
            $form->setValues($model->getData());
        }
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Popup Settings');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Popup Settings');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}
