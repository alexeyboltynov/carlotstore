<?php
/**
 * Copyright © 2016 MB Vienas bitas. All rights reserved.
 * @website    www.magetrend.com
 * @package    Newsletter Popup Pro from M2
 * @author     Edvinas Stulpinas <edwin@magetrend.com>
 */

namespace Magetrend\NewsletterPopup\Block\Adminhtml\Popup\Edit\Tab\Field\Type;

use Magento\Catalog\Block\Adminhtml\Product\Edit\Tab\Options\Type\AbstractType;

class Checkbox extends AbstractType
{

    /**
     * @var string
     */
    protected $_template = 'popup/edit/tab/field/type/checkbox.phtml';
}
