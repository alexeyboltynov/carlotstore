<?php
/**
 * Copyright © 2016 MB Vienas bitas. All rights reserved.
 * @website    www.magetrend.com
 * @package    Newsletter Popup Pro from M2
 * @author     Edvinas Stulpinas <edwin@magetrend.com>
 */

namespace Magetrend\NewsletterPopup\Block;

class Cms extends \Magetrend\NewsletterPopup\Block\Popup
{
    /**
     * @var \Magento\Framework\Locale\CurrencyInterface
     */
    protected $_localeCurrency;

    /**
     * @var array
     */
    protected $_social = [
        [
            'name' => 'facebook',
            'label' => 'Facebook'
        ],
        [
            'name' => 'twitter',
            'label' => 'Twitter'
        ],
        [
            'name' => 'pinterest',
            'label' => 'Pinterest'
        ],
        [
            'name' => 'gplus',
            'label' => 'Google Plus'
        ],
        [
            'name' => 'instagram',
            'label' => 'Instagram'
        ],
        [
            'name' => 'youtube',
            'label' => 'Youtube'
        ],
        [
            'name' => 'tumblr',
            'label' => 'Tumblr'
        ],
        [
            'name' => 'linkedin',
            'label' => 'Linked In'
        ],
    ];

    /**
     * @param $name
     * @return mixed
     */
    public function getSocialLink($name)
    {
        return $this->getPopup()->getData('link_'.$name);
    }

    /**
     * @return array
     */
    public function getSocial()
    {
        $socialList = [];
        foreach ($this->_social as $social) {
            $link = $this->getSocialLink($social['name']);
            if (empty($link)) {
                continue;
            }

            $socialList[] = [
                'title' => __($social['label']),
                'name' => $social['name'],
                'link' => $link,
            ];
        }
        return $socialList;
    }

    /**
     * @param $field
     * @return mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getFieldHtml($field)
    {
        switch ($field['type']) {
            case 'field':
                $blockClass = 'Magetrend\NewsletterPopup\Block\Popup\Field\Text';
                break;
            case 'area':
                $blockClass = 'Magetrend\NewsletterPopup\Block\Popup\Field\Textarea';
                break;
            case 'radio':
                $blockClass = 'Magetrend\NewsletterPopup\Block\Popup\Field\Radio';
                break;
            case 'checkbox':
                $blockClass = 'Magetrend\NewsletterPopup\Block\Popup\Field\Checkbox';
                break;
            case 'drop_down':
                $blockClass = 'Magetrend\NewsletterPopup\Block\Popup\Field\Select';
                break;
        }

        $block = $this->getLayout()->createBlock($blockClass)->setData($field);
        return $block->toHtml();
    }


    /**
     * Returns text from configuration
     * @param $id
     * @return mixed
     */
    public function getTextHtml($id)
    {
        return $this->getPopup()->getData('text_'.$id);
    }

    /**
     * Returns color from configuration
     * @param $id
     * @return mixed
     */
    public function getColor($id)
    {
        return $this->getPopup()->getData('color_'.$id);
    }

    public function getImage($key)
    {
        $fileName = $this->getPopup()->getData($key);
        $img = $this->_storeManager->getStore()
                ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).$fileName;
        if (empty($fileName)) {
            $img = $this->_assetRepo->createAsset('Magetrend_NewsletterPopup::images/300x450.png')->getUrl();
        }

        return $img;
    }

    public function getHelper()
    {
        return $this->_helper;
    }
}
