<?php
/**
 * Copyright © 2016 MB Vienas bitas. All rights reserved.
 * @website    www.magetrend.com
 * @package    Newsletter Popup Pro from M2
 * @author     Edvinas Stulpinas <edwin@magetrend.com>
 */

namespace Magetrend\NewsletterPopup\Block;

use Magetrend\NewsletterPopup\Helper;
use \Magento\Framework\View\Element\Template;

class Popup extends Template
{

    /**
     * @var Helper\Data|null
     */
    protected $_helper = null;

    /**
     * @var \Magetrend\NewsletterPopup\Model\Popup
     */
    protected $_popup = null;

    /**
     * @var \Magetrend\NewsletterPopup\Model\Campaign
     */
    protected $_campaign = null;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var \Magento\Framework\Stdlib\CookieManagerInterface
     */
    protected $_cookieManager;

    /**
     * Popup constructor.
     * @param Template\Context $context
     * @param Helper\Data $helper
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Framework\Stdlib\CookieManagerInterface $cookieManagerInterface
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magetrend\NewsletterPopup\Helper\Data $helper,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\Stdlib\CookieManagerInterface $cookieManagerInterface,
        array $data = []
    ) {
        $this->_objectManager = $objectManager;
        $this->_helper = $helper;
        $this->_cookieManager = $cookieManagerInterface;
        parent::__construct($context, $data);
    }

    /**
     * @return bool
     */
    public function isActivePopup()
    {
        if (!$this->_helper->isActive()) {
            return false;
        }

        $popup = $this->getPopup();
        if (!$popup) {
            return false;
        }

        return true;
    }

    /**
     * @return \Magetrend\NewsletterPopup\Model\Popup|mixed|null
     */
    public function getPopup()
    {
        if ($this->_popup == null) {
            $campaign = $this->getCampaign();
            if ($campaign) {
                $this->_popup = $campaign->getPopup();
            }
        }
        return $this->_popup;
    }

    /**
     * @return \Magetrend\NewsletterPopup\Model\Campaign
     */
    public function getCampaign()
    {
        if ($this->_campaign == null) {
            $campaign = $this->_objectManager->create('\Magetrend\NewsletterPopup\Model\Campaign');
            $this->_campaign = $campaign->getCurrentCampaign();
        }
        return $this->_campaign;
    }

    /**
     * @return string
     */
    public function getThemeHtml()
    {
        return $this->getChildHtml($this->getPopup()->getTheme());
    }

    /**
     * @return string
     */
    public function getConfigJs()
    {
        return json_encode($this->getConfig());
    }

    /**
     * @return array
     */
    public function getConfig()
    {

        $popup = $this->getPopup();
        $campaign = $this->getCampaign();

        $config = [
            'translate' => $this->_helper->getTranslation(),
            'actionUrl' => $this->getActionUrl(),
            'layerClose' => $campaign->getLayerClose()?true:false,
            'autoPosition' => $popup->getData('mobile_auto_position'),
            'cookieName' => $campaign->getCookieName(),
            'subscriberCookieName' => $this->_helper->getSubscriberCookieName(),
            'popup_id' => $this->getPopup()->getId(),
            'campaign_id' => $this->getCampaign()->getId(),
            'openSelector' => $this->getPopup()->getButtonSelector(),
            'color1' => $this->getPopup()->getColor(1)
        ];

        if ($popup->getButtonIsActive() && $popup->getButtonOnly() == 1) {
            $config['autoStart'] = false;
        }

        if (is_numeric($campaign->getPopupDelay())) {
            $config['delayTime'] = $campaign->getPopupDelay();
        }

        if (is_numeric($campaign->getCookieLifetime())) {
            $config['cookieLifeTime'] = $campaign->getCookieLifetime();
        }

        return $config;
    }

    /**
     * Returns cookie name
     * @return string
     */
    public function getCookieName()
    {
        $cookiePrefix = $this->_helper->getCookiePrefix();
        return $cookiePrefix;
    }

    /**
     * Returns action url
     * @return string
     */
    public function getActionUrl()
    {
        return str_replace(['http:', 'https:'], '', $this->getUrl('newsletterpopup/subscriber/subscribe'));
    }

    /**
     * @return bool
     */
    public function canShowPopup()
    {
        $campaign = $this->getCampaign();
        if ($this->_cookieManager->getCookie($campaign->getCookieName(), false) &&  !$this->canShowButton()) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function canShowButton()
    {
        if ($this->getPopup()->getButtonIsactive()) {
            return false;
        }

        return true;
    }
}
