<?php
/**
 * Copyright © 2016 MB Vienas bitas. All rights reserved.
 * @website    www.magetrend.com
 * @package    Newsletter Popup Pro from M2
 * @author     Edvinas Stulpinas <edwin@magetrend.com>
 */

namespace Magetrend\NewsletterPopup\Block\Popup;

use Magetrend\NewsletterPopup\Helper;
use Magento\Framework\View\Element\Template;

class Button extends Template
{

    /**
     * @var Helper\Data|null
     */
    protected $_helper = null;

    /**
     * Button constructor.
     * @param Template\Context $context
     * @param Helper\Data $helper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magetrend\NewsletterPopup\Helper\Data $helper,
        array $data = []
    ) {
        $this->_helper = $helper;
        parent::__construct($context, $data);
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->getPopup()->getButtonIsActive();
    }

    /**
     * @return \Magetrend\NewsletterPopup\Model\Popup
     */
    public function getPopup()
    {
        return $this->getParentBlock()->getPopup();
    }

    /**
     * @return bool
     */
    public function canShow()
    {
        return true;
    }
}
