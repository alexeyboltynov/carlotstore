<?php
/**
 * Copyright © 2016 MB Vienas bitas. All rights reserved.
 * @website    www.magetrend.com
 * @package    Newsletter Popup Pro from M2
 * @author     Edvinas Stulpinas <edwin@magetrend.com>
 */

namespace Magetrend\NewsletterPopup\Block\Popup\Field;

class Textarea extends \Magetrend\NewsletterPopup\Block\Popup\Field
{
    protected $_template = 'newsletterpopup/popup/field/textarea.phtml';
}
