<?php
/**
 * Copyright © 2016 MB Vienas bitas. All rights reserved.
 * @website    www.magetrend.com
 * @package    Newsletter Popup Pro from M2
 * @author     Edvinas Stulpinas <edwin@magetrend.com>
 */

namespace Magetrend\NewsletterPopup\Block\Popup\Field;

class Checkbox extends \Magetrend\NewsletterPopup\Block\Popup\Field
{
    protected $_template = 'newsletterpopup/popup/field/checkbox.phtml';

    public function getJsonErrorMessage()
    {
        $data = $this->getData();
        $jsonErrorMessage = [];
        foreach ($data as $key => $value) {
            if (strpos($key, 'error_message_') === false) {
                continue;
            }
            $jsonErrorMessage[str_replace('error_message_', '', $key)] = $value;
        }

        return $this->jsonHelper->jsonEncode($jsonErrorMessage);
    }
}
