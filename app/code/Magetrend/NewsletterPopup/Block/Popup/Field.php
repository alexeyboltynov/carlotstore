<?php
/**
 * Copyright © 2016 MB Vienas bitas. All rights reserved.
 * @website    www.magetrend.com
 * @package    Newsletter Popup Pro from M2
 * @author     Edvinas Stulpinas <edwin@magetrend.com>
 */

namespace Magetrend\NewsletterPopup\Block\Popup;

use Magetrend\NewsletterPopup\Helper;
use \Magento\Framework\View\Element\Template;

class Field extends Template
{
    /**
     * @var Helper\Data|null
     */
    protected $_helper = null;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    public $jsonHelper;

    /**
     * Field constructor.
     * @param Template\Context $context
     * @param Helper\Data $helper
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magetrend\NewsletterPopup\Helper\Data $helper,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        array $data = []
    ) {
        $this->_helper = $helper;
        $this->jsonHelper = $jsonHelper;
        parent::__construct($context, $data);
    }

    /**
     * @return Helper\Data|null
     */
    public function getHelper()
    {
        return $this->_helper;
    }
}
