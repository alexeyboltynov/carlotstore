<?php
/**
 * Copyright © 2016 MB Vienas bitas. All rights reserved.
 * @website    www.magetrend.com
 * @package    Newsletter Popup Pro from M2
 * @author     Edvinas Stulpinas <edwin@magetrend.com>
 */

namespace Magetrend\NewsletterPopup\Block\Popup;

use Magetrend\NewsletterPopup\Helper;
use Magento\Framework\View\Element\Template;

class Theme extends Template
{
    /**
     * @var \Magento\Framework\Locale\CurrencyInterface
     */
    protected $_localeCurrency;

    /**
     * @var array
     */
    protected $_social = [
        [
            'name' => 'facebook',
            'label' => 'Facebook'
        ],
        [
            'name' => 'twitter',
            'label' => 'Twitter'
        ],
        [
            'name' => 'pinterest',
            'label' => 'Pinterest'
        ],
        [
            'name' => 'gplus',
            'label' => 'Google Plus'
        ],
        [
            'name' => 'instagram',
            'label' => 'Instagram'
        ],
        [
            'name' => 'youtube',
            'label' => 'Youtube'
        ],
        [
            'name' => 'tumblr',
            'label' => 'Tumblr'
        ],
        [
            'name' => 'linkedin',
            'label' => 'Linked In'
        ],
    ];

    /**
     * @var Helper\Data|null
     */
    protected $_helper = null;

    /**
     * Theme constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Locale\CurrencyInterface $localeCurrency
     * @param Helper\Data $helper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Locale\CurrencyInterface $localeCurrency,
        \Magetrend\NewsletterPopup\Helper\Data $helper,
        array $data = []
    ) {
        $this->_localeCurrency = $localeCurrency;
        $this->_helper = $helper;
        parent::__construct($context, $data);
    }

    /**
     * @return Helper\Data|null
     */
    public function getHelper()
    {
        return $this->_helper;
    }

    /**
     * @return mixed
     */
    public function getPopup()
    {
        return $this->getParentBlock()->getPopup();
    }

    /**
     * @param $name
     * @return mixed
     */
    public function getSocialLink($name)
    {
        return $this->getPopup()->getData('link_'.$name);
    }

    /**
     * @return array
     */
    public function getSocial()
    {
        $socialList = [];
        foreach ($this->_social as $social) {
            $link = $this->getSocialLink($social['name']);
            if (empty($link)) {
                continue;
            }

            $socialList[] = [
                'title' => __($social['label']),
                'name' => $social['name'],
                'link' => $link,
            ];
        }
        return $socialList;
    }

    /**
     * @param $field
     * @return mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getFieldHtml($field)
    {
        switch ($field['type']) {
            case 'field':
                $blockClass = 'Magetrend\NewsletterPopup\Block\Popup\Field\Text';
                break;
            case 'area':
                $blockClass = 'Magetrend\NewsletterPopup\Block\Popup\Field\Textarea';
                break;
            case 'radio':
                $blockClass = 'Magetrend\NewsletterPopup\Block\Popup\Field\Radio';
                break;
            case 'checkbox':
                $blockClass = 'Magetrend\NewsletterPopup\Block\Popup\Field\Checkbox';
                break;
            case 'drop_down':
                $blockClass = 'Magetrend\NewsletterPopup\Block\Popup\Field\Select';
                break;
            case 'hidden':
                $blockClass = 'Magetrend\NewsletterPopup\Block\Popup\Field\Hidden';
                break;
        }

        $block = $this->getLayout()->createBlock($blockClass)->setData($field);
        return $block->toHtml();
    }

    /**
     * @return mixed
     */
    public function getAdditionalFields()
    {
        return $this->getPopup()->getAdditionalFields();
    }

    public function isOnlyOneVisibleCheckbox()
    {
        $additionalFields = $this->getAdditionalFields();
        if (empty($additionalFields)) {
            return false;
        }

        $counter = 0;
        foreach ($additionalFields as $field) {
            if ($field['type'] == 'hidden') {
                continue;
            }
            if ($field['type'] == 'checkbox') {
                $counter++;
                continue;
            }

            return false;
        }

        return $counter == 1;
    }

    /**
     * @return mixed
     */
    public function getCurrencySymbol()
    {
        return $this->_storeManager->getStore()->getCurrentCurrency()->getCurrencySymbol();
    }

    /**
     * Returns popup text
     * @param int $id
     */
    public function getTextHtml($id)
    {
        return $this->getPopup()->getData('text_'.$id);
    }

    /**
     * Returns image path
     * @param int $id
     */
    public function getColor($id)
    {
        return $this->getPopup()->getData('color_'.$id);
    }

    public function getImage($key)
    {
        $fileName = $this->getPopup()->getData($key);
        $img = $this->_storeManager->getStore()
            ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).$fileName;
        if (empty($fileName)) {
            $img = $this->_assetRepo->createAsset('Magetrend_NewsletterPopup::images/300x450.png')->getUrl();
        }

        return $img;
    }
}
