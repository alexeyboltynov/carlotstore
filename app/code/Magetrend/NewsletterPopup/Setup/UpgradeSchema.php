<?php
/**
 * MB "Vienas bitas" (Magetrend.com)
 *
 * @category MageTrend
 * @package  Magetend/NewsletterPopup
 * @author   Edvinas Stulpinas <edwin@magetrend.com>
 * @license  http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link     http://www.magetrend.com/magento-2-newsletter-popup-extension
 */
namespace Magetrend\NewsletterPopup\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        if (version_compare($context->getVersion(), '2.0.1') < 0) {
            $this->update201($setup);
        }

        if (version_compare($context->getVersion(), '2.0.2') < 0) {
            $this->update202($setup);
        }

        if (version_compare($context->getVersion(), '2.0.3') < 0) {
            $this->update203($setup);
        }

        if (version_compare($context->getVersion(), '2.0.4') < 0) {
            $this->update204($setup);
        }

        if (version_compare($context->getVersion(), '2.0.5') < 0) {
            $this->update205($setup);
        }

        if (version_compare($context->getVersion(), '2.0.7') < 0) {
            $this->update207($setup);
        }

        if (version_compare($context->getVersion(), '2.0.8') < 0) {
            $this->update208($setup);
        }

        $setup->endSetup();
    }

    public function update201($setup)
    {
        $tableName = $setup->getTable('mt_np_popup');
        if ($setup->getConnection()->isTableExists($tableName) == true) {
            $columns = [
                'followup_is_active' => [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'length'=> 1,
                    'nullable' => false,
                    'comment' => 'Is Follow Up Email Active',
                ],

                'followup_delay' => [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'length'=> 10,
                    'nullable' => false,
                    'comment' => 'Email Delay',
                ],

                'followup_email_template' => [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length'=> 255,
                    'nullable' => false,
                    'comment' => 'Follow Up Email Template',
                ],

                'followup_sender_name' => [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length'=> 255,
                    'nullable' => false,
                    'comment' => 'Follow Up Sender Name',
                ],

                'followup_sender_email' => [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length'=> 255,
                    'nullable' => false,
                    'comment' => 'Follow Up Sender Email',
                ],
            ];

            $connection = $setup->getConnection();
            foreach ($columns as $name => $definition) {
                $connection->addColumn($tableName, $name, $definition);
            }
        }

        $tableName = $setup->getTable('newsletter_subscriber');
        if ($setup->getConnection()->isTableExists($tableName) == true) {
            $columns = [
                'np_followup_status' => [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'length'=> 1,
                    'nullable' => false,
                    'comment' => 'Follow Up Message Status',
                ],

                'np_followup_date' => [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                    'length'=> null,
                    'nullable' => false,
                    'comment' => 'Follow up date',
                ],

                'np_popup_id' => [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'length'=> 10,
                    'nullable' => false,
                    'comment' => 'Popup Id',
                ],
            ];

            $connection = $setup->getConnection();
            foreach ($columns as $name => $definition) {
                $connection->addColumn($tableName, $name, $definition);
            }
        }
    }

    public function update202($setup)
    {
        $tableName = $setup->getTable('mt_np_popup');
        if ($setup->getConnection()->isTableExists($tableName) == true) {
            $columns = [
                'apply_to_cart' => [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'length'=> 1,
                    'nullable' => false,
                    'comment' => 'Apply to cart automatically',
                ],
            ];

            $connection = $setup->getConnection();
            foreach ($columns as $name => $definition) {
                $connection->addColumn($tableName, $name, $definition);
            }
        }
    }

    public function update203($setup)
    {
        $tableName = $setup->getTable('mt_np_popup');
        if ($setup->getConnection()->isTableExists($tableName) == true) {
            $columns = [
                'bg_1' => [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length'=> 255,
                    'nullable' => true,
                    'comment' => 'Background Image',
                ],
            ];

            $connection = $setup->getConnection();
            foreach ($columns as $name => $definition) {
                $connection->addColumn($tableName, $name, $definition);
            }
        }
    }



    public function update204($setup)
    {
        $tableName = $setup->getTable('mt_np_popup');
        if ($setup->getConnection()->isTableExists($tableName) == true) {
            $columns = [
                'color_3' => [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length'=> 7,
                    'nullable' => true,
                    'comment' => 'Color Code',
                ],
                'color_4' => [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length'=> 7,
                    'nullable' => true,
                    'comment' => 'Color Code',
                ],
                'color_5' => [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length'=> 7,
                    'nullable' => true,
                    'comment' => 'Color Code',
                ],
            ];

            $connection = $setup->getConnection();
            foreach ($columns as $name => $definition) {
                $connection->addColumn($tableName, $name, $definition);
            }
        }
    }

    public function update205($setup)
    {
        $tableName = $setup->getTable('mt_np_popup');
        if ($setup->getConnection()->isTableExists($tableName) == true) {
            $connection = $setup->getConnection();
            $connection->addColumn($tableName, 'coupon_expire_in_days', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                'length'=> '10,2',
                'nullable' => true,
                'comment' => 'Expire in Days',
            ]);
        }
    }

    public function update207($setup)
    {
        $tableName = $setup->getTable('mt_np_campaign');
        if ($setup->getConnection()->isTableExists($tableName) == true) {
            $columns = [
                'method' => [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length'=> 255,
                    'nullable' => true,
                    'comment' => 'Way to show popup',
                ],
            ];

            $connection = $setup->getConnection();
            foreach ($columns as $name => $definition) {
                $connection->addColumn($tableName, $name, $definition);
            }
        }
    }

    public function update208($setup)
    {
        $tableName = $setup->getTable('mt_np_field');
        if ($setup->getConnection()->isTableExists($tableName) == true) {
            $connection = $setup->getConnection();
            $connection->addColumn($tableName, 'default_value', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Default field value',
            ]);

            $connection->addColumn($tableName, 'frontend_label', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Frontend Label',
            ]);

            $connection->addColumn($tableName, 'error_message', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Error Messages',
            ]);
        }

        $tableName = $setup->getTable('newsletter_subscriber');
        if ($setup->getConnection()->isTableExists($tableName) == true) {
            $connection = $setup->getConnection();
            $connection->addColumn($tableName, 'created_at', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                'nullable' => true,
                'comment' => 'Created At',
            ]);
        }
    }
}