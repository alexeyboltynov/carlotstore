var config = {
    config: {
        mixins: {
            'Magento_Checkout/js/action/place-order': {
                'Mageants_FrontOrderComment/js/place-order-with-comments-mixin': true
            }
        }
    },
	"map": {
    "*": {
      "Magento_Sales/email/order_new.html": 
          "Mageants_FrontOrderComment/email/order_new.html"
    }
  }
};
