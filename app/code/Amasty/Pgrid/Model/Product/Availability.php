<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Pgrid
 */

namespace Amasty\Pgrid\Model\Product;

use Magento\Framework\Data\OptionSourceInterface;
use Magento\Framework\DataObject;
use Magento\Framework\Data\Collection;

class Availability extends DataObject implements OptionSourceInterface
{
    const DISABLE_MANAGE_STOCK = 2;
    const IN_STOCK = 1;
    const OUT_OF_STOCK = 0;

    public static function getOptionArray()
    {
        return [
            self::DISABLE_MANAGE_STOCK => __('Manage Stock Disabled'),
            self::IN_STOCK => __('In Stock'),
            self::OUT_OF_STOCK => __('Out Of Stock')
        ];
    }

    public static function getAllOptions()
    {
        $res = [];
        foreach (self::getOptionArray() as $index => $value) {
            $res[] = ['value' => $index, 'label' => $value];
        }
        return $res;
    }

    public function toOptionArray()
    {
        return $this->getAllOptions();
    }

    /**
     * Added filter by use_config_manage_stock to collection
     *
     * @param \Magento\Framework\Data\Collection $collection
     * @param string $field
     * @param array $condition
     */
    public function addFilter(Collection $collection, $field = null, $condition = null)
    {
        if ($collection->getFlag('amasty_instock_filter')) {
            return;
        }

        $collection->joinField(
            'amasty_availability',
            'cataloginventory_stock_item',
            'IF(at_amasty_availability.manage_stock = 1, at_amasty_availability.is_in_stock, '
            . self::DISABLE_MANAGE_STOCK . ')',
            'product_id=entity_id',
            '{{table}}.stock_id=1',
            'left'
        );

        if ($condition['eq'] == self::DISABLE_MANAGE_STOCK) {
            $collection->getSelect()->where('at_amasty_availability.use_config_manage_stock = 0');
        } else {
            $collection->getSelect()
                ->where('at_amasty_availability.is_in_stock = ?', $condition['eq'])
                ->where('at_amasty_availability.use_config_manage_stock = ?', 1);
        }

        $collection->setFlag('amasty_instock_filter', 1);
    }
}
