<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */


namespace Amasty\Faq\Block\Adminhtml\System\Config;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Module\Manager;

class SEOToolkit extends Field
{
    /**
     * @var Manager
     */
    private $manager;

    public function __construct(
        Manager $manager,
        \Magento\Backend\Block\Template\Context $context,
        array $data = []
    ) {
        $this->manager = $manager;
        parent::__construct($context, $data);
    }

    public function render(AbstractElement $element)
    {
        if ($this->manager->isEnabled('Amasty_SeoToolKit')) {
            $element->setValue(__('Installed'));
            $element->setHtmlId('amasty_is_instaled');
            $url = $this->getUrl('adminhtml/system_config/edit', ['section' => 'amseotoolkit']);
            $element->setComment(__('Specify SEO Toolkit settings properly '
                . '<a href="%1" target="_blank">See more details here</a>', $url
            ));
        } else {
            $element->setValue(__('Not Installed'));
            $element->setHtmlId('amasty_not_instaled');
            $element->setComment(__(' SEO Toolkit is the all-in-one Magento 2 SEO Extension designed to make your '
                . 'store stand out in Google Search. Maximize your Magento SEO effectiveness: '
                . 'make your store more visible in search results, get more organic traffic, '
                . 'increase your customer flow. '
                . 'See more details <a href="https://amasty.com/magento-seo-toolkit.html'
                . '?utm_source=extension&utm_medium=backend&utm_campaign=from_faq_to_seotoolkit_m2" target="_blank">here.</a>'
            ));
        }

        return parent::render($element);
    }
}
