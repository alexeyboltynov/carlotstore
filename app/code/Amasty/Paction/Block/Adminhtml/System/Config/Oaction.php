<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_Paction
 */


namespace Amasty\Paction\Block\Adminhtml\System\Config;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Module\Manager;

class Oaction extends Field
{
    /**
     * @var Magento\Framework\Module\Manager
     */
    private $manager;

    public function __construct(
        Manager $manager,
        \Magento\Backend\Block\Template\Context $context,
        array $data = []
    ) {
        $this->manager = $manager;
        parent::__construct($context, $data);
    }

    public function render(AbstractElement $element)
    {
        if ($this->manager->isEnabled('Amasty_Oaction')) {
            $element->setValue(__('Installed'));
            $element->setHtmlId('amasty_is_instaled');
            $url = $this->getUrl('adminhtml/system_config/edit', ['section' => 'amasty_oaction']);
            $element->setComment(__('Specify Mass Order Actions settings properly. See more details '
                . '<a href="%1" target="_blank">here</a>',
                $url
            ));
        } else {
            $element->setValue(__('Not Installed'));
            $element->setHtmlId('amasty_not_instaled');
            $element->setComment(__('Expand default order grid with new options.'
                    . ' Easily send invoices, submit shipments and edit tracking numbers right from grid just in a few clicks.'
                    . ' Customize grid view according to your needs and make order processing easy and fast. '
                    . ' See more details <a target="_blank" href="https://amasty.com/mass-order-actions-for-magento-2.html?utm_source=extension&amp;utm_medium=backend&amp;utm_campaign=m2_product_actions_to_order_actions">here</a>')
            );
        }

        return parent::render($element);
    }
}
