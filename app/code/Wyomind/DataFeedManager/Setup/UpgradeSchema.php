<?php
/*
 * Copyright © 2015 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\DataFeedManager\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{

    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {

        // $context->getVersion() = version du module actuelle
        // 9.0.0 = version en cours d'installation
        if (version_compare($context->getVersion(), '11.6.0') < 0) {
            $installer = $setup;
            $installer->startSetup();

            $installer->getConnection()->addColumn(
                $installer->getTable('datafeedmanager_feeds'),
                'ftp_ssl',
                ['type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, 'default' => '0', 'length' => 1, "comment" => "Enable SSL"]
            );

            $installer->endSetup();
        }
    }
}
