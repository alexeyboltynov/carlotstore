<?php
/**
 * Ecomteck
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Ecomteck.com license that is
 * available through the world-wide-web at this URL:
 * https://ecomteck.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Ecomteck
 * @package     Ecomteck_AutoRelatedProducts
 * @copyright   Copyright (c) 2018 Ecomteck (https://ecomteck.com/)
 * @license     https://ecomteck.com/LICENSE.txt
 */
namespace Ecomteck\AutoRelatedProducts\Controller\Adminhtml\Autorelatedproducts;

class Save extends \Ecomteck\AutoRelatedProducts\Controller\Adminhtml\Autorelatedproducts
{
    /**
     * Save action
     *
     * @return void
     */
    public function execute()
    {
        $redirectPath = '*/*/';
        $redirectParams = [];

        $data = $this->getRequest()->getPostValue();

        if ($this->getRequest()->isPost() && $data) {
            /* @var $model \Ecomteck\AutoRelatedProducts\Model\Rule */
            $model = $this->_objectManager->create('Ecomteck\AutoRelatedProducts\Model\Rule');

            try {

                $inputFilter = new \Zend_Filter_Input(
                    ['from_date' => $this->_dateFilter, 'to_date' => $this->_dateFilter],
                    [],
                    $data
                );
                $data = $inputFilter->getUnescaped();
                $ruleId = $this->getRequest()->getParam('rule_id');
                $errors = $this->validateData($data, $model, $ruleId);

                if (empty($errors)) {
                    $data['conditions'] = $data['rule']['conditions'];
                    $data['actions'] = $data['rule']['actions'];
                    unset($data['rule']);

                    $model->loadPost($data);
                    $model->save();

                    $this->messageManager->addSuccess(__('You saved the rule.'));

                    if ($this->getRequest()->getParam('back', false)) {
                        $redirectPath = 'adminhtml/*/edit';
                        $redirectParams = ['id' => $model->getId(), '_current' => true];
                    }
                }
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('We can\'t save the product rule right now.'));
                $errors[] = $e->getMessage();
            }
            if (!empty($errors)) {
                foreach ($errors as $errorMessage) {
                    $this->messageManager->addError($errorMessage);
                }
                $this->_getSession()->setFormData($data);
                $redirectPath = 'adminhtml/*/edit';
                $redirectParams = ['id' => $this->getRequest()->getParam('rule_id')];
            }
        }
        $this->_redirect($redirectPath, $redirectParams);
    }

    /**
     * Validate data
     *
     * @param array $data
     * @param \Ecomteck\AutoRelatedProducts\Model\Rule $model
     * @param int $ruleId
     *
     * @return array
     */
    public function validateData($data, $model, $ruleId)
    {
        $errors = [];

        if ($ruleId) {
            $model->load($ruleId);
            if ($ruleId != $model->getId()) {
                $errors[] = __('Please specify a correct rule.')->getText();
                return $errors;
            }
        }
        $validateResult = $model->validateData(new \Magento\Framework\DataObject($data));
        if ($validateResult !== true) {
            foreach ($validateResult as $errorMessage) {
                $errors[] = $errorMessage;
            }
        }
        return $errors;
    }
}
