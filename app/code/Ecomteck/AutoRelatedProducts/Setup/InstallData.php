<?php
/**
 * Ecomteck
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Ecomteck.com license that is
 * available through the world-wide-web at this URL:
 * https://ecomteck.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Ecomteck
 * @package     Ecomteck_AutoRelatedProducts
 * @copyright   Copyright (c) 2018 Ecomteck (https://ecomteck.com/)
 * @license     https://ecomteck.com/LICENSE.txt
 */

namespace Ecomteck\AutoRelatedProducts\Setup;

use Magento\Catalog\Model\ResourceModel\Eav\Attribute;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Setup\EavSetupFactory;

/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{
    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * Constructor
     * 
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }
    
    /**
     * {@inheritdoc}
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        /** @var \Magento\Eav\Setup\EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        // add config attributes to catalog product
        $eavSetup->addAttribute(
            'catalog_product',
            'related_tgtr_position_limit',
            [
                'label' => 'Related Auto Related Products Rule Based Positions',
                'visible' => false,
                'user_defined' => false,
                'required' => false,
                'type' => 'int',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'input' => 'text',
                'backend' => 'Ecomteck\AutoRelatedProducts\Model\Catalog\Product\Attribute\Backend\Rule'
            ]
        );

        $eavSetup->addAttribute(
            'catalog_product',
            'related_tgtr_position_behavior',
            [
                'label' => 'Related Auto Related Products Position Behavior',
                'visible' => false,
                'user_defined' => false,
                'required' => false,
                'type' => 'int',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'input' => 'text',
                'backend' => 'Ecomteck\AutoRelatedProducts\Model\Catalog\Product\Attribute\Backend\Rule'
            ]
        );

        $eavSetup->addAttribute(
            'catalog_product',
            'upsell_tgtr_position_limit',
            [
                'label' => 'Upsell Auto Related Products Rule Based Positions',
                'visible' => false,
                'user_defined' => false,
                'required' => false,
                'type' => 'int',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'input' => 'text',
                'backend' => 'Ecomteck\AutoRelatedProducts\Model\Catalog\Product\Attribute\Backend\Rule'
            ]
        );

        $eavSetup->addAttribute(
            'catalog_product',
            'upsell_tgtr_position_behavior',
            [
                'label' => 'Upsell Auto Related Products Position Behavior',
                'visible' => false,
                'user_defined' => false,
                'required' => false,
                'type' => 'int',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'input' => 'text',
                'backend' => 'Ecomteck\AutoRelatedProducts\Model\Catalog\Product\Attribute\Backend\Rule'
            ]
        );

    }
}
