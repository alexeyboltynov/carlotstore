<?php
/**
 * Ecomteck
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Ecomteck.com license that is
 * available through the world-wide-web at this URL:
 * https://ecomteck.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Ecomteck
 * @package     Ecomteck_AutoRelatedProducts
 * @copyright   Copyright (c) 2018 Ecomteck (https://ecomteck.com/)
 * @license     https://ecomteck.com/LICENSE.txt
 */
namespace Ecomteck\AutoRelatedProducts\Block\Product;

/**
 * AutoRelatedProducts abstract Products Block
 *
 */
abstract class AbstractProduct extends \Magento\Catalog\Block\Product\AbstractProduct
{
    /**
     * Link collection
     *
     * @var null|\Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    protected $_linkCollection = null;

    /**
     * Catalog Product List Item Collection array
     *
     * @var null|array
     */
    protected $_items = null;

    /**
     * Get link collection for specific target
     *
     * @abstract
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    abstract protected function _getTargetLinkCollection();

    /**
     * Get target rule products
     *
     * @abstract
     * @return array
     */
    abstract protected function _getAutoRelatedProductsProducts();

    /**
     * Retrieve Catalog Product List Type identifier
     *
     * @return int
     */
    abstract public function getProductListType();

    /**
     * Retrieve Maximum Number Of Product
     *
     * @return int
     */
    abstract public function getPositionLimit();

    /**
     * Retrieve Position Behavior
     *
     * @return int
     */
    abstract public function getPositionBehavior();

    /**
     * Target rule data
     *
     * @var \Ecomteck\AutoRelatedProducts\Helper\Data
     */
    protected $_autoRelatedProductsData = null;

    /**
     * @var \Ecomteck\AutoRelatedProducts\Model\ResourceModel\Index
     */
    protected $_resourceIndex;

    /**
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Ecomteck\AutoRelatedProducts\Model\ResourceModel\Index $index
     * @param \Ecomteck\AutoRelatedProducts\Helper\Data $autoRelatedProductsData
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Ecomteck\AutoRelatedProducts\Model\ResourceModel\Index $index,
        \Ecomteck\AutoRelatedProducts\Helper\Data $autoRelatedProductsData,
        array $data = []
    ) {
        $this->_resourceIndex = $index;
        $this->_autoRelatedProductsData = $autoRelatedProductsData;
        parent::__construct(
            $context,
            $data
        );
    }

    /**
     * Return the behavior positions applicable to products based on the rule(s)
     *
     * @return int[]
     */
    public function getRuleBasedBehaviorPositions()
    {
        return [
            \Ecomteck\AutoRelatedProducts\Model\Rule::BOTH_SELECTED_AND_RULE_BASED,
            \Ecomteck\AutoRelatedProducts\Model\Rule::RULE_BASED_ONLY
        ];
    }

    /**
     * Retrieve the behavior positions applicable to selected products
     *
     * @return int[]
     */
    public function getSelectedBehaviorPositions()
    {
        return [
            \Ecomteck\AutoRelatedProducts\Model\Rule::BOTH_SELECTED_AND_RULE_BASED,
            \Ecomteck\AutoRelatedProducts\Model\Rule::SELECTED_ONLY
        ];
    }

    /**
     * Get link collection
     *
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection|null
     */
    public function getLinkCollection()
    {
        if ($this->_linkCollection === null) {
            $this->_linkCollection = $this->_getTargetLinkCollection();

            if ($this->_linkCollection) {
                // Perform rotation mode
                $select = $this->_linkCollection->getSelect();
                $rotationMode = $this->_autoRelatedProductsData->getRotationMode($this->getProductListType());
                if ($rotationMode == \Ecomteck\AutoRelatedProducts\Model\Rule::ROTATION_SHUFFLE) {
                    $this->_resourceIndex->orderRand($select);
                } else {
                    $select->order('link_attribute_position_int.value ASC');
                }
            }
        }

        return $this->_linkCollection;
    }

    /**
     * Get linked products
     *
     * @return array
     */
    protected function _getLinkProducts()
    {
        $items = [];
        $linkCollection = $this->getLinkCollection();
        if ($linkCollection) {
            foreach ($linkCollection as $item) {
                $items[$item->getEntityId()] = $item;
            }
        }
        return $items;
    }

    /**
     * Whether rotation mode is set to "shuffle"
     *
     * @return bool
     */
    public function isShuffled()
    {
        $rotationMode = $this->_autoRelatedProductsData->getRotationMode($this->getProductListType());
        return $rotationMode == \Ecomteck\AutoRelatedProducts\Model\Rule::ROTATION_SHUFFLE;
    }

    /**
     * Order product items
     *
     * @param array $items
     * @return array
     */
    protected function _orderProductItems(array $items)
    {
        if ($this->isShuffled()) {
            // shuffling assoc
            $ids = array_keys($items);
            shuffle($ids);
            $result = [];
            foreach ($ids as $id) {
                $result[$id] = $items[$id];
            }
            return $result;
        } else {
            uasort($items, [$this, 'compareItems']);
            return $items;
        }
    }

    /**
     * Compare two items for ordered list
     *
     * @param \Magento\Framework\DataObject $item1
     * @param \Magento\Framework\DataObject $item2
     * @return int
     */
    public function compareItems($item1, $item2)
    {
        // Prevent rule-based items to have any position
        if ($item2->getPosition() === null && $item1->getPosition() !== null) {
            return -1;
        } elseif ($item1->getPosition() === null && $item2->getPosition() !== null) {
            return 1;
        }
        $positionDiff = (int)$item1->getPosition() - (int)$item2->getPosition();
        if ($positionDiff != 0) {
            return $positionDiff;
        }
        return (int)$item1->getEntityId() - (int)$item2->getEntityId();
    }

    /**
     * Slice items to limit
     *
     * @return $this
     */
    protected function _sliceItems()
    {
        if ($this->_items !== null) {
            if ($this->isShuffled()) {
                $this->_items = array_slice($this->_items, 0, $this->_autoRelatedProductsData->getMaxProductsListResult(), true);
            } else {
                $this->_items = array_slice($this->_items, 0, $this->getPositionLimit(), true);
            }
        }
        return $this;
    }

    /**
     * Retrieve Catalog Product List Items
     *
     * @return array
     */
    public function getItemCollection()
    {
        if ($this->_items === null) {
            $behavior = $this->getPositionBehavior();

            $this->_items = [];

            if (in_array($behavior, $this->getSelectedBehaviorPositions())) {
                $this->_items = $this->_orderProductItems($this->_getLinkProducts());

            }

            if (in_array($behavior, $this->getRuleBasedBehaviorPositions())) {
                foreach ($this->_orderProductItems($this->_getAutoRelatedProductsProducts()) as $id => $item) {
                    $this->_items[$id] = $item;
                }
            }
            $this->_sliceItems();
        }

        return $this->_items;
    }
}
