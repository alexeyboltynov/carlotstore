<?php
/**
 * Ecomteck
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Ecomteck.com license that is
 * available through the world-wide-web at this URL:
 * https://ecomteck.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Ecomteck
 * @package     Ecomteck_AutoRelatedProducts
 * @copyright   Copyright (c) 2018 Ecomteck (https://ecomteck.com/)
 * @license     https://ecomteck.com/LICENSE.txt
 */
namespace Ecomteck\AutoRelatedProducts\Block\Adminhtml\Autorelatedproducts\Edit\Tab;

/**
 * AutoRelatedProducts Adminhtml Edit Tab Actions Block
 *
 */
class Actions extends \Magento\Backend\Block\Widget\Form\Generic implements
    \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * @var \Magento\Backend\Block\Widget\Form\Renderer\Fieldset
     */
    protected $_fieldset;

    /**
     * @var \Ecomteck\AutoRelatedProducts\Block\Adminhtml\Actions\Conditions
     */
    protected $_conditions;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Ecomteck\AutoRelatedProducts\Block\Adminhtml\Actions\Conditions $conditions
     * @param \Magento\Backend\Block\Widget\Form\Renderer\Fieldset $fieldset
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Ecomteck\AutoRelatedProducts\Block\Adminhtml\Actions\Conditions $conditions,
        \Magento\Backend\Block\Widget\Form\Renderer\Fieldset $fieldset,
        array $data = []
    ) {
        $this->_conditions = $conditions;
        $this->_fieldset = $fieldset;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare target rule actions form before rendering HTML
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        /* @var $model \Ecomteck\AutoRelatedProducts\Model\Rule */
        $model = $this->_coreRegistry->registry('current_target_rule');
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('rule_');

        $fieldset = $form->addFieldset(
            'actions_fieldset',
            ['legend' => __('Product Result Conditions (leave blank for matching all products)')]
        );
        $newCondUrl = $this->getUrl('adminhtml/autorelatedproducts/newActionsHtml/', ['form' => $fieldset->getHtmlId()]);
        $renderer = $this->_fieldset->setTemplate(
            'Ecomteck_AutoRelatedProducts::edit/conditions/fieldset.phtml'
        )->setNewChildUrl(
            $newCondUrl
        );
        $fieldset->setRenderer($renderer);

        $element = $fieldset->addField('actions', 'text', ['name' => 'actions', 'required' => true]);
        $element->setRule($model);
        $element->setRenderer($this->_conditions);

        $model->getActions()->setJsFormObject($fieldset->getHtmlId());
        $form->setValues($model->getData());

        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Retrieve Tab label
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Products to Display');
    }

    /**
     * Retrieve Tab title
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Products to Display');
    }

    /**
     * Check is can show tab
     *
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Check tab is hidden
     *
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }
}
