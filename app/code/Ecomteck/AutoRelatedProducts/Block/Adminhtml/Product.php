<?php
/**
 * Ecomteck
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Ecomteck.com license that is
 * available through the world-wide-web at this URL:
 * https://ecomteck.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Ecomteck
 * @package     Ecomteck_AutoRelatedProducts
 * @copyright   Copyright (c) 2018 Ecomteck (https://ecomteck.com/)
 * @license     https://ecomteck.com/LICENSE.txt
 */
namespace Ecomteck\AutoRelatedProducts\Block\Adminhtml;

class Product extends \Magento\Backend\Block\Widget
{
    /**
     * Attributes is read only flag
     *
     * @var bool
     */
    protected $_readOnly = false;

    /**
     * Target rule data
     *
     * @var \Ecomteck\AutoRelatedProducts\Helper\Data
     */
    protected $_autoRelatedProductsData = null;

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var \Ecomteck\AutoRelatedProducts\Model\Source\Position
     */
    protected $_position;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Ecomteck\AutoRelatedProducts\Model\Source\Position $position
     * @param \Ecomteck\AutoRelatedProducts\Helper\Data $autoRelatedProductsData
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Ecomteck\AutoRelatedProducts\Model\Source\Position $position,
        \Ecomteck\AutoRelatedProducts\Helper\Data $autoRelatedProductsData,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->_position = $position;
        $this->_coreRegistry = $registry;
        $this->_autoRelatedProductsData = $autoRelatedProductsData;
        parent::__construct($context, $data);
    }

    /**
     * Retrieve Product List Type by current Form Prefix
     *
     * @return string|int
     */
    protected function _getProductListType()
    {
        $listType = '';
        switch ($this->getFormPrefix()) {
            case 'related':
                $listType = \Ecomteck\AutoRelatedProducts\Model\Rule::RELATED_PRODUCTS;
                break;
            case 'upsell':
                $listType = \Ecomteck\AutoRelatedProducts\Model\Rule::UP_SELLS;
                break;
        }
        return $listType;
    }

    /**
     * Retrieve current edit product instance
     *
     * @return \Magento\Catalog\Model\Product
     */
    public function getProduct()
    {
        return $this->_coreRegistry->registry('current_product');
    }

    /**
     * Get data for Position Behavior selector
     *
     * @return array
     */
    public function getPositionBehaviorOptions()
    {
        return $this->_position->toOptionArray();
    }

    /**
     * Get value of Rule Based Positions
     *
     * @return mixed
     */
    public function getPositionLimit()
    {
        $position = $this->_getValue('position_limit');
        if ($position === null) {
            $position = $this->_autoRelatedProductsData->getMaximumNumberOfProduct($this->_getProductListType());
        }
        return $position;
    }

    /**
     * Get value of Position Behavior
     *
     * @return mixed
     */
    public function getPositionBehavior()
    {
        $show = $this->_getValue('position_behavior');
        if ($show === null) {
            $show = $this->_autoRelatedProductsData->getShowProducts($this->_getProductListType());
        }
        return $show;
    }

    /**
     * Get value from Product model
     *
     * @param string $field
     * @return mixed
     */
    protected function _getValue($field)
    {
        return $this->getProduct()->getDataUsingMethod($this->getFieldName($field));
    }

    /**
     * Get name of the field
     *
     * @param string $field
     * @return string
     */
    public function getFieldName($field)
    {
        return $this->getFormPrefix() . '_tgtr_' . $field;
    }

    /**
     * Define is value should me marked as default
     *
     * @param string $value
     * @return bool
     */
    public function isDefault($value)
    {
        return $this->_getValue($value) === null ? true : false;
    }

    /**
     * Set AutoRelatedProducts Attributes is ReadOnly
     *
     * @param bool $flag
     * @return \Ecomteck\AutoRelatedProducts\Block\Adminhtml\Product
     */
    public function setIsReadonly($flag)
    {
        return $this->setData('is_readonly', (bool)$flag);
    }

    /**
     * Retrieve AutoRelatedProducts Attributes is ReadOnly flag
     * Default return false if does not exists any instruction
     *
     * @return bool
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getIsReadonly()
    {
        $flag = $this->_getData('is_readonly');
        if ($flag === null) {
            $flag = false;
        }
        return $flag;
    }

    /**
     * Get is single store mode
     *
     * @return bool
     */
    public function isSingleStoreMode()
    {
        return $this->_storeManager->isSingleStoreMode();
    }
}
