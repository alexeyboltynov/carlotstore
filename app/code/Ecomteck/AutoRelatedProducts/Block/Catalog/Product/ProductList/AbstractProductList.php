<?php
/**
 * Ecomteck
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Ecomteck.com license that is
 * available through the world-wide-web at this URL:
 * https://ecomteck.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Ecomteck
 * @package     Ecomteck_AutoRelatedProducts
 * @copyright   Copyright (c) 2018 Ecomteck (https://ecomteck.com/)
 * @license     https://ecomteck.com/LICENSE.txt
 */
namespace Ecomteck\AutoRelatedProducts\Block\Catalog\Product\ProductList;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\DataObject\IdentityInterface;
use Ecomteck\AutoRelatedProducts\Block\Product\AbstractProduct;

/**
 * AutoRelatedProducts Catalog Product List Abstract Block
 *
 * @SuppressWarnings(PHPMD.LongVariable)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
abstract class AbstractProductList extends AbstractProduct implements IdentityInterface
{
    /**
     * AutoRelatedProducts Index instance
     *
     * @var \Ecomteck\AutoRelatedProducts\Model\Index
     */
    protected $_currentIndex;

    /**
     * Array of exclude Product Ids
     *
     * @var array
     */
    protected $_excludeProductIds;

    /**
     * Array of all product ids in list
     *
     * @var null|array
     */
    protected $_allProductIds = null;

    /**
     * @var \Ecomteck\AutoRelatedProducts\Model\IndexFactory
     */
    protected $_indexFactory;

    /**
     * @var \Magento\Catalog\Model\Product\Visibility
     */
    protected $_visibility;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $_productCollectionFactory;

    /**
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Ecomteck\AutoRelatedProducts\Model\ResourceModel\Index $index
     * @param \Ecomteck\AutoRelatedProducts\Helper\Data $autoRelatedProductsData
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
     * @param \Magento\Catalog\Model\Product\Visibility $visibility
     * @param \Ecomteck\AutoRelatedProducts\Model\IndexFactory $indexFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Ecomteck\AutoRelatedProducts\Model\ResourceModel\Index $index,
        \Ecomteck\AutoRelatedProducts\Helper\Data $autoRelatedProductsData,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Model\Product\Visibility $visibility,
        \Ecomteck\AutoRelatedProducts\Model\IndexFactory $indexFactory,
        array $data = []
    ) {
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_visibility = $visibility;
        $this->_indexFactory = $indexFactory;
        parent::__construct(
            $context,
            $index,
            $autoRelatedProductsData,
            $data
        );
    }

    /**
     * Retrieve current product instance (if actual and available)
     *
     * @return \Magento\Catalog\Model\Product
     */
    public function getProduct()
    {
        return $this->_coreRegistry->registry('product');
    }

    /**
     * Retrieve Catalog Product List Type prefix
     * without last underscore
     *
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _getTypePrefix()
    {
        switch ($this->getProductListType()) {
            case \Ecomteck\AutoRelatedProducts\Model\Rule::RELATED_PRODUCTS:
                $prefix = 'related';
                break;

            case \Ecomteck\AutoRelatedProducts\Model\Rule::UP_SELLS:
                $prefix = 'upsell';
                break;

            default:
                throw new LocalizedException(__('Undefined Catalog Product List Type'));
                break;
        }
        return $prefix;
    }

    /**
     * Retrieve Auto Related Products Index instance
     *
     * @return \Ecomteck\AutoRelatedProducts\Model\Index
     */
    protected function _getAutoRelatedProductsIndex()
    {
        if ($this->_currentIndex === null) {
            $this->_currentIndex = $this->_indexFactory->create();
        }
        return $this->_currentIndex;
    }

    /**
     * Retrieve position limit product attribute name
     *
     * @return string
     */
    protected function _getPositionLimitField()
    {
        return sprintf('%s_tgtr_position_limit', $this->_getTypePrefix());
    }

    /**
     * Retrieve position behavior product attribute name
     *
     * @return string
     */
    protected function _getPositionBehaviorField()
    {
        return sprintf('%s_tgtr_position_behavior', $this->_getTypePrefix());
    }

    /**
     * Retrieve Maximum Number Of Product
     *
     * @return int
     */
    public function getPositionLimit()
    {
        $limit = $this->getProduct()->getData($this->_getPositionLimitField());
        if ($limit === null) {
            // use configuration settings
            $limit = $this->_autoRelatedProductsData->getMaximumNumberOfProduct($this->getProductListType());
            $this->getProduct()->setData($this->_getPositionLimitField(), $limit);
        }
        return $this->_autoRelatedProductsData->getMaxProductsListResult($limit);
    }

    /**
     * Retrieve Position Behavior
     *
     * @return int
     */
    public function getPositionBehavior()
    {
        $behavior = $this->getProduct()->getData($this->_getPositionBehaviorField());
        if ($behavior === null) {
            // use configuration settings
            $behavior = $this->_autoRelatedProductsData->getShowProducts($this->getProductListType());
            $this->getProduct()->setData($this->_getPositionBehaviorField(), $behavior);
        }
        return $behavior;
    }

    /**
     * Retrieve array of exclude product ids
     *
     * @return array
     */
    public function getExcludeProductIds()
    {
        if ($this->_excludeProductIds === null) {
            $this->_excludeProductIds = [$this->getProduct()->getEntityId()];
        }
        return $this->_excludeProductIds;
    }

    /**
     * Get link collection with limit parameter
     *
     * @param null|int $limit
     * @return \Magento\Catalog\Model\ResourceModel\Product\Link\Product\Collection|null
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _getPreparedTargetLinkCollection($limit = null)
    {
        $linkCollection = null;
        switch ($this->getProductListType()) {
            case \Ecomteck\AutoRelatedProducts\Model\Rule::RELATED_PRODUCTS:
                $linkCollection = $this->getProduct()->getRelatedProductCollection();
                break;

            case \Ecomteck\AutoRelatedProducts\Model\Rule::UP_SELLS:
                $linkCollection = $this->getProduct()->getUpSellProductCollection();
                break;

            default:
                throw new LocalizedException(__('Undefined Catalog Product List Type'));
                break;
        }

        if ($limit !== null) {
            $this->_addProductAttributesAndPrices($linkCollection);
            $linkCollection->setPageSize($limit);
        }

        $linkCollection->setVisibility(
            $this->_visibility->getVisibleInCatalogIds()
        )->setFlag(
            'do_not_use_category_id',
            true
        );

        $excludeProductIds = $this->getExcludeProductIds();
        if ($excludeProductIds) {
            $linkCollection->addAttributeToFilter('entity_id', ['nin' => $excludeProductIds]);
        }

        return $linkCollection;
    }

    /**
     * Get link collection for related and up-sell
     *
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection|null
     */
    protected function _getTargetLinkCollection()
    {
        return $this->_getPreparedTargetLinkCollection($this->_autoRelatedProductsData->getMaxProductsListResult());
    }

    /**
     * Retrieve count of related linked products assigned to product
     *
     * @return int
     */
    public function getLinkCollectionCount()
    {
        return count($this->getLinkCollection()->getItems());
    }

    /**
     * Get target rule collection ids
     *
     * @param null|int $limit
     * @return array
     */
    public function _getAutoRelatedProductsProductIds($limit = null)
    {
        $excludeProductIds = $this->getExcludeProductIds();
        if ($this->_items !== null) {
            $excludeProductIds = array_merge(array_keys($this->_items), $excludeProductIds);
        }
        $indexModel = $this->_getAutoRelatedProductsIndex()->setType(
            $this->getProductListType()
        )->setLimit(
            $limit
        )->setProduct(
            $this->getProduct()
        )->setExcludeProductIds(
            $excludeProductIds
        );
        if ($limit !== null) {
            $indexModel->setLimit($limit);
        }

        return $indexModel->getProductIds();
    }

    /**
     * Get target rule collection for related and up-sell
     *
     * @return array
     */
    protected function _getAutoRelatedProductsProducts()
    {
        $limit = $this->_autoRelatedProductsData->getMaxProductsListResult();

        $productIds = $this->_getAutoRelatedProductsProductIds($limit);

        $items = [];
        if ($productIds) {
            /** @var $collection \Magento\Catalog\Model\ResourceModel\Product\Collection */
            $collection = $this->_productCollectionFactory->create();
            $collection->addFieldToFilter('entity_id', ['in' => $productIds]);
            $this->_addProductAttributesAndPrices($collection);

            $collection->setPageSize(
                $limit
            )->setFlag(
                'do_not_use_category_id',
                true
            )->setVisibility(
                $this->_visibility->getVisibleInCatalogIds()
            );

            foreach ($collection as $item) {
                $items[$item->getEntityId()] = $item;
            }
        }

        return $items;
    }

    /**
     * Check is has items
     *
     * @return bool
     */
    public function hasItems()
    {
        return $this->getItemsCount() > 0;
    }

    /**
     * Retrieve count of product in collection
     *
     * @return int
     */
    public function getItemsCount()
    {
        return count($this->getItemCollection());
    }

    /**
     * Get ids of all assigned products
     *
     * @return array
     */
    public function getAllIds()
    {
        if ($this->_allProductIds === null) {
            if (!$this->isShuffled()) {
                $this->_allProductIds = array_keys($this->getItemCollection());
                return $this->_allProductIds;
            }

            $autoRelatedProductsProductIds = $this->_getAutoRelatedProductsProductIds();
            $linkProductCollection = $this->_getPreparedTargetLinkCollection();
            $linkProductIds = [];
            foreach ($linkProductCollection as $item) {
                $linkProductIds[] = $item->getEntityId();
            }
            $this->_allProductIds = array_unique(array_merge($autoRelatedProductsProductIds, $linkProductIds));
            shuffle($this->_allProductIds);
        }

        return $this->_allProductIds;
    }

    /**
     * Return identifiers for produced content
     *
     * @return array
     */
    public function getIdentities()
    {
        $identities = [];
        foreach ($this->getItemCollection() as $item) {
            $identities = array_merge($identities, $item->getIdentities());
        }
        return $identities;
    }

    /**
     * Get all items
     *
     * @return array
     */
    public function getAllItems()
    {
        return $this->getItemCollection();
    }
}
