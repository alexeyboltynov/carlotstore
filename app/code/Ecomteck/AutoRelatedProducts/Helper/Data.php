<?php
/**
 * Ecomteck
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Ecomteck.com license that is
 * available through the world-wide-web at this URL:
 * https://ecomteck.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Ecomteck
 * @package     Ecomteck_AutoRelatedProducts
 * @copyright   Copyright (c) 2018 Ecomteck (https://ecomteck.com/)
 * @license     https://ecomteck.com/LICENSE.txt
 */

/**
 * AutoRelatedProducts data helper
 *
 */
namespace Ecomteck\AutoRelatedProducts\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const XML_PATH_AUTO_RELATED_PRODUCTS_CONFIG = 'ecomteck_personalized_recommendations/auto_related_products/general/';

    const MAX_PRODUCT_LIST_RESULT = 20;

    /**
     * Retrieve Maximum Number of Products in Product List
     *
     * @param int $type product list type
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return int
     */
    public function getMaximumNumberOfProduct($type)
    {
        switch ($type) {
            case \Ecomteck\AutoRelatedProducts\Model\Rule::RELATED_PRODUCTS:
                $number = $this->scopeConfig->getValue(
                    self::XML_PATH_AUTO_RELATED_PRODUCTS_CONFIG . 'related_position_limit',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                );
                break;
            case \Ecomteck\AutoRelatedProducts\Model\Rule::UP_SELLS:
                $number = $this->scopeConfig->getValue(
                    self::XML_PATH_AUTO_RELATED_PRODUCTS_CONFIG . 'upsell_position_limit',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                );
                break;
            case \Ecomteck\AutoRelatedProducts\Model\Rule::CROSS_SELLS:
                $number = $this->scopeConfig->getValue(
                    self::XML_PATH_AUTO_RELATED_PRODUCTS_CONFIG . 'crosssell_position_limit',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                );
                break;
            default:
                throw new \Magento\Framework\Exception\LocalizedException(__('Invalid product list type'));
                break;
        }

        return $this->getMaxProductsListResult($number);
    }

    /**
     * Show Related/Upsell/Cross-Sell Products behavior
     *
     * @param int $type
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return int
     */
    public function getShowProducts($type)
    {
        switch ($type) {
            case \Ecomteck\AutoRelatedProducts\Model\Rule::RELATED_PRODUCTS:
                $show = $this->scopeConfig->getValue(
                    self::XML_PATH_AUTO_RELATED_PRODUCTS_CONFIG . 'related_position_behavior',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                );
                break;
            case \Ecomteck\AutoRelatedProducts\Model\Rule::UP_SELLS:
                $show = $this->scopeConfig->getValue(
                    self::XML_PATH_AUTO_RELATED_PRODUCTS_CONFIG . 'upsell_position_behavior',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                );
                break;
            case \Ecomteck\AutoRelatedProducts\Model\Rule::CROSS_SELLS:
                $show = $this->scopeConfig->getValue(
                    self::XML_PATH_AUTO_RELATED_PRODUCTS_CONFIG . 'crosssell_position_behavior',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                );
                break;
            default:
                throw new \Magento\Framework\Exception\LocalizedException(__('Invalid product list type'));
                break;
        }

        return $show;
    }

    /**
     * Retrieve maximum number of products can be displayed in product list
     *
     * if number is 0 (unlimited) or great global maximum return global maximum value
     *
     * @param int $number
     * @return int
     */
    public function getMaxProductsListResult($number = 0)
    {
        if ($number == 0 || $number > self::MAX_PRODUCT_LIST_RESULT) {
            $number = self::MAX_PRODUCT_LIST_RESULT;
        }

        return $number;
    }

    /**
     * Retrieve Rotation Mode in Product List
     *
     * @param int $type product list type
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return int
     */
    public function getRotationMode($type)
    {
        switch ($type) {
            case \Ecomteck\AutoRelatedProducts\Model\Rule::RELATED_PRODUCTS:
                $mode = $this->scopeConfig->getValue(
                    self::XML_PATH_AUTO_RELATED_PRODUCTS_CONFIG . 'related_rotation_mode',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                );
                break;
            case \Ecomteck\AutoRelatedProducts\Model\Rule::UP_SELLS:
                $mode = $this->scopeConfig->getValue(
                    self::XML_PATH_AUTO_RELATED_PRODUCTS_CONFIG . 'upsell_rotation_mode',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                );
                break;
            case \Ecomteck\AutoRelatedProducts\Model\Rule::CROSS_SELLS:
                $mode = $this->scopeConfig->getValue(
                    self::XML_PATH_AUTO_RELATED_PRODUCTS_CONFIG . 'crosssell_rotation_mode',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                );
                break;
            default:
                throw new \Magento\Framework\Exception\LocalizedException(__('Invalid rotation mode type'));
                break;
        }
        return $mode;
    }
}
