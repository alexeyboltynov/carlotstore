<?php
/**
 * Ecomteck
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Ecomteck.com license that is
 * available through the world-wide-web at this URL:
 * https://ecomteck.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Ecomteck
 * @package     Ecomteck_AutoRelatedProducts
 * @copyright   Copyright (c) 2018 Ecomteck (https://ecomteck.com/)
 * @license     https://ecomteck.com/LICENSE.txt
 */
namespace Ecomteck\AutoRelatedProducts\Observer;

use Magento\Framework\Event\ObserverInterface;

/**
 * AutoRelatedProducts observer
 *
 */
class CatalogProductDeleteCommitAfterObserver implements ObserverInterface
{
    /**
     * @var \Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Product\Rule
     */
    protected $_productRuleIndexer;

    /**
     * @param \Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Product\Rule $productRuleIndexer
     */
    public function __construct(
        \Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Product\Rule $productRuleIndexer
    ) {
        $this->_productRuleIndexer = $productRuleIndexer;
    }

    /**
     * Process event on 'delete_commit_after' event
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var $product \Magento\Catalog\Model\Product */
        $product = $observer->getEvent()->getProduct();

        $this->_productRuleIndexer->cleanAfterProductDelete($product->getId());
    }
}
