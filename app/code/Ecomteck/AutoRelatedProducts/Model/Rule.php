<?php
/**
 * Ecomteck
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Ecomteck.com license that is
 * available through the world-wide-web at this URL:
 * https://ecomteck.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Ecomteck
 * @package     Ecomteck_AutoRelatedProducts
 * @copyright   Copyright (c) 2018 Ecomteck (https://ecomteck.com/)
 * @license     https://ecomteck.com/LICENSE.txt
 */

namespace Ecomteck\AutoRelatedProducts\Model;

use Magento\Framework\Exception\LocalizedException;

/**
 * AutoRelatedProducts Rule Model
 *
 * @method \Ecomteck\AutoRelatedProducts\Model\ResourceModel\Rule _getResource()
 * @method \Ecomteck\AutoRelatedProducts\Model\ResourceModel\Rule getResource()
 * @method string getName()
 * @method \Ecomteck\AutoRelatedProducts\Model\Rule setName(string $value)
 * @method string getFromDate()
 * @method \Ecomteck\AutoRelatedProducts\Model\Rule setFromDate(string $value)
 * @method string getToDate()
 * @method \Ecomteck\AutoRelatedProducts\Model\Rule setToDate(string $value)
 * @method int getIsActive()
 * @method \Ecomteck\AutoRelatedProducts\Model\Rule setIsActive(int $value)
 * @method string getConditionsSerialized()
 * @method \Ecomteck\AutoRelatedProducts\Model\Rule setConditionsSerialized(string $value)
 * @method string getActionsSerialized()
 * @method \Ecomteck\AutoRelatedProducts\Model\Rule setActionsSerialized(string $value)
 * @method \Ecomteck\AutoRelatedProducts\Model\Rule setPositionsLimit(int $value)
 * @method int getApplyTo()
 * @method \Ecomteck\AutoRelatedProducts\Model\Rule setApplyTo(int $value)
 * @method int getSortOrder()
 * @method \Ecomteck\AutoRelatedProducts\Model\Rule setSortOrder(int $value)
 * @method int getUseCustomerSegment()
 * @method \Ecomteck\AutoRelatedProducts\Model\Rule setUseCustomerSegment(int $value)
 * @method string getActionSelect()
 * @method \Ecomteck\AutoRelatedProducts\Model\Rule setActionSelect(string $value)
 * @method array getCustomerSegmentIds()
 * @method \Ecomteck\AutoRelatedProducts\Model\Rule setCustomerSegmentIds(array $ids)
 * @method \Ecomteck\AutoRelatedProducts\Model\Rule\Condition\Combine getConditions()
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 * @SuppressWarnings(PHPMD.LongVariable)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Rule extends \Magento\Rule\Model\AbstractModel
{
    /**
     * Position behavior selectors
     */
    const BOTH_SELECTED_AND_RULE_BASED = 0;

    const SELECTED_ONLY = 1;

    const RULE_BASED_ONLY = 2;

    /**
     * Product list types
     */
    const RELATED_PRODUCTS = 1;

    const UP_SELLS = 2;

    const CROSS_SELLS = 3;

    /**
     * Shuffle mode by default
     */
    const ROTATION_SHUFFLE = 0;

    const ROTATION_NONE = 1;

    /**
     * Store default product positions limit
     */
    const POSITIONS_DEFAULT_LIMIT = 20;

    /**
     * Store matched product Ids
     *
     * @var array
     */
    protected $_productIds;

    /**
     * Store flags per store is applicable rule by date
     *
     * @var array
     */
    protected $_checkDateForStore = [];

    /**
     * Product factory
     *
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * Rule factory
     *
     * @var \Ecomteck\AutoRelatedProducts\Model\Rule\Condition\CombineFactory
     */
    protected $_ruleFactory;

    /**
     * Action factory
     *
     * @var \Ecomteck\AutoRelatedProducts\Model\Actions\Condition\CombineFactory
     */
    protected $_actionFactory;

    /**
     * Locale date
     *
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $_localeDate;

    /**
     * Rule-Product indexer processor instance
     *
     * @var \Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Rule\Product\Processor
     */
    protected $_ruleProductIndexerProcessor;

    /**
     * Sql builder
     *
     * @var \Magento\Rule\Model\Condition\Sql\Builder
     */
    protected $_sqlBuilder;

    /**
     * Rule constructor.
     *
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
     * @param Rule\Condition\CombineFactory $ruleFactory
     * @param Actions\Condition\CombineFactory $actionFactory
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param Indexer\AutoRelatedProducts\Rule\Product\Processor $ruleProductIndexerProcessor
     * @param \Magento\Rule\Model\Condition\Sql\Builder $sqlBuilder
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Ecomteck\AutoRelatedProducts\Model\Rule\Condition\CombineFactory $ruleFactory,
        \Ecomteck\AutoRelatedProducts\Model\Actions\Condition\CombineFactory $actionFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Rule\Product\Processor $ruleProductIndexerProcessor,
        \Magento\Rule\Model\Condition\Sql\Builder $sqlBuilder,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->_localeDate = $localeDate;
        $this->_productFactory = $productFactory;
        $this->_ruleFactory = $ruleFactory;
        $this->_actionFactory = $actionFactory;
        $this->_ruleProductIndexerProcessor = $ruleProductIndexerProcessor;
        $this->_sqlBuilder = $sqlBuilder;
        parent::__construct(
            $context,
            $registry,
            $formFactory,
            $localeDate,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     * Set resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Ecomteck\AutoRelatedProducts\Model\ResourceModel\Rule');
    }

    /**
     * Reset action cached select if actions conditions has changed
     *
     * @return \Ecomteck\AutoRelatedProducts\Model\Rule
     */
    public function beforeSave()
    {
        parent::beforeSave();

        if ($this->dataHasChangedFor('actions_serialized')) {
            $this->setData('action_select', null);
            $this->setData('action_select_bind', null);
        }

        return $this;
    }

    /**
     * AfterSave callback
     *
     * @return $this
     */
    public function afterSave()
    {
        if ($this->isObjectNew() || $this->dataHasChangedForAny([
            'is_active',
            'from_date',
            'to_date',
            'conditions',
            'apply_to',
            'actions',
            'customer_segment_ids',
        ])) {
            $this->_ruleProductIndexerProcessor->reindexRow($this->getId());
        }
        return parent::afterSave();
    }

    /**
     * After delete callback
     *
     * @return $this
     */
    public function afterDeleteCommit()
    {
        $this->_ruleProductIndexerProcessor->reindexRow($this->getId());
        return parent::afterDeleteCommit();
    }

    /**
     * Check is data changed for any of provided fields
     *
     * @param string[] $fields
     * @return bool
     */
    public function dataHasChangedForAny(array $fields = [])
    {
        foreach ($fields as $field) {
            if ($this->dataHasChangedFor($field)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Getter for rule combine conditions instance
     *
     * @return \Ecomteck\AutoRelatedProducts\Model\Rule\Condition\Combine
     */
    public function getConditionsInstance()
    {
        return $this->_ruleFactory->create();
    }

    /**
     * Getter for rule actions collection instance
     *
     * @return \Ecomteck\AutoRelatedProducts\Model\Actions\Condition\Combine
     */
    public function getActionsInstance()
    {
        return $this->_actionFactory->create();
    }

    /**
     * Get options for `Apply to` field
     *
     * @param bool $withEmpty
     * @return string[]|\Magento\Framework\Phrase[]
     */
    public function getAppliesToOptions($withEmpty = false)
    {
        $result = [];
        if ($withEmpty) {
            $result[''] = __('-- Please Select --');
        }
        $result[\Ecomteck\AutoRelatedProducts\Model\Rule::RELATED_PRODUCTS] = __('Related Products');
        $result[\Ecomteck\AutoRelatedProducts\Model\Rule::UP_SELLS] = __('Up-sells');
        $result[\Ecomteck\AutoRelatedProducts\Model\Rule::CROSS_SELLS] = __('Cross-sells');

        return $result;
    }

    /**
     * Prepare array of product ids which are matched by rule
     *
     * @return $this
     */
    public function prepareMatchingProducts()
    {
        $productCollection = $this->_productFactory->create()->getCollection();
        $this->setCollectedAttributes([]);
        $this->getConditions()->collectValidatedAttributes($productCollection);
        $this->_sqlBuilder->attachConditionToCollection($productCollection, $this->getConditions());
        $this->_productIds = [];
        foreach (array_unique($productCollection->getAllIds()) as $productId) {
            if ($this->getConditions()->validateByEntityId($productId)) {
                $this->_productIds[] = $productId;
            }
        }
        return $this;
    }

    /**
     * Retrieve array of product Ids that are matched by rule
     *
     * @return int[]
     */
    public function getMatchingProductIds()
    {
        if (null === $this->_productIds) {
            $this->prepareMatchingProducts();
        }
        return $this->_productIds;
    }

    /**
     * Check if rule is applicable by date for specified store
     *
     * @param int $storeId
     * @return bool
     */
    public function checkDateForStore($storeId)
    {
        if (!isset($this->_checkDateForStore[$storeId])) {
            $this->_checkDateForStore[$storeId] = $this->_localeDate->isScopeDateInInterval(
                null,
                $this->getFromDate(),
                $this->getToDate()
            );
        }
        return $this->_checkDateForStore[$storeId];
    }

    /**
     * Get product positions for current rule
     *
     * @return int If positions limit is not set, then default limit will be returned
     */
    public function getPositionsLimit()
    {
        $limit = $this->getData('positions_limit');
        if (!$limit) {
            $limit = 20;
        }

        return $limit;
    }

    /**
     * Retrieve Action select bind array
     *
     * @return string[]
     */
    public function getActionSelectBind()
    {
        $bind = $this->getData('action_select_bind');
        if ($bind && is_string($bind)) {
            $bind = unserialize($bind);
        }

        return $bind;
    }

    /**
     * Set action select bind array or serialized string
     *
     * @param string[]|string $bind
     * @return $this
     */
    public function setActionSelectBind($bind)
    {
        if (is_array($bind)) {
            $bind = serialize($bind);
        }
        return $this->setData('action_select_bind', $bind);
    }

    /**
     * Validate rule data
     *
     * @param \Magento\Framework\DataObject $object
     * @return string[]|bool - Return true if validation passed successfully. Array with errors description otherwise
     * @throws LocalizedException
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function validateData(\Magento\Framework\DataObject $object)
    {
        $result = parent::validateData($object);

        if (!is_array($result)) {
            $result = [];
        }

        $validator = new \Zend_Validate_Regex(['pattern' => '/^[a-z][a-z0-9_\/]{1,255}$/']);
        $actionArgsList = $object->getData('rule');
        if (is_array($actionArgsList) && isset($actionArgsList['actions'])) {
            foreach ($actionArgsList['actions'] as $actionArgsIndex => $actionArgs) {
                if (1 === $actionArgsIndex) {
                    continue;
                }
                if (!class_exists($actionArgs['type'])) {
                    throw new LocalizedException(__('Model class name for attribute is invalid'));
                }
                if (isset($actionArgs['attribute']) && !$validator->isValid($actionArgs['attribute'])) {
                    $result[] = __(
                        'This attribute code is invalid. Please use only letters (a-z), '
                        . 'numbers (0-9) or underscores (_), and be sure the code begins with a letter.'
                    );
                }
            }
        }

        return !empty($result) ? $result : true;
    }

    /**
     * Validate rule conditions to determine if rule can run
     *
     * @param int $entityId
     *
     * @return bool
     */
    public function validateByEntityId($entityId)
    {
        return $this->getConditions()->validateByEntityId($entityId);
    }
}
