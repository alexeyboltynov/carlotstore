<?php
/**
 * Ecomteck
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Ecomteck.com license that is
 * available through the world-wide-web at this URL:
 * https://ecomteck.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Ecomteck
 * @package     Ecomteck_AutoRelatedProducts
 * @copyright   Copyright (c) 2018 Ecomteck (https://ecomteck.com/)
 * @license     https://ecomteck.com/LICENSE.txt
 */

namespace Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts;

abstract class AbstractProcessor extends \Magento\Framework\Indexer\AbstractProcessor
{
    /**
     * State container
     *
     * @var Status\Container
     */
    protected $_statusContainer;

    /**
     * @param \Magento\Framework\Indexer\IndexerRegistry $indexerRegistry
     * @param Status\Container $statusContainer
     */
    public function __construct(
        \Magento\Framework\Indexer\IndexerRegistry $indexerRegistry,
        \Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Status\Container $statusContainer
    ) {
        parent::__construct($indexerRegistry);
        $this->_statusContainer = $statusContainer;
    }

    /**
     * Get processor state container
     *
     * @return Status\Container
     */
    public function getStatusContainer()
    {
        return $this->_statusContainer;
    }

    /**
     * Is full reindex passed
     *
     * @return bool
     */
    public function isFullReindexPassed()
    {
        return $this->getStatusContainer()->isFullReindexPassed($this->getIndexerId());
    }

    /**
     * Set full reindex passed
     *
     * @return void
     */
    public function setFullReindexPassed()
    {
        $this->getIndexer()->getState()->setStatus(\Magento\Framework\Indexer\StateInterface::STATUS_VALID)->save();
        $this->getStatusContainer()->setFullReindexPassed($this->getIndexerId());
    }
}
