<?php
/**
 * Ecomteck
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Ecomteck.com license that is
 * available through the world-wide-web at this URL:
 * https://ecomteck.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Ecomteck
 * @package     Ecomteck_AutoRelatedProducts
 * @copyright   Copyright (c) 2018 Ecomteck (https://ecomteck.com/)
 * @license     https://ecomteck.com/LICENSE.txt
 */
namespace Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Plugin;

class AttributeSet extends AbstractPlugin
{
    /**
     * Invalidate target rule indexer after deleting attribute set
     *
     * @param \Magento\Eav\Model\Entity\Attribute\Set $attributeSet
     *
     * @return \Magento\Eav\Model\Entity\Attribute\Set
     */
    public function afterDelete(\Magento\Eav\Model\Entity\Attribute\Set $attributeSet)
    {
        $this->invalidateIndexers();
        return $attributeSet;
    }
}
