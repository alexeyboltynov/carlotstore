<?php
/**
 * Ecomteck
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Ecomteck.com license that is
 * available through the world-wide-web at this URL:
 * https://ecomteck.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Ecomteck
 * @package     Ecomteck_AutoRelatedProducts
 * @copyright   Copyright (c) 2018 Ecomteck (https://ecomteck.com/)
 * @license     https://ecomteck.com/LICENSE.txt
 */
namespace Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Plugin;

use Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Product\Rule\Processor as ProductRuleProcessor;
use Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Rule\Product\Processor as RuleProductProcessor;

abstract class AbstractPlugin
{
    /**
     * @var ProductRuleProcessor
     */
    protected $_productRuleindexer;

    /**
     * @var RuleProductProcessor
     */
    protected $_ruleProductIndexer;

    /**
     * @param ProductRuleProcessor $productRuleProcessor
     * @param RuleProductProcessor $ruleProductProcessor
     */
    public function __construct(ProductRuleProcessor $productRuleProcessor, RuleProductProcessor $ruleProductProcessor)
    {
        $this->_productRuleindexer = $productRuleProcessor;
        $this->_ruleProductIndexer = $ruleProductProcessor;
    }

    /**
     * Invalidate indexers
     *
     * @return $this
     */
    protected function invalidateIndexers()
    {
        $this->_productRuleindexer->markIndexerAsInvalid();
        $this->_ruleProductIndexer->markIndexerAsInvalid();
        return $this;
    }
}
