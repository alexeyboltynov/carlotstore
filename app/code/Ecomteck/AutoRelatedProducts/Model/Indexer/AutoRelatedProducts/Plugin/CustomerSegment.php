<?php
/**
 * Ecomteck
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Ecomteck.com license that is
 * available through the world-wide-web at this URL:
 * https://ecomteck.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Ecomteck
 * @package     Ecomteck_AutoRelatedProducts
 * @copyright   Copyright (c) 2018 Ecomteck (https://ecomteck.com/)
 * @license     https://ecomteck.com/LICENSE.txt
 */
namespace Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Plugin;

class CustomerSegment extends AbstractPlugin
{
    /**
     * Invalidate target rule indexer after deleting customer segment
     *
     * @param \Magento\CustomerSegment\Model\Segment $customerSegment
     * @return \Magento\CustomerSegment\Model\Segment
     */
    public function afterDelete(\Magento\CustomerSegment\Model\Segment $customerSegment)
    {
        $this->invalidateIndexers();
        return $customerSegment;
    }

    /**
     * Invalidate target rule indexer after changing customer segment
     *
     * @param \Magento\CustomerSegment\Model\Segment $customerSegment
     * @return \Magento\CustomerSegment\Model\Segment
     */
    public function afterSave(\Magento\CustomerSegment\Model\Segment $customerSegment)
    {
        if (!$customerSegment->isObjectNew()) {
            $this->invalidateIndexers();
        }
        return $customerSegment;
    }
}
