<?php
/**
 * Ecomteck
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Ecomteck.com license that is
 * available through the world-wide-web at this URL:
 * https://ecomteck.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Ecomteck
 * @package     Ecomteck_AutoRelatedProducts
 * @copyright   Copyright (c) 2018 Ecomteck (https://ecomteck.com/)
 * @license     https://ecomteck.com/LICENSE.txt
 */

namespace Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Rule;


class Product implements \Magento\Framework\Indexer\ActionInterface, \Magento\Framework\Mview\ActionInterface
{
    /**
     * @var \Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Rule\Product\Action\Row
     */
    protected $_ruleProductIndexerRow;

    /**
     * @var \Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Rule\Product\Action\Rows
     */
    protected $_ruleProductIndexerRows;

    /**
     * @var \Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Action\Full
     */
    protected $_ruleProductIndexerFull;

    /**
     * @var \Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Product\Rule\Processor
     */
    protected $_productRuleProcessor;

    /**
     * @var Product\Processor
     */
    protected $_ruleProductProcessor;

    /**
     * @param \Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Rule\Product\Action\Row $ruleProductIndexerRow
     * @param \Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Rule\Product\Action\Rows $ruleProductIndexerRows
     * @param \Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Action\Full $ruleProductIndexerFull
     * @param \Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Product\Rule\Processor $productRuleProcessor
     * @param \Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Rule\Product\Processor $ruleProductProcessor
     */
    public function __construct(
        \Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Rule\Product\Action\Row $ruleProductIndexerRow,
        \Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Rule\Product\Action\Rows $ruleProductIndexerRows,
        \Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Action\Full $ruleProductIndexerFull,
        \Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Product\Rule\Processor $productRuleProcessor,
        \Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Rule\Product\Processor $ruleProductProcessor
    ) {
        $this->_ruleProductIndexerRow = $ruleProductIndexerRow;
        $this->_ruleProductIndexerRows = $ruleProductIndexerRows;
        $this->_ruleProductIndexerFull = $ruleProductIndexerFull;
        $this->_productRuleProcessor = $productRuleProcessor;
        $this->_ruleProductProcessor = $ruleProductProcessor;
    }

    /**
     * Execute materialization on ids entities
     *
     * @param int[] $ruleId
     *
     * @return void
     */
    public function execute($ruleId)
    {
        $this->_ruleProductIndexerRows->execute($ruleId);
    }

    /**
     * Execute full indexation
     *
     * @return void
     */
    public function executeFull()
    {
        if (!$this->_productRuleProcessor->isFullReindexPassed()) {
            $this->_ruleProductIndexerFull->execute();
            $this->_productRuleProcessor->setFullReindexPassed();
        }
    }

    /**
     * Execute partial indexation by ID list
     *
     * @param int[] $ruleIds
     *
     * @return void
     */
    public function executeList(array $ruleIds)
    {
        $this->_ruleProductIndexerRows->execute($ruleIds);
    }

    /**
     * Execute partial indexation by ID
     *
     * @param int $ruleId
     *
     * @return void
     */
    public function executeRow($ruleId)
    {
        $this->_ruleProductIndexerRow->execute($ruleId);
    }
}
