<?php
/**
 * Ecomteck
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Ecomteck.com license that is
 * available through the world-wide-web at this URL:
 * https://ecomteck.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Ecomteck
 * @package     Ecomteck_AutoRelatedProducts
 * @copyright   Copyright (c) 2018 Ecomteck (https://ecomteck.com/)
 * @license     https://ecomteck.com/LICENSE.txt
 */

namespace Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Product;


class Rule implements \Magento\Framework\Indexer\ActionInterface, \Magento\Framework\Mview\ActionInterface
{
    /**
     * Product-Rule indexer row action
     *
     * @var \Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Product\Rule\Action\Row
     */
    protected $_productRuleIndexerRow;

    /**
     * Product-Rule indexer rows action
     *
     * @var \Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Product\Rule\Action\Rows
     */
    protected $_productRuleIndexerRows;

    /**
     * Product-Rule indexer full reindex action
     *
     * @var \Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Action\Full
     */
    protected $_productRuleIndexerFull;

    /**
     * Rule-Product indexer processor
     *
     * @var \Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Rule\Product\Processor
     */
    protected $_ruleProductProcessor;

    /**
     * Product-Rule indexer processor
     *
     * @var \Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Product\Rule\Processor
     */
    protected $_productRuleProcessor;

    /**
     * Product-Rule indexer clean action
     *
     * @var \Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Action\Clean
     */
    protected $_productRuleIndexerClean;

    /**
     * Product-Rule indexer clean products relations action
     *
     * @var \Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Product\Rule\Action\CleanDeleteProduct
     */
    protected $_productRuleIndexerCleanDeleteProduct;

    /**
     * Construct
     *
     * @param \Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Product\Rule\Action\Row $productRuleIndexerRow
     * @param \Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Product\Rule\Action\Rows $productRuleIndexerRows
     * @param \Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Action\Full $productRuleIndexerFull
     * @param \Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Rule\Product\Processor $ruleProductProcessor
     * @param \Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Product\Rule\Processor $productRuleProcessor
     * @param \Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Action\Clean $productRuleIndexerClean
     * @param Rule\Action\CleanDeleteProduct $productRuleIndexerCleanDeleteProduct
     */
    public function __construct(
        \Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Product\Rule\Action\Row $productRuleIndexerRow,
        \Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Product\Rule\Action\Rows $productRuleIndexerRows,
        \Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Action\Full $productRuleIndexerFull,
        \Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Rule\Product\Processor $ruleProductProcessor,
        \Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Product\Rule\Processor $productRuleProcessor,
        \Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Action\Clean $productRuleIndexerClean,
        Rule\Action\CleanDeleteProduct $productRuleIndexerCleanDeleteProduct
    ) {
        $this->_productRuleIndexerRow = $productRuleIndexerRow;
        $this->_productRuleIndexerRows = $productRuleIndexerRows;
        $this->_productRuleIndexerFull = $productRuleIndexerFull;
        $this->_ruleProductProcessor = $ruleProductProcessor;
        $this->_productRuleProcessor = $productRuleProcessor;
        $this->_productRuleIndexerClean = $productRuleIndexerClean;
        $this->_productRuleIndexerCleanDeleteProduct = $productRuleIndexerCleanDeleteProduct;
    }

    /**
     * Execute materialization on ids entities
     *
     * @param int[] $productIds
     *
     * @return void
     */
    public function execute($productIds)
    {
        $this->_productRuleIndexerRows->execute($productIds);
    }

    /**
     * Execute full indexation
     *
     * @return void
     */
    public function executeFull()
    {
        if (!$this->_ruleProductProcessor->isFullReindexPassed()) {
            $this->_productRuleIndexerFull->execute();
            $this->_ruleProductProcessor->setFullReindexPassed();
        }
    }

    /**
     * Execute partial indexation by ID list
     *
     * @param int[] $productIds
     *
     * @return void
     */
    public function executeList(array $productIds)
    {
        $this->_productRuleIndexerRows->execute($productIds);
    }

    /**
     * Execute partial indexation by ID
     *
     * @param int $productId
     *
     * @return void
     */
    public function executeRow($productId)
    {
        $this->_productRuleIndexerRow->execute($productId);
    }

    /**
     * Execute clean index
     *
     * @return void
     */
    public function cleanByCron()
    {
        $this->_productRuleIndexerClean->execute();
    }

    /**
     * Clean deleted products from index
     *
     * @param int $productId
     *
     * @return void
     */
    public function cleanAfterProductDelete($productId)
    {
        $this->_productRuleIndexerCleanDeleteProduct->execute($productId);
    }
}
