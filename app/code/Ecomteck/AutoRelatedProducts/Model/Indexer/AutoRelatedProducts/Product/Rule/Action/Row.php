<?php
/**
 * Ecomteck
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Ecomteck.com license that is
 * available through the world-wide-web at this URL:
 * https://ecomteck.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Ecomteck
 * @package     Ecomteck_AutoRelatedProducts
 * @copyright   Copyright (c) 2018 Ecomteck (https://ecomteck.com/)
 * @license     https://ecomteck.com/LICENSE.txt
 */

namespace Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Product\Rule\Action;

/**
 * Class Row reindex action
 *
 * @package Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Product\Rule\Action
 */
class Row extends \Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\AbstractAction
{
    /**
     * Execute Row reindex
     *
     * @param int|null $productId
     * @throws \Magento\Framework\Exception\LocalizedException
     *
     * @return void
     */
    public function execute($productId = null)
    {
        if (!isset($productId) || empty($productId)) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('We can\'t rebuild the index for an undefined product.')
            );
        }
        try {
            $this->_reindexByProductId($productId);
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\LocalizedException(__($e->getMessage()), $e);
        }
    }
}
