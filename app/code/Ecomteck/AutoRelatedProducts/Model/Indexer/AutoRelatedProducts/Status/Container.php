<?php
/**
 * Ecomteck
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Ecomteck.com license that is
 * available through the world-wide-web at this URL:
 * https://ecomteck.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Ecomteck
 * @package     Ecomteck_AutoRelatedProducts
 * @copyright   Copyright (c) 2018 Ecomteck (https://ecomteck.com/)
 * @license     https://ecomteck.com/LICENSE.txt
 */

namespace Ecomteck\AutoRelatedProducts\Model\Indexer\AutoRelatedProducts\Status;

class Container
{
    /**
     * @var array
     */
    protected $_fullReindexPassed = [];

    /**
     * Set indexer full reindex was passed
     *
     * @param string $indexerIdString
     *
     * @return void
     */
    public function setFullReindexPassed($indexerIdString)
    {
        $this->_fullReindexPassed[$indexerIdString] = true;
    }

    /**
     * Get indexer full reindex was passed
     *
     * @param string $indexerIdString
     * @return bool
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getFullReindexPassed($indexerIdString)
    {
        return isset($this->_fullReindexPassed[$indexerIdString]) ? $this->_fullReindexPassed[$indexerIdString] : false;
    }

    /**
     * Is full reindex for specified indexer passed
     *
     * @param string $indexerIdString
     * @return bool
     */
    public function isFullReindexPassed($indexerIdString)
    {
        return $this->getFullReindexPassed($indexerIdString) === true;
    }
}
