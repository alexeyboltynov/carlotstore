<?php
/**
 * Ecomteck
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Ecomteck.com license that is
 * available through the world-wide-web at this URL:
 * https://ecomteck.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Ecomteck
 * @package     Ecomteck_AutoRelatedProducts
 * @copyright   Copyright (c) 2018 Ecomteck (https://ecomteck.com/)
 * @license     https://ecomteck.com/LICENSE.txt
 */
namespace Ecomteck\AutoRelatedProducts\Model\Source;

class Rotation implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Get data for Rotation mode selector
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            \Ecomteck\AutoRelatedProducts\Model\Rule::ROTATION_NONE => __('Do not rotate'),
            \Ecomteck\AutoRelatedProducts\Model\Rule::ROTATION_SHUFFLE => __('Shuffle')
        ];
    }
}
