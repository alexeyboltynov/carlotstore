<?php
/**
 * Ecomteck
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Ecomteck.com license that is
 * available through the world-wide-web at this URL:
 * https://ecomteck.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Ecomteck
 * @package     Ecomteck_AutoRelatedProducts
 * @copyright   Copyright (c) 2018 Ecomteck (https://ecomteck.com/)
 * @license     https://ecomteck.com/LICENSE.txt
 */
namespace Ecomteck\AutoRelatedProducts\Model\Source;

class Position implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Get data for Position behavior selector
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            \Ecomteck\AutoRelatedProducts\Model\Rule::BOTH_SELECTED_AND_RULE_BASED => __('Both Selected and Rule-Based'),
            \Ecomteck\AutoRelatedProducts\Model\Rule::SELECTED_ONLY => __('Selected Only'),
            \Ecomteck\AutoRelatedProducts\Model\Rule::RULE_BASED_ONLY => __('Rule-Based Only')
        ];
    }
}
