<?php
/**
 * Ecomteck
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Ecomteck.com license that is
 * available through the world-wide-web at this URL:
 * https://ecomteck.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Ecomteck
 * @package     Ecomteck_AutoRelatedProducts
 * @copyright   Copyright (c) 2018 Ecomteck (https://ecomteck.com/)
 * @license     https://ecomteck.com/LICENSE.txt
 */
namespace Ecomteck\AutoRelatedProducts\Model\ResourceModel;

use Magento\Framework\ObjectManagerInterface;
use Ecomteck\AutoRelatedProducts\Model\ResourceModel\Index\IndexInterface;

/**
 * AutoRelatedProducts Product Index Pool
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class IndexPool
{
    /**
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var array
     */
    private $types;

    /**
     * @param ObjectManagerInterface $objectManager
     * @param array $types
     */
    public function __construct(ObjectManagerInterface $objectManager, array $types = [])
    {
        $this->objectManager = $objectManager;
        $this->types = $types;
    }

    /**
     * Get index shared object
     *
     * @param string $type
     * @throws \LogicException
     * @return IndexInterface
     */
    public function get($type)
    {
        if (!isset($this->types[$type])) {
            throw new \LogicException($type . ' is undefined catalog product list type');
        }

        /** @var IndexInterface $index */
        $index = $this->objectManager->get($this->types[$type]);

        if (!$index instanceof IndexInterface) {
            throw new \LogicException(
                $this->types[$type] . ' doesn\'t implement \Ecomteck\AutoRelatedProducts\Model\ResourceModel\Index\IndexInterface'
            );
        }

        return $index;
    }
}
