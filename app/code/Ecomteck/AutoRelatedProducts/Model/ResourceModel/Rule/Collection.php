<?php
/**
 * Ecomteck
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Ecomteck.com license that is
 * available through the world-wide-web at this URL:
 * https://ecomteck.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Ecomteck
 * @package     Ecomteck_AutoRelatedProducts
 * @copyright   Copyright (c) 2018 Ecomteck (https://ecomteck.com/)
 * @license     https://ecomteck.com/LICENSE.txt
 */
namespace Ecomteck\AutoRelatedProducts\Model\ResourceModel\Rule;

/**
 * Target rules resource collection model
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Collection extends \Magento\Rule\Model\ResourceModel\Rule\Collection\AbstractCollection
{
    /**
     * Set resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Ecomteck\AutoRelatedProducts\Model\Rule', 'Ecomteck\AutoRelatedProducts\Model\ResourceModel\Rule');
    }

    /**
     * Run "afterLoad" callback on items if it is applicable
     *
     * @return $this
     */
    protected function _afterLoad()
    {
        foreach ($this->_items as $rule) {
            /* @var $rule \Ecomteck\AutoRelatedProducts\Model\Rule */
            if (!$this->getFlag('do_not_run_after_load')) {
                $rule->afterLoad();
            }
        }

        parent::_afterLoad();
        return $this;
    }

    /**
     * Add Apply To Product List Filter to Collection
     *
     * @param int|array $applyTo
     * @return $this
     */
    public function addApplyToFilter($applyTo)
    {
        $this->addFieldToFilter('apply_to', $applyTo);
        return $this;
    }

    /**
     * Set Priority Sort order
     *
     * @param string $direction
     * @return $this
     */
    public function setPriorityOrder($direction = self::SORT_ORDER_ASC)
    {
        $this->setOrder('sort_order', $direction);
        return $this;
    }

    /**
     * Add filter by product id to collection
     *
     * @param int $productId
     * @return $this
     */
    public function addProductFilter($productId)
    {
        $this->getSelect()->join(
            ['product_idx' => $this->getTable('ecomteck_auto_related_products_product')],
            'product_idx.rule_id = main_table.rule_id',
            []
        )->where(
            'product_idx.product_id = ?',
            $productId
        );

        return $this;
    }

    /**
     * Add filter by segment id to collection
     *
     * @param int $segmentId
     * @return $this
     */
    public function addSegmentFilter($segmentId)
    {
        if (!empty($segmentId)) {
            $this->getSelect()->join(
                ['segement_idx' => $this->getTable('ecomteck_auto_related_products_customersegment')],
                'segement_idx.rule_id = main_table.rule_id',
                []
            )->where(
                'segement_idx.segment_id = ?',
                $segmentId
            );
        } else {
            $this->getSelect()->joinLeft(
                ['segement_idx' => $this->getTable('ecomteck_auto_related_products_customersegment')],
                'segement_idx.rule_id = main_table.rule_id',
                []
            )->where(
                'segement_idx.segment_id IS NULL'
            );
        }
        return $this;
    }
}
