<?php
/**
 * Ecomteck
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Ecomteck.com license that is
 * available through the world-wide-web at this URL:
 * https://ecomteck.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Ecomteck
 * @package     Ecomteck_AutoRelatedProducts
 * @copyright   Copyright (c) 2018 Ecomteck (https://ecomteck.com/)
 * @license     https://ecomteck.com/LICENSE.txt
 */
namespace Ecomteck\AutoRelatedProducts\Model\ResourceModel\Index;

use Ecomteck\AutoRelatedProducts\Model\Index as IndexModel;

interface IndexInterface
{
    /**
     * Load products by segment ID
     *
     * @param IndexModel $indexModel
     * @param int $segmentId
     * @return int[]
     */
    public function loadProductIdsBySegmentId(IndexModel $indexModel, $segmentId);

    /**
     * Save matched product Ids by customer segments
     *
     * @param IndexModel $indexModel
     * @param int $segmentId
     * @param int[] $productIds
     * @return $this
     */
    public function saveResultForCustomerSegments(IndexModel $indexModel, $segmentId, array $productIds);

    /**
     * Clean index by store
     *
     * @param \Magento\Store\Model\Store|int|array|null $store
     * @return $this
     */
    public function cleanIndex($store = null);

    /**
     * Delete index by product
     *
     * @param int|null $entityId
     * @return $this
     */
    public function deleteProductFromIndex($entityId = null);
}
