<?php
/**
 * Ecomteck
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Ecomteck.com license that is
 * available through the world-wide-web at this URL:
 * https://ecomteck.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Ecomteck
 * @package     Ecomteck_AutoRelatedProducts
 * @copyright   Copyright (c) 2018 Ecomteck (https://ecomteck.com/)
 * @license     https://ecomteck.com/LICENSE.txt
 */
namespace Ecomteck\AutoRelatedProducts\Model\ResourceModel;
use Magento\Framework\App\ObjectManager;
/**
 * AutoRelatedProducts Product Index by Rule Product List Type Resource Model
 *
 * @SuppressWarnings(PHPMD.LongVariable)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Index extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Increment value for generate unique bind names
     *
     * @var int
     */
    protected $_bindIncrement = 0;

    /**
     * Target rule data
     *
     * @var \Ecomteck\AutoRelatedProducts\Helper\Data
     */
    protected $_autoRelatedProductsData;

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * Customer segment data
     *
     * @var \Magento\CustomerSegment\Helper\Data
     */
    protected $_customerSegmentData;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_session;

    /**
     * @var \Magento\CustomerSegment\Model\Customer
     */
    protected $_customer;

    /**
     * @var \Magento\Catalog\Model\Product\Visibility
     */
    protected $_visibility;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\CustomerSegment\Model\ResourceModel\Segment
     */
    protected $_segmentCollectionFactory;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $_productCollectionFactory;

    /**
     * @var \Ecomteck\AutoRelatedProducts\Model\ResourceModel\Rule
     */
    protected $_rule;

    /**
     * @var \Ecomteck\AutoRelatedProducts\Model\ResourceModel\IndexPool
     */
    protected $_indexPool;

    /**
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param \Ecomteck\AutoRelatedProducts\Model\ResourceModel\IndexPool $indexPool
     * @param \Ecomteck\AutoRelatedProducts\Model\ResourceModel\Rule $rule
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Catalog\Model\Product\Visibility $visibility
     * @param \Magento\Customer\Model\Session $session
     * @param \Ecomteck\AutoRelatedProducts\Helper\Data $autoRelatedProductsData
     * @param \Magento\Framework\Registry $coreRegistry
     * @param string $connectionName
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        \Ecomteck\AutoRelatedProducts\Model\ResourceModel\IndexPool $indexPool,
        \Ecomteck\AutoRelatedProducts\Model\ResourceModel\Rule $rule,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\Product\Visibility $visibility,
        \Magento\Customer\Model\Session $session,
        \Ecomteck\AutoRelatedProducts\Helper\Data $autoRelatedProductsData,
        \Magento\Framework\Registry $coreRegistry,
        $connectionName = null
    ) {
        $this->_indexPool = $indexPool;
        $this->_rule = $rule;
        
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_storeManager = $storeManager;
        $this->_visibility = $visibility;
        $this->_session = $session;
        $this->_coreRegistry = $coreRegistry;
        $this->_autoRelatedProductsData = $autoRelatedProductsData;

        //Get Object Manager Instance
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        if(class_exists('\Magento\CustomerSegment\Model\ResourceModel\Segment')){
            $this->_segmentCollectionFactory = $objectManager->create('Magento\CustomerSegment\Model\ResourceModel\Segment');
        }

        if(class_exists('\Magento\CustomerSegment\Model\Customer')){
            $this->_customer = $objectManager->create('Magento\CustomerSegment\Model\Customer');
        }
        if(class_exists('\Magento\CustomerSegment\Helper\Data')){
            $this->_customerSegmentData = $objectManager->create('Magento\CustomerSegment\Helper\Data');
        }
        


        parent::__construct($context, $connectionName);
    }

    /**
     * Initialize connection and define main table
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('ecomteck_auto_related_products_index', 'entity_id');
    }

    /**
     * Retrieve constant value overfill limit for product ids index
     *
     * @return int
     */
    public function getOverfillLimit()
    {
        return 20;
    }

    /**
     * Retrieve array of defined product list type id
     *
     * @return int[]
     */
    public function getTypeIds()
    {
        return [
            \Ecomteck\AutoRelatedProducts\Model\Rule::RELATED_PRODUCTS,
            \Ecomteck\AutoRelatedProducts\Model\Rule::UP_SELLS,
            \Ecomteck\AutoRelatedProducts\Model\Rule::CROSS_SELLS
        ];
    }

    /**
     * Retrieve product Ids
     *
     * @param \Ecomteck\AutoRelatedProducts\Model\Index $object
     * @return array
     */
    public function getProductIds($object)
    {
        $segmentsIds = array_merge([0], $this->_getSegmentsIdsFromCurrentCustomer());

        $productIds = [];
        foreach ($segmentsIds as $segmentId) {
            $matchedProductIds = $this->_indexPool->get($object->getType())
                ->loadProductIdsBySegmentId($object, $segmentId);

            if (empty($matchedProductIds)) {
                $matchedProductIds = $this->_matchProductIdsBySegmentId($object, $segmentId);
                $this->_indexPool->get($object->getType())
                    ->saveResultForCustomerSegments(
                        $object,
                        $segmentId,
                        $matchedProductIds
                    );
            }

            $productIds = array_merge($matchedProductIds, $productIds);
        }

        $productIds = array_diff(array_unique($productIds), $object->getExcludeProductIds());
        $rotationMode = $this->_autoRelatedProductsData->getRotationMode($object->getType());
        if ($rotationMode == \Ecomteck\AutoRelatedProducts\Model\Rule::ROTATION_SHUFFLE) {
            shuffle($productIds);
        }
        return array_slice($productIds, 0, $object->getLimit());
    }

    /**
     * Match, save and return applicable product ids by segmentId object
     *
     * @param \Ecomteck\AutoRelatedProducts\Model\Index $object
     * @param string $segmentId
     * @return array
     */
    protected function _matchProductIdsBySegmentId($object, $segmentId)
    {
        $limit = $object->getLimit() + $this->getOverfillLimit();
        $productIds = [];
        $ruleCollection = $object->getRuleCollection();
        if ($this->_customerSegmentData && $this->_customerSegmentData->isEnabled()) {
            $ruleCollection->addSegmentFilter($segmentId);
        }
        foreach ($ruleCollection as $rule) {
            /* @var $rule \Ecomteck\AutoRelatedProducts\Model\Rule */
            if (count($productIds) >= $limit) {
                break;
            }
            if (!$rule->checkDateForStore($object->getStoreId())) {
                continue;
            }
            $excludeProductIds = array_merge([$object->getProduct()->getEntityId()], $productIds);
            $resultIds = $this->_getProductIdsByRule($rule, $object, $rule->getPositionsLimit(), $excludeProductIds);
            $productIds = array_merge($productIds, $resultIds);
        }
        return $productIds;
    }

    /**
     * Retrieve found product ids by Rule action conditions
     * If rule has cached select - get it
     *
     * @param \Ecomteck\AutoRelatedProducts\Model\Rule $rule
     * @param \Ecomteck\AutoRelatedProducts\Model\Index $object
     * @param int $limit
     * @param array $excludeProductIds
     * @return array
     */
    protected function _getProductIdsByRule($rule, $object, $limit, $excludeProductIds = [])
    {
        $rule->afterLoad();

        /* @var $collection \Magento\Catalog\Model\ResourceModel\Product\Collection */
        $collection = $this->_productCollectionFactory->create()->setStoreId(
            $object->getStoreId()
        )->addPriceData(
            $object->getCustomerGroupId()
        )->setVisibility(
            $this->_visibility->getVisibleInCatalogIds()
        );

        $actionSelect = $rule->getActionSelect();
        $actionBind = $rule->getActionSelectBind();

        if ($actionSelect === null) {
            $actionBind = [];
            $actionSelect = $rule->getActions()->getConditionForCollection($collection, $object, $actionBind);
            $rule->setActionSelect((string)$actionSelect)->setActionSelectBind($actionBind)->save();
        }

        if ($actionSelect) {
            $collection->getSelect()->where($actionSelect);
        }
        if ($excludeProductIds) {
            $collection->addFieldToFilter('entity_id', ['nin' => $excludeProductIds]);
        }

        $select = $collection->getSelect();
        $select->reset(\Magento\Framework\DB\Select::COLUMNS);
        $select->columns('entity_id', 'e');
        $select->limit($limit);

        $bind = $this->_prepareRuleActionSelectBind($object, $actionBind);
        $result = $this->getConnection()->fetchCol($select, $bind);

        return $result;
    }

    /**
     * Prepare bind array for product select
     *
     * @param \Ecomteck\AutoRelatedProducts\Model\Index $object
     * @param array $actionBind
     * @return array
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    protected function _prepareRuleActionSelectBind($object, $actionBind)
    {
        $bind = [];
        if (!is_array($actionBind)) {
            $actionBind = [];
        }

        foreach ($actionBind as $bindData) {
            if (!is_array($bindData) || !array_key_exists('bind', $bindData) || !array_key_exists('field', $bindData)
            ) {
                continue;
            }
            $k = $bindData['bind'];
            $v = $object->getProduct()->getDataUsingMethod($bindData['field']);

            if (!empty($bindData['callback'])) {
                $callbacks = $bindData['callback'];
                if (!is_array($callbacks)) {
                    $callbacks = [$callbacks];
                }
                foreach ($callbacks as $callback) {
                    if (is_array($callback)) {
                        $v = $this->{$callback[0]}($v, $callback[1]);
                    } else {
                        $v = $this->{$callback}($v);
                    }
                }
            }

            if (is_array($v)) {
                $v = join(',', $v);
            }

            $bind[$k] = $v;
        }

        return $bind;
    }

    /**
     * Retrieve new SELECT instance (used Read Adapter)
     *
     * @return \Magento\Framework\DB\Select
     */
    public function select()
    {
        return $this->getConnection()->select();
    }

    /**
     * Retrieve SQL condition fragment by field, operator and value
     *
     * @param string $field
     * @param string $operator
     * @param int|string|array $value
     * @return string
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function getOperatorCondition($field, $operator, $value)
    {
        switch ($operator) {
            case '!=':
            case '>=':
            case '<=':
            case '>':
            case '<':
                $selectOperator = sprintf('%s?', $operator);
                break;
            case '{}':
            case '!{}':
                if ($field == 'category_id' && is_array($value)) {
                    $selectOperator = ' IN (?)';
                } else {
                    $selectOperator = ' LIKE ?';
                    $value = '%' . $value . '%';
                }
                if (substr($operator, 0, 1) == '!') {
                    $selectOperator = ' NOT' . $selectOperator;
                }
                break;

            case '()':
                $selectOperator = ' IN(?)';
                break;

            case '!()':
                $selectOperator = ' NOT IN(?)';
                break;

            default:
                $selectOperator = '=?';
                break;
        }
        $field = $this->getConnection()->quoteIdentifier($field);
        return $this->getConnection()->quoteInto("{$field}{$selectOperator}", $value);
    }

    /**
     * Retrieve SQL condition fragment by field, operator and binded value
     * also modify bind array
     *
     * @param string $field
     * @param mixed $attribute
     * @param string $operator
     * @param array $bind
     * @param array $callback
     * @return string
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function getOperatorBindCondition($field, $attribute, $operator, &$bind, $callback = [])
    {
        $field = $this->getConnection()->quoteIdentifier($field);
        $bindName = ':autorelatedproducts_bind_' . $this->_bindIncrement++;
        switch ($operator) {
            case '!=':
            case '>=':
            case '<=':
            case '>':
            case '<':
                $condition = sprintf('%s%s%s', $field, $operator, $bindName);
                break;
            case '{}':
                $condition = sprintf('%s LIKE %s', $field, $bindName);
                $callback[] = 'bindLikeValue';
                break;

            case '!{}':
                $condition = sprintf('%s NOT LIKE %s', $field, $bindName);
                $callback[] = 'bindLikeValue';
                break;

            case '()':
                $condition = $this->getConnection()->prepareSqlCondition(
                    $bindName,
                    ['finset' => new \Zend_Db_Expr($field)]
                );
                break;

            case '!()':
                $condition = $this->getConnection()->prepareSqlCondition(
                    $bindName,
                    ['finset' => new \Zend_Db_Expr($field)]
                );
                $condition = sprintf('NOT (%s)', $condition);
                break;

            default:
                $condition = sprintf('%s=%s', $field, $bindName);
                break;
        }

        $bind[] = ['bind' => $bindName, 'field' => $attribute, 'callback' => $callback];

        return $condition;
    }

    /**
     * Prepare bind value for LIKE condition
     * Callback method
     *
     * @param string $value
     * @return string
     */
    public function bindLikeValue($value)
    {
        return '%' . $value . '%';
    }

    /**
     * Prepare bind array of ids from string or array
     *
     * @param string|int|array $value
     * @return array
     */
    public function bindArrayOfIds($value)
    {
        if (!is_array($value)) {
            $value = explode(',', $value);
        }

        $value = array_map('trim', $value);
        $value = array_filter($value, 'is_numeric');

        return $value;
    }

    /**
     * Prepare bind value (percent of value)
     *
     * @param float $value
     * @param int $percent
     * @return float
     */
    public function bindPercentOf($value, $percent)
    {
        return round($value * ($percent / 100), 4);
    }

    /**
     * Remove index data from index tables
     *
     * @param int|null $typeId
     * @param \Magento\Store\Model\Store|int|array|null $store
     * @return $this
     */
    public function cleanIndex($typeId = null, $store = null)
    {
        $connection = $this->getConnection();

        if ($store instanceof \Magento\Store\Model\Store) {
            $store = $store->getId();
        }

        if ($typeId === null) {
            foreach ($this->getTypeIds() as $typeId) {
                $this->_indexPool->get($typeId)->cleanIndex($store);
            }

            $where = $store === null ? '' : ['store_id IN(?)' => $store];
            $connection->delete($this->getMainTable(), $where);
        } else {
            $where = ['type_id=?' => $typeId];
            if ($store !== null) {
                $where['store_id IN(?)'] = $store;
            }
            $connection->delete($this->getMainTable(), $where);
            $this->_indexPool->get($typeId)->cleanIndex($store);
        }

        return $this;
    }

    /**
     * Remove products from index tables
     *
     * @param int|null $productId
     * @return $this
     */
    public function deleteProductFromIndex($productId = null)
    {
        foreach ($this->getTypeIds() as $typeId) {
            $this->_indexPool->get($typeId)->deleteProductFromIndex($productId);
        }
        return $this;
    }

    /**
     * Remove target rule matched product index data by product id or/and rule id
     *
     * @param int|null $productId
     * @param array|int|string $ruleIds
     * @return $this
     */
    public function removeProductIndex($productId = null, $ruleIds = [])
    {
        $this->_rule->unbindRuleFromEntity($ruleIds, $productId, 'product');
        return $this;
    }

    /**
     * Bind target rule to specified product
     *
     * @param \Ecomteck\AutoRelatedProducts\Model\Rule $object
     * @return $this
     */
    public function saveProductIndex($object)
    {
        $this->_rule->bindRuleToEntity($object->getId(), $object->getMatchingProductIds(), 'product');
        return $this;
    }

    /**
     * Adds order by random to select object
     *
     * @param \Magento\Framework\DB\Select $select
     * @param string|null $field
     * @return $this
     */
    public function orderRand(\Magento\Framework\DB\Select $select, $field = null)
    {
        $this->getConnection()->orderRand($select, $field);
        return $this;
    }

    /**
     * Get SegmentsIds From Current Customer
     *
     * @return array
     */
    protected function _getSegmentsIdsFromCurrentCustomer()
    {
        $segmentIds = [];
        if(!$this->_customerSegmentData){
            return $segmentIds;
        }
        if ($this->_customerSegmentData->isEnabled()) {
            $customer = $this->_coreRegistry->registry('segment_customer');
            if (!$customer) {
                $customer = $this->_session->getCustomer();
            }
            $websiteId = $this->_storeManager->getWebsite()->getId();

            if (!$customer->getId()) {
                $allSegmentIds = $this->_session->getCustomerSegmentIds();
                if (is_array($allSegmentIds) && isset($allSegmentIds[$websiteId])) {
                    $segmentIds = $allSegmentIds[$websiteId];
                }
            } else {
                $segmentIds = $this->_customer->getCustomerSegmentIdsForWebsite($customer->getId(), $websiteId);
            }

            if (count($segmentIds)) {
                $segmentIds = $this->_segmentCollectionFactory->getActiveSegmentsByIds($segmentIds);
            }
        }
        return $segmentIds;
    }
}
