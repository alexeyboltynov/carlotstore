<?php
/**
 * Ecomteck
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Ecomteck.com license that is
 * available through the world-wide-web at this URL:
 * https://ecomteck.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Ecomteck
 * @package     Ecomteck_AutoRelatedProducts
 * @copyright   Copyright (c) 2018 Ecomteck (https://ecomteck.com/)
 * @license     https://ecomteck.com/LICENSE.txt
 */

/**
 * AutoRelatedProducts Catalog Product Attributes Backend Model
 *
 */
namespace Ecomteck\AutoRelatedProducts\Model\Catalog\Product\Attribute\Backend;

class Rule extends \Magento\Eav\Model\Entity\Attribute\Backend\AbstractBackend
{
    /**
     * Before attribute save prepare data
     *
     * @param \Magento\Catalog\Model\Product $object
     * @return \Ecomteck\AutoRelatedProducts\Model\Catalog\Product\Attribute\Backend\Rule
     */
    public function beforeSave($object)
    {
        $attributeName = $this->getAttribute()->getName();
        $useDefault = $object->getData($attributeName . '_default');

        if ($useDefault == 1) {
            $object->setData($attributeName, null);
        }

        return $this;
    }
}
