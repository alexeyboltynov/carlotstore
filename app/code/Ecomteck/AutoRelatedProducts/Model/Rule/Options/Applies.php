<?php
/**
 * Ecomteck
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Ecomteck.com license that is
 * available through the world-wide-web at this URL:
 * https://ecomteck.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Ecomteck
 * @package     Ecomteck_AutoRelatedProducts
 * @copyright   Copyright (c) 2018 Ecomteck (https://ecomteck.com/)
 * @license     https://ecomteck.com/LICENSE.txt
 */
namespace Ecomteck\AutoRelatedProducts\Model\Rule\Options;

/**
 * Statuses option array
 *
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Applies implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Auto Related Products model
     *
     * @var \Ecomteck\AutoRelatedProducts\Model\Rule
     */
    protected $_autoRelatedProductsModel;

    /**
     * @param \Ecomteck\AutoRelatedProducts\Model\Rule $autoRelatedProductsModel
     */
    public function __construct(\Ecomteck\AutoRelatedProducts\Model\Rule $autoRelatedProductsModel)
    {
        $this->_autoRelatedProductsModel = $autoRelatedProductsModel;
    }

    /**
     * Return statuses array
     *
     * @return array
     */
    public function toOptionArray()
    {
        return $this->_autoRelatedProductsModel->getAppliesToOptions();
    }
}
