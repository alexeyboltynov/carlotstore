<?php
/**
 * Ecomteck
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Ecomteck.com license that is
 * available through the world-wide-web at this URL:
 * https://ecomteck.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Ecomteck
 * @package     Ecomteck_ProductSlider
 * @copyright   Copyright (c) 2018 Ecomteck (https://ecomteck.com/)
 * @license     https://ecomteck.com/LICENSE.txt
 */

namespace Ecomteck\ProductSlider\Model\ResourceModel\ProductSlider;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection {

    /**
     * Initialize resources
     * @return void
     */
    protected function _construct(){
        $this->_init('Ecomteck\ProductSlider\Model\ProductSlider','Ecomteck\ProductSlider\Model\ResourceModel\ProductSlider');
    }

    public function setStoreFilters($storeId)
    {
        $stores = [\Magento\Store\Model\Store::DEFAULT_STORE_ID, $storeId];
        $this->getSelect()
            ->joinLeft(['trs' => $this->getTable(\Ecomteck\ProductSlider\Model\ResourceModel\ProductSlider::SLIDER_STORES_TABLE)],
                        'main_table.slider_id = trs.slider_id',
                        [])
            ->where('trs.store_id IN (?)', $stores);
    }

}