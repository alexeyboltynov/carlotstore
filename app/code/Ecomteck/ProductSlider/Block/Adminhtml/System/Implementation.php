<?php
/**
 * Ecomteck
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Ecomteck.com license that is
 * available through the world-wide-web at this URL:
 * https://ecomteck.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Ecomteck
 * @package     Ecomteck_ProductSlider
 * @copyright   Copyright (c) 2018 Ecomteck (https://ecomteck.com/)
 * @license     https://ecomteck.com/LICENSE.txt
 */

namespace Ecomteck\ProductSlider\Block\Adminhtml\System;

use Magento\Framework\Data\Form\Element\AbstractElement;

class Implementation extends \Magento\Config\Block\System\Config\Form\Field
{
    /**
     * @param AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(AbstractElement $element)
    {
        $html = '<div class="notices-wrapper">
                    <div class="message"><strong>'.__('Use widgets in Magento admin for custom location implementation').'</strong></div>
                    <div class="message">
                        <strong>'.__('Copy code from below to the template file (between php tags) and replace custom_slider_id with proper slider id').'</strong><br/>
                        echo $this->getLayout()->createBlock("Ecomteck\ProductSlider\Block\Slider\Items")->setSliderId("custom_slider_id")->toHtml();
                    </div>
                    <div class="message">
                        <strong>'.__('Copy code from below to the CMS page or block and replace custom_slider_id with proper slider id').'</strong><br/>
                        {{block class="Ecomteck\ProductSlider\Block\Slider\Items" slider_id="custom_slider_id"}}
                    </div>
                    <div class="message">
                        <strong>'.__('Copy code from below to the layout XML file and replace custom_slider_id with proper slider id').'</strong><br/>
                        &lt;block class="Ecomteck\ProductSlider\Block\Slider\Items"&gt;<br/>
                           &nbsp;&nbsp;&lt;action method="setSliderId"&gt;<br/>
                               &nbsp;&nbsp;&nbsp;&nbsp;&lt;argument name="sliderId" xsi:type="string"&gt;your_slider_id&lt;/argument&gt;<br/>
                           &nbsp;&nbsp;&lt;/action&gt;<br/>
                        &lt;/block>
                    </div>
                </div>';

        return $html;

    }
}