<?php
/**
 * @category Jute
 * @package Jute_Ecommerce
 */
namespace Ecomteck\ProductSlider\Setup;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
class UpgradeSchema implements UpgradeSchemaInterface{
	public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context){
	    if (version_compare($context->getVersion(), '1.0.3') < 0) {
	        $setup->startSetup();
			$setup->getConnection()->addColumn(
			$setup->getTable('ecomteck_productslider'),
				'slider_image',
					['type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					'length' => '255',
					'nullable' => false,
					'comment' => 'slider_image']);
				$setup->endSetup();
		    } 
		} 
}

