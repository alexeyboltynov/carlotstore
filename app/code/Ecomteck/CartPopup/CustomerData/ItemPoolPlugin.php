<?php
/**
 * Ecomteck
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Ecomteck.com license that is
 * available through the world-wide-web at this URL:
 * https://ecomteck.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Ecomteck
 * @package     Ecomteck_CartPopup
 * @copyright   Copyright (c) 2018 Ecomteck (https://ecomteck.com/)
 * @license     https://ecomteck.com/LICENSE.txt
 */
namespace Ecomteck\CartPopup\CustomerData;

use Magento\Quote\Model\Quote\Item;
use Ecomteck\CartPopup\Model\Storage as Storage;

class ItemPoolPlugin
{
    /**
     * @var \Magento\Catalog\Helper\Image
     */
    protected $imageHelper;

    /**
     * @var Storage
     */
    protected $storage;

    /**
     * @param \Magento\Catalog\Helper\Image $imageHelper
     * @param Storage $storage
     */
    public function __construct(
        \Magento\Catalog\Helper\Image $imageHelper,
        Storage $storage
    ) {
        $this->imageHelper = $imageHelper;
        $this->storage = $storage;
    }

    /**
     * @return \Magento\Checkout\CustomerData\ItemPoolInterface $subject
     * @return callable $proceed
     * @return Item $item
     */
    public function aroundGetItemData(
        \Magento\Checkout\CustomerData\ItemPoolInterface $subject,
        callable $proceed,
        Item $item
    ) {
        $return = $proceed($item);

        $items = $this->storage->getItems();
        $imageView = count($items) > 1 ? 'ecomteck_cart_popup_cart_multi' : 'ecomteck_cart_popup_cart_single';
        $product = $item->getProduct();
        if ($option = $item->getOptionByCode('simple_product')) {
            $childProduct = $option->getProduct();
            if (!($childProduct->getThumbnail() && $childProduct->getThumbnail() != 'no_selection')) {
            } else {
                $product = $childProduct;
            }
        }
        
        $imageHelper = $this->imageHelper->init($product, $imageView);
        $return['ecomteck_cart_popup_image'] = [
            'src' => $imageHelper->getUrl(),
            'alt' => $imageHelper->getLabel(),
            'width' => $imageHelper->getWidth(),
            'height' => $imageHelper->getHeight(),
        ];

        return $return;
    }
}
