<?php
/**
 * Ecomteck
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Ecomteck.com license that is
 * available through the world-wide-web at this URL:
 * https://ecomteck.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Ecomteck
 * @package     Ecomteck_CartPopup
 * @copyright   Copyright (c) 2018 Ecomteck (https://ecomteck.com/)
 * @license     https://ecomteck.com/LICENSE.txt
 */
namespace Ecomteck\CartPopup\CustomerData;

use Magento\Customer\CustomerData\SectionSourceInterface;
use Magento\Catalog\Model\ResourceModel\Product\Collection as ProductCollection;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Framework\View\Result\PageFactory;

use Ecomteck\CartPopup\Model\Storage as Storage;
use Ecomteck\CartPopup\Helper\Data as DataHelper;

/**
 * Popup source
 */
class Popup extends \Magento\Framework\DataObject implements SectionSourceInterface
{
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var \Magento\Checkout\Model\Cart
     */
    protected $checkoutCart;

    /**
     * @var ProductCollectionFactory
     */
    protected $productCollectionFactory;

    /**
     * @var \Magento\Catalog\Model\Product\Visibility
     */
    protected $productVisibility;

    /**
     * @var \Magento\Checkout\Helper\Data
     */
    protected $checkoutHelper;

    /**
     * @var \Magento\Catalog\Helper\Image
     */
    protected $imageHelper;

    /**
     * @var DataHelper
     */
    protected $dataHelper;

    /**
     * @var Storage
     */
    protected $storage;

    /**
     * @var \Magento\Quote\Model\Quote|null
     */
    protected $quote = null;

    /**
     * Layout
     *
     * @var \Magento\Framework\View\LayoutInterface
     */
    protected $_layout;

    /**
     * Layout
     *
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Checkout\Model\Cart $checkoutCart
     * @param ProductCollectionFactory $productCollectionFactory
     * @param \Magento\Catalog\Model\Product\Visibility $productVisibility
     * @param \Magento\Checkout\Helper\Data $checkoutHelper
     * @param \Magento\Catalog\Helper\Image $imageHelper
     * @param \Magento\Framework\View\LayoutInterface $layout
     * @param \Magento\Framework\Registry $registry
     * @param DataHelper $dataHelper
     * @param Storage $storage
     * @param array $data
     */
    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Checkout\Model\Cart $checkoutCart,
        ProductCollectionFactory $productCollectionFactory,
        \Magento\Catalog\Model\Product\Visibility $productVisibility,
        \Magento\Checkout\Helper\Data $checkoutHelper,
        \Magento\Catalog\Helper\Image $imageHelper,
        \Magento\Framework\View\LayoutInterface $layout,
        PageFactory $resultPageFactory,
        \Magento\Framework\Registry $registry,
        DataHelper $dataHelper,
        Storage $storage,
        array $data = []
    ) {
        parent::__construct($data);
        $this->checkoutSession = $checkoutSession;
        $this->checkoutCart = $checkoutCart;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->productVisibility = $productVisibility;
        $this->checkoutHelper = $checkoutHelper;
        $this->imageHelper = $imageHelper;
        $this->dataHelper = $dataHelper;
        $this->storage = $storage;
        $this->_layout = $layout;
        $this->registry     = $registry;
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function getSectionData()
    {
        $data = [];
        if (!$this->dataHelper->isEnabled()) {
            return $data;
        }

        $items = $this->getAddedItems();
        if (!empty($items)) {
            $data = [
                'items' => $items,
                'product_list' => $this->getProductList($items),
                'summary' => $this->getSummary(),
            ];
        }
        return $data;
    }

    /**
     * Get array of added items
     *
     * @return int[]
     */
    protected function getAddedItems()
    {
        $items = $this->storage->getItems();
        if (!$this->checkoutCart->getSummaryQty()) {
            $items = [];
        }
        return $items;
    }

    /**
     * Get product list
     *
     * @param array $affectedItems
     * @return array
     */
    protected function getProductList(array $affectedItems)
    {
        $productList = [];
        if (!$this->dataHelper->isProductListEnabled()) {
            return $productList;
        }

        $affectedProductIds = [];
        $categoryIds = [];
        $sliderId = $this->dataHelper->getProductListFetchType();
        $items = $this->getAddedItems();
        $currentAddedProductId = reset($items);
        $quote = $this->getQuote();
        $items = $quote->getItems();
        if(!empty($sliderId) && is_array($items)){
            foreach ($items as $item) {
                if ($item->getId() == $currentAddedProductId) {
                    $product = $item->getProduct();
                    $this->registry->register('product',$product);
                    $resultPage = $this->resultPageFactory->create();
                    $resultPage->addHandle('cart_popup_products');
                    $resultPage->getLayout()->getUpdate()->removeHandle('default');
                    $resultPage->getLayout()->getUpdate()->removeHandle('customer_section_load');
                    $sliderBlock = $resultPage->getlayout()->getBlock('product_slider');
                    if($sliderBlock){
                        $sliderBlock->setData('widget_slider_id',$sliderId);
                    }
                    $productList = $resultPage->getLayout()->renderElement('content',false);
                    $productList = str_replace('[data-role=swatch-options]','.cart-popup-category [data-role=swatch-options]',$productList);
                    break;
                }
            }
        }

        return $productList;
    }

    /**
     * Get summary items
     *
     * @return array
     */
    public function getSummary()
    {
        $summaryItems = [];

        $qty = (int)$this->checkoutCart->getSummaryQty();
        $totals = $this->getQuote()->getTotals();

        if (isset($totals['subtotal'])) {
            $value = $totals['subtotal']->getValue();
            $summaryItems[] = [
                'label' => $qty . ' ' . __('product%1', $qty > 1 ? 's' : ''),
                'value' => $this->checkoutHelper->formatPrice($totals['subtotal']->getValue()),
            ];
        }

        if (isset($totals['shipping'])) {
            $value = $totals['shipping']->getValue();
            $summaryItems[] = [
                'label' => $totals['shipping']->getTitle(),
                'value' => __($value ? $this->checkoutHelper->formatPrice($value) : 'Free'),
            ];
        }

        return $summaryItems;
    }

    /**
     * Get active quote
     *
     * @return \Magento\Quote\Model\Quote
     */
    public function getQuote()
    {
        if (null === $this->quote) {
            $this->quote = $this->checkoutSession->getQuote();
        }
        return $this->quote;
    }

    /**
     * Get product collection
     *
     * @param array $categoryIds
     * @param array $affectedProductIds
     * @return ProductCollection
     */
    protected function getProductCollection(array $categoryIds = [], array $affectedProductIds = [])
    {
        $productCollection = $this->productCollectionFactory
            ->create()
            ->addAttributeToSelect('*')
            ->addCategoriesFilter(['in' => $categoryIds])
            ->addFinalPrice()
            ->setVisibility($this->productVisibility->getVisibleInSiteIds());
        if (!empty($affectedProductIds)) {
            $productCollection->addFieldToFilter('entity_id', ['nin' => $affectedProductIds]);
        }
        if ($productCount = $this->dataHelper->getProductListProductCount()) {
            $productCollection->setPageSize($productCount);
        }
        $productCollection->getSelect()->orderRand();
        $productCollection->addOptionsToResult();
        return $productCollection;
    }

    /**
     * Get product list data
     *
     * @param ProductCollection $productCollection
     * @return array
     */
    protected function getProductListData($productCollection)
    {
        $productList = [];

        foreach ($productCollection as $product) {
            $imageHelper = $this->imageHelper->init($product, 'ecomteck_cart_popup_product_list');
            $productList[] = [
                'product_id' => $product->getId(),
                'product_name' => $product->getName(),
                'product_sku' => $product->getSku(),
                'product_url' => $product->getUrlModel()->getUrl($product),
                'product_price' => $this->checkoutHelper->formatPrice($product->getFinalPrice()),
                'product_price_value' => $product->getFinalPrice(),
                'product_image' => [
                    'src' => $imageHelper->getUrl(),
                    'alt' => $imageHelper->getLabel(),
                    'width' => $imageHelper->getWidth(),
                    'height' => $imageHelper->getHeight(),
                ],
            ];
        }
        return $productList;
    }
}
