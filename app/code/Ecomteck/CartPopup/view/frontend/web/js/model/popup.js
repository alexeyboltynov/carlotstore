/**
 * Ecomteck
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Ecomteck.com license that is
 * available through the world-wide-web at this URL:
 * https://ecomteck.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Ecomteck
 * @package     Ecomteck_CartPopup
 * @copyright   Copyright (c) 2018 Ecomteck (https://ecomteck.com/)
 * @license     https://ecomteck.com/LICENSE.txt
 */
define([
    'jquery',
    'Magento_Ui/js/modal/modal',
    'mage/translate',
    'Magento_Ui/js/model/messageList',
    'catalogAddToCart'
],function ($, modal, $t, messageList) {
    'use strict';

    return {
        modalWindow: null,
        messages: messageList,

        /** Create popup window */
        createPopUp: function (element) {
            this.modalWindow = element;
            var options = {
                'title': $t('Added to your shopping cart'),
                'type': 'popup',
                'modalClass': 'ecomteck-cart-popup-modal',
                'responsive': true,
                'buttons': [],
                'opened': function(){
                    
                },
                'closed': function(){
                    
                }
            };
            modal(options, $(this.modalWindow));
        },

        /** Show save cart popup window */
        showModal: function () {
            $(this.modalWindow).find('[data-role=tocart-form]').catalogAddToCart({});
            $(this.modalWindow).modal('openModal');
        },

        /** Close save cart popup window */
        closeModal: function () {
            $(this.modalWindow).modal('closeModal');
        }
    }
});
