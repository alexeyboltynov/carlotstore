<?php
/**
 * Ecomteck
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Ecomteck.com license that is
 * available through the world-wide-web at this URL:
 * https://ecomteck.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Ecomteck
 * @package     Ecomteck_CartPopup
 * @copyright   Copyright (c) 2018 Ecomteck (https://ecomteck.com/)
 * @license     https://ecomteck.com/LICENSE.txt
 */
namespace Ecomteck\CartPopup\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Quote\Api\Data\CartItemInterface;

use Ecomteck\CartPopup\Model\Storage as Storage;
use Ecomteck\CartPopup\Helper\Data as DataHelper;

class CartAddComplete implements ObserverInterface
{
    /**
     * @var \Magento\Framework\Registry
     */
    private $registry;

    /**
     * @var Storage
     */
    private $storage;

    /**
     * @param \Magento\Framework\Registry $registry
     * @param Storage $storage
     */
    public function __construct(
        \Magento\Framework\Registry $registry,
        Storage $storage
    ) {
        $this->registry = $registry;
        $this->storage = $storage;
    }

    /**
     * @return void
     */
    public function execute(Observer $observer)
    {
        $items = $this->registry->registry(DataHelper::REGISTER_ITEMS_KEY);
        if (is_array($items)) {
            $storageItems = [];
            foreach ($items as $item) {
                if (!$item->getParentItemId()) {
                    $storageItems[] = $item->getId();
                }
            }
            if (!empty($storageItems)) {
                $this->storage->setItems($storageItems);
            }
        }
    }
}
