<?php
/**
 * Ecomteck
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Ecomteck.com license that is
 * available through the world-wide-web at this URL:
 * https://ecomteck.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Ecomteck
 * @package     Ecomteck_CartPopup
 * @copyright   Copyright (c) 2018 Ecomteck (https://ecomteck.com/)
 * @license     https://ecomteck.com/LICENSE.txt
 */
namespace Ecomteck\CartPopup\Model\Config\Source;

class FetchType implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * @var \Ecomteck\ProductSlider\Model\ResourceModel\ProductSlider\CollectionFactory
     */
    protected $sliderCollectionFactory;

    /**
     * @param CategoryHelper $categoryHelper
     */
    public function __construct(
        \Ecomteck\ProductSlider\Model\ResourceModel\ProductSlider\CollectionFactory $sliderCollectionFactory
    ) {
        $this->sliderCollectionFactory = $sliderCollectionFactory;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $sliders = $this->sliderCollectionFactory->create();
        $options = [];
        foreach($sliders as $slider) {
            $options[] = ['value' => $slider->getId(), 'label' => $slider->getTitle()];
        }
        return $options;
    }
}
