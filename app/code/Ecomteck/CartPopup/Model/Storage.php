<?php
/**
 * Ecomteck
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Ecomteck.com license that is
 * available through the world-wide-web at this URL:
 * https://ecomteck.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Ecomteck
 * @package     Ecomteck_CartPopup
 * @copyright   Copyright (c) 2018 Ecomteck (https://ecomteck.com/)
 * @license     https://ecomteck.com/LICENSE.txt
 */
namespace Ecomteck\CartPopup\Model;

class Storage
{
    /**
     * Keys
     */
    const SESS_PREFIX = 'ecomteck_cart_popup_';

    const SESS_KEY_ITEMS = 'items';

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var array
     */
    protected $_data = [];

    /**
     * @param \Magento\Checkout\Model\Session $checkoutSession
     */
    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession
    ) {
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * Get data
     *
     * @param string $key
     * @return array
     */
    public function getData($key)
    {
        if (!isset($this->_data[$key])) {
            $this->_data[$key] = $this->checkoutSession->getData(static::SESS_PREFIX . $key, true);
        }
        return $this->_data[$key];
    }

    /**
     * Set data
     *
     * @param string $key
     * @param string $value
     * @return array
     */
    public function setData($key, $value)
    {
        $this->_data[$key] = $value;
        $this->checkoutSession->setData(static::SESS_PREFIX . $key, $value);
        return $this;
    }

    /**
     * Get items
     *
     * @return array
     */
    public function getItems()
    {
        $items = $this->getData(static::SESS_KEY_ITEMS);
        if (!is_array($items)) {
            $items = [];
        }
        return $items;
    }

    /**
     * Set items
     *
     * @param array $items
     * @return array
     */
    public function setItems(array $items = [])
    {
        $this->setData(static::SESS_KEY_ITEMS, $items);
        return $this;
    }
}
