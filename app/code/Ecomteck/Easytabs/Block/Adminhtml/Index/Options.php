<?php
/**
 * Ecomteck
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Ecomteck.com license that is
 * available through the world-wide-web at this URL:
 * https://ecomteck.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Ecomteck
 * @package     Ecomteck_Easytabs
 * @copyright   Copyright (c) 2018 Ecomteck (https://ecomteck.com/)
 * @license     https://ecomteck.com/LICENSE.txt
 */
namespace Ecomteck\Easytabs\Block\Adminhtml\Index;

class Options extends \Magento\Widget\Block\Adminhtml\Widget\Options
{
    /**
     * @var \Ecomteck\Easytabs\Model\TabsFactory
     */
    protected $tabsOptionsFactory;
    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Framework\Option\ArrayPool $sourceModelPool
     * @param \Magento\Widget\Model\Widget $widget
     * @param \Ecomteck\Easytabs\Model\TabsFactory $tabsOptionsFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Framework\Option\ArrayPool $sourceModelPool,
        \Magento\Widget\Model\Widget $widget,
        \Ecomteck\Easytabs\Model\TabsFactory $tabsOptionsFactory,
        array $data = []
    ) {
        $this->tabsOptionsFactory = $tabsOptionsFactory;
        parent::__construct($context, $registry, $formFactory, $sourceModelPool, $widget, $data);
    }
    /**
     * Add fields to main fieldset based on specified tab type
     *
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    public function addFields()
    {
        // get configuration node and translation helper
        if (!$this->getWidgetType()) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Please specify a Widget Type.'));
        }
        $config = $this->tabsOptionsFactory->create()->getConfigAsObject($this->getWidgetType());
        if (!$config->getParameters()) {
            return $this;
        }
        foreach ($config->getParameters() as $parameter) {
            $this->_addField($parameter);
        }
        return $this;
    }
}
