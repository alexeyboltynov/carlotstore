<?php
/**
 * Ecomteck
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Ecomteck.com license that is
 * available through the world-wide-web at this URL:
 * https://ecomteck.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Ecomteck
 * @package     Ecomteck_Easytabs
 * @copyright   Copyright (c) 2018 Ecomteck (https://ecomteck.com/)
 * @license     https://ecomteck.com/LICENSE.txt
 */
namespace Ecomteck\Easytabs\Block;

class WidgetTabs extends Tabs implements \Magento\Widget\Block\BlockInterface
{
    protected function _getCollection()
    {
        $collection = parent::_getCollection();
        $filterTabs = str_replace(' ', '', $this->getFilterTabs());
        $filterTabs = explode(',', $filterTabs);
        $collection->addFieldToFilter('alias', array('in' => $filterTabs));
        $collection->addWidgetTabFilter();
        return $collection;
    }

    public function getInitOptions()
    {
        return '{"collapsible": false, "openedState": "active"}';
    }
}
