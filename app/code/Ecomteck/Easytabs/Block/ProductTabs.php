<?php
/**
 * Ecomteck
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Ecomteck.com license that is
 * available through the world-wide-web at this URL:
 * https://ecomteck.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Ecomteck
 * @package     Ecomteck_Easytabs
 * @copyright   Copyright (c) 2018 Ecomteck (https://ecomteck.com/)
 * @license     https://ecomteck.com/LICENSE.txt
 */
namespace Ecomteck\Easytabs\Block;

use Magento\Store\Model\ScopeInterface;

class ProductTabs extends Tabs
{
    protected function _getCollection()
    {
        $collection = parent::_getCollection();
        $collection->addProductTabFilter();
        return $collection;
    }

    public function getInitOptions()
    {
        return $this->getVar('options');
    }

    /**
     * Get product tabs layout name
     *
     * @return string
     */
    public function getTabsLayout()
    {
        return $this->_scopeConfig->getValue(
            'ecomteck_personalized_recommendations/easytabs/product_tabs/layout',
            ScopeInterface::SCOPE_STORE
        );
    }
}
