<?php
/**
 * Ecomteck
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Ecomteck.com license that is
 * available through the world-wide-web at this URL:
 * https://ecomteck.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Ecomteck
 * @package     Ecomteck_Easytabs
 * @copyright   Copyright (c) 2018 Ecomteck (https://ecomteck.com/)
 * @license     https://ecomteck.com/LICENSE.txt
 */
namespace Ecomteck\Easytabs\Block\Tab;

use Magento\Framework\DataObject\IdentityInterface;

class Cms extends \Magento\Framework\View\Element\Template
     implements IdentityInterface
{
    public function getCmsBlockId()
    {
        return $this->getWidgetIdentifier();
    }
    /**
     * Return unique ID(s) for each object in system
     *
     * @return array
     */
    public function getIdentities()
    {
        return [\Ecomteck\Easytabs\Model\Entity::CACHE_TAG . '_' . $this->getCmsBlockId()];
    }
}
