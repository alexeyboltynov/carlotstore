<?php
/**
 * Ecomteck
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Ecomteck.com license that is
 * available through the world-wide-web at this URL:
 * https://ecomteck.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Ecomteck
 * @package     Ecomteck_Easytabs
 * @copyright   Copyright (c) 2018 Ecomteck (https://ecomteck.com/)
 * @license     https://ecomteck.com/LICENSE.txt
 */
namespace Ecomteck\Easytabs\Controller\Adminhtml\Index;

use Ecomteck\Easytabs\Controller\Adminhtml\AbstractMassStatus;

/**
 * Class MassEnable
 */
class MassEnable extends AbstractMassStatus
{
    /**
     * Field id
     */
    const ID_FIELD = 'tab_id';

    /**
     * Admin resource
     */
    const ADMIN_RESOURCE = 'Ecomteck_Easytabs::easytabs_product_status';

    /**
     * Resource collection
     *
     * @var string
     */
    protected $collection = 'Ecomteck\Easytabs\Model\ResourceModel\Entity\Collection';

    /**
     * Easytabs model
     *
     * @var string
     */
    protected $model = 'Ecomteck\Easytabs\Model\Entity';

    /**
     * Tab enable status
     *
     * @var boolean
     */
    protected $status = 1;
}
