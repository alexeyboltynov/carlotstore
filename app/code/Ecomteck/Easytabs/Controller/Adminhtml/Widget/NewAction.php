<?php
/**
 * Ecomteck
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Ecomteck.com license that is
 * available through the world-wide-web at this URL:
 * https://ecomteck.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Ecomteck
 * @package     Ecomteck_Easytabs
 * @copyright   Copyright (c) 2018 Ecomteck (https://ecomteck.com/)
 * @license     https://ecomteck.com/LICENSE.txt
 */
namespace Ecomteck\Easytabs\Controller\Adminhtml\Widget;

use Ecomteck\Easytabs\Controller\Adminhtml\Index\NewAction as IndexNewAction;

class NewAction extends IndexNewAction
{
    /**
     * Admin resource
     */
    const ADMIN_RESOURCE = 'Ecomteck_Easytabs::easytabs_widget_save';
}
