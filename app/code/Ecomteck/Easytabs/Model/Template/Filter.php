<?php
/**
 * Ecomteck
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Ecomteck.com license that is
 * available through the world-wide-web at this URL:
 * https://ecomteck.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Ecomteck
 * @package     Ecomteck_Easytabs
 * @copyright   Copyright (c) 2018 Ecomteck (https://ecomteck.com/)
 * @license     https://ecomteck.com/LICENSE.txt
 */
namespace Ecomteck\Easytabs\Model\Template;

class Filter extends \Magento\Widget\Model\Template\Filter
{
    protected $_scope;

    public function setScope($scope)
    {
        $this->_scope = $scope;
        return $this;
    }

    public function getScope()
    {
        return $this->_scope;
    }

    public function evalDirective($construction)
    {
        $params = $this->getParameters($construction[2]);

        if (isset($params['scope'])) {
            $scope = $params['scope'];
            $scope = $this->getLayout()->getBlock($scope);
            $this->setScope($scope);
        }

        $scope = $this->getScope();
        if (!$scope || !isset($params['code'])) {
            return '';
        }

        $methods = explode('->', str_replace(array('(', ')'), '', $params['code']));

        foreach ($methods as $method) {
            $callback = array($scope, $method);
            if(!is_callable($callback)) {
                continue;
            }

            try {
                $replacedValue = call_user_func($callback);
            } catch (\Exception $e) {
                throw $e;
            }

            $scope = $replacedValue;
        }

        return (string) $replacedValue;
    }
}
