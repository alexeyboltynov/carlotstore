<?php
/**
 * Ecomteck
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Ecomteck.com license that is
 * available through the world-wide-web at this URL:
 * https://ecomteck.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Ecomteck
 * @package     Ecomteck_Easytabs
 * @copyright   Copyright (c) 2018 Ecomteck (https://ecomteck.com/)
 * @license     https://ecomteck.com/LICENSE.txt
 */
namespace Ecomteck\Easytabs\Model\Config\Source;

class WidgetTabs implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @var \Ecomteck\Easytabs\Model\ResourceModel\Entity\CollectionFactory
     */
    protected $tabsCollectionFactory;

    /**
     * @param \Ecomteck\Easytabs\Model\ResourceModel\Entity\CollectionFactory $tabsCollectionFactory
     */
    public function __construct(
        \Ecomteck\Easytabs\Model\ResourceModel\Entity\CollectionFactory $tabsCollectionFactory
    )
    {
        $this->tabsCollectionFactory = $tabsCollectionFactory;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options[] = ['label' => '', 'value' => ''];
        $widgetTabs = $this->tabsCollectionFactory->create()->addWidgetTabFilter();
        foreach ($widgetTabs as $tab) {
            $options[] = [
                'label' => $tab->getTitle(),
                'value' => $tab->getAlias(),
            ];
        }

        return $options;
    }
}
